## Stub

A set of stubs for testing access to the system and the internal API of NVim

This is a library that allows you to emulate (stub or mock) such things as:
- file system structure(function of standard io and some things of luv)
  (emulation of moving through directories and
   checking for the presence of files in them)
- responses of the system cli commands (access via io.popen)
  (emulations of the required responses from console commands)
- responses from the internal nvim API(to write tests for nvim plugins)
  (emulation of NVim operation, buffer contents and text editing in them)


## Briefly what each module is responsible for

Interfaces for the most frequent use:

- stub.os.OSystem - api for stub(mock) OS (cli commands and FileSystem)
- stub.vim.NVim - api for stub nvim
- ioutils.lua - a set of helper-function for write tests
  provide the ability to access the real file system then stub is active

Under the hood(Entrails/implementation):

- stub.os.FileSystem - can also be used separately without OSystem.
  it's responsible for file system emulation, but not for console command
  responses (access to io.popen)
  Automatically created in the OSystem object constructor at the time of its
  creation. Therefore, for the brevity of the code in tests, by design it
  should be accessed through the Osystem object like `sys.fs.set_file(...)`
- IOHandle - is responsible for emulating descriptive I/O at the time of
  reading and writing data from files and I/O streams.
  This is part of the internal implementation of FileSystem stub


## Why

The main idea that inspired the writing of this library is to provide the
ability to easy and quickly override real-existed files and directories,
and console command responses. So often necessary when writing tests.

That is, to create the ability to emulate the behavior of the system
in the manner necessary for testing.
Such things as the presence and contents of files, directories, permissions
and so one.
For NVim - things like emulating the contents of a buffer (opened files)
and changing its contents interactively with the ability to check the result.

First of all, this library is written specifically for the development
environment and use in tests.


## How it works

The entire library is based on a simple property of the Lua language.
Namely, the ability to redefine the values of fields of global objects in
any tables right in runtime.
When you create new an OSystem or FileSystem or NVim object in the constructor
of this object, functions from the standard library are automatically redefined
to stub functions that change the behavior of the actual system as a whole.
In this case, the standard functions are replaced with their own functions
emulating the desired behavior.
And the standard functions themselves are preserved(backup) for the possibility
of rolling back to its original original state.


## Usage Examples

stub.os.OSystem - API to create sub of real system

### io

```lua
local sys = OS:new()

function io_is_exist_file(file)
  local h = io.open(file, "rb")
  if h then h:close() end
  return h ~= nil
end

function luv_is_exist_file(path)
  local uv = require("luv")
  if path then
    local stat = uv.fs_stat(path)
    local exists = (stat and stat.type == "file") == true
    return exists
  end
  return false
end

local fn = '/home/user/emulated-file.json'
local lines = {
  '# this is a content of the emulated file',
  '  second line'
}

sys.fs:set_file(fn, lines) -- define file as existed in FS

-- check
print(io_is_exist_file(fn))  -- true
print(luv_is_exist_file(fn)) -- true

-- read content of emulated file
local fh = io.open(fn)
if fh then
  local data = fh:read('*a')
  fh:close()
  print(data) -- # this is a content of the emulated file\n  second line
end

```


## io.stdin io.stdout

```lua
local OS = require 'stub.os.OSystem'

local sys = OS:new() -- create instance of OSystem(stubs for OS and FileSystem)
-- now access to input and output will go not to the real host machine,
-- but to the emulated system
-- and all calls to standard functions will go through overridden functions

-- define user answer triggers on occurs this line in the io.stdout
sys.fs:stdin_add_input('user-input')

-- access to FileSystem which will actually access the stub and not the real
-- filesystem
local res_input = io.stdin:read('*l')
print(res_input) -- user-input

sys.fs:restore_original() -- backup original system behavior
```

See examples:
[test work with io.stdin](./examples/in_tests.lua)


## io.popen

```lua
  -- read content from file via cat
  it("handle_sys_cmd cat:read `cat fn`", function()
    local sys = OS:new()
    local cmd = 'cat /etc/hosts' -- this file provided by a default OS stub setup

    -- inner check (stuff under the hood)
    local output = { '127.0.0.1  localhost' }
    assert.same(true, sys:is_file_exits('/etc/hosts'))
    assert.same(output, sys:get_file_lines('/etc/hosts'))
    -- call emulator of running the sys command
    assert.same(output, (sys:handle_sys_cmd(cmd) or E).lines) -- ok

    -- check access to cli-tool via standard io operations (std lua lib)
    local fh = io.popen(cmd)
    assert.is_not_nil(fh) ---@cast fh file*
    assert.same('127.0.0.1  localhost', fh:read('*a'))
    fh:close()
  end)
```


## Debugging and Troubleshooting

- just look at the entire contents of the emulated file system as a whole,
  all the files and directories that currently exist:

```lua
local OS = require("stub.os.OSystem");
local sys = OS:new()
-- ... some work
print(sys:tree('/')) -- will print all files and directories
```


- `dprint` is a small external library that adds logger-like functionality for
   debugging.

Its main idea is to be able to enable debug mode with message output when
needed, while having minimal overhead when using it.

This `dprint` library allows you to enable and disable debug output both
globally for all `dprint` functions and individually for each individual module.

In order to be able to turn off the output for a specific module, a `dprint`
wrapper must be used during its creation to indicate whether the debugging
output for this module is enabled or disabled.
Since this library is designed only for use in tests, all individual modules,
although they have their own `dprint`-wrappers are created in activation mode.
Therefore, to activate debugging output, simply enable global output for the
dprint library.

In order to enable debug-mode showing in detail the operation of the stubs use:
```lua
  require'dprint'.enable()   --
```

If you need to turn off some modules to reduce the output of debugging
information, you can do it like this:

```lua
  require'dprint'.enable_module(require('stub.io.FileStub'), false)
```

But again, this is only possible when the module uses a wrapper over `dprint`:
```lua
local D = require("dprint")
local dprint = D.mk_dprint_for(M)
```

And each of the modules, which implements its part of the system emulation
functionality, shipped with the `dprint` function wrapper.
Therefore, you can turn the output on and off both globally and locally for
each individual module.
But it is worth considering that in order for debugging information to be
output from an activated module, the global `dprint` output must be enabled.
[See more at dprint repository](https://gitlab.com/lua_rocks/dprint)

## Whatis - helper

The method `stub.io.OSystem:whatis` can be very useful if something went wrong
and you need to understand where the error is.

```lua
  -- take original modules before creating a stub of all I/O operations
  -- these can be used to determine if the stub actually works
  local prev_io = io                   -- stantard lua library for work with io
  local prev_io_open, prev_io_popen = io.open, io.popen
  local uv = require("luv")            -- external library for works with io
  local prev_uv_fs_mkdir = uv.fs_mkdir

  local sys = OS:new()  -- create new Stub and override std io operations

  print(sys:whatis(sys))                  --> its me stub.io.OSystem
  print(sys:whatis(sys.fs))               --> its me stub.io.OSystem

  print(sys:whatis(io.open))              --> [emulated] io.open
  print(sys:whatis(sys.fs.io.open))       --> [emulated] io.open
  print(sys:whatis(prev_io_open))         --> [original] io.open

  print(sys:whatis(io.popen))             --> [emulated] io.popen
  print(sys:whatis(sys.fs.io.popen))      --> [emulated] io.popen
  print(sys:whatis(prev_io_popen))        --> [original] io.popen

  print(sys:whatis(sys.fs.luv.fs_mkdir))  --> [emulated] luv.fs_mkdir',
  print(sys:whatis(prev_uv_fs_mkdir))     --> [original] luv.fs_mkdir',

  print(sys:whatis(require))              --> proxy_require
  print(sys:whatis(orig_require))         --> original require

  print(sys:whatis(sys.fs.luv))           --> emulated luv module
  print(sys:whatis(require('luv'))        --> emulated luv module
  print(sys:whatis(uv))                   --> original luv module

  -- roll back to original io-ops
  sys:restore_original()
  print(sys:whatis(io.open))              --> [original] io.open
  -- ...
```


## Supports popen:

- workaround to save files from luaapp with sudo right(interactively ask pass)

```sh
sudo sh << SU_EOF
cat > filename << EOF
multi line content for to as root
EOF
SU_EOF
```

See [stub.os.OSystem:handle_sys_cmd(cmd)](src/stub/os/OSystem.lua)


## TODO List

- stub for os:
  - os.execute, os.getenv(), os.remove, os.rename, os.tmpname,
  - https://www.lua.org/manual/5.1/manual.html#pdf-os.execute

- stub.os.OSystem:handle_sys_cmd() - implements more commands for already
  existing stuff as tree ls chown chmod and so on

- stub.os.FileStub:stat() -- access human readable and octal digits, and fmts
  IOHandle(as file*: file:seek file:setvbuf


## License

[See MIT License](.LICENSE)


