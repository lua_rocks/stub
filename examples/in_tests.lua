require("busted.runner")()
local assert = require("luassert")

local OS = require 'stub.os.OSystem'
local sys = OS:new() -- it also creates a stub for the FileSystem too
-- including for such things as standard io streams (stdin, stdout, stderr)


describe("example", function()
  setup(function()
    _G._TEST = true -- expose private function for testing
    -- M = require("my-module-here")
  end)

  teardown(function()
    _G._TEST = nil
  end)

  after_each(function()
    sys:clear_state()
    -- sys.fs:clear_state()
  end)

  it("check io.stdin", function()
    sys.fs:stdin_add_input('user-input')

    local res_input = io.stdin:read('*l')
    assert.same('user-input', res_input)
  end)

  it("emulate user answer on stdout", function()
    -- require'dprint'.enable()
    -- local sys = OS:new()

    -- define user answer on output
    -- a phrase upon the appearance of which in stdout the user must respond
    local phrase = 'Enter your name: '
    sys:set_user_answer(phrase, 'My name is...')

    -- check work in action
    io.stdout:write(phrase) -- inside triggers the reply_to_stdout
    io.stdout:flush()

    local reply = io.stdin:read('*l') -- emulate ask data from user

    assert.same('My name is...', reply)

    io.stdout:write('Something strange')
    assert.is_nil(io.stdin:read('*l')) -- no reply
  end)


  it("emulate Project with dirs and files", function()
    local proj_dir = '/tmp/dev/workspace/projects/proj_a/'
    -- Note that after creating a new directory, mk always automatically apply
    -- cd into it so that the next file or directory is immediately created in
    -- it. so here we use up navigation to create the directory test/
    sys.fs:mk(proj_dir, 'mix.exs', './lib/', 'src.ex', '../test/', 'src_test.exs')
    -- print(sys:tree(proj_dir))
    --[[
    /tmp/dev/workspace/projects/proj_a
      mix.exs
      lib/
          src.ex
      test/
          src_test.exs
    ]]
    assert.same('directory', sys.fs:file_type(proj_dir))
    assert.same('directory', sys.fs:file_type(proj_dir .. "lib/"))
    assert.same('directory', sys.fs:file_type(proj_dir .. "test/"))

    assert.same('file', sys.fs:file_type(proj_dir .. "mix.exs"))
    assert.same('file', sys.fs:file_type(proj_dir .. "lib/src.ex"))
    assert.same('file', sys.fs:file_type(proj_dir .. "test/src_test.exs"))
    -- ... work with emulated FS
  end)
end)

