-- 19-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

---@diagnostic disable-next-line unused-local
local class = require("oop.class")
local NVim = require("stub.vim.NVim")

---@diagnostic disable-next-line unused-local
local D = require("dprint")

describe("stub.vim.NVim", function()
  before_each(function()
    _G.vim = nil
  end)

  --
  it('mk_vim_api - create and restore', function()
    assert.is_nil(_G.vim)
    -- local prev_vim = _G.vim
    _G.vim = {}
    local original_api = { __tag = 'original' }
    _G.vim.api = original_api


    local nvim = NVim() ---@type stub.vim.NVim

    nvim:new_buf({ 'abc' }, 1)

    local api = (_G.vim or {}).api -- stub_api(wrapped)
    assert.same('abc', api.nvim_get_current_line())
    assert.same(nvim.api, _G.vim.api)
    nvim:restore_original_vim_api()

    assert.same(original_api, _G.vim.api)

    -- finally(function() _G.vim = prev_vim end) -- works in after_each
  end)
  -----

  it("new api", function()
    assert.is_nil(_G.vim)
    local nvim = NVim() ---@type stub.vim.NVim
    assert.is_not_nil(_G.vim)
    assert.is_not_nil(_G.vim.api)

    assert.is_table(nvim:get_state())
    local exp = {
      id = 2,
      bufs = {},
      wins = {
        [1000] = {
          winnr = 1000,
          cursor_pos = { 1, 0 },
          current_bufnr = false,
        }
      },
      next_bufnr = 0,
      executed_cmds = {},
    }
    assert.same(exp, nvim:get_state())
  end)

  --
  it("new api", function()
    local nvim = NVim() ---@type stub.vim.NVim
    local a = _G.vim.api

    assert.same({}, a.nvim_list_bufs())

    -- D.enable()
    local lines = { '--0', '--1', '--2', '--3', }
    assert.same(1, nvim:new_buf(lines))

    assert.same('--0', a.nvim_get_current_line())
    assert.same({ 1, 0 }, a.nvim_win_get_cursor(0))
    assert.same('No Name', a.nvim_buf_get_name(0))
    assert.same(4, a.nvim_buf_line_count(0))

    assert.same(lines, a.nvim_buf_get_lines(0, 0, -1, true))           -- all
    assert.same({ '--0', '--1' }, a.nvim_buf_get_lines(0, 0, 2, true)) -- part

    assert.same(true, a.nvim_win_set_cursor(0, { 2, 1 }))
    assert.same({ 2, 1 }, a.nvim_win_get_cursor(0))
    assert.same(1, a.nvim_win_get_buf(0))
    assert.same({ 1 }, a.nvim_list_bufs())
  end)


  it("nvim_buf_get_lines", function()
    local nvim = NVim() ---@type stub.vim.NVim
    local a = _G.vim.api
    local lines = {
      --      nvim  lnum
      '--0', -- 0  1
      '--1', -- 1
      '--2', -- 2
      '--3', -- 3
    }
    assert.same(1, nvim:new_buf(lines))

    assert.same({}, a.nvim_buf_get_lines(0, 0, 0, true))
    assert.same({ '--0' }, a.nvim_buf_get_lines(0, 0, 1, true))
    assert.same({ '--0', '--1' }, a.nvim_buf_get_lines(0, 0, 2, true))
    assert.same({ '--0', '--1', '--2' }, a.nvim_buf_get_lines(0, 0, 3, true))
    assert.same({}, a.nvim_buf_get_lines(0, 1, 1, true))

    assert.same({ '--1' }, a.nvim_buf_get_lines(0, 1, 2, true))
    assert.same({ '--1', '--2' }, a.nvim_buf_get_lines(0, 1, 3, true))
    assert.same({ '--1', '--2', '--3' }, a.nvim_buf_get_lines(0, 1, 4, true))

    assert.same({ '--3' }, a.nvim_buf_get_lines(0, -2, -1, true))
    assert.same({}, a.nvim_buf_get_lines(0, -1, -1, true))
    assert.same({}, a.nvim_buf_get_lines(0, -2, -2, true))
    -- D.enable()

    assert.match_error(function()
      a.nvim_buf_get_lines(0, 3, 6, true)
    end, 'Index out of bounds')

    assert.match_error(function()
      a.nvim_buf_get_lines(0, -8, -8, true)
    end, 'Index out of bounds')
  end)

  it("nvim_buf_get_lines", function()
    local nvim = NVim() ---@type stub.vim.NVim
    local a = _G.vim.api
    local lines = { '--0', '--1', '--2', '--3', }
    local bn = nvim:new_buf(lines)
    a.nvim_buf_set_lines(bn, 0, 1, true, { 'a', 'b' })
    assert.same({ 'a', 'b', '--1', '--2', '--3' }, a.nvim_buf_get_lines(bn, 0, -1, true))
  end)

  --
  --
  it('mk_vim_api - make sure the original api is stored safely', function()
    assert.is_nil(_G.vim)
    _G.vim = {}
    local original_api = { __tag = 'original' }
    _G.vim.api = original_api

    -- make sure multiples mk_vim_api not rewrite original vim.api
    local n1 = NVim()
    n1:new_buf({ '111' })
    assert.same('111', _G.vim.api.nvim_get_current_line())

    local n2 = NVim()
    n2:new_buf({ '222' })
    assert.same('222', _G.vim.api.nvim_get_current_line())

    local n3 = NVim() ---@type stub.vim.NVim
    n3:new_buf({ '333' })
    assert.same('333', _G.vim.api.nvim_get_current_line())

    n1:restore_original_vim_api() -- n3:restore_original throw error
    assert.same(original_api, _G.vim.api)

    local n4 = NVim() ---@type stub.vim.NVim
    n4:new_buf({ '444' })
    assert.same('444', _G.vim.api.nvim_get_current_line())

    n4:restore_original_vim_api()
    assert.same(original_api, _G.vim.api)
  end)

  assert.is_nil(_G.vim)
  it("nvim_buf_get_lines", function()
    local lines = {
      --       vim cursor
      "abc", -- 0   1
      "def", -- 1   2
      "jhi", -- 2   3
      "jkl", -- 3   4
      "lnm", -- 4   5
    }
    local n = NVim() ---@type stub.vim.NVim
    n:new_buf(lines)
    assert.equal(n.api, _G.vim.api)
    local api = n.api
    assert.same(lines, api.nvim_buf_get_lines(0, 0, 5, false))
    assert.same({}, api.nvim_buf_get_lines(0, 0, 0, false))
    assert.same({ "abc" }, api.nvim_buf_get_lines(0, 0, 1, false))
    assert.same({ "abc", "def" }, api.nvim_buf_get_lines(0, 0, 2, false))
    assert.same({}, api.nvim_buf_get_lines(0, -1, -1, false))
    assert.same({ "lnm" }, api.nvim_buf_get_lines(0, -2, -1, false))
    assert.same({}, api.nvim_buf_get_lines(0, -1, -2, false))
  end)

  it("mk_vim_api", function()
    local lines = { 'a', 'b', 'c' }
    local n = NVim()
    n:new_buf(lines)
    assert.same({ "a", 'b' }, _G.vim.api.nvim_buf_get_lines(0, 0, 2, false))
  end)


  it("nvim_win_set_cursor", function()
    local lines = { "abc", "def", "jhi", "jkl", "lnm", }
    local n = NVim()
    n:new_buf(lines)
    assert.same("abc", n.api.nvim_get_current_line())
    n.api.nvim_win_set_cursor(0, { 2, 1 })
    assert.same("def", n.api.nvim_get_current_line())
  end)

  it("nvim_buf_set_lines", function()
    local lines = { 'a', 'b', 'c', 'd' }
    local n = NVim()
    n:new_buf(lines, 2)
    assert.same("b", n.api.nvim_get_current_line())
    assert.same(lines, n.api.nvim_buf_get_lines(0, 0, -1, false))
    local new_lines = { 'B', 'C' }
    -- [1, 3)
    n.api.nvim_buf_set_lines(0, 1, 3, false, new_lines)
    assert.same({ 'a', 'B', 'C', 'd' }, n.api.nvim_buf_get_lines(0, 0, -1, false))
  end)

  it("nvim_buf_set_lines - all", function()
    local lines = { '0', '1', '2', '3' }
    local replacement = { 'a', 'b', 'c', 'd' }
    local n = NVim()
    local bn = n:new_buf(lines, 2)
    assert.same(lines, n.api.nvim_buf_get_lines(0, 0, -1, false))

    n.api.nvim_buf_set_lines(bn, 0, -1, false, replacement)
    local exp = replacement
    assert.same(exp, n.api.nvim_buf_get_lines(0, 0, -1, false))
  end)

  it("nvim_buf_set_lines - 0,0 (before)", function()
    local lines = { '0', '1', '2', '3' }
    local replacement = { 'a', 'b', 'c' }
    local n = NVim()
    local bn = n:new_buf(lines, 2, 0, 16)
    assert.same(lines, n.api.nvim_buf_get_lines(bn, 0, -1, false))

    n.api.nvim_buf_set_lines(bn, 0, 0, false, replacement)
    local exp = { 'a', 'b', 'c', '0', '1', '2', '3' }
    assert.same(exp, n.api.nvim_buf_get_lines(bn, 0, -1, false))
  end)

  it("nvim_buf_set_lines - -1,-1 (append to end)", function()
    local lines = { '0', '1', '2', '3' }
    local replacement = { 'a', 'b', 'c' }
    local n = NVim()
    local bn = n:new_buf(lines, 2)
    assert.same(lines, n.api.nvim_buf_get_lines(bn, 0, -1, false))

    n.api.nvim_buf_set_lines(bn, -1, -1, false, replacement)
    local exp = { '0', '1', '2', '3', 'a', 'b', 'c' }
    assert.same(exp, n.api.nvim_buf_get_lines(bn, 0, -1, false))
  end)

  it("nvim_buf_set_lines - 1,1 (middle-- insert before 1)", function()
    local lines = { '0', '1', '2', '3' }
    local replacement = { 'a', 'b', 'c' }
    local n = NVim()
    local bn = n:new_buf(lines, 2)
    assert.same(lines, n.api.nvim_buf_get_lines(bn, 0, -1, false))

    n.api.nvim_buf_set_lines(bn, 1, 1, false, replacement)
    local exp = { '0', 'a', 'b', 'c', '1', '2', '3', }
    assert.same(exp, n.api.nvim_buf_get_lines(bn, 0, -1, false))
  end)

  it("nvim_buf_set_lines - 1,2 (middle-- replace 1)", function()
    local lines = { '0', '1', '2', '3' }
    local replacement = { 'a', 'b', 'c' }
    local n = NVim()
    local bn = n:new_buf(lines, 2)
    assert.same(lines, n.api.nvim_buf_get_lines(bn, 0, -1, false))

    n.api.nvim_buf_set_lines(bn, 1, 2, false, replacement)
    local exp = { '0', 'a', 'b', 'c', '2', '3', }
    assert.same(exp, n.api.nvim_buf_get_lines(bn, 0, -1, false))
  end)

  it("nvim_buf_set_lines - 3,3 (middle-- replace 1)", function()
    local lines = { '0', '1', '2', '3' }
    local replacement = { 'a', 'b', 'c' }
    local n = NVim()
    local bn = n:new_buf(lines, 2)
    assert.same(lines, n.api.nvim_buf_get_lines(bn, 0, -1, false))

    n.api.nvim_buf_set_lines(bn, 3, 3, false, replacement)
    local exp = { '0', '1', '2', 'a', 'b', 'c', '3' }
    assert.same(exp, n.api.nvim_buf_get_lines(bn, 0, -1, false))
  end)

  it("nvim_buf_get_var", function()
    local n = NVim()
    local bufnr = n:new_buf()

    n.api.nvim_buf_set_var(bufnr, 'varname', 'value')
    assert.same("value", n.api.nvim_buf_get_var(bufnr, 'varname'))
    -- inner
    assert.same("value", n:get_state().bufs[bufnr].vars.varname)
  end)

  it("nvim_{set|get}_current_win", function()
    local n = NVim()
    assert.same(1000, n.api.nvim_get_current_win())
    n.api.nvim_set_current_win(1002)
    assert.same(1002, n.api.nvim_get_current_win())
  end)

  it("nvim_win_{get|set}_buf", function()
    local n = NVim()
    local bufnr = n:new_buf(nil, nil, nil, 8)
    assert.same(8, bufnr)
    assert.same(8, n.api.nvim_win_get_buf(0))
    n.api.nvim_win_set_buf(0, 1)
    assert.same(1, n.api.nvim_win_get_buf(0))
  end)

  it("new_buf bufname", function()
    local nvim = NVim()
    local bufnr = nvim:new_buf({}, 1, 1, nil, '/tmp/bufname')
    assert.same('/tmp/bufname', nvim.api.nvim_buf_get_name(bufnr))
  end)

  it("get_lines aliase for nvim_buf_get_lines", function()
    local nvim = NVim() ---@type stub.vim.NVim
    local lines = { 'a', 'b', 'c' }
    local bufnr = nvim:new_buf(lines)
    assert.same(lines, nvim:get_lines())
    assert.same(lines, nvim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
    assert.same(bufnr, nvim.api.nvim_get_current_buf())
  end)

  it("get_os", function()
    local nvim = NVim() ---@type stub.vim.NVim
    assert.is_nil(nvim.fs)
    nvim:get_os()
    assert.is_not_nil(nvim.os)
    local prev = nvim.os
    assert.same('stub.os.OSystem', nvim.os:getClassName())
    assert.equal(prev, nvim:get_os()) -- same instance
  end)

  it("restore_original_os", function()
    local nvim = NVim() ---@type stub.vim.NVim
    nvim:get_os()
    assert.is_not_nil(nvim.os)
    assert.is_nil(nvim:restore_original_os().os)
  end)

  ---------------------------  vim.ui  ----------------------------

  -- check tooling
  it("mk_ui_stub vim.ui.input", function()
    local nvim = NVim() ---@type stub.vim.NVim

    nvim:add_ui_input('abc')
    local exp = { data = { 'abc' }, opts = {}, next = 1 }
    assert.same(exp, nvim.o.ui_input)
    assert.same('abc', nvim:get_next_ui_input())
    assert.same('abc', nvim:pop_ui_input())
    assert.is_nil(nvim:get_next_ui_input())

    nvim:add_ui_input('a')
    nvim:add_ui_input('b')
    nvim:add_ui_input('c')
    assert.same('a', nvim:pop_ui_input())
    assert.same('b', nvim:pop_ui_input())
    assert.same('c', nvim:pop_ui_input())
    assert.same(false, nvim:pop_ui_input())
  end)

  --
  --
  it("vim.ui.input input from user", function()
    local nvim = NVim() ---@type stub.vim.NVim

    nvim:add_ui_input('first_input')
    local res = nil

    local opts = { prompt = 'msg 1', default = 'def', completion = {} }
    _G.vim.ui.input(opts, function(input) res = input end)

    assert.same('first_input', res)
    assert.same(opts, nvim:get_ui_input_opts())

    res = nil
    _G.vim.ui.input(opts, function(input) res = input end)
    assert.is_nil(res)
  end)

  --
  it("vim.ui.input canceled by user", function()
    local nvim = NVim() ---@type stub.vim.NVim
    nvim:add_ui_input(nil) -- cancel

    local res, called = nil, false

    local opts = { prompt = 'msg 2', default = 'def' }
    _G.vim.ui.input(opts, function(input)
      called = true; res = input
    end)

    assert.is_false(called)
    assert.is_nil(res)

    local exp = { prompt = 'msg 2', default = 'def' }
    assert.same(exp, nvim:get_ui_input_opts())
  end)

  it("logger_setup", function()
    local prev_p = _G.print
    local t = {}
    ---@diagnostic disable-next-line: duplicate-set-field
    _G.print = function(line) t[#t + 1] = line end
    local log = NVim.logger_setup(nil, 0) -- 0 is trace log level
    _G.print = prev_p

    if not log then return end
    local exp = ': Setup Verbose Output'
    assert.match(exp, t[1])

    assert.is_table(log)
    assert.same(false, log.__get_config().silent_debug)
    local exp2 = '0:TRACE[V], Save: false, File: nil Existed: false [No Date]'
    assert.same(exp2, log.status())
    NVim.logger_off()
  end)

  it("logger log_to_mem and last_msg", function()
    local log = NVim.logger_log_to_mem()
    if log then
      assert.match('Start Logging to Memory', NVim.logger_last_msg() or '')
      assert.match('Setup Verbose Output', NVim.logger_last_msg(1) or '')
      assert.is_table(log.messages)
      NVim.logger_off()
      -- make sure cleaned collected messages
      assert.is_nil(log.messages)
    end
  end)

  it("vim.notify", function()
    local n = NVim:new()
    n:clear_nitify() -- init to collect to table, by default print to the StdOut
    vim.notify('message.', 1, {})
    assert.same({ 'message.' }, n:get_nitify_msgs())
  end)

  it("nvim_replace_termcodes", function()
    local nvim = NVim() ---@type stub.vim.NVim
    assert.is_table(nvim)
    local f = vim.api.nvim_replace_termcodes
    assert.same(string.char(27), f('<esc>', true, false, true)) -- "^A"
    assert.same(string.char(1), f('<c-a>', true, false, true))  -- "^A"
    assert.same(string.char(2), f('<c-b>', true, false, true))  -- "^B"
    assert.same(string.char(13), f('<c-m>', true, false, true)) -- "^M"
    assert.same(string.char(26), f('<c-z>', true, false, true)) -- "^Z"
    -- assert.same(string.char(26), f('<c-a-z>', true, false, true)) -- "^Z"
    assert.same('h', f('h', true, false, true))
  end)

  local OUTBUF_ERR = 'Cursor position outside buffer'
  it("valdate_cursor_pos", function()
    local lines = { 'a', 'bc' }
    local f = NVim.validate_cursor_pos
    assert.same({ 1, 0 }, f({ 1, 0 }, lines))
    assert.same({ 2, 0 }, f({ 2, 0 }, lines))

    assert.match_error(function() f({ 3, 0 }, lines) end, OUTBUF_ERR)
    assert.match_error(function() f({ 0, 0 }, lines) end, OUTBUF_ERR)
    assert.match_error(function() f({ 1, -1 }, lines) end, OUTBUF_ERR)
    assert.match_error(function() f({ 1, 2 }, lines) end, OUTBUF_ERR)
    assert.match_error(function() f({ 2, 3 }, lines) end, OUTBUF_ERR)
  end)

  it("getpos'.' get_cursor feedkeys", function()
    local nvim = NVim() ---@type stub.vim.NVim
    assert.is_table(nvim)
    local bn = nvim:new_buf({ 'line 1', 'line 2', 'line3' }, 2, 1)
    assert.is_number(bn)
    assert.same({ 0, 2, 1, 0 }, vim.fn.getpos('.'))
    assert.same({ 2, 1 }, vim.api.nvim_win_get_cursor(0))
    vim.api.nvim_feedkeys('k', 'n') -- move up
    assert.same({ 0, 1, 1, 0 }, vim.fn.getpos('.'))
    assert.same({ 1, 1 }, vim.api.nvim_win_get_cursor(0))

    assert.match_error(function()
      vim.api.nvim_feedkeys('k', 'n')
    end, OUTBUF_ERR)
  end)

  -- note vim.fn.getpos('.') means current cursor_pos in current buffer
  it("vim.fn.setpos sync with vim.api.nvim_win_set_cursor", function()
    local nvim = NVim() ---@type stub.vim.NVim
    assert.is_table(nvim)
    local bn = nvim:new_buf({ 'line 1', 'line 2', 'line3' }, 2, 1)
    assert.is_number(bn)

    vim.fn.setpos('.', { 0, 3, 4, 0 })
    assert.same({ 0, 3, 4, 0 }, vim.fn.getpos('.'))
    assert.same({ 3, 4 }, vim.api.nvim_win_get_cursor(0))

    vim.api.nvim_win_set_cursor(0, { 1, 0 })

    assert.same({ 0, 1, 0, 0 }, vim.fn.getpos('.'))
    assert.same({ 1, 0 }, vim.api.nvim_win_get_cursor(0))
  end)
end)
