-- 10-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("stub.vim.utils");

describe("stub.vim.utils", function()
  it("replace_termcodes", function()
    local f = M.replace_termcodes
    local ba = string.byte
    assert.same('a', f('a', true, false, true))
    assert.same('A', f('A', true, false, true))
    assert.same('h', f('h', true, false, true))
    assert.same('0', f('0', true, false, true))

    assert.same(string.char(1), f('<c-a>', true, false, true))  -- "^A"
    assert.same(string.char(2), f('<c-b>', true, false, true))  -- "^B"
    assert.same(string.char(13), f('<c-m>', true, false, true)) -- "^M"
    assert.same(string.char(26), f('<c-z>', true, false, true)) -- "^Z"
    -- assert.same(string.char(26), f('<c-a-z>', true, false, true))

    assert.same(string.char(128, 252, 8, 97), f('<a-a>', true, false, true))
    assert.same(string.char(128, 252, 8, 98), f('<a-b>', true, false, true))
    assert.same(string.char(128, 252, 8, 110), f('<a-n>', true, false, true))
    assert.same(string.char(128, 252, 8, 122), f('<a-z>', true, false, true))

    assert.same(string.char(128, 252, 8, 1), f('<c-a-a>', true, false, true))
    assert.same(string.char(128, 252, 8, 2), f('<c-a-b>', true, false, true))
    assert.same(string.char(128, 252, 8, 14), f('<c-a-n>', true, false, true))
    assert.same(string.char(128, 252, 8, 26), f('<c-a-z>', true, false, true))

    assert.same(string.char(128, 107, 49), f('<f1>', true, false, true))
    assert.same(string.char(128, 107, 53), f('<f5>', true, false, true))
    assert.same({ 128 }, { ba(f('<f10>', true, false, true)) }) -- ?
    assert.same(string.char(128, 70, 49), f('<f11>', true, false, true))
    assert.same(string.char(128, 70, 50), f('<f12>', true, false, true))
  end)

  it("CODE_ESC", function()
    assert.same(string.char(27), M.TERMCODE_ESC)
  end)
end)
