-- 24-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local sid = require("stub.id")
local IOHandle = require("stub.os.IOHandle");
local FileStub = require("stub.os.FileStub");

---@diagnostic disable-next-line: unused-local
local D = require("dprint")

---@param name string
---@param ug table? {uid, gid}
---@param mode number?
---@param time number?
---@return stub.os.FileStub file
---@return stub.os.FileStub root
local function mk_filestub(dir, name, lines, ug, mode, time)
  local root = dir or FileStub.new_dir(nil, '/')
  return FileStub.new_file(root, name, lines, ug, mode, time), root
end


describe("stub.os.IOHandle", function()
  after_each(function()
    sid.reset_all()
    IOHandle.bus._cleanup()
  end)

  it("_init", function()
    local lines = { 'a', 'b', 'c' }
    local h = IOHandle:new(nil, mk_filestub(nil, 'myfilename', lines))
    assert.same('myfilename', h.file.name) -- basename
    assert.same('/myfilename', tostring(h.file))
    assert.same('file', h.file.fs_stat.type)
    assert.same(true, h.open)
    assert.same(0, h.lnum)
    assert.same(lines, h.lines)
  end)

  it("read file *a", function()
    local lines = { 'a', 'b', 'c' }
    local h = IOHandle:new(nil, mk_filestub(nil, 'myfilename', lines), 'r')
    assert.same("a\nb\nc", h:read('*a'))
    assert.same(nil, h:read())
    assert.same(#lines, h.lnum)
    h:close()
  end)

  it("read file line-by-line", function()
    local lines = { 'a', 'b', 'c' }
    local h = IOHandle:new(nil, mk_filestub(nil, 'myfilename', lines), 'r')
    assert.same("a", h:read())
    assert.same("b", h:read())
    assert.same("c", h:read())
    assert.same(nil, h:read())
    assert.same(#lines + 1, h.lnum)
    h:close()
  end)

  it("read file by blocks with given size", function()
    local lines = { '0123456789', 'abcdefjhij', 'ABCDEFJHIJ' }
    local h = IOHandle:new(nil, mk_filestub(nil, 'myfilename', lines), 'r')
    assert.same("0123456789\nabcde", h:read(16))
    assert.same("fjhij\nABCDEFJHIJ", h:read(16))
    assert.is_nil(h:read(16))
    assert.is_nil(h:read(16))
    h:close()
  end)

  it("read files in dir", function()
    local root = FileStub.new_dir(nil, '/')
    local d1 = FileStub.new_dir(root, 'dir1')

    local f1 = FileStub.new_dir(d1, 'file1')
    local f2 = FileStub.new_dir(d1, 'file2')
    local f3 = FileStub.new_dir(d1, 'file3')

    assert.same({ file1 = f1, file2 = f2, file3 = f3 }, d1.files)
    assert.same(true, d1:is_dir())

    -- D.enable()
    local h = IOHandle:new(nil, d1)
    -- assert.same(1, h.lines)
    assert.same('file1', h:read())
    assert.same("file2", h:read())
    assert.same("file3", h:read())
    assert.same(nil, h:read())
    assert.same(4, h.lnum)
    h:close()
  end)

  --
  --
  it("write cannot write bad mode", function()
    local h = IOHandle:new(nil, mk_filestub(nil, 'out'))

    assert.match_error(function()
      h:write("ab\ncd\nef")
    end, 'expected can_write')
  end)

  it("write", function()
    local h = IOHandle:new(nil, mk_filestub(nil, 'out'), 'w')
    assert.same(8, h:write("ab\ncd\nef"))
    h:close()
    assert.same({ 'ab', 'cd', 'ef' }, h.lines)
    assert.same({ 'ab', 'cd', 'ef' }, h.file.lines)
  end)


  -- to emulate stdin without FileStub
  it("factory IOHandle.of read (stdin)", function()
    local lines = { 'ab', 'cd', 'ef' }
    local h = IOHandle.of(lines, 'r') -- io.open
    assert.is_not_nil(h)              ---cast h file* -- stub.os.IOHandle
    local exp_opened = {
      id = 1,
      mode = 'r',
      type = IOHandle.TYPE.PIPE,
      lines = { 'ab', 'cd', 'ef' },
      can_write = false,
      can_read = true,
      open = true,
      lnum = 0,
    }
    assert.same(exp_opened, h)

    local txt = h:read('*a')

    assert.same("ab\ncd\nef", txt)
    h:close()

    local exp_closed = {
      id = 1,
      mode = 'r',
      type = IOHandle.TYPE.PIPE,
      lines = { 'ab', 'cd', 'ef' },
      can_read = true,
      can_write = false,
      open = false,
      lnum = 3,
    }
    assert.same(exp_closed, h)
  end)

  -- to emulate stdout without FileStub
  it("factory IOHandle.of read (stdin)", function()
    local h = IOHandle.of({}, 'w') -- io.open
    assert.is_not_nil(h)           ---cast h file* -- stub.os.IOHandle
    h:write('ab')
    h:write('cd')
    h:write('e\nf')
    h:close()
    assert.same({ 'ab', 'cd', 'e', 'f' }, h.lines)
  end)

  it("_status", function()
    local h = IOHandle.of({}, 'w')
    assert.same('&?:POW:0/0 id: 1', IOHandle._status(h))
    --            ^fd

    h = IOHandle.of({}, 'w')
    assert.same('&1:POW:0/0 id: 2', IOHandle._status(h, 1))

    h = IOHandle.of({ '1', '2' }, 'r')
    assert.same('&0:POR:0/2 id: 3', IOHandle._status(h, 0))
    h:read('*l') -- read one line
    assert.same('&0:POR:1/2 id: 3', IOHandle._status(h, 0))
    h:read('*a')
    assert.same('&0:PCR:2/2 id: 3', IOHandle._status(h, 0))
    h:close()
    assert.same('&0:PCR:2/2 id: 3', IOHandle._status(h, 0))

    h = IOHandle:new(nil, mk_filestub(nil, 'filename', { '1', '2', '3' }), 'r')
    assert.same('&?:FOR:0/3 id: 4 /filename', IOHandle._status(h))

    local exp = '&?:FOR:0/3 id: 4 %(stub.os.IOHandle%) @.* /filename'
    assert.match(exp, IOHandle._status(h, nil, true))

    ---@diagnostic disable-next-line: param-type-mismatch
    assert.same('nil', IOHandle._status(nil))
  end)

  it("_is_stdio", function()
    local h = IOHandle.of({}, 'r', IOHandle.TYPE.STD_STREAM)
    assert.same(true, IOHandle._is_stdio(h))

    io.stdin = {}
    assert.same(false, IOHandle._is_stdio(h, io.stdin))

    io.stdin = h -- emulate stubbing by FileSystem
    assert.same(true, IOHandle._is_stdio(h, io.stdin))
  end)

  --[[
  it("get_file_name", function()

    local function get_file_name(self)
      if (self or E).file then
        if self.file.get_absolute_path then
          return FileStub.get_absolute_path(self.file)
        else
          return self.file.name
        end
      end
      return '?'
    end
    local h = IOHandle:new(nil, mk_filestub(nil, 'file', {}), 'r')
    assert.same('/file', get_file_name(h))
  end)
]]
end)
