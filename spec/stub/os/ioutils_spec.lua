-- 27-01-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local M = require("stub.os.ioutils");

describe("stub.os.iohelper", function()
  it("to_bool", function()
    local f = M.to_bool
    assert.is_nil(f(nil))
    assert.is_true(f(nil, true))
    assert.is_false(f(nil, false))
    assert.is_false(f('no'))
    assert.is_false(f(0))
    assert.is_true(f('yes'))
    assert.is_true(f(1))
  end)

  it("add_lines", function()
    local f = M.add_lines
    assert.same({ 'abc' }, f({}, 'abc'))
    assert.same({ 'abc', 'de' }, f({}, "abc\nde"))
    assert.same({ 'abc', 'de' }, f({}, { "abc\nde" }))
    assert.same({ 'abc', 'd', 'e' }, f({}, { "abc\nd", "e" }))
    assert.same({ 'abc', 'd', 'efg' }, f({}, { "abc\nd", "efg" }))
  end)

  it("add_lines keep empty lines", function()
    local f = M.add_lines
    assert.same({ 'abc', '', '', 'de', 'efg' }, f({}, { "abc\n\n\nde", "efg" }))
  end)

  it("join_path", function()
    -- throw error on empty strings
    assert.has_error(function() M.join_path('', '') end)
    assert.has_error(function() M.join_path('/', '') end)
    assert.has_error(function() M.join_path('', '/') end)

    assert.same('/', M.join_path('/', '/'))
    assert.same('/', M.join_path('/', '/'))
    assert.same('/bin', M.join_path('/usr', '/bin'))
    assert.same('/usr/bin', M.join_path('/usr', 'bin'))
  end)

  it("mk_res_path", function()
    _G.resources_dir = '/tmp'
    assert.same('/tmp/some', M.mk_res_path('some'))

    _G.resources_dir = nil -- take from PWD
    assert.matches('.*/spec/resources/some$', M.mk_res_path('some'))
    assert.same('/full/path', M.mk_res_path('/full/path'))

    assert.matches('.*/spec/resources/some$', M.mk_res_path('./some'))
  end)

  it('basename', function()
    local f = M.basename
    assert.is_nil(f(nil))
    assert.same("", f(""))
    assert.same('/', f("/"))
    assert.same('/', f("//"))
    assert.same('.', f("/./"))
    assert.same("file", f("file"))
    assert.same("file", f("/file"))
    assert.same("file", f("/dir/file"))
    assert.same("file", f("path/to/file"))
    assert.same("file", f("/path/to/file"))
    assert.same('dir', f("/path/to/dir/"))
    assert.same('dir', f("/path/to/dir//"))
    assert.same('dir', f("/path/to/dir///"))
    assert.same('dir', f("/dir/"))
  end)

  it("extract_path", function()
    local f = M.extract_path
    assert.is_nil(f("file"))
    assert.is_nil(f("file.md"))
    assert.same("/", f("/"))
    assert.same("./", f("./."))
    assert.same("./", f("./"))
    assert.same("../", f("../"))
    assert.same("../", f("../x"))
    assert.same('//', f("//")) -- todo fix
    assert.same("/", f("/dir-or-file"))
    assert.same("/home/u/.lo/share/app/", f("/home/u/.lo/share/app/some_file"))
    assert.same("C:\\Program Files\\app\\", f("C:\\Program Files\\app\\file"))
    assert.same("C:\\", f("C:\\"))
    assert.same("/dir/", f("/dir/"))
    assert.same("/dir/", f("/dir/f"))
  end)

  it("tbl_flat_copy", function()
    local f = M.tbl_flat_copy
    local src = { k = '1', k2 = 'b', 'c' }
    local res = f(nil, src)
    assert.same(src, res)
    assert.are_not_equal(src, res) -- sure a new one table
    local dst = { k = '0' }
    assert.same({ k = '1', k2 = 'b', 'c' }, f(dst, src))
  end)

  it("ensure_dir", function()
    assert.same("/", M.ensure_dir('/'))
    assert.same("/dir/", M.ensure_dir('/dir'))
    assert.same("/dir/", M.ensure_dir('/dir/'))
  end)

  it("replace_ext", function()
    local f = M.replace_ext
    assert.same('/tmp/file.new', f("/tmp/file.old", "old", "new"))
    assert.is_nil(f("/tmp/file.old", "new", "old"))
    assert.is_nil(f("/tmp/file.old", "old2", "new2"))
    assert.is_nil(f("/tmp/file.old", "ol2", "new2"))
  end)

  it("extract_filename", function()
    local f = M.extract_filename
    assert.same('file', f("/tmp/file"))
    assert.same('file', f("/tmp/file.e"))
    assert.same('file', f("/tmp/file.ext"))
    assert.same('.dotfile', f("/tmp/.dotfile"))
    assert.same('.file', f("/tmp/.file.ext"))
    assert.same('.gitignore', f("/tmp/.gitignore"))
  end)

  it("split_to_path_and_file", function()
    local f = M.split_to_path_and_file
    assert.same({ '/tmp/', 'file' }, { f("/tmp/file") })
    assert.same({ '/tmp/' }, { f("/tmp/") })
    assert.same({ '.', 'file' }, { f("file") })
    assert.same({ 'file/' }, { f("file/") })
  end)

  it("is_absolute_path", function()
    local f = M.is_absolute_path
    assert.same(true, f("/etc/"))
  end)

  it("path_to_classname", function()
    assert.same('a.b.C', M.path_to_classname('a/b/C'))
    assert.same('a/b/C', M.classname_to_path('a.b.C'))
  end)

  it("tohex & fromhex", function()
    assert.same('4f72616e6765', M.tohex("Orange"))
    assert.same('Orange', M.fromhex("4f72616e6765"))

    assert.same('31323334353637383930', M.tohex("1234567890"))
    assert.same('1234567890', M.fromhex("31323334353637383930"))
  end)

  it("starts_with", function()
    local f = M.starts_with
    assert.same(true, f("abcd", "ab"))
    assert.same(false, f("abcd", "Ab"))
    assert.same(false, f("abcd", "cd"))
    assert.same(true, f("abcd", ""))
    assert.same(false, f("abcd", "."))
    assert.same(true, f("abcd", "a"))
  end)

  it("ends_with", function()
    local f = M.ends_with
    assert.same(true, f("abcd", "cd"))
    assert.same(false, f("abcd", "cde"))
    assert.same(false, f("abcd", "."))
    assert.same(true, f("abcd", ""))
  end)
end)
