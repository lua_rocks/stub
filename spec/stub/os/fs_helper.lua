-- 26-11-2023 @author Swarg
local M = {}
-- Goal: utils to test stub.os.FileSystem

local dprint = require("dprint").mk_dprint_for(M)

-- is existed file via io
-- local files = require 'env.files'
function M.io_is_exist_file(file)
  local h = io.open(file, "rb")
  if h then h:close() end
  return h ~= nil
end

---
--- is existed file via luv
---
---@return boolean
function M.luv_is_exist_file(path)
  local uv = require("luv") -- package.loaded['luv']
  if path then
    local stat = uv.fs_stat(path)
    local exists = (stat and stat.type == "file") == true
    return exists
  end
  return false
end

---
--- is existed dir via luv
---@return boolean
function M.luv_is_exist_dir(path)
  local uv = require("luv")
  if path then
    local stat = uv.fs_stat(path)
    local exists = (stat and stat.type == "directory") == true
    return exists
  end
  return false
end

-- ENOENT: no such file or directory: /tmp/55
function M.luv_list_of_files(dir)
  local uv = require("luv")
  local handle, errmsg = uv.fs_scandir(dir)
  if type(handle) == "string" then
    return false, handle
  elseif not handle then
    return false, errmsg
  end
  local ls = {}
  while true do
    local name, _ = uv.fs_scandir_next(handle)
    if not name then
      break
    end
    table.insert(ls, name)
  end
  return ls
end

M.path_sep = '/'

function M.mkdir(dir, mode)
  local uv = require("luv")
  mode = mode or 448 -- 0700 -> decimal
  if not uv.fs_mkdir(dir, mode) then
    -- create the full deep of all subdirs
    local path = ''
    if not M.is_win then path = '/' end
    for s in string.gmatch(dir, '([^/\\]+)') do
      path = path .. M.path_sep .. s
      if not M.luv_is_exist_dir(path) then
        if not uv.fs_mkdir(path, mode) then
          return false
        end
      end
    end
    return true
  end
  return true;
end

-- run external os command and return its output
---@param cmd string
---@return string? Output of command
function M.execr(cmd)
  dprint("execr: RUN ", cmd)
  local fh = io.popen(cmd)
  dprint("execr: io.popen ret: fh:", fh)
  if fh then
    local data = fh:read('*a')
    fh:close()
    dprint("execr: ret:", data)
    return data
  end
end

function M.getDeveloperName()
  local developerName = '?'
  -- take from git
  local user
  user = M.execr('git config user.name')
  if user then
    user = user:gsub('\n', '');
  else
    user = os.getenv('USER')
  end
  if user then
    developerName = user:sub(1, 1):upper() .. user:sub(2)
  end
  return developerName
end

--
---@param output string
function M.on_stdout(output)
  io.stdout:write(output)
  local res = io.stdin:read('*l')
  return res
end

--
return M
