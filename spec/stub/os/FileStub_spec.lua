-- 26-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local UG = require("stub.os.UG")
local M = require("stub.os.FileStub");

describe("stub.os.FileStub", function()
  it("stat", function()
    local root = M.new_dir(nil, '/')
    local f = M.new_file(root, 'file')
    local exp = [[
  File: /file
  Size: 0                 Blocks: 8           IO Block: 4096     file
Device: 2053              Inode: 2            Links: 1
Access: (33188/         ) Uid: (  1000/        ) Gid:(  1000/        )
Access: 2023-11-24 09:54:48.900000000
Modify: 2023-11-24 09:54:48.900000000
Change: 2023-11-24 09:54:48.900000000
 Birth: 2023-11-24 09:54:48.900000000
]]
    assert.same(exp, f:stat())
  end)

  it("stat format", function()
    local root = M.new_dir(nil, '/')
    local f = M.new_file(root, 'file')
    local exp = '1000:username 1000:groupname'
    local users = { [1000] = { uname = 'username', gname = 'groupname' } }
    assert.same(exp, f:stat('%u:%U %g:%G', users))
  end)

  it("__tostring", function()
    local root = M.new_dir(nil, '/')
    assert.same('/', root:__tostring())


    local f = M.new_file(root, 'file')
    assert.same('/file', f:__tostring())

    local d = M.new_dir(root, 'bin')
    assert.same('/bin/', d:__tostring())

    local var = M.new_dir(root, 'var')
    local var_log = M.new_dir(var, 'log')
    local syslog = M.new_file(var_log, 'syslog')

    assert.same('/var/log/syslog', syslog:__tostring())
    assert.same('/var/log/', var_log:__tostring())
  end)

  -- mv /file1 /dir1/file1
  it("bind_to (mv)", function()
    local root = M.new_dir(nil, '/')

    local f1 = M.new_file(root, 'file1') -- :bind_to(root) inside
    assert.same('/', f1.parent.name)
    assert.same(f1, f1.parent.files['file1'])
    assert.same(f1, root.files['file1'])

    local d1 = M.new_dir(root, 'dir1') -- /dir1
    f1:bind_to(d1)                     -- mv /file1 /dir1/file1

    assert.same('dir1', f1.parent.name)
    assert.same(f1, f1.parent.files['file1'])
    assert.same(nil, root.files['file1'])
    assert.same(f1, d1.files['file1'])
  end)


  it("new file root", function()
    local root = M.new_dir(nil, '/')

    local f1 = M.new_file(root, 'file1', {}, UG.default())
    assert.same(UG.DEFAULT_USER_ID, f1.fs_stat.uid)
    assert.same(UG.DEFAULT_GROUP_ID, f1.fs_stat.gid)

    local f2 = M.new_file(root, 'file2', {}, UG.root())
    assert.same(0, f2.fs_stat.uid)
    assert.same(0, f2.fs_stat.gid)
  end)
end)
