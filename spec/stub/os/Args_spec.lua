-- 04-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local class = require("oop.class")
local M = require("stub.os.Args");

function Aof(t)
  return M:new({ ai = 1, args = t })
end

describe("stub.os.Args", function()
  after_each(function()
    require 'dprint'.enable(false)
  end)

  it("unwrap", function()
    assert.same("a b", M.unwrap('a b'))
    assert.same('"a b', M.unwrap('"a b'))
    assert.same('a b"', M.unwrap('a b"'))
    assert.same("a b", M.unwrap('"a b"'))
    assert.same("a b", M.unwrap("'a b'"))
  end)


  it("Q", function()
    assert.same("'a b'", M.Q('a b'))
    assert.same('ab', M.Q('ab'))
  end)


  it("parse_cmdline quotes", function()
    local f = M.parse_cmdline
    assert.same({ 'a', 'b', 'c' }, f('a b c'))
    assert.same({ 'a b', 'c' }, f('"a b" c'))
    assert.same({ 'a b', 'c' }, f("'a b' c"))
    assert.same({ 'a "b', '"c d' }, f("'a \"b' \"c d"))
  end)


  it("get_cmdline join parsed args", function()
    assert.same('a b c', Aof({ 'a', 'b', 'c' }):get_cmdline())
    assert.same("ls -l 'a b'", Aof({ 'ls', '-l', 'a b' }):get_cmdline())
    assert.same('', Aof({}):get_cmdline())
    assert.same('x', Aof({ '', 'x' }):get_cmdline())
  end)


  it("parse_cmdline quotes << EOF .. EOF", function()
    local f = M.parse_cmdline
    local exp = {
      'cat', '>', 'file', '<<', "multi line\nfile content\n",
    }
    assert.same(exp, f('cat > file << EOF\nmulti line\nfile content\nEOF'))
    -- extra chars
    assert.same(exp, f('cat > file << EOF\nmulti line\nfile content\nEOF\n..'))
  end)


  it("parse_cmdline > <<<", function()
    local f = M.parse_cmdline
    local exp = { 'cat', '>', 'fn', '<<', "multi line\nfile content\n" }
    assert.same(exp, f('cat > fn << EOF\nmulti line\nfile content\nEOF'))
  end)


  it("parse_cmdline quotes", function()
    local f = M.parse_cmdline
    local exp = { 'sudo', 'sh', '<<', "multi line\nfile content\n" }
    assert.same(exp, f('sudo sh << EOF\nmulti line\nfile content\nEOF'))

    local cmd = [[
sudo sh << SUDO_CMD_EOF
  cat > filename << END_OF_FILE
 ab c
 d e f
END_OF_FILE
SUDO_CMD_EOF
    ]]
    local exp2 = {
      'sudo', 'sh', '<<',
      "  cat > filename << END_OF_FILE\n ab c\n d e f\nEND_OF_FILE\n"
    }
    assert.same(exp2, f(cmd))


    local exp3 = { 'cat', '>', 'filename', '<<', " ab c\n d e f\n" }
    assert.same(exp3, f(exp2[4]))
  end)


  it("new obj + parse line", function()
    local obj = M.of('ls -l')
    local exp = { ai = 1, cmdline = 'ls -l', args = { 'ls', '-l' } }
    assert.same(exp, obj)
    assert.same(true, class.instanceof(obj, M))
    assert.same('stub.os.Args', obj:getClassName())
  end)


  it("parse_cmdline", function()
    local f = M.parse_cmdline
    assert.same({ 'a', 'b  c', 'd', 'e' }, f('a  "b  c"  d e'))
    assert.same({ 'a', 'b | c', 'd', 'e' }, f('a  "b | c"  d e'))
    assert.same({ 'a', 'b', '|', 'c', 'd', 'e' }, f('a  b | c  d e'))
  end)


  it("parse_cmdline TODO FIXME", function()
    local f = M.parse_cmdline
    assert.same({ 'a', 'b|', 'c', 'd', 'e' }, f('a  b| c  d e'))
    assert.same({ 'a', 'b;c', 'd', 'e' }, f('a  b;c  d e'))
    assert.same({ 'cat', '<', '<(cmd', 'a)' }, f('cat < <(cmd a)'))
    assert.same({ 'cat', '2>&1' }, f('cat 2>&1'))          -- dublication
    assert.same({ 'cat', '2>&1-' }, f('cat 2>&1-'))
    assert.same({ 'cat', '2>', 'file' }, f('cat 2> file')) -- stderr to file
    -- 2>/dev/null
    -- `3>&2`, `2>&1` or `1>&3-`
  end)


  it("new obj directly", function()
    local obj = M.of({ ai = 1, cmdline = 'ls -l', args = { 'ls', '-l' } })
    assert.same({ args = { 'ls', '-l' }, ai = 1 }, obj)
    assert.same(true, class.instanceof(obj, M))
    assert.same('stub.os.Args', obj:getClassName())
  end)


  it("pop_arg", function()
    local a = M:new(nil, 'a b c d')
    assert.same({ 'a', 'b', 'c', 'd' }, a.args)
    assert.same(1, a.ai)
    assert.same("a", a:pop_arg())
    assert.same(2, a.ai)
    assert.same('b', a:pop_arg())
    assert.same("c", a:pop_arg())
    assert.same("d", a:pop_arg())
    assert.same(nil, a:pop_arg())
    assert.same(6, a.ai)
  end)


  it("is(cmd)", function()
    local a = M.of('sudo ls -l')
    assert.same(true, a:is('sudo')) -- auto inc ai inside
    assert.same(true, a:is('ls'))
    assert.same(true, a:is('-l'))
    assert.same(false, a:is('x'))
  end)


  it("iseq", function()
    local a = M.of('a b c')
    assert.same(1, a.ai)
    assert.same('a', a:arg(0))
    assert.same(true, a:iseq('a', 0))
    assert.same(true, a:iseq('a'))
    assert.same(true, a:iseq('b', 1))
    assert.same(true, a:iseq('c', 2))
    assert.same(false, a:iseq('d', 3))
    assert.same(1, a.ai) -- not change ai
  end)


  it("has_args :arg", function()
    local a = M.of('a b')
    assert.same('a', a:arg(0))
    assert.same(true, a:has_args())
    assert.same(true, a:is('a')) -- inc ai
    assert.same('b', a:arg(0))
    assert.same(true, a:has_args())

    assert.same(true, a:is('b'))
    assert.same(false, a:has_args())
    assert.same(nil, a:arg(0))
    assert.same('b', a:arg(-1))
    assert.same('a', a:arg(-2))
  end)


  -- it("has_args", function()
  --   assert.same("", M.has_args())
  -- end)

  it("args_cnt", function()
    local a = M.of('a b c')
    assert.same(3, a:args_cnt())
    assert.same(true, a:iseq('a', 0))
    assert.same(true, a:iseq('b', 1))
    assert.same(true, a:iseq('c', 2))
  end)


  it("no", function()
    local a = M.of('a b c')
    assert.same(true, a:no(-1))
    assert.same(false, a:no(0)) -- a
    assert.same(false, a:no(1)) -- b
    assert.same(false, a:no(2)) -- c
    assert.same(true, a:no(3))  -- nil
  end)


  it("inc", function()
    local a = M.of('a b c d e')

    assert.same(1, a.ai)
    assert.same("a", a:arg())
    assert.same(2, (a:inc() or {}).ai)  -- return self
    assert.same("b", a:arg())
    assert.same(4, (a:inc(2) or {}).ai) -- return self
    assert.same('d', a:arg())
  end)


  it("idxof", function()
    --      ai      1 2 3 4 5 6
    local a = M.of('a x c d e f')
    assert.same(1, a.ai)
    assert.same(1, a:idxof('x'))          -- offset from current arg(a)
    assert.same(2, a:idxof('x', 0, true)) -- absolute index from 1
    assert.same(6, a:idxof('f', 0, true)) -- absolute index from 1
    a.ai = 4
    assert.same('d', a:arg(0))
    assert.same(0, a:idxof('d'))  -- offset is 0 -- current arg is 'c'
    assert.same(-1, a:idxof('a')) -- not found
    assert.same(-1, a:idxof('x')) -- not found
    assert.same(2, a:idxof('f'))  --  d, e, f offset from d to f is 2
    assert.same('f', a:arg(2))
  end)


  it("idx_t", function()
    local a = M.of('a x c d e f >>')
    local t = a:idxof_t({ 'x', 'c', 'f', '>>' })
    assert.same({ x = 1, c = 2, f = 5, ['>>'] = 6 }, t)
    assert.same('f', a:arg(t.f))
    assert.same('>>', a:arg(t['>>']))
  end)


  it("clear_passed", function()
    --              1   2 3 4
    local a = M.of('cat x > d')
    assert.same('cat', a:arg(0))
    a.ai = 2
    assert.same({ 'cat', 'x', '>', 'd' }, a.args)
    assert.same(1, a:clear_passed())
    assert.same({ [2] = 'x', [3] = '>', [4] = 'd' }, a.args)
    a.ai = 1
    assert.same(nil, a:arg(0))
  end)


  it("match", function()
    local a = M.of('cat src >> dst')
    assert.same(true, a:is('cat')) -- inc ai)
    assert.same('src', a:arg(0))   -- inc ai)
    assert.same('src', a:match('^src$'))
    assert.same('src', a:match('^src$', 0))
    assert.same(nil, a:match('^src$', 1))
    assert.same('>', a:match('>', 1))
  end)


  it("sub", function()
    --               1  2  3  4  5  6
    local a = M.of('cat f1 f2 | wc -l')

    local exp = { ai = 1, args = { 'cat', 'f1', 'f2', '|', 'wc', '-l' } }
    assert.same(exp, a:sub(1))

    assert.same({ ai = 1, args = { 'cat', 'f1' } }, a:sub(1, 2))

    assert.same({ ai = 1, args = {} }, a:sub(0, 0))
    assert.same({ ai = 1, args = { 'wc', '-l' } }, a:sub(5, 6))

    a:inc()
    assert.same({ ai = 1, args = { 'f1', 'f2' } }, a:sub(1, 2))
  end)


  -- inner tooling
  it("set_nil", function()
    --              0  1  2  3  4  5  6
    local a = M.of('x a1 -k a2 -K val a3 ')
    assert.same({ 'x', 'a1', '-k', 'a2', '-K', 'val', 'a3' }, a.args)
    local f = M._remove_arg
    f(a, 2); f(a, 4); f(a, 5) -- a:_set_nil(offset)
    assert.same({ [1] = 'x', [2] = 'a1', [4] = 'a2', [7] = 'a3' }, a.args)
  end)


  it("_last_idx", function()
    -- offset(from ai)     0         1          2     3          4
    local a = Aof({ [1] = 'A', [2] = 'B', [3] = 'C', [4] = 'D', [5] = 'E' })
    assert.same(5, a:_last_idx())
    assert.same(5, a:args_cnt())
    assert.same(1, a.ai)
    assert.same('B', a:_remove_arg(1)) -- a.args[a.ai+1] = nil
    assert.same(5, a:_last_idx())
    assert.same(1, a:args_cnt())       -- falls into a hole and goes no further

    a.args[a.ai + 3] = nil
    assert.same(5, a:_last_idx())
    assert.same(1, a:args_cnt())

    a.args[a.ai + 4] = nil
    assert.same(3, a:_last_idx())
    assert.same(1, a:args_cnt())
    assert.same({ [1] = 'A', [3] = 'C' }, a.args)
  end)


  it("_patchup_holes", function()
    --              0  1  2  3  4  5  6
    local a = Aof({ [1] = '1', [2] = '2', [4] = '4', [8] = '8' })
    a:_patchup_holes()
    assert.same({ '1', '2', '4', '8' }, a.args)
  end)


  it("pull_opts full-name", function()
    local a = M.of('cmd -c N arg')
    local ko = { ['-a'] = { M.O_FLAG }, [{ '--count', '-c' }] = { M.O_WVAL } }
    local exp = { count = 'N' }
    assert.same(exp, a:pull_opts(ko))

    assert.same(exp, M.of('cmd --count N arg'):pull_opts(ko))
  end)


  it("pull_opts unknown-opts", function()
    local kopts = { ['-k'] = { M.O_WVAL } }
    local exp = {
      k = '16', -- overrided
      UNKNOWN_OPTS = { '--count', '-u', '-U' },
    }
    assert.same(exp, M.of('cmd --count N -k 16 -u -U arg'):pull_opts(kopts))
  end)


  it("pull_opts default_tbl and overriding", function()
    local kopts = { ['-k'] = { M.O_WVAL } }
    local def = { k = 8, b = 'default-value' }
    local exp = {
      k = '16',            -- overrided
      b = 'default-value', -- keep default
    }
    assert.same(exp, M.of('cmd -k 16 arg'):pull_opts(kopts, def))
  end)


  it("pull_opts full-name", function()
    local a = M.of('cmd -c N arg')
    local ko = { ['-a'] = { M.O_FLAG }, [{ '--count', '-c' }] = { M.O_WVAL } }
    local exp = { count = 'N' }
    assert.same(exp, a:pull_opts(ko))
  end)


  it("pull_opts", function()
    local f = M.pull_opts
    local a = M.of('cmd arg1 -a a2 -b value a3 --count N a4')
    ---@diagnostic disable-next-line: param-type-mismatch
    assert.match_error(function() f(a, nil) end, 'known opts must be a table')
    assert.match_error(function() f(a, {}) end, 'known opts must be not empty')
    local known_opts = {
      ['-a'] = { M.O_FLAG }, ['-b'] = { M.O_WVAL }, [{ '--count', '-c' }] = { M.O_WVAL }
    }
    local exp = { count = 'N', b = 'value', a = true }
    assert.same(exp, f(a, known_opts))
  end)


  it("_patchup_holes", function()
    local a = Aof({ [1] = 'cmd', [3] = 'fn', [8] = 'x' })
    assert.same({ [1] = 'cmd', [3] = 'fn', [8] = 'x' }, a.args)
    assert.same({ 'cmd', 'fn', 'x' }, a:_patchup_holes().args)
  end)


  it("pull_opts full-name", function()
    local a = M.of('cmd -a fn')
    local ko = { ['-a'] = { M.O_FLAG } }
    local t = {}
    assert.same({ 'cmd', 'fn' }, a:parse_input(ko, t).args)
    assert.same({ a = true }, t)
  end)


  it("parse_input & is_input_valid", function()
    local a = M.of('cmd --count N -k 16 -u -U arg')
    local kopts = { ['-k'] = { M.O_WVAL } }
    local exp_opts = {
      k = '16', -- overrided
      UNKNOWN_OPTS = { '--count', '-u', '-U' },
    }
    assert.same(exp_opts, a:parse_input(kopts).opts)
    assert.same(M.IV_INVALID_OPT, a:is_input_valid())
  end)

  it("parse `sh << SUEOF cat > fn << INPUT SUEOF`", function()
    local cmd = [[
sh << SU_EOF
cat << EOF
{
  "key": ["a b", "c"],
}
EOF
SU_EOF
]]
    local exp = {
      'sh',
      '<<',
      "cat << EOF\n{\n  \"key\": [\"a b\", \"c\"],\n}\nEOF\n"
    }
    assert.same(exp, M.of(cmd).args)
  end)

  it("parse `sh << SUEOF cat > fn << INPUT SUEOF`", function()
    local cmd = [[
sh << SU_EOF
cat > /etc/app/conf.json << EOF
{
  "key": ["a b", "c"],
}
EOF
SU_EOF
]]
    local exp = {
      'sh',
      '<<',
      "cat > /etc/app/conf.json << EOF\n{\n  \"key\": [\"a b\", \"c\"],\n}\nEOF\n"
    }
    assert.same(exp, M.parse_cmdline(cmd))
  end)


  it("parse `cat > fn << EOF .. EOF`", function()
    local cmd = [[
cat > /etc/app/conf.json << EOF
{
  "key": ["a b", "c"],
}
EOF
]]
    local exp = {
      'cat',
      '>',
      '/etc/app/conf.json',
      '<<',
      "{\n  \"key\": [\"a b\", \"c\"],\n}\n"
    }
    assert.same(exp, M.parse_cmdline(cmd))
  end)


  it("parse `sudo sh << SUEOF cat > fn << INPUT SUEOF`", function()
    local cmd = [[
sudo sh << SU_EOF
cat > /etc/app/conf.json << EOF
{
  "key": ["a b", "c"],
}
EOF
SU_EOF
]]
    local w = M.of(cmd)
    local exp = {
      ai = 1,
      cmdline = "sudo sh << SU_EOF\ncat > /etc/app/conf.json << EOF\n" ..
          "{\n  \"key\": [\"a b\", \"c\"],\n}\nEOF\nSU_EOF\n",
      args = {
        'sudo', 'sh', '<<',
        "cat > /etc/app/conf.json << EOF\n{\n  \"key\": [\"a b\", \"c\"],\n}\nEOF\n"
      }
    }
    assert.same(exp, w)
  end)

  it("parse nested sh", function()
    local cmd = "sh -c 'sh -c \"echo 123 | cat\"'"

    local w = M.of(cmd)
    local exp = { 'sh', '-c', '\'sh -c "echo 123 | cat"\'' }
    assert.same(exp, w.args)
  end)

  --[[
  it("assert", function()
    local h, err = io.popen("cat > /tmp/f1 << EOF\nline1\nline2\nEOF")
    assert.is_nil(err)
    assert.is_not_nil(h) ---@cast h file*
    local out = h:read('*a') -- *a give '' but *l give nil on empty input
    assert.same('', out)
    h:close()
  end)
]]
end)
