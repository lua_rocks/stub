-- 06-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local sid = require("stub.id")
local IOPipe = require("stub.os.IOPipe");
local IOHandle = require("stub.os.IOHandle")
local D = require("dprint")
local E = {}

describe("stub.os.IOPipe", function()
  after_each(function()
    sid.reset_all()
    IOHandle.bus._cleanup()
    D.disable()
  end)


  it("default pipe", function()
    local pipe = IOPipe:new()
    assert.is_table(pipe.read)
    assert.is_table(pipe.write)
    local hin = pipe.read
    local hout = pipe.write

    assert.match_error(function() hin:write('error') end, 'expected can_write')
    assert.match_error(function() hout:read('error') end, 'expected can_read')

    assert.are_not_equal({}, {}) -- equal is checks ref to the same object
    -- assert.equal(pipe.read.lines, pipe.write.lines)
    assert.same({}, hout.lines)
    hout:write('data from pipe.out')
    assert.same({ 'data from pipe.out' }, hout.lines)

    assert.same({ 'data from pipe.out' }, hin.lines)
  end)


  it("subscribe handler to IOHandle.close", function()
    local pipe = IOPipe:new()
    assert.same(pipe, IOPipe._get_pipe(pipe.id)) -- is-alive
    assert.same(IOHandle.TYPE.PIPE, (pipe.write or E).type)
    assert.same(IOHandle.TYPE.PIPE, (pipe.read or E).type)

    local exp_listeners = {
      [pipe.write] = {
        close = {                           -- channel-name
          [IOPipe.handler_on_close] = pipe, -- handler & userdata(self)
        }
      },
      [pipe.read] = {
        close = { [IOPipe.handler_on_close] = pipe, }
      }
    }
    assert.same(exp_listeners, IOHandle.bus._get_listeners())

    pipe.write:close() -- trigger IOPipe.handler_on_close with autounsubscibe

    assert.is_table(pipe.write)
    assert.is_table(pipe.read)

    local exp2 = {
      [pipe.read] = { close = { [IOPipe.handler_on_close] = pipe, } }
    }
    assert.same(exp2, IOHandle.bus._get_listeners())
    assert.same(pipe, IOPipe._get_pipe(pipe.id))

    pipe.read:close() -- trigger IOPipe.handler_on_close with autounsubscibe

    assert.same({}, IOHandle.bus._get_listeners())
    assert.same(nil, IOPipe._get_pipe(pipe.id)) -- is-not-alive
  end)


  -- issue: stdio streams shared across all processes
  -- and therefore by design they cannot be closed (IOHandle.open = false)
  -- check inner way in IOPipe to process onclose for stdio
  it("subscribe handler to IOHandle.close", function()
    local pipe = IOPipe:new({
      write = IOHandle.of({}, 'w', IOHandle.TYPE.STD_STREAM),
      read  = IOHandle.of({}, 'r', IOHandle.TYPE.STD_STREAM),
    })
    assert.same(pipe, IOPipe._get_pipe(pipe.id)) -- is-alive
    assert.same(IOHandle.TYPE.STD_STREAM, (pipe.write or E).type)
    assert.same(IOHandle.TYPE.STD_STREAM, (pipe.read or E).type)

    local exp_listeners = {
      [pipe.write] = { close = { [IOPipe.handler_on_close] = pipe, } },
      [pipe.read]  = { close = { [IOPipe.handler_on_close] = pipe, } },
    }
    assert.same(exp_listeners, IOHandle.bus._get_listeners())

    pipe.write:close() -- trigger IOPipe.handler_on_close with autounsubscibe

    assert.is_nil(pipe.write)
    assert.is_table(pipe.read)

    local exp2 = {
      [pipe.read] = { close = { [IOPipe.handler_on_close] = pipe, } }
    }
    assert.same(exp2, IOHandle.bus._get_listeners())
    assert.same(pipe, IOPipe._get_pipe(pipe.id)) -- is-alive

    pipe.read:close()

    assert.same({}, IOHandle.bus._get_listeners())
    assert.same(nil, IOPipe._get_pipe(pipe.id)) -- auto removed from alive
  end)



  it("connect new reader (pipe-in) to existed writer(pipe-out)", function()
    local hout = IOHandle.of({}, 'w')
    local hin = nil -- not defined - autocreate in the constructor
    local pipe = IOPipe:new({ write = hout, read = hin })

    assert.is_table(pipe.read)
    assert.is_table(pipe.write)
    hin = pipe.read
    assert.equal(hout, pipe.write)
    assert.equal(pipe.read.lines, pipe.write.lines) -- same shared table

    assert.match_error(function() hin:write('error') end, 'expected can_write')
    assert.match_error(function() hout:read('error') end, 'expected can_read')

    hout:write('data from pipe.out')
    assert.same({ 'data from pipe.out' }, hout.lines)
    assert.same({ 'data from pipe.out' }, hin.lines)
    assert.same('data from pipe.out', hin:read())
  end)


  it("connect new writer (pipe.write(out) to existed reader(pipe.read(in)", function()
    local hout = nil -- not defined - autocreate in the constructor
    local h_in = IOHandle.of({}, 'r')
    local pipe = IOPipe:new({ write = hout, read = h_in })

    assert.is_table(pipe.read)
    assert.is_table(pipe.write)
    hout = pipe.write
    assert.equal(h_in, pipe.read)
    assert.equal(pipe.read.lines, pipe.write.lines) -- same shared table

    assert.match_error(function() h_in:write('error') end, 'expected can_write')
    assert.match_error(function() hout:read('error') end, 'expected can_read')

    hout:write('data from pipe.out')
    assert.same({ 'data from pipe.out' }, hout.lines)
    assert.same({ 'data from pipe.out' }, h_in.lines)
    assert.same('data from pipe.out', h_in:read())
  end)


  it("connect existed writer and reader", function()
    local hout = IOHandle.of({}, 'w')
    local h_in = IOHandle.of({}, 'r')
    local pipe = IOPipe:new({ write = hout, read = h_in })

    assert.equal(h_in, pipe.read)
    assert.equal(hout, pipe.write)
    assert.equal(pipe.read.lines, pipe.write.lines) -- same shared table

    assert.match_error(function() h_in:write('error') end, 'expected can_write')
    assert.match_error(function() hout:read('error') end, 'expected can_read')

    hout:write('data from pipe.out')
    assert.same({ 'data from pipe.out' }, hout.lines)
    assert.same({ 'data from pipe.out' }, h_in.lines)
    assert.same('data from pipe.out', h_in:read())
  end)


  it("connect existed writer and reader keep data from writer", function()
    local hout = IOHandle.of({ 'text from IOHandle factory before _init' }, 'w')
    local h_in = IOHandle.of({}, 'r')
    local pipe = IOPipe:new({ write = hout, read = h_in })

    assert.equal(h_in, pipe.read)
    assert.equal(hout, pipe.write)
    assert.equal(pipe.read.lines, pipe.write.lines) -- same shared table
    local exp0 = { 'text from IOHandle factory before _init' }
    assert.same(exp0, hout.lines)

    hout:write('data from pipe.out(writer) to pipe.in(reader)')
    local exp = {
      'text from IOHandle factory before _init',
      'data from pipe.out(writer) to pipe.in(reader)'
    }
    assert.same(exp, hout.lines)
    assert.same(exp, h_in.lines)
    assert.same(exp[1], h_in:read()) -- first line
  end)


  it("_find_pipeid_by_handler", function()
    local hw1 = IOHandle.of({}, 'w')
    local hr1 = IOHandle.of({}, 'r')
    local p1 = IOPipe:new({ write = hw1, read = hr1 })

    local hw2 = IOHandle.of({}, 'w')
    local hr2 = IOHandle.of({}, 'r')
    local p2 = IOPipe:new({ write = hw2, read = hr2 })

    local h3 = IOHandle.of({}, 'r')
    assert.same(1, p1.id)
    assert.same(2, p2.id)
    assert.same(nil, IOPipe._find_pipeid_by_handler(h3))
    assert.same(p1.id, IOPipe._find_pipeid_by_handler(hw1))
    assert.same(p1.id, IOPipe._find_pipeid_by_handler(hr1))
    assert.same(p2.id, IOPipe._find_pipeid_by_handler(hw2))
    assert.same(p2.id, IOPipe._find_pipeid_by_handler(hr2))
  end)
end)
