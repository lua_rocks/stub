-- 26-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local FS = require("stub.os.FileSystem")
local OS = require("stub.os.OSystem");
local UG = require("stub.os.UG")
local Proc = require("stub.os.Proc");
local IOHandle = require("stub.os.IOHandle");
local sid = require("stub.id")

local H = require("spec.stub.os.fs_helper")
local E = {}

local actually_existed_file_01 = os.getenv('PWD') .. '/spec/resources/file_01'

local D = require 'dprint'

-- helper
---@param with_fs_stub boolean?
local function newOSnProc(with_fs_stub)
  local _os = OS:new(nil, FS:new({ nostub = not with_fs_stub }))
  local _proc = assert(_os.o.p_init, 'has init process') -- local proc = Proc.of(_os)
  return _os, _proc
end

local sys, proc

describe("stub.os.OSystem", function()
  after_each(function()
    if sys and sys.fs then
      sys.fs:restore_original()
    end
    sid.reset_all()
    D.disable()
  end)


  it("init process", function()
    sys = OS:new(nil, FS:new())
    local p = sys.o.p_init
    assert.is_table(p)
    assert.same(1, p.pid)
    assert.same(0, p.ppid)
    assert.same(2, sys:next_pid())
    assert.same(3, sys:next_pid())
    assert.same(sys.fs.io.stdin, p:stdin())
    assert.same(sys.fs.io.stdout, p:stdout())
    assert.same(sys.fs.io.stderr, p:stderr())
    assert.same(IOHandle.TYPE.STD_STREAM, p:stdout().type)
    local exp = "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" .. -- T     - mean terminal i.g. stdio-streams
        "&1:TOW:0/0 id: 2\n" .. --  O    - open
        "&2:TOW:0/0 id: 3\n"    --   R|W - read|write
    assert.same(exp, Proc._opened_fds(p, true))
  end)

  --
  it("io.open real file", function()
    assert.is_true(H.io_is_exist_file(actually_existed_file_01))
  end)

  it("io.popen set_popen my-app", function()
    sys = OS:new(nil, FS:new())

    local cmd = 'my-app status'
    assert.is_nil(sys:get_popen(cmd)) -- ensure answer not exists

    sys:set_popen(cmd, { "Answer", "To Command" })
    assert.is_not_nil(sys:get_popen(cmd)) -- answer installed


    local h, _ = io.popen(cmd)
    assert.is_not_nil(h) ---@cast h file* -- stub.os.IOHandle
    local txt = h:read('*a')
    io.close(h)

    assert.same("Answer\nTo Command", txt)
  end)

  -- check is sys actually restored after each tests
  it("io.open real file -- check fs:restore_original", function()
    assert.is_true(H.io_is_exist_file(actually_existed_file_01))
  end)

  --
  it("popen default setup", function()
    sys = OS:new(nil, FS:new())

    local cmd = 'my-app'
    sys:set_popen(cmd, { "AnswerFromApp" })
    assert.is_not_nil(sys:get_popen(cmd))
    assert.same('AnswerFromApp', H.execr(cmd))

    sys:set_popen(cmd, nil) -- remove
    assert.is_nil(sys:get_popen(cmd))

    assert.same('', H.execr(cmd))
  end)

  --
  it("popen default setup", function()
    sys = OS:new(nil, FS:new())

    local cmd = 'git config user.name'
    assert.is_not_nil(sys:get_popen(cmd))

    assert.same('AuthorName', H.execr(cmd))
    assert.same('AuthorName', OS.DEF_DEV_NAME)
  end)

  --
  it("popen default setup configured", function()
    sys = OS:new({ no_def_progs = false, devname = 'Dev' }, FS:new())

    local cmd = 'git config user.name'

    assert.is_not_nil(sys:get_popen(cmd))
    assert.same('Dev', H.execr(cmd))
  end)

  it("popen getDeveloperName", function()
    local fs = FS:new()
    OS:new({ no_def_progs = false }, fs)

    assert.same('AuthorName', H.getDeveloperName())
  end)


  it("emulate user answer on stdout", function()
    sys = OS:new()             -- under the hood runs: IOHandle.bus.subscribe(stdout, write)
    sys:clear_state(nil, true) -- to check issue with broken subscribe

    -- define user answer on output
    local phrase = 'Enter your name: '
    sys:set_user_answer(phrase, 'user')

    -- check inner things of implementation
    assert.same({ [phrase] = 'user' }, sys.o.user_answers)
    -- assert.same({ 'user' }, sys:reply_to_stdout({phrase})) -- lines

    -- check work in action
    io.stdout:write(phrase) -- without \n,  inside triggers the reply_to_stdout
    io.stdout:flush()

    -- under the hood its works via eventbus:
    local exp1 = {
      write = {
        [OS.handler_on_write_to_stdout] = sys,
      }
    }
    assert.same(exp1, IOHandle.bus._get_listeners()[io.stdout])

    local reply = io.stdin:read('*l')                 -- emulate ask data from user

    assert.same(true, IOHandle._is_stdout(io.stdout)) -- own with hook
    assert.same('user', reply)

    io.stdout:write('Something strange')
    assert.is_nil(io.stdin:read('*l')) -- no reply
  end)


  it("host_read_lines -- without stub", function()
    sys = OS:new(nil, FS:new({ nostub = true }))
    assert.match_error(function()
      sys:host_read_lines('./not_exists_file')
    end, 'File not found: "./not_exists_file"')

    assert.no_error(function()
      sys:host_read_lines(actually_existed_file_01)
    end)
  end)

  -- emulate responce on cli command
  -- taking the response from the specified real-existed file
  it("set_popen_from_file", function()
    sys = OS:new()
    -- setup answer to the command:
    local cmd = 'cat ./file'
    local fstub = sys:set_popen_from_file(cmd, actually_existed_file_01)

    -- check is file sucessfully readed from disk
    assert.is_table(fstub)
    local exp = {
      'this lines will be loaded from',
      'spec/resources/file',
    }
    assert.same(exp, fstub.lines)

    local h = io.popen(cmd, 'r')
    assert.is_not_nil(h)
    ---@cast h table
    assert.same('this lines will be loaded from', h:read('*l'))
    h:close()
  end)


  it("keep_persistent", function()
    sys = OS:new()
    assert.is_table(sys.o.p_init)
    -- setup
    sys:set_popen('ls0 dir1', { 'file1', 'file2' }, true)
    sys:set_popen('ls0 dir2', { 'file3', 'file4' }, false)

    sys:set_user_answer('Enter passwd: ', 'passwd', true)
    sys:set_user_answer('Your location: ', 'here', false)

    -- assert.match_error(function()
    --     sys:set_env('varname', 'value')
    --   end, 'Not implemented yet TODO mk stub for _G.os')

    -- check before clear state
    assert.same("file1\nfile2", H.execr('ls0 dir1'))
    assert.same("file3\nfile4", H.execr('ls0 dir2'))

    assert.same("passwd", H.on_stdout('Enter passwd: '))
    assert.same('here', H.on_stdout('Your location: '))

    sys:clear_state() -- keep persistent setups

    -- check same after clear state
    assert.same("file1\nfile2", H.execr('ls0 dir1'))
    -- print(sys:tree())

    assert.is_table(sys.o.p_init)
    assert.same('', H.execr('ls0 dir2'))

    assert.same("passwd", H.on_stdout('Enter passwd: '))
    assert.is_nil(H.on_stdout('Your location: '))

    sys:clear_state(true) -- full with persistent setups

    -- check same after full clear state
    assert.same('', H.execr('ls0 dir1'))
    assert.same('', H.execr('ls0 dir2'))

    assert.is_nil(H.on_stdout('Enter passwd: '))
    assert.is_nil(H.on_stdout('Your location: '))
  end)


  it("keep_persistent pass itself", function()
    sys = OS:new()
    -- setup
    sys:set_popen('ls0 dir1', { 'file1', 'file2' }, true)
    sys:set_popen('ls0 dir2', { 'file3', 'file4' }, false)

    -- check before clear state
    assert.same("file1\nfile2", H.execr('ls0 dir1'))
    assert.same("file3\nfile4", H.execr('ls0 dir2'))

    sys:clear_state() -- keep persistent setups
    assert.is_not_nil(sys.o.persistents)

    -- check same after clear state
    assert.same("file1\nfile2", H.execr('ls0 dir1'))
    assert.same('', H.execr('ls0 dir2'))

    -- make sure persistents itself passed after cleanup
    sys:clear_state() -- keep persistent setups
    assert.is_not_nil(sys.o.persistents)

    -- check same after clear state
    assert.same("file1\nfile2", H.execr('ls0 dir1'))
    assert.same('', H.execr('ls0 dir2'))

    sys:clear_state(true) -- full with persistent setups
    assert.is_nil(sys.o.persistents)

    -- check same after full clear state
    assert.same('', H.execr('ls0 dir1'))
    assert.same('', H.execr('ls0 dir2'))
  end)

  --
  -- tool for debugging and make sure stub works
  it("whatis", function()
    local uv = require("luv")
    local prev_io, prev_io_open, prev_io_popen = io, io.open, io.popen
    local prev_uv_fs_mkdir = uv.fs_mkdir
    local orig_require = require

    sys = OS:new()

    assert.match('its me os: .stub.os.OSystem. @.*', sys:whatis(sys))
    assert.match('binded fs: .stub.os.FileSystem. @.*', sys:whatis(sys.fs))

    local exp = '^.stub.os.FileSystem. @.* .noStub.$'
    assert.match(exp, sys:whatis(FS:new({ nostub = true })))

    local class = require("oop.class");
    assert.is_true(class.instanceof(OS, class.Object))
    assert.is_true(class.instanceof(sys, class.Object))
    assert.match('^class .stub.os.OSystem. @.*', sys:whatis(OS))

    assert.same('[emulated] io.open', sys:whatis(io.open))
    assert.same('[emulated] io.open', sys:whatis(sys.fs.io.open))
    assert.same('[original] io.open', sys:whatis(prev_io_open))

    assert.same('[emulated] io.popen', sys:whatis(io.popen))
    assert.same('[emulated] io.popen', sys:whatis(sys.fs.io.popen))
    assert.same('[original] io.popen', sys:whatis(prev_io_popen))

    assert.same('[emulated] luv.fs_mkdir', sys:whatis(sys.fs.luv.fs_mkdir))
    assert.same('[original] luv.fs_mkdir', sys:whatis(prev_uv_fs_mkdir))

    assert.same('original luv module', sys:whatis(uv))
    assert.same('emulated luv module', sys:whatis(sys.fs.luv))
    assert.same('emulated luv module', sys:whatis(require('luv')))

    assert.same('original io module', sys:whatis(prev_io))
    assert.same('emulated io module', sys:whatis(sys.fs.io))

    assert.same('proxy_require', sys:whatis(require))
    assert.same('original require', sys:whatis(orig_require))
  end)

  --
  it("whatis", function()
    sys = OS:new(nil, FS:new({ nostub = true }))

    assert.match('its me os: .stub.os.OSystem. @.* .noStub.$', sys:whatis(sys))
    local exp = 'binded fs: .stub.os.FileSystem. @.* .noStub.$'
    assert.match(exp, sys:whatis(sys.fs))

    -- not binded shown with @hash
    local exp2 = '^.stub.os.FileSystem. @.* .noStub.$'
    assert.match(exp2, sys:whatis(FS:new({ nostub = true })))
  end)

  it("get_groups", function()
    sys = OS:new(nil, FS:new({ nostub = true }))

    local line = 'audio:x:29:pulse,user'
    local ptrn = '^([^:]+):[^:]+:([^:]+):(.*)$'
    local a, b, c = string.match(line, ptrn)
    assert.same('audio', a)
    assert.same('29', b)
    assert.same('pulse,user', c)

    local exp = {
      [0] = { name = "root", members = {}, },
      [4] = { name = "adm", members = {}, },
      [27] = { name = "sudo", members = { "user" }, },
      [1000] = { name = "user", members = {}, },
    }
    assert.same(exp, sys:get_groups()) --lines))
  end)

  --
  -- from emulated /etc/passwd & /etc/group
  it("get_users", function()
    sys = OS:new(nil, FS:new({ nostub = true }))
    local exp = {
      [0] = {
        uid = 0,
        gid = 0,
        uname = "root",
        gname = "root",
        home = "/root",
        info = "root",
        shell = "/bin/bash",
      },
      [1000] = {
        uid = 1000,
        gid = 1000,
        uname = "user",
        gname = "user",
        home = "/home/user",
        info = "User,,,",
        shell = "/bin/bash",
      }
    }
    assert.same(exp, sys:get_users())
  end)

  it("get_user_name", function()
    sys = OS:new(nil, FS:new({ nostub = true }))
    assert.same('user', sys:get_user_name(1000))
    assert.same('user', sys:get_user_name())  -- UG.DEFAULT_USER_ID
    assert.same('root', sys:get_user_name(0)) -- UG.DEFAULT_USER_ID
  end)

  it("get_user_home", function()
    sys = OS:new(nil, FS:new({ nostub = true }))
    assert.same('/root', sys:get_user_home(0))
    assert.same('/home/user', sys:get_user_home(1000))
    assert.same('/home/user', sys:get_user_home()) --def user is 1000
  end)

  --
  --
  it("stat of given file format user-group", function()
    sys = OS:new(nil, FS:new({ nostub = true }))
    sys:is_file_exits('/etc/passwd')

    local exp = '1000:user%/1000:user'
    local line, users = sys:stat('/etc/passwd', '%u:%U%/%g:%G')
    assert.same(exp, line)

    assert.same('0:root 0:root', sys:stat_owner('/usr/sbin/adduser'))

    local exp2 = {
      uid = 1000,
      gid = 1000,
      uname = 'user',
      gname = 'user',
      info = 'User,,,',
      home = '/home/user',
      shell = '/bin/bash',
    }
    assert.same(exp2, (users or {})[1000])
  end)


  it("match", function()
    local function f(line)
      local dst, msg = line:match('\'([^\']+)\': (.*)$')
      return tostring(dst) .. '|' .. tostring(msg)
    end
    assert.same('/tmp/f|No such file or directory',
      f("mv: cannot stat '/tmp/f': No such file or directory"))

    assert.same('/tmp/f|No such file or directory',
      f("mv: cannot stat '/tmp/f': No such file or directory"))
  end)


  it("os.rename (FileSystem:mv)", function()
    local f = function(oldname, newname)
      local st, errmsg = os.rename(oldname, newname)
      return tostring(st) .. '|' .. tostring(errmsg)
    end
    assert.same('false|nil', f('/tmp/not-exists', ''))
    assert.same('false|nil', f('', '')) -- mv: missing file operand
    assert.same('false|nil', f('/tmp/f', '/tmp/f'))

    assert.same(false, sys:is_file_exits('/tmp/f2'))
    local exp = 'false|/tmp/f: No such file or directory'
    assert.same(exp, f('/tmp/f', '/tmp/f2'))

    sys.fs:set_file('/tmp/f')
    assert.same('true|nil', f('/tmp/f', '/tmp/f2'))
    assert.same(true, sys:is_file_exits('/tmp/f2'))
  end)


  it("remove", function()
    sys.fs:set_file('/tmp/file')
    assert.same(true, sys:is_file_exits('/tmp/file'))
    assert.same(true, os.remove('/tmp/file'))
    assert.same(false, sys:is_file_exits('/tmp/file'))
    --
    local st, err = os.remove('/tmp/file')
    assert.same(false, st)
    assert.same('/tmp/file: No such file or directory', err)
  end)


  it("log_cmd_access", function()
    sys = OS:new()
    assert.same(false, sys:log_cmd_access('pwd'))
    sys:collect_cmd_access(true)
    assert.same(true, sys:log_cmd_access('pwd'))
    assert.same(true, sys:log_cmd_access('ls -l'))
    assert.same({ 'pwd', 'ls -l' }, sys:get_cmd_access_log())
  end)

  local os_execute = function(cmd)
    local code, err = os.execute(cmd)
    return tostring(code) .. '|' .. tostring(err)
  end


  it("cmd_access_log via os.execute", function()
    sys = OS:new()

    sys:collect_cmd_access(true)

    assert.same('0|nil', os_execute('pwd')) -- no errors
    assert.same('2|sh: 1: bad-cmd -l: not found', os_execute('bad-cmd -l'))
    assert.same('256|nil', os_execute('echo hellow world'))
    local exp = { 'pwd', 'bad-cmd -l', 'echo hellow world' }
    assert.same(exp, sys:get_cmd_access_log())

    --
    sys:clear_cmd_access_log()
    assert.same({}, sys:get_cmd_access_log())
  end)



  -----------------------------------------------------------------------------
  --
  --                            libc stdio
  --
  -----------------------------------------------------------------------------

  -- stdio: fopen
  it("c_fopen invalid mode", function()
    sys, proc = newOSnProc()
    local fn, mode = '/etc/passwd', 'x'
    local h = sys:c_fopen(proc, fn, mode)
    assert.same('22|Invalid argument', Proc._errno(proc))
    assert.is_nil(h)
  end)


  it("c_fopen read existed file - success", function()
    sys, proc = newOSnProc()
    assert.same('0|nil', Proc._errno(proc))
    local fn, mode = '/etc/passwd', 'r'
    local h = sys:c_fopen(proc, fn, mode)
    assert.same('0|nil', Proc._errno(proc))
    assert.is_table(h) ---@cast h table
    assert.is_true(#(h.lines or E) > 0) -- file found and not empty
    assert.is_string(h:read())          -- can read
    assert.same(true, h.can_read)
    assert.same(false, h.can_write)
  end)


  it("c_fopen read fail", function()
    sys, proc = newOSnProc()
    local fn, mode = '/etc/no-existed', 'r'
    local h = sys:c_fopen(proc, fn, mode)
    assert.same('2|/etc/no-existed: No such file or directory', proc:_errno())
    assert.is_nil(h)
  end)


  it("c_fopen read fail", function()
    sys, proc = newOSnProc()
    local fn, mode = '/etc/-no-existed', 'r'

    local h = sys:c_fopen(proc, fn, mode)
    assert.is_nil(h)
  end)


  it("c_fopen open to read try write", function()
    sys, proc = newOSnProc()
    local fn, mode = '/etc/passwd', 'r'

    local h = sys:c_fopen(proc, fn, mode)

    assert.same('0|nil', Proc._errno(proc))
    assert.is_table(h) ---@cast h stub.os.IOHandle
    assert.same(-1, h:write('try-write-to-file'))
    assert.same('expected can_write', h.last_err)
    h:close()
  end)


  it("c_fopen create new as root", function()
    sys, proc = newOSnProc()
    local psudo = proc:c_fork({ ug = UG.root() })
    local fn, mode = '/tmp/file', 'w'

    local h = sys:c_fopen(psudo, fn, mode)
    assert.same({ gid = 0, uid = 0 }, psudo.ug)

    assert.same('0|nil', Proc._errno(proc))
    assert.is_table(h) ---@cast h stub.os.IOHandle
    assert.same(17, h:write('try-write-to-file'))
    assert.is_nil(h.last_err)
    h:close()

    local line = sys:stat(fn, '%u:%U%/%g:%G')
    assert.same('0:root%/0:root', line)
  end)


  -- tmp file with autoremove on close
  it("c_tmpfile + on_close_delete_file", function()
    sys, proc = newOSnProc()

    local h = proc:c_tmpfile()

    assert.same({ '/tmp/tmp1_1' }, sys.o.tmpfiles)
    assert.same(true, h.can_write)
    assert.same(true, h.can_read)

    local exp = "pid: 1 ppid: 0 uid:1000 (stub.os.Args) init\n" ..
        "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" ..
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        "&3:FORW:0/0 id: 4 /tmp/tmp1_1\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp, proc:_status(true))
    assert.same(h, proc.fds[3])
    assert.same(16, proc:c_printf(3, 'data to tmp file'))
    assert.same({ 'data to tmp file' }, h.lines)
    assert.is_true(sys:is_file_exits('/tmp/tmp1_1'))
    assert.same({ 'data to tmp file' }, sys:get_file_lines('/tmp/tmp1_1'))

    assert.same(0, proc:c_close(3)) -- autodetele file on close IOHandle
    assert.is_false(sys:is_file_exits('/tmp/tmp1_1'))

    local exp2 = "pid: 1 ppid: 0 uid:1000 (stub.os.Args) init\n" ..
        "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" ..
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp2, proc:_status(true))
    assert.same({}, sys.o.tmpfiles)
  end)


  it("get_file_content", function()
    sys = OS:new()
    local exp = "root:x:0:0:root:/root:/bin/bash\n" ..
        "user:x:1000:1000:User,,,:/home/user:/bin/bash\n"
    assert.same(exp, sys:get_file_content('/etc/passwd'))
  end)


  it("set_file_content", function()
    sys = OS:new()
    local fn = "/tmp/new-file"
    assert.same(true, sys:set_file_content(fn, "line1\nline2"))
    assert.same("line1\nline2\n", sys:get_file_content(fn))

    -- change already existed without errors
    assert.same(true, sys:set_file_content(fn, "ABC\nDEF\nG"))
    assert.same("ABC\nDEF\nG\n", sys:get_file_content(fn))
  end)


  it("c_tmpfile + on_close_delete_file", function()
    sys, proc = newOSnProc()
    local a = {}
    assert.same(false, sys:is_file_exits('/tmp/tmp1_1'))
    assert.same(false, sys:is_file_exits('/tmp/tmp1_5'))

    for i = 1, 3 do a[i] = proc:c_tmpfile() end

    -- event bus used to auto remove on close
    local handlers = IOHandle.bus._get_listeners()
    local exp_channel_bus = { [OS.handler_delete_file_on_close] = sys }
    assert.same(exp_channel_bus, handlers[a[1]]['close'])

    local exp = "pid: 1 ppid: 0 uid:1000 (stub.os.Args) init\n" ..
        "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" ..
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        "&3:FORW:0/0 id: 4 /tmp/tmp1_1\n" ..
        "&4:FORW:0/0 id: 5 /tmp/tmp1_2\n" ..
        "&5:FORW:0/0 id: 6 /tmp/tmp1_3\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp, proc:_status(true))
    assert.same(true, sys:is_file_exits('/tmp/tmp1_3'))
    local exp_tf = { '/tmp/tmp1_1', '/tmp/tmp1_2', '/tmp/tmp1_3' }
    assert.same(exp_tf, sys.o.tmpfiles)

    for i = 1, 3 do proc:c_fclose(a[i]) end

    local exp2 = "pid: 1 ppid: 0 uid:1000 (stub.os.Args) init\n" ..
        "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" ..
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp2, proc:_status(true))
    assert.same(false, sys:is_file_exits('/tmp/tmp1_5'))
    assert.same({}, sys.o.tmpfiles)

    -- autoremoved all subscribers after close
    handlers = IOHandle.bus._get_listeners()
    assert.same(nil, handlers[a[1]])
  end)


  it("getenv", function()
    sys = OS:new(nil, FS:new({ no_stub = true }))
    assert.same('user', os.getenv('USER'))
  end)

  it("set_envvar", function()
    sys = OS:new(nil, FS:new({ no_stub = false }))
    assert.same('/home/user/', os.getenv('HOME'))
    assert.same('/home/user/', sys:set_env('HOME', '/home/new'))
    assert.same('/home/new', os.getenv('HOME'))
    assert.is_nil(sys:set_env('MY_VAR', 'value'))
    assert.same('value', os.getenv('MY_VAR'))
    assert.same('value', sys:set_env('MY_VAR', 'overrided'))
    assert.same('overrided', os.getenv('MY_VAR'))
  end)

  it("PWD", function()
    sys = OS:new(nil, FS:new({ no_stub = false }))
    assert.same('/home/user/', os.getenv('PWD')) -- uv.cwd()
    local uv = require("luv")
    assert.same('/', uv.cwd())
    -- D.enable()
    assert.same('/home/user/', sys:set_env('PWD', '/home/user/'))
    assert.same('/home/user', uv.cwd()) -- updated via set PWD

    sys:cd('/home/')
    assert.same('/home', uv.cwd())
  end)

  it("mkcdir", function()
    sys = OS:new(nil, FS:new({ no_stub = false }))
    assert.same('/home/user/', os.getenv('PWD'))
    assert.same(true, sys:mkcdir('/home/user/dev/project_a'))
    assert.same('/home/user/dev/project_a', os.getenv('PWD'))
  end)

  it("treel", function()
    sys = OS:new(nil, FS:new({ no_stub = false }))
    local exp = {
      '/etc',
      '    group',
      '    hosts',
      '    passwd',
      '    shadow',
      '0 directories, 4 files'
    }
    assert.same(exp, sys:treel('/etc'))

    local exp2 = {
      '/home/',
      '    user',
      '        proj',
      '2 directories, 0 files'
    }
    assert.same(exp2, sys:treel('/home/', { no_trailing_slashes = 1 }))
    local exp3 = {
      '/home/',
      '    user/',
      '        proj/',
      '2 directories, 0 files'
    }
    assert.same(exp3, sys:treel('/home/', { no_trailing_slashes = false }))

    local exp4 = {
      '/home',
      '    user/',
      '        proj/',
      '2 directories, 0 files'
    }
    -- same as original `tree -F` output
    assert.same(exp4, sys:treel('/home', { no_trailing_slashes = false }))
  end)
end)
