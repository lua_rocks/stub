-- 05-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local class = require("oop.class")
local instanceof = class.instanceof
local IOHandle = require("stub.os.IOHandle")
local FileStub = require("stub.os.FileStub")
local Args = require("stub.os.Args")
local Proc = require("stub.os.Proc");
local UG = require("stub.os.UG")
local C = require("stub.os.consts")
local sid = require("stub.id")

local E = {}

---@param t table?{main: boolean?, ug:stub.os.UG?, args:stub.os.Args?, envs:table?)
---@return stub.os.Proc
local function newProc(t)
  t = t or E
  local sys = t._os or { o = { fs } } -- stub for stub.OSystem
  -- local _io = sys.fs.io
  local proc = Proc:new({
    os = sys,
    fds = {
      [C.STDIN] = t.stdin or IOHandle.of({}, 'r', IOHandle.TYPE.STD_STREAM),
      [C.STDOUT] = t.stdout or IOHandle.of({}, 'w', IOHandle.TYPE.STD_STREAM),
      [C.STDERR] = t.stderr or IOHandle.of({}, 'w', IOHandle.TYPE.STD_STREAM),
    },
    ug = t.ug or UG.default(),
    args = t.args or Args:new({ cmdline = t.cmdline or '' }),
    envs = t.envs or sys.o.envs,
  })

  return proc
end


describe("stub.os.Proc", function()
  after_each(function()
    require 'dprint'.enable(false)
    sid.reset_all()
    IOHandle.bus._cleanup()
  end)


  it("Proc.of factory - mk proc used inside the emulated program", function()
    local proc = newProc()
    assert.same(true, instanceof(proc, Proc))
    assert.same(true, instanceof(proc:stdin(), IOHandle))
    assert.same(true, instanceof(proc:stdout(), IOHandle))
    assert.same(true, instanceof(proc:stderr(), IOHandle))
    --
    assert.same(true, proc:stdin().can_read)
    assert.same(false, proc:stdin().can_write)

    assert.same(false, proc:stdout().can_read)
    assert.same(true, proc:stdout().can_write)

    assert.same(false, proc:stderr().can_read)
    assert.same(true, proc:stderr().can_write)

    assert.same(nil, proc.exitcode)
    assert.same(nil, proc:wait()) -- ? nil - process still alive(working)
  end)

  it("stdin", function()
    local proc = newProc()
    ---@diagnostic disable-next-line: missing-parameter
    assert.match_error(function() proc.stdin() end, 'expected  Proc, got: nil')
    --                                ^ instead : (access to method of object)
    assert.no_error(function() proc:stdin() end, 'expected  Proc, got: nil')
  end)

  -- bridge back from IOHandle to Proc
  it("event cannot-write", function()
    local proc = newProc()
    local root = FileStub.new_dir(nil, '/', nil, UG.root())
    local f = FileStub.new_file(root, 'file', { 'line1' })
    local h = IOHandle:new(nil, f, 'r')
    assert.is_nil(h.errno_cb)

    proc:bind(h)

    assert.is_function(h.errno_cb)
    assert.same('0|nil', proc:_errno())

    assert.same(-1, h:write('attempt to write to readonly')) -- workload

    assert.same('9|/file: Bad file descriptor', proc:_errno())
  end)


  it("c_printf print to stderr, stdio streams of the process", function()
    local proc = newProc()
    proc:c_printf(C.STDERR, '%s: %s', 'cat', 'No such file') -- _err
    proc:with_exitcode(1)

    assert.is_table(proc)
    local exp_in = {
      id = 1,
      type = IOHandle.TYPE.STD_STREAM,
      mode = 'r',
      lines = {},
      can_write = false,
      can_read = true,
      open = true,
      lnum = 0,
    }
    assert.same(exp_in, proc:stdin())
    local exp_out = {
      type = IOHandle.TYPE.STD_STREAM,
      id = 2,
      mode = 'w',
      lines = {},
      can_write = true,
      can_read = false,
      open = true,
      lnum = 0,
    }
    assert.same(exp_out, proc:stdout())
    local exp2 = {
      type = IOHandle.TYPE.STD_STREAM,
      id = 3,
      mode = 'w',
      lines = { 'cat: No such file' },
      can_write = true,
      can_read = false,
      open = true,
      lnum = 0,
    }
    assert.same(exp2, proc:stderr())
    assert.same(1, proc.exitcode)
  end)

  it("set_errno ENOENT with prefix via strgin.format", function()
    local p = newProc()
    local exp = '/filename: No such file or directory'
    assert.same(exp, p:set_errno(C.ERRNO.ENOENT, "%s: %s", '/filename'))
    assert.same(exp, Proc.set_errno(p, C.ERRNO.ENOENT, "%s: %s", '/filename'))
  end)

  it("set_errno EINVAL without string.format", function()
    local p = newProc()
    assert.same('Invalid argument', p:set_errno(C.ERRNO.EINVAL))
  end)

  -- helper for testing
  it("_errno", function()
    local p = newProc()
    assert.same('0|nil', Proc._errno(p))
    assert.same('Invalid argument', p:set_errno(C.ERRNO.EINVAL))
    assert.same('22|Invalid argument', Proc._errno(p))

    p:set_errno(C.ERRNO.ENOENT, "%s: %s", 'fn')
    assert.same('2|fn: No such file or directory', Proc._errno(p))
    p:clear_last_err()

    assert.same('0|nil', Proc._errno(p))
  end)


  it("c_fprintf write to own stdin stdout(to use in prog inside proc)", function()
    local p = newProc()
    ---@diagnostic disable-next-line: missing-fields
    p.os.fs = { o = { os = p.os } }

    assert.same(p.fds[C.STDOUT], p:stdout())
    assert.same(17, p:c_fprintf(p:stdout(), 'message to stdout'))
    assert.same(5, p:c_fprintf(p:stdout(), 'line2'))
    assert.same(5, p:c_fprintf(p:stdout(), 'line3'))
    assert.same({ 'message to stdout', 'line2', 'line3' }, p:stdout().lines)

    assert.same(15, p:c_fprintf(p:stderr(), 'first to stderr'))
    assert.same(16, p:c_fprintf(p:stderr(), 'second to stderr'))
    assert.same({ 'first to stderr', 'second to stderr' }, p:stderr().lines)

    assert.match_error(function()
      p:c_fprintf(p:stdin(), 'attempt to write to stdin')
    end, '|!errno_cb| expected can_write &.:TOR:0/0 id: 1; 9:Bad file descriptor')
  end)


  it("_toHandle", function()
    local p = newProc()
    assert.same(p:stdin(), p:_toHandle(0))
    assert.same(p:stdout(), p:_toHandle(1))
    assert.same(p:stderr(), p:_toHandle(2))
    assert.match_error(function() p:_toHandle(99) end, 'No such fd: 99')
  end)


  ---@param proc stub.os.Proc
  local function show_refcount_n_opened(proc, h)
    local c, maxfd = proc:_fd_refcount(h)
    local ts = tostring
    return 'c:' .. ts(c) .. '|' .. ts(maxfd) .. '| ' .. proc:_opened_fds()
  end


  it("_fd_refcount + c_fclose", function()
    local p, f = newProc(), show_refcount_n_opened
    local p_stdout = p.fds[C.STDOUT] -- p.stdout()
    assert.is_not_nil(p_stdout) ---@cast p_stdout stub.os.IOHandle
    assert.same(true, p_stdout.open)

    -- stdin:0 stdout:1 stderr:2
    --        count | maxfd | opened
    assert.same('c:1|1| opened fd: 0 1 2', f(p, p:stdout()))
    --               ^ maxfd for given IOHandle in proc.fds
    --             ^ means then count of refs to ^ existed in proc.fds
    p.fds[3] = p_stdout
    assert.same('c:2|3| opened fd: 0 1 2 3', f(p, p:stdout()))

    p.fds[5] = p_stdout
    assert.same('c:3|5| opened fd: 0 1 2 3 5', f(p, p:stdout()))

    assert.same({ 1, 3, 5 }, p:_get_all_fd_for(p:stdout())) -- before close

    -- will close fd:1 keep p:stdout() opened update it fd to 5
    p:c_fclose(p_stdout)                                 -- close max fd

    assert.same(true, p_stdout.open)                     -- do not close stdio-streams

    assert.same({ 1, 3 }, p:_get_all_fd_for(p:stdout())) -- after close
    assert.same(nil, p.fds[5])                           -- after close

    assert.same('c:2|3| opened fd: 0 1 2 3', f(p, p_stdout))

    p:c_fclose(p_stdout)
    assert.same('c:1|1| opened fd: 0 1 2', f(p, p_stdout))

    p:c_fclose(p_stdout)
    assert.same('c:0|-1| opened fd: 0 2', f(p, p_stdout))
    assert.same(true, p_stdout.open) -- do not close stdio-streams
  end)


  -- dup fd, new_fd is a minimal available num in proc.fds(auto patchup holes)
  it("c_dup + _fd_refcount + c_close", function()
    local p, f = newProc(), show_refcount_n_opened
    --        count | maxfd | opened
    assert.same('c:1|1| opened fd: 0 1 2', f(p, p:stdout()))

    local fd3 = p:c_dup(C.STDOUT) -- dup(1)
    assert.same('c:2|3| opened fd: 0 1 2 3', f(p, p:stdout()))
    assert.same(3, fd3)

    local fd4 = p:c_dup(C.STDOUT)
    assert.same('c:3|4| opened fd: 0 1 2 3 4', f(p, p:stdout()))
    assert.same(4, fd4)

    local fd5 = p:c_dup(C.STDOUT)
    assert.same('c:4|5| opened fd: 0 1 2 3 4 5', f(p, p:stdout()))
    assert.same(5, fd5)


    assert.same(0, p:c_close(fd3))
    assert.same('c:3|5| opened fd: 0 1 2 4 5', f(p, p:stdout()))

    assert.same(-1, p:c_close(fd3)) -- try to close already closed
    assert.same('9|3: Bad file descriptor', p:_errno())
    assert.same('c:3|5| opened fd: 0 1 2 4 5', f(p, p:stdout()))

    p:c_close(fd4)
    assert.same('c:2|5| opened fd: 0 1 2 5', f(p, p:stdout()))
    local new_fd = p:c_dup(1)
    assert.same(3, new_fd)
    assert.same('c:3|5| opened fd: 0 1 2 3 5', f(p, p:stdout()))
  end)


  it("c_dup2 + c_fclose", function()
    local p, f = newProc(), show_refcount_n_opened
    local new_fd = p:c_dup2(C.STDOUT, 8) -- dup2(1, 8)
    assert.same('c:2|8| opened fd: 0 1 2 8', f(p, p:stdout()))
    assert.same(8, new_fd)
  end)


  it("_is_fd_stdio", function()
    local p = newProc()
    assert.same(true, p:_is_fd_stdio(C.STDIN))
    assert.same(true, p:_is_fd_stdio(C.STDOUT))
    assert.same(true, p:_is_fd_stdio(C.STDERR))
    p.fds[3] = IOHandle.of({}, 'r', IOHandle.TYPE.FILE)
    assert.same(false, p:_is_fd_stdio(3))
    assert.same(false, p:_is_fd_stdio(4))
  end)
end)

--------------------------------------------------------------------------------
--
--                Integration tests: Proc+OSystem+FileSystem
--
--------------------------------------------------------------------------------

describe("stub.os.Proc", function()
  local OS = require("stub.os.OSystem");
  local FS = require("stub.os.FileSystem")


  after_each(function()
    require 'dprint'.enable(false)
    sid.reset_all()
  end)


  it("_status", function()
    local sys = OS:new(nil, FS:new({ nostub = true }))
    local p = assert(sys.o.p_init, 'has intit process') ---@cast p stub.os.Proc
    p:c_pipe()
    local exp = "pid: 1 ppid: 0 uid:1000 (stub.os.Args) init\n" ..
        "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" ..
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        "&3:POR:0/0 id: 5  pipeid:1\n" ..
        "&4:POW:0/0 id: 4  pipeid:1\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp, p:_status(true))
  end)


  it("c_fork", function()
    local sys = OS:new() --nil, FS:new({ nostub = true }))
    local p = assert(sys.o.p_init, 'has intit process')
    local cp = p:c_fork()
    assert.is_table(cp)
    assert.same('2|1', tostring(cp.pid) .. '|' .. tostring(cp.ppid))
  end)


  -- redistribution of new file descriptors after streams are closed
  it("c_fopen + c_fclose + self.fds", function()
    local sys = OS:new()   --nil, FS:new({ nostub = true }))
    local p = sys.o.p_init --:new_proc({ main = false })
    assert.same('stub.os.Proc', class.base.getClassName(p))

    assert.same('opened fd: 0 1 2', Proc._opened_fds(p))

    local fn = '/tmp/file1'
    local h1 = p:c_fopen(fn, 'w')
    assert.is_table(h1) ---@cast h1 stub.os.IOHandle
    assert.same(3, p:_fh2fd(h1))
    assert.same('opened fd: 0 1 2 3', Proc._opened_fds(p))

    local h2 = p:c_fopen('/tmp/file2', 'w')
    local h3 = p:c_fopen('/tmp/file3', 'w')
    assert.same('opened fd: 0 1 2 3 4 5', Proc._opened_fds(p))

    assert.same(p.fds[3], h1)
    assert.same(p.fds[4], h2)
    assert.same(p.fds[5], h3)

    p:c_fclose(h1)
    p:c_fclose(h2)
    assert.is_nil(p.fds[4])
    assert.is_table(p.fds[5]) -- not close yet

    -- before open new, notice gap 2--5
    assert.same('opened fd: 0 1 2 5', p:_opened_fds())

    local h4 = p:c_fopen('/tmp/file4', 'w') -- next fd is 3 not a 6! correct
    assert.same('opened fd: 0 1 2 3 5', Proc._opened_fds(p))

    local h5 = p:c_fopen('/tmp/file5', 'w')
    local h6 = p:c_fopen('/tmp/file6', 'w')
    -- bypassed fd=5 without overwriting it
    assert.same('opened fd: 0 1 2 3 4 5 6', Proc._opened_fds(p))

    assert.same(p.fds[3], h4)
    assert.same(p.fds[4], h5)
    assert.same(p.fds[6], h6)
    p:c_fclose(h4); p:c_fclose(h5); p:c_fclose(h6)
  end)


  it("c_fread", function()
    local sys = OS:new(nil, FS:new({ nostub = false }))
    local p = sys.o.p_init
    local h = p:c_fopen('/etc/passwd')
    assert.is_table(h) ---@cast h stub.os.IOHandle
    assert.same(true, h.open)
    local buf = {}
    assert.same(2, p:c_fread(h, buf)) -- 2 lines read
    local exp = {
      'root:x:0:0:root:/root:/bin/bash',
      'user:x:1000:1000:User,,,:/home/user:/bin/bash'
    }
    assert.same(exp, buf)
    assert.same(false, h.open)
    p:c_fclose(h)
  end)


  it("redirect c_dup2 + c_fclose", function()
    local sys = OS:new() --nil, FS:new({ nostub = true }))
    local p = sys.o.p_init

    local exp1 = "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" ..
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n"
    --      ^ T - terminal - stdio-streams(std{in|out|err}
    assert.same(exp1, p:_opened_fds(true, true, true))

    local h, fd = p:c_fopen('/etc/passwd')
    assert.is_table(h) ---@cast h stub.os.IOHandle
    assert.same(3, fd)
    assert.same(p.fds[3], h)

    local stdin_bak = p:stdin()
    assert.same(p.fds[C.STDIN], stdin_bak)

    local new_fd = p:c_dup2(fd, 0) -- cmd < /etc/passwd + remove stdin
    assert.equal(p.fds[C.STDIN], h)

    local exp = "opened fd:\n" ..
        "&0:FOR:0/2 id: 4 /etc/passwd\n" ..
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        "&3:FOR:0/2 id: 4 /etc/passwd\n"
    assert.same(exp, p:_opened_fds(true, true, true))

    assert.same(0, new_fd)
    assert.same(nil, stdin_bak.fd) -- fd removed on close
  end)


  it("exit and cleanup opened files", function()
    local sys = OS:new() --nil, FS:new({ nostub = true }))
    local p = sys.o.p_init ---@cast p stub.os.Proc

    local h, fd = p:c_fopen('/tmp/file', 'w')
    assert.is_not_nil(h) ---@cast h stub.os.IOHandle
    assert.same(3, fd)
    assert.same(9, p:c_fprintf(h, "some\ndata"))
    assert.same(true, h.open)

    local th, tfd = p:c_tmpfile()
    assert.same({ '/tmp/tmp1_1' }, sys.o.tmpfiles)
    assert.same('/tmp/tmp1_1', IOHandle._get_file_name(th))
    assert.same(true, sys:is_file_exits('/tmp/tmp1_1'))
    assert.same(4, tfd)
    assert.same(true, th.open)

    local exp = "pid: 1 ppid: 0 uid:1000 (stub.os.Args) init\n" ..
        "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" ..
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        "&3:FOW:0/2 id: 4 /tmp/file\n" ..
        "&4:FORW:0/0 id: 5 /tmp/tmp1_1\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp, p:_status(true))

    p:exit(0)
    assert.same(false, h.open)
    assert.same(false, th.open)
    assert.same(false, sys:is_file_exits('/tmp/tmp1_1')) -- autoremoved via event

    local exp2 = "pid: 1 ppid: 0 uid:1000 (stub.os.Args) init\n" ..
        "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" ..
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        " errno: 0(nil) exitcode:0\n"
    assert.same(exp2, p:_status(true))
    assert.same('&?:FCW:0/2 id: 4 /tmp/file', IOHandle._status(h))
    assert.same('&?:FCRW:0/0 id: 5 tmp1_1', IOHandle._status(th))
  end)


  it("c_getcwd", function()
    local sys = OS:new()
    local p = sys.o.p_init ---@cast p stub.os.Proc
    assert.same('/', p:c_getcwd())
  end)


  it("chdir", function()
    local sys = OS:new()
    local p = sys.o.p_init ---@cast p stub.os.Proc
    local f = function(path)
      local ret = p:c_chdir(path)
      local cwd = p:c_getcwd()
      return tostring(ret) .. '|' .. tostring(cwd)
    end
    assert.same("/", p:c_getcwd()) -- pwd

    assert.same(0, p:c_chdir('tmp'))
    assert.same("/tmp", p:c_getcwd())

    assert.same('0|/', f('../'))
    assert.same('0|/var', f('var'))
    assert.same('0|/var/log', f('log'))
    assert.same('0|/tmp', f('../../tmp'))
    assert.same('0|/usr', f('/usr'))

    assert.same('/', sys.fs:pwd()) -- not affected by proc
    assert.same(true, sys.fs:cd('/var/log'))
    assert.same('/var/log', sys.fs:pwd())

    assert.same('/usr', p:c_getcwd()) -- not affected by FileSystem
    local h = p:c_fopen('../bin/bash', 'r')
    assert.is_table(h) ---@cast h stub.os.IOHandle
    h:close()

    local h2 = p:c_fopen('/bin/bash', 'r')
    assert.is_table(h2) ---@cast h2 stub.os.IOHandle
    assert.same(true, h2:_is_file())
    assert.same(true, h2:close())
  end)
end)
