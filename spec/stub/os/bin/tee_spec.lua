-- 13-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local OS = require("stub.os.OSystem");
local SH = require("stub.os.bin.sh");
-- local M = require("stub.os.bin.tee");

describe("stub.os.bin.tee", function()
  it("tee in pipe", function()
    local sys = OS:new()

    assert.same(nil, sys:get_file_lines('/tmp/f2'))

    local proc = SH.run_cmdline(sys, 'pwd | tee /tmp/f2')

    assert.is_table(proc)
    assert.same({ '/home/user' }, proc:stdout().lines)
    assert.same({ '/home/user' }, sys:get_file_lines('/tmp/f2'))

    proc = SH.run_cmdline(sys, 'pwd | tee -a /tmp/f2')
    assert.same({ '/home/user', '/home/user' }, sys:get_file_lines('/tmp/f2'))
  end)

end)
