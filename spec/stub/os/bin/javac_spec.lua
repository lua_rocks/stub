-- 20-07-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local OS = require("stub.os.OSystem");
local SH = require("stub.os.bin.sh");
local M = require 'stub.os.bin.javac'


local run = SH.run_cmdline

-- require 'dprint'.enable(true)


local class_body_org_Main = [[
package org.company;

/**
 * @author AuthorName
 */
public class Main {
}
]]

describe("stub.os.bin.javac", function()
  local sys ---@cast sys stub.os.OSystem
  before_each(function()
    sys = OS:new() -- sys:clear_state()
  end)

  it("match", function()
    local p = "^%s*package%s+([%w_%.]+)%s*;?%s*"
    local line = "package pkg;"
    local f = function(s)
      return string.match(s, p)
    end
    assert.same('pkg', f(line))
  end)


  it("parse_java_source", function()
    local dir = '/tmp/dev/'
    local proc = sys.o.p_init:c_fork({
      cwd = dir,
      errno = 0,
      ug = sys.o.p_init.ug,
    })
    local body = class_body_org_Main
    local fn = dir .. '/pkg/Main.java'
    assert.same(true, sys:set_file_content(fn, body))

    local exp = {
      pkg = { lnum = 1, line = 'package org.company;', value = 'org.company' },
      classname = { line = 'public class Main {', lnum = 6, value = 'Main' }
    }
    assert.same(exp, M.parse_java_source(proc, fn))
  end)

  it("mk_output_path", function()
    local f = M.mk_output_path
    local p = { output_dir = 'build' }
    local t = { pkg = { value = 'org.comp' }, classname = 'MainApp' }
    local exp = { 'build/org/comp', 'MainApp.class' }
    assert.same(exp, { f(p, t, '/tmp/src/org/comp/MainApp.java') })
  end)


  it("javac help", function()
    local proc = run(sys, 'javac')
    assert.is_table(proc)

    local exp = {
      'Usage: javac <options> <source files>',
      'where possible options include:',
      '  @<filename>                  Read options and filenames from file',
      '  -Akey[=value]                Options to pass to annotation processors',
      '  -d <directory>               Specify where to place generated class files',
      '  -g                           Generate all debugging info'
    }
    assert.same(exp, proc:stderr().lines)
  end)


  it("javac -g (no sources)", function()
    local proc = run(sys, 'javac -g')

    assert.is_table(proc)
    local exp = { 'error: no source files' }
    assert.same(exp, proc:stderr().lines)
  end)


  it("javac not existed file", function()
    local proc = run(sys, 'javac -g /tmp/Main.java')
    assert.is_table(proc)
    local exp = {
      'error: file not found: /tmp/Main.java',
      'Usage: javac <options> <source files>',
      'use --help for a list of possible options'
    }
    assert.same(exp, proc:stderr().lines)
  end)


  it("javac classname and file name do not match", function()
    local body = "public class A {\n}\n"
    assert.same(true, sys:set_file_content('/tmp/Main.java', body))

    local proc = run(sys, 'javac -g /tmp/Main.java')
    assert.is_table(proc)

    local exp = {
      '/tmp/Main.java:1: error: class A is public, should be declared in a file named A.java',
      'public class A {',
      '^',
      '1 error'
    }
    assert.same(exp, proc:stderr().lines)
  end)


  it("javac success one source file", function()
    local body = "public class Main {\n}\n"

    assert.same(false, sys:is_file_exits('/tmp/Main.class', 'file'))
    assert.same(true, sys:set_file_content('/tmp/Main.java', body))

    local proc = run(sys, 'javac -g /tmp/Main.java')
    assert.is_table(proc)
    assert.same({ {}, 0 }, { proc:stderr().lines, proc.exitcode }) -- no erros

    assert.same(true, sys:is_file_exits('/tmp/Main.class', 'file'))
    local exp = 'fecabeba00003d000d00000a000207030400000c'
    assert.same(exp, sys:get_hexdump('/tmp/Main.class', 1, 20))
  end)


  it("javac success two sources", function()
    local main = "public class Main {\n}\n"
    local app = "public class App {\n}\n"

    assert.same(true, sys:set_file_content('/tmp/Main.java', main))
    assert.same(true, sys:set_file_content('/tmp/App.java', app))

    local proc = run(sys, 'javac -g /tmp/Main.java /tmp/App.java')
    assert.is_table(proc)
    assert.same({ {}, 0 }, { proc:stderr().lines, proc.exitcode }) -- no erros

    assert.same(true, sys:is_file_exits('/tmp/Main.class', 'file'))
    assert.same(true, sys:is_file_exits('/tmp/App.class', 'file'))
  end)


  it("javac success two with output_dir full paths", function()
    local main = "public class Main {\n}\n"
    local app = "public class App {\n}\n"

    assert.same(true, sys:set_file_content('/tmp/Main.java', main))
    assert.same(true, sys:set_file_content('/tmp/App.java', app))

    local proc = run(sys, 'javac -d /tmp/build/ /tmp/Main.java /tmp/App.java')
    assert.is_table(proc)
    assert.same({ {}, 0 }, { proc:stderr().lines, proc.exitcode }) -- no erros

    local exp = {
      '/tmp/',
      '    App.java',
      '    build/',
      '        App.class',
      '        Main.class',
      '    Main.java',
      '1 directory, 4 files'
    }
    assert.same(exp, sys:treel('/tmp/'))
    assert.same(true, sys:is_file_exits('/tmp/build/Main.class', 'file'))
    assert.same(true, sys:is_file_exits('/tmp/build/App.class', 'file'))
  end)



  it("javac success two with output_dir cwd", function()
    local main = "public class Main {\n}\n"
    local app = "public class App {\n}\n"
    local dir = '/home/user/proj/'

    assert.same(true, sys:cd(dir))
    assert.same('', sys:ls())
    assert.same(true, sys:set_file_content('./Main.java', main))
    assert.same(true, sys:set_file_content('./App.java', app))
    assert.same('App.java Main.java', sys:ls())
    assert.same('/home/user/proj', sys:pwd())

    local proc = run(sys, 'javac -d ./build/classes Main.java App.java')
    assert.is_table(proc)
    assert.same({ {}, 0 }, { proc:stderr().lines, proc.exitcode }) -- no erros

    local exp = {
      '/home/user/proj/',
      '    App.java',
      '    build/',
      '        classes/',
      '            App.class',
      '            Main.class',
      '    Main.java',
      '2 directories, 4 files'
    }
    assert.same(exp, sys:treel(dir))
    assert.same(true, sys:is_file_exits(dir .. 'build/classes/Main.class', 'file'))
    assert.same(true, sys:is_file_exits(dir .. 'build/classes/App.class', 'file'))
  end)

  it("javac success output_dir cwd pkg ~= place on disk", function()
    local main = "package org.ns;\npublic class Main {\n}\n"
    local app = "package org.ns;\npublic class App {\n}\n"
    local dir = '/home/user/proj/'

    assert.same(true, sys:cd(dir))
    assert.same('', sys:ls())
    assert.same(true, sys:set_file_content('./src/Main.java', main))
    assert.same(true, sys:set_file_content('./src/App.java', app))
    assert.same('App.java Main.java', sys:ls('./src'))
    local exp = {
      '/home/user/proj',
      '    src/',
      '        App.java',
      '        Main.java',
      '1 directory, 2 files'
    }
    assert.same(exp, sys:treel('.'))

    local proc = run(sys, 'javac -d ./build/  ./src/Main.java ./src/App.java')

    assert.is_table(proc)
    assert.same({ {}, 0 }, { proc:stderr().lines, proc.exitcode }) -- no erros

    local exp2 = {
      '/home/user/proj/',
      '    build/',
      '        org/',
      '            ns/',
      '                App.class',
      '                Main.class',
      '    src/',
      '        App.java',
      '        Main.java',
      '4 directories, 4 files'
    }
    assert.same(exp2, sys:treel(dir))
    assert.same(true, sys:is_file_exits(dir .. 'build/org/ns/Main.class', 'file'))
    assert.same(true, sys:is_file_exits(dir .. '/build/org/ns/App.class', 'file'))
  end)


    -- assert.same({}, javac("-sourcepath src -d build/classes src/org/ns/Main.java"))
end)
