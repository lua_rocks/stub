-- 19-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local OS = require("stub.os.OSystem");
local SH = require("stub.os.bin.sh");

-- require 'dprint'.enable(true)

describe("stub.os.bin.tee", function()
  it("touch create new file", function()
    local sys = OS:new()

    assert.same(nil, sys:get_file_lines('/tmp/f2'))

    local proc = SH.run_cmdline(sys, 'touch /tmp/f2')

    assert.is_table(proc)
    assert.same({}, proc:stdout().lines)
    assert.is_true(sys:is_file_exits('/tmp/f2'))
  end)


  it("touch attempt to touch non-existed with --no-create", function()
    local sys = OS:new()

    assert.same(nil, sys:get_file_lines('/tmp/f2'))

    local proc = SH.run_cmdline(sys, 'touch --no-create /tmp/f2')

    assert.same(nil, sys:get_file_lines('/tmp/f2'))

    assert.is_table(proc)
    assert.same({}, proc:stdout().lines)

    assert.is_false(sys:is_file_exits('/tmp/f2'))
  end)


  it("touch create modify lmtime in already existed file", function()
    local sys = OS:new()

    assert.same(nil, sys:get_file_lines('/tmp/f2'))

    local proc = SH.run_cmdline(sys, 'touch /tmp/f2')

    assert.same({}, sys:get_file_lines('/tmp/f2'))

    assert.is_table(proc)
    assert.same({}, proc:stdout().lines)

    assert.is_true(sys:is_file_exits('/tmp/f2'))
  end)
end)
