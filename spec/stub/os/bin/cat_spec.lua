-- 04-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local M = require("stub.os.bin.cat");
local OS = require("stub.os.OSystem");
local H = require("spec.stub.os.fs_helper")
local Args = require("stub.os.Args");
local Proc = require("stub.os.Proc");
local IOPipe = require("stub.os.IOPipe");
local IOHandle = require("stub.os.IOHandle");
local sid = require("stub.id")

local D = require 'dprint'


describe("stub.os.bin.cat", function()
  local sys
  after_each(function()
    require 'dprint'.enable(false)
    if sys and sys.fs then
      sys.fs:restore_original()
    end
    sid.reset_all()
  end)

  -- helper for asserts
  ---@return stub.os.Proc
  local function cat(sys0, cmdline)
    return M.run(Proc.of(sys0, Args.of(cmdline)))
  end

  --

  it("simple read `cat fn` success", function()
    sys = OS:new()
    local cmd = 'cat /etc/passwd'
    local exp = {
      'root:x:0:0:root:/root:/bin/bash',
      'user:x:1000:1000:User,,,:/home/user:/bin/bash'
    }
    assert.same(exp, cat(sys, cmd):stdout().lines)
  end)


  it("simple read `cat fn fn` success", function()
    sys = OS:new()
    local cmd = 'cat /etc/passwd /etc/passwd'
    local exp = {
      'root:x:0:0:root:/root:/bin/bash',
      'user:x:1000:1000:User,,,:/home/user:/bin/bash',
      'root:x:0:0:root:/root:/bin/bash',
      'user:x:1000:1000:User,,,:/home/user:/bin/bash'
    }
    assert.same(exp, cat(sys, cmd):stdout().lines)
  end)


  it("simple read `cat fn fn` success & fail", function()
    sys = OS:new()
    local cmd = 'cat /etc/passwd /etc/not-exists'
    local exp = {
      'root:x:0:0:root:/root:/bin/bash',
      'user:x:1000:1000:User,,,:/home/user:/bin/bash'
    }
    assert.same(exp, cat(sys, cmd):stdout().lines)
    local exp2 = { 'cat: /etc/not-exists: No such file or directory' }
    assert.same(exp2, cat(sys, cmd):stderr().lines)
  end)


  it("simple read `cat fn - fn` success", function()
    sys = OS:new()
    local cmd = 'cat /etc/passwd - /etc/passwd'
    local proc = Proc.of(sys, Args.of(cmd))

    local pipe = IOPipe:new({
      read = proc:stdin(),
      write = IOHandle.of({ 'this text will be send to input' }, 'w')
    })
    -- emulate writin to the pipe
    pipe.write:write('another one line to the input of the cat')

    local exp = {
      'root:x:0:0:root:/root:/bin/bash',
      'user:x:1000:1000:User,,,:/home/user:/bin/bash',
      'this text will be send to input',
      'another one line to the input of the cat',
      'root:x:0:0:root:/root:/bin/bash',
      'user:x:1000:1000:User,,,:/home/user:/bin/bash'
    }
    assert.same(exp, M.run(proc):stdout().lines)
    assert.same(0, proc.exitcode)
  end)


  it("simple read `cat fn` fail", function()
    sys = OS:new()
    local cmd = 'cat /etc/not-existed'
    local exp = { 'cat: /etc/not-existed: No such file or directory' }
    assert.same(exp, cat(sys, cmd):stderr().lines)
  end)
end)

--------------------------------------------------------------------------------
--
--               Integration tests: cat+sh+OSystem+FileSystem
--
--------------------------------------------------------------------------------
--
describe("stub.os.bin.cat", function()
  local SH = require("stub.os.bin.sh");
  local sys

  ---@param sys0 stub.os.OSystem
  ---@param cmdline string
  ---@param ug stub.os.UG?
  ---@return stub.os.Proc
  local function shell(sys0, cmdline, ug)
    return SH.run_cmdline(sys0, cmdline, ug)
  end

  after_each(function()
    D.enable(false)
    if sys and sys.fs then sys.fs:restore_original() end
    IOHandle.bus._cleanup()
    sid.reset_all()
  end)


  it("simple write `cat src > dst` success", function()
    sys = OS:new()
    assert.same(false, sys:is_file_exits('/tmp/passwd'))

    -- file prepared and opened by shell for child process(cat)
    -- but the cat-program itself for this process has not yet worked
    local p = shell(sys, 'cat /etc/passwd > /tmp/passwd')
    assert.same(true, sys:is_file_exits('/tmp/passwd'))
    local exp = {
      'root:x:0:0:root:/root:/bin/bash',
      'user:x:1000:1000:User,,,:/home/user:/bin/bash'
    }
    assert.same(exp, sys:get_file_lines('/tmp/passwd'))
    assert.same({}, p:stdout().lines) -- todo childs
  end)


  it("simple write `cat src srcN > dst` success", function()
    sys = OS:new()
    local p = shell(sys, 'cat /etc/passwd /etc/hosts > /tmp/output')

    assert.same({}, p:stdout().lines)
    assert.same(true, sys:is_file_exits('/tmp/output'))
    local exp = {
      'root:x:0:0:root:/root:/bin/bash',
      'user:x:1000:1000:User,,,:/home/user:/bin/bash',
      '127.0.0.1  localhost'
    }
    assert.same(exp, sys:get_file_lines('/tmp/output'))
  end)


  it("simple write `cat src > dst` fail", function()
    sys = OS:new()
    local p = shell(sys, 'cat /etc/passwd > /tmp/ex/a2')
    -- todo no such file or directory (/tmp/ex
    local exp = {
      'sh: 1: cannot create /tmp/ex/a2: No such file or directory'
    }
    assert.same(exp, p:stderr().lines)
  end)


  --
  -- write content to file via cat > file << content
  it("handle_sys_cmd cat:write `cat > fn << INPUT` fail", function()
    sys = OS:new()
    local fn, dir = '/etc/custom_app/conf', '/etc/custom_app/'
    local cmd = "cat > /etc/custom_app/conf << EOF\n" ..
        "line 1\n" ..
        "line 2\n" ..
        "EOF\n"
    assert.is_false(sys:is_file_exits(fn))
    assert.is_false(sys:is_file_exits(dir)) -- ! reason for the failure

    local exp = {
      'sh: 1: cannot create /etc/custom_app/conf: No such file or directory'
    }
    assert.same(exp, shell(sys, cmd):stderr().lines)
  end)


  -- write content to file via cat > file << content
  it("handle_sys_cmd cat:write `cat > fn << INPUT` success", function()
    sys = OS:new()
    local fn, dir = '/etc/custom_app/conf.json', '/etc/custom_app/'
    local cmd = "cat > /etc/custom_app/conf.json << EOF\n" ..
        "{\n" ..
        '  "key-1": ["a b", "c"],' .. "\n" ..
        '  "debug": false' .. "\n" ..
        "}\n" ..
        "EOF"
    assert.is_false(sys:is_file_exits(fn))
    assert.is_false(sys:is_file_exits(dir))
    assert.same(nil, sys:get_file_lines(fn))

    -- prepare
    sys.fs:mk_dir(dir)                     -- mkdir /etc/custom_app
    assert.is_true(sys:is_file_exits(dir)) -- ok
    assert.is_false(sys:is_file_exits(fn))

    -- run cmd - the empty output is ok - no errors from cat
    assert.same({}, shell(sys, cmd):stdout().lines)
    assert.is_true(sys:is_file_exits(fn))
    local exp_content = {
      '{',
      '  "key-1": ["a b", "c"],',
      '  "debug": false',
      '}'
    }
    assert.same(exp_content, sys:get_file_lines(fn))

    local f = sys.fs:get_file(fn)
    assert.is_not_nil(f) ---@cast f stub.os.FileStub - file was created
    assert.same(1000, f.fs_stat.uid) -- user
    assert.same(1000, f.fs_stat.gid)
    assert.same(exp_content, f.lines)
  end)


  it("handle_sys_cmd cat:write `cat > fn << INPUT` success", function()
    sys = OS:new()
    local cmd = "cat > /tmp/file << EOF\n line1\n line2\nEOF"
    assert.same('', H.execr(cmd)) -- empty output all reditect to the file
    local exp = { ' line1', ' line2' }
    assert.same(exp, sys:get_file_lines('/tmp/file'))
  end)
end)
