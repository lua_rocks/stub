-- 05-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local class = require("oop.class")
local OS = require("stub.os.OSystem");
local FS = require("stub.os.FileSystem");
local UG = require("stub.os.UG")
local C = require("stub.os.consts")
local Args = require("stub.os.Args");
local IOHandle = require("stub.os.IOHandle");
local FileStub = require("stub.os.FileStub")
local Proc = require("stub.os.Proc");
local sid = require("stub.id")

local D = require 'dprint'
local M = require("stub.os.bin.sh");
local E = {}

local sys

--
--  mock without full stub.os.OSystem and stub.os.FileSystem
--  pshell = assert(_os, 'os').o.p_init:c_fork({ ... })
--
---@return stub.os.Proc
---@param t table? {fs -- emulated filesystem(files)}
local function mk_shell(cmd, t)
  local a = Args.of(cmd)
  t = t or {}
  -- build minimalistic stub of stub.os.OSystem + stub.os.FileSystem
  local _os = t._os or { o = {}, fs = { o = {} } }
  FS.bind_std_iostreams(_os.fs) -- _os.fs.io = {stdin = ... stdout...}
  _os.fs.bind_os = FS.bind_os
  OS.bind_fs(_os, _os.fs)
  local next_pid = 7

  -- stubs:
  _os.next_pid = function()
    next_pid = next_pid + 1
    return next_pid
  end
  local root = FileStub.new_dir(nil, '/', nil, UG.root())

  _os.c_fopen = function(_, proc, fn, mode)
    local content = (t.fs or E)[fn]
    if content then
      local f = FileStub.new_file(root, FS.basename(fn), content)
      return proc:bind(IOHandle:new(nil, f, mode)) --, {}))
    else
      proc:set_errno(C.ERRNO.ENOENT, "%s: %s", fn) -- no such file or dir
      return nil, nil
    end
  end

  _os.c_tmpfile = function(_, proc)
    assert(type(proc) == 'table', 'proc')
    local f = FileStub.new_file(root, 'tmpfile', {})
    return proc:bind(IOHandle:new(nil, f, 'wb+'))
  end

  local _io = _os.fs.io

  local proc = Proc:new({
    ppid = 1,
    pid = _os.next_pid(),
    os = _os,
    fds = {
      [C.STDIN] = _io.stdin, [C.STDOUT] = _io.stdout, [C.STDERR] = _io.stderr,
    },
    ug = t.ug or UG.default(),
    args = a,
    envs = t.envs or _os.o.envs,
    cwd = '/home/user', -- _os.get_user_home(_os, (ug or E).uid)
    exitcode = nil,     -- alive
  })

  return proc
end


describe("stub.os.bin.sh", function()
  after_each(function()
    D.enable(false)
    sid.reset_all()
  end)

  it("split_by_pipes", function()
    --                  1  2  3  4  5  6
    local a = Args.of('cat f1 f2 > f3 | wc -l | cat f3 | cat')
    local exp = {
      { ai = 1, args = { 'cat', 'f1', 'f2', '>', 'f3' } },
      { ai = 1, args = { 'wc', '-l' } },
      { ai = 1, args = { 'cat', 'f3' } },
      { ai = 1, args = { 'cat' } }
    }
    assert.same(exp, M.split_by_pipes(a))
  end)

  it("split_by_pipes with envvars", function()
    --                  1  2  3  4  5  6
    local a = Args.of('V=A cat f1 > f3 | V=B wc -l | V=C cat f3')
    local exp = {
      { args = { 'V=A', 'cat', 'f1', '>', 'f3' }, ai = 1 },
      { args = { 'V=B', 'wc', '-l' },             ai = 1 },
      { args = { 'V=C', 'cat', 'f3' },            ai = 1 }
    }
    assert.same(exp, M.split_by_pipes(a))
  end)


  it("apply_effect cwd", function()
    local _os = {}
    local proc = Proc.of(_os, Args.of(''))
    assert.same('/', proc.cwd)
    local effect = { cwd = '/tmp' }
    local list = { proc }
    assert.same('/tmp', M.apply_effect(effect, list, 0)[1].cwd)
  end)


  it("mk_shell", function()
    local p = mk_shell('cat f1 f2')
    assert.is_table(p)
    assert.no_error(function() IOHandle.cast(p:stdin()) end)
    assert.no_error(function() IOHandle.cast(p:stdout()) end)
    assert.no_error(function() IOHandle.cast(p:stderr()) end)
    assert.same(8, p.pid)
    assert.same(1, p.ppid)
  end)


  it("setup childs single cmd same stdio streams", function()
    local pshell = mk_shell('cat f1 f2')
    local procs = M.setup_childs(pshell)
    assert.is_table(procs)
    assert.same(1, #procs)
    local cat = procs[1] -- first child
    assert.same({ 'cat', 'f1', 'f2' }, cat.args.args)

    -- shell:i|o|e         |e|o|i  <- child proc(cat)
    --       | ^ ^----------` | ^
    --       |  `-------------' |
    --       '------------------'
    --
    assert.equal(cat:stdin(), pshell:stdin()) -- shared with child and parent
    assert.equal(cat:stdout(), pshell:stdout())
    assert.equal(cat:stderr(), pshell:stderr())
  end)


  it("setup childs single cmd redirect stdin", function()
    local pshell = mk_shell('cat < f1', { fs = { f1 = { 'content-of-f1' } } })
    local procs = M.setup_childs(pshell)
    assert.is_table(procs)
    assert.same(1, #procs)
    local cat = procs[1]
    assert.same({ 'cat' }, cat.args.args) -- removed < f1

    -- shell:i|o|e   proc: |e|o|i
    --         ^ ^----------` | ^
    --          `-------------'  \
    --                             ---- f1
    --
    assert.are_not_same(cat:stdin(), pshell:stdin())
    assert.equal(cat:stdout(), pshell:stdout())
    assert.equal(cat:stderr(), pshell:stderr())

    assert.same({ 'content-of-f1' }, cat:stdin().lines)
  end)


  it("setup childs single cmd redirect stdin fail no such file", function()
    local pshell = mk_shell('cat < f1', {})

    local procs = M.setup_childs(pshell)
    assert.is_table(procs)
    assert.same(1, #procs)
    local cat = procs[1]
    assert.same({ 'cat' }, cat.args.args) -- remove < f1

    assert.same('2|f1: No such file or directory', pshell:_errno())
    assert.same('2|f1: No such file or directory', cat:_errno())

    assert.equal(cat:stdin(), pshell:stdin()) -- did not change
    assert.equal(cat:stdout(), pshell:stdout())
    assert.equal(cat:stderr(), pshell:stderr())
  end)


  it("setup childs single cmd redirect stdout", function()
    local pshell = mk_shell('cat > f1', { fs = { f1 = { 'content-of-f1' } } })
    local procs = M.setup_childs(pshell)
    assert.same(1, #(procs or E))
    local cat = procs[1]
    assert.same({ 'cat' }, cat.args.args) -- removed > f1

    assert.equal(cat:stdin(), pshell:stdin())
    assert.are_not_same(cat:stdout(), pshell:stdout())
    assert.equal(cat:stderr(), pshell:stderr())

    assert.same({}, cat:stdout().lines) -- erased after open to write
  end)


  --
  it("build_procs_with_pipes multiple", function()
    local pshell = mk_shell('cat f1 f2 | wc -l | cat f3 | cat')

    local pa = M.setup_childs(pshell) -- array of child processes
    assert.is_table(pa)
    assert.same(4, #pa)

    assert.same({ 'cat', 'f1', 'f2', }, pa[1].args.args)
    assert.same({ 'wc', '-l' }, pa[2].args.args)
    assert.same({ 'cat', 'f3' }, pa[3].args.args)
    assert.same({ 'cat' }, pa[4].args.args)

    -- ensure the pipes are built properly
    assert.equal(pshell:stdin(), pa[1]:stdin())   -- first in pipe
    assert.equal(pshell:stdout(), pa[4]:stdout()) -- last in pipe

    -- enshure connected via pipes (under the hood implemented via same lines
    assert.equal(pa[1]:stdout().lines, pa[2]:stdin().lines)
    assert.equal(pa[2]:stdout().lines, pa[3]:stdin().lines)
    assert.equal(pa[3]:stdout().lines, pa[4]:stdin().lines)
    assert.equal(pa[4]:stdout(), pshell:stdout())

    assert.is_table(pa[1]:stdin().lines)
    assert.is_table(pa[2]:stdin().lines)
    assert.is_table(pa[3]:stdin().lines)
    assert.is_table(pa[4]:stdin().lines)
    local exp = "pid: 9 ppid: 8 uid:1000 (stub.os.Args) cat f1 f2\n" ..
        "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" ..
        "&1:POW:0/0 id: 4  pipeid:1\n" ..
        "&2:TOW:0/0 id: 3\n" .. -- stderr
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp, pa[1]:_status(true))

    local exp2 = "pid: 10 ppid: 8 uid:1000 (stub.os.Args) wc -l\n" ..
        "opened fd:\n" ..
        "&0:POR:0/0 id: 5  pipeid:1\n" ..
        "&1:POW:0/0 id: 6  pipeid:2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp2, pa[2]:_status(true))
  end)


  it("build_procs_with_pipes multiple + envvars", function()
    local pshell = mk_shell('V=A cat f1 | B=2 C=3 wc -l | D=4 cat f3 | cat')
    local procs = M.setup_childs(pshell)

    assert.is_table(procs)
    assert.same(4, #procs)
    assert.same({ 'cat', 'f1' }, procs[1].args.args)
    assert.same({ V = 'A' }, procs[1].envs)

    assert.same({ 'wc', '-l' }, procs[2].args.args)
    assert.same({ C = '3', B = '2' }, procs[2].envs)

    assert.same({ 'cat', 'f3' }, procs[3].args.args)
    assert.same({ D = '4' }, procs[3].envs)
    assert.same({ 'cat' }, procs[4].args.args)

    -- ensure the pipes are built properly
    assert.equal(pshell:stdin(), procs[1]:stdin())
    assert.equal(pshell:stdout(), procs[4]:stdout())
    -- proc_1 out -->  proc2_in
    assert.equal(procs[1]:stdout().lines, procs[2]:stdin().lines)
    assert.equal(procs[2]:stdout().lines, procs[3]:stdin().lines)
    assert.equal(procs[3]:stdout().lines, procs[4]:stdin().lines)

    assert.is_table(procs[1]:stdin().lines)
    assert.is_table(procs[2]:stdin().lines)
    assert.is_table(procs[3]:stdin().lines)
    assert.is_table(procs[4]:stdin().lines)
  end)

  -- builtins

  it("cmd_echo", function()
    local _os, args = {}, Args:new(nil, 'echo a  "b  c"  de')
    local proc = Proc.of(_os, args)

    assert.same('a b  c de', M.cmd_echo(proc))
    -- sure write to stdout
    assert.same({ 'a b  c de' }, proc:stdout().lines)
  end)


  it("cmd_builtin_show_envvar", function()
    local _os, args = {}, Args:new(nil, 'show_envvar vn')
    local proc = Proc.of(_os, args)
    assert.same({ "vn: ''" }, M.cmd_builtin_show_envvar(proc):stdout().lines)
  end)


  it("pull_envvars", function()
    local args = Args:new(nil, 'KEY=VALUE B=2 show_envvar KEY B')
    local exp = { 'KEY=VALUE', 'B=2', 'show_envvar', 'KEY', 'B' }
    assert.same(exp, args.args)
    assert.same('KEY=VALUE', args:arg()) -- before pop_envvars

    assert.same({ KEY = 'VALUE', B = '2' }, M.pull_envvars(args, {}))
    -- ensure envvars is taken and replaced from args
    assert.same('show_envvar', args:arg())
    assert.same('KEY', args:arg(1))
    assert.same('B', args:arg(2))
    assert.same(nil, args:arg(-1)) -- removed
    assert.same(nil, args:arg(-2))
  end)


  it("merge_keys", function()
    local f = M.merge_keys
    assert.same({ key = 16 }, f({}, { key = 16 }))
    assert.same({ key = 16 }, f({}, { key = 16 }))
    assert.same({ b = 3, c = 8, a = 1 }, f({ a = 3, c = 8 }, { a = 1, b = 3 }))
  end)


  it("match `lhs op rhs`", function()
    local function f(s)
      local lhs, op, rhs = string.match(s, '([^%s><]*)([><]+)([^s]*)')
      local ts = tostring
      return ts(lhs) .. '|' .. ts(op) .. '|' .. ts(rhs)
    end
    assert.same('|>|/dev/null', f(">/dev/null"))
    assert.same('|<| <(cmd a)', f("< <(cmd a)")) --?
    assert.same('2|>|&1', f("2>&1"))
    assert.same('2|>|&1-', f("2>&1-"))
    assert.same('2|>|', f("2>"))
    assert.same('|>|', f(">"))
    assert.same('|>>|', f(">>"))
    assert.same('|>>|file', f(">>file"))
    assert.same('a|>>|b', f("a>>b"))
    assert.same('a|<<|b', f("a<<b"))
    assert.same('a|<<<|b', f("a<<<b"))

    --[[
    TODO
    assert.same({ 'cat', '<', '<(cmd', 'a)' }, f('cat < <(cmd a)'))
    assert.same({ 'cat', '2>&1' }, f('cat 2>&1')) -- dublication
    assert.same({ 'cat', '2>&1-' }, f('cat 2>&1-'))
    assert.same({ 'cat', '2>', 'file' }, f('cat 2> file')) -- stderr to file
    -- 2>/dev/null
    -- `3>&2`, `2>&1` or `1>&3-`
    ]]
  end)


  it("mk_io_redirects error: bad syntax no rhs", function()
    local pshell = mk_shell('cat >')

    local childs = M.setup_childs(pshell)
    local c1 = childs[1]
    assert.same(2, c1.exitcode)
    assert.same('0|nil', c1:_errno())
    local exp = { 'sh: 1: Syntax error: end of file unexpected' }
    assert.same(exp, pshell:stderr().lines)
    assert.same(nil, pshell.exitcode) -- finished
  end)


  --
  -- echo 123 > /tmp/f1 | cat  --> give "" and 123 in file /tmp/f1
  --
  -- echo 456 | echo 123 > /tmp/f1 | cat  --> give "" and 123 in file /tmp/f1
  --
  it("reditect to file(write) + pipe", function()
    -- emulate the presence of a file /tmp/f1 in the system for mk_shell stub
    local t = { fs = { ['/tmp/f1'] = {} } }
    local pshell = mk_shell("echo abc > /tmp/f1 | cat", t)
    assert.same({ 'echo', 'abc', '>', '/tmp/f1', '|', 'cat' }, pshell.args.args)

    local childs = M.setup_childs(pshell)
    assert.same(2, #(childs or E))
    local exp = "pid: 9 ppid: 8 uid:1000 (stub.os.Args) echo abc\n" .. "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" ..
        "&1:FOW:0/0 id: 6 /f1\n" .. -- pipe will overrided by redirect to file
        "&2:TOW:0/0 id: 3\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp, Proc._status(childs[1], true))

    local exp2 = "pid: 10 ppid: 8 uid:1000 (stub.os.Args) cat\n" .. "opened fd:\n" ..
        "&0:POR:0/0 id: 5  pipeid:1\n" .. -- from nowhere
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp2, Proc._status(childs[2], true))
  end)


  it("reditect to file(read) + pipe", function()
    -- emulate the presence of a file /tmp/f1 in the system for mk_shell stub
    local t = { fs = { ['/tmp/f1'] = {} } }
    local pshell = mk_shell("echo abc | cat <<< def", t)
    assert.same({ 'echo', 'abc', '|', 'cat', '<<<', 'def' }, pshell.args.args)

    local childs = M.setup_childs(pshell)
    assert.same(2, #(childs or E))
    local exp = "pid: 9 ppid: 8 uid:1000 (stub.os.Args) echo abc\n" .. "opened fd:\n" ..
        "&0:TOR:0/0 id: 1\n" ..
        "&1:POW:0/0 id: 4  pipeid:1\n" .. -- going nowhere
        "&2:TOW:0/0 id: 3\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp, Proc._status(childs[1], true))

    local exp2 = "pid: 10 ppid: 8 uid:1000 (stub.os.Args) cat\n" .. "opened fd:\n" ..
        "&0:FORW:0/1 id: 6 /tmpfile\n" ..
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp2, Proc._status(childs[2], true))
    assert.same({}, pshell:stdout().lines)
    assert.same({}, childs[1]:stdout().lines)
    assert.same({}, childs[2]:stdout().lines)
  end)


  --[[
    case with redirect stdout to file

    cat f1 | wc
          1       1      11
    cat f1 > aa1 | wc
          0       0       0
  --]]
  it("build_procs_with_pipes multiple", function()
    --                cmd-1                    | cmd-2 |    cmd-3    | cmd-4
    local cmd = 'cat /tmp/f1 /tmp/f2 > /tmp/f3 | wc -l | cat /tmp/f3 | cat'

    local pshell = mk_shell(cmd, {
      fs = {
        ['/tmp/f1'] = { 'f1-content' },
        ['/tmp/f2'] = { 'f2-content' },
        ['/tmp/f3'] = { 'f3-content' },
      }
    })
    local ca = M.setup_childs(pshell) ---@cast ca table{stub.io.Proc}
    assert.same(4, #ca)
    assert.same('0|nil', ca[1]:_errno())

    assert.equal(pshell:stdin(), ca[1]:stdin())
    assert.same('&0:TOR:0/0 id: 1', ca[1]:_fd_status(C.STDIN))
    assert.same('&1:FOW:0/0 id: 6 /f3', ca[1]:_fd_status(C.STDOUT))

    assert.same('&0:POR:0/0 id: 5  pipeid:1', ca[2]:_fd_status(C.STDIN))
    assert.same('&1:POW:0/0 id: 7  pipeid:2', ca[2]:_fd_status(C.STDOUT))
    assert.same('&0:POR:0/0 id: 8  pipeid:2', ca[3]:_fd_status(C.STDIN))
    assert.same('&1:POW:0/0 id: 9  pipeid:3', ca[3]:_fd_status(C.STDOUT))
    assert.same('&0:POR:0/0 id: 10  pipeid:3', ca[4]:_fd_status(C.STDIN))
    assert.same('&1:TOW:0/0 id: 2', ca[4]:_fd_status(C.STDOUT))
    assert.same('&1:TOW:0/0 id: 2', pshell:_fd_status(C.STDOUT))
    assert.equal(pshell:stdout(), ca[4]:stdout())
  end)


  it("sh cmd from stdin simple here-doc", function()
    local cmd = [[ sh << CMD_EOF
echo 123 | cat
CMD_EOF]]
    local pshell = mk_shell(cmd)
    local exp = {
      cmdline = " sh << CMD_EOF\necho 123 | cat\nCMD_EOF",
      ai = 1,
      args = { 'sh', '<<', "echo 123 | cat\n" }
    }
    assert.same(exp, pshell.args)
    local ca = M.setup_childs(pshell) ---@cast ca table{stub.io.Proc}
    assert.same(1, #ca)
    local exp2 = "pid: 9 ppid: 8 uid:1000 (stub.os.Args) sh\n" .. "opened fd:\n" ..
        "&0:FORW:0/1 id: 4 /tmpfile\n" ..
        "&1:TOW:0/0 id: 2\n" ..
        "&2:TOW:0/0 id: 3\n" ..
        " errno: 0(nil) exitcode:nil\n"
    assert.same(exp2, ca[1]:_status(true))
  end)


  it("sh cmd from stdin nested here-doc", function()
    local cmd = [[ sudo sh << SUDO_CMD_EOF
cat > /etc/app/config.json << END_OF_FILE
{"debug": true}
END_OF_FILE
SUDO_CMD_EOF]]
    local pshell = mk_shell(cmd)
    local exp = {
      'sudo',
      'sh',
      '<<',
      "cat > /etc/app/config.json << END_OF_FILE\n{\"debug\": true}\nEND_OF_FILE\n"
    }
    assert.same(exp, pshell.args.args)

    local procs = M.setup_childs(pshell)
    assert.same(1, #procs)
    local p1 = procs[1]
    local exp2 = [[
cat > /etc/app/config.json << END_OF_FILE
{"debug": true}
END_OF_FILE]]
    assert.same(exp2, p1:stdin():read('*a'))
    assert.same({ gid = 0, uid = 0 }, p1.ug)
  end)
end)


-------------------------------------------------------------------------------
--
--                     Integration with OSystem
--
-------------------------------------------------------------------------------

describe("stub.os.bin.sh", function()
  after_each(function()
    require 'dprint'.enable(false)
    if sys and sys.fs then
      sys.fs:restore_original()
    end
    sys:clear_state()
    sid.reset_all()
    IOHandle.bus._cleanup()
  end)


  it("sh run_cmdline without args", function()
    sys = OS:new()
    assert.match_error(function()
      M.run_cmdline(sys, '')
    end, 'not supports works without args')
  end)


  it("sh run_cmdline builtin pwd", function()
    sys = OS:new()
    local proc = M.run_cmdline(sys, 'pwd')
    assert.is_table(proc)
    assert.same({ '/home/user' }, proc:stdout().lines)
  end)

  -- check inner tooling
  it("sh builtin_save_stdin_to (empty stdin)", function()
    sys = OS:new()
    assert.same(false, sys:is_file_exits('/tmp/f1')) -- before

    local proc = M.run_cmdline(sys, 'builtin_save_stdin_to /tmp/f1')
    assert.is_table(proc)
    assert.same({ "/tmp/f1: saved:true line1:'nil'" }, proc:stdout().lines)
    assert.same(true, sys:is_file_exits('/tmp/f1'))
    assert.same({}, sys:get_file_lines('/tmp/f1')) -- empty
  end)


  it("sh run_cmdline builtin pwd with redirect stdout", function()
    sys = OS:new()
    -- pwd | cat > /tmp/f1
    local proc = M.run_cmdline(sys, 'pwd | builtin_save_stdin_to /tmp/f1')
    assert.is_table(proc)
    local exp = { "/tmp/f1: saved:true line1:'/home/user'" }
    assert.same(exp, proc:stdout().lines)
  end)



  it("sh run_cmdline builtin pwd with redirect stdout to file", function()
    sys = OS:new()
    local f, proc = M.run_cmdline, nil

    assert.same(false, sys:is_file_exits('/tmp/pwd'))
    -- write to file
    proc = f(sys, 'pwd > /tmp/pwd')
    -- pwd | cat # /
    assert.is_table(proc)
    assert.same(true, sys:is_file_exits('/tmp/pwd'))
    assert.same({ '/home/user' }, sys:get_file_lines('/tmp/pwd'))
    -- append to file
    proc = f(sys, 'pwd >> /tmp/pwd')
    assert.same({ '/home/user', '/home/user' }, sys:get_file_lines('/tmp/pwd'))

    -- rewrite existed
    proc = f(sys, 'pwd > /tmp/pwd')
    assert.same({ '/home/user' }, sys:get_file_lines('/tmp/pwd'))
  end)


  it("sh run_cmdline redirect here-doc from stdin to stdout", function()
    sys = OS:new()
    -- local cmd = "cat > /etc/custom_app/conf.json << EOF\n" ..
    local cmd = "cat << EOF\n" .. -- from input to stdout
        "line1\n" ..
        "line2\n" ..
        "EOF\n"
    local proc = M.run_cmdline(sys, cmd)
    assert.is_table(proc)
    local exp = { 'line1', 'line2' }
    assert.same(exp, proc:stdout().lines)
  end)


  it("sh run_cmdline redirect here-doc from stdin to file", function()
    sys = OS:new()
    assert.same(false, sys:is_file_exits('/tmp/file'))
    local cmd = "cat > /tmp/file << EOF\n" ..
        "line1\n" ..
        "line2\n" ..
        "EOF\n"
    local proc = M.run_cmdline(sys, cmd)
    assert.is_table(proc)
    assert.same({}, proc:stdout().lines)
    assert.same(true, sys:is_file_exits('/tmp/file'))
    assert.same({ 'line1', 'line2' }, sys:get_file_lines('/tmp/file'))
  end)


  it("sh run_cmdline redirect here-string from stdin to file", function()
    sys = OS:new()
    assert.same(false, sys:is_file_exits('/tmp/file'))
    local cmd = "cat > /tmp/file <<< here-string"
    local proc = M.run_cmdline(sys, cmd)
    assert.is_table(proc)
    assert.same({}, proc:stdout().lines)
    assert.same(true, sys:is_file_exits('/tmp/file'))
    assert.same({ 'here-string' }, sys:get_file_lines('/tmp/file'))
  end)


  it("pipeline + redirect", function()
    sys = OS:new()
    local run, proc = M.run_cmdline, nil

    proc = run(sys, "echo abc | cat")
    assert.same({ 'abc' }, proc:stdout().lines)
    proc:stdout().lines = {} -- cleanup

    proc = run(sys, "echo abc > /tmp/f1 | cat")
    assert.same('0|nil', proc:_errno())
    assert.same({}, proc:stdout().lines)
    assert.same({ 'abc' }, sys:get_file_lines('/tmp/f1'))
    proc:stdout().lines = {} -- cleanup

    proc = run(sys, "echo abc | cat | cat")
    assert.same('0|nil', proc:_errno())
    assert.same({ 'abc' }, proc:stdout().lines)
    proc:stdout().lines = {} -- cleanup

    proc = run(sys, "echo abc | cat | cat > /tmp/f2")
    assert.same('0|nil', proc:_errno())
    assert.same({}, proc:stdout().lines)
    assert.same({ 'abc' }, sys:get_file_lines('/tmp/f2'))
    proc:stdout().lines = {} -- cleanup

    proc = run(sys, "echo abc | cat > /tmp/f3 | cat")
    assert.same('0|nil', proc:_errno())
    assert.same({}, proc:stdout().lines)
    assert.same({ 'abc' }, sys:get_file_lines('/tmp/f2'))
    proc:stdout().lines = {}
  end)

  -- without cheking can_read in IOhandle
  local function read0(proc, fd)
    assert(proc and proc.fds[fd], 'has fd ' .. tostring(fd))
    local h = proc.fds[fd]
    if h then
      local s = ''
      for i = h.lnum + 1, #h.lines do
        local line = h.lines[i]
        s = s .. tostring(line) .. "\n"
      end
      h.lnum = #h.lines
      return s
    end
    return nil
  end


  it("cmd_cd", function()
    sys = OS:new()
    local f, p = M.run_cmdline, nil
    -- local p = sys.o.p_init
    p = f(sys, "pwd")
    assert.same("/home/user\n", read0(p, C.STDOUT))

    p = f(sys, "cd /")
    assert.same('', read0(p, C.STDOUT))

    p = f(sys, "pwd")
    assert.same("/home/user\n", read0(p, C.STDOUT))
    -- note: output is collected and not cleanup:
    assert.same({ '/home/user', '/home/user' }, p:stdout().lines)


    p = f(sys, "cd /tmp | pwd")
    assert.same("/tmp\n", read0(p, C.STDOUT))

    -- p = f(sys, "cd /tmp && pwd") -- TODO
  end)


  it("cmd_cd", function()
    sys = OS:new()
    local f, p = M.run_cmdline, nil
    p = f(sys, "sh -c 'echo 123 | cat'")
    assert.same("123\n", read0(p, C.STDOUT))

    p = f(sys, "sh -c 'sh -c \"echo 123 | cat\"'")
    assert.same("123\n", read0(p, C.STDOUT))
  end)



  it("sh cmd from stdin nested here-doc", function()
    sys = OS:new()
    sys.fs:set_dir('/etc/app/')
    local f, p = M.run_cmdline, nil
    local cmd = [[ sh << SUDO_CMD_EOF
cat > /etc/app/conf.json << END_OF_FILE
{"debug": true}
END_OF_FILE
SUDO_CMD_EOF]]

    p = f(sys, cmd)
    assert.same('', read0(p, C.STDOUT))
    assert.same({ '{"debug": true}' }, sys:get_file_lines('/etc/app/conf.json'))
  end)


  it("check_sudo", function()
    local a = Args.of('sudo sh -c "echo abc"')
    assert.same({ 'sudo', 'sh', '-c', '"echo abc"' }, a.args)
    assert.same({ gid = 0, uid = 0 }, M.check_sudo(a))
    assert.same({ 'sh', '-c', '"echo abc"' }, a.args)
  end)


  it("sudo sh cmd from stdin nested here-doc", function()
    sys = OS:new()
    sys.fs:set_dir('/etc/app/')
    local f, p = M.run_cmdline, nil
    local cmd = [[ sudo sh << SUDO_CMD_EOF
cat > /etc/app/conf.json << END_OF_FILE
{"debug": true}
END_OF_FILE
SUDO_CMD_EOF]]

    p = f(sys, cmd)
    assert.same('', read0(p, C.STDOUT))
    assert.same({ '{"debug": true}' }, sys:get_file_lines('/etc/app/conf.json'))
    -- saved with sudo perms:
    assert.same('0:root 0:root', sys:stat_owner('/etc/app/conf.json'))
  end)


  it("sudo & pipes", function()
    sys = OS:new()
    local f, p = M.run_cmdline, nil
    local cmd = "echo 123 | sudo tee /tmp/f1"

    p = f(sys, cmd)
    assert.same("123\n", read0(p, C.STDOUT))
    assert.same({ '123' }, sys:get_file_lines('/tmp/f1'))
    assert.same('0:root 0:root', sys:stat_owner('/tmp/f1'))
  end)


  it("mk_io_redirect 2>&1 -- redirect stderr to stdour", function()
    sys = OS:new()
    local proc = C.mkProc(sys)

    assert.same({}, proc.fds[C.STDOUT].lines)
    assert.same({}, proc.fds[C.STDERR].lines)
    assert.are_not_equal(proc.fds[C.STDERR], proc.fds[C.STDOUT])

    -- proc:_err(nil, "print to stderr")
    proc:c_printf(C.STDERR, "print to stderr")

    local exp_iohandler = {
      id = 3,
      type = IOHandle.TYPE.STD_STREAM,
      can_read = false,
      can_write = true,
      lines = { 'print to stderr' },
      open = true,
      lnum = 0,
      mode = 'w',
    }
    assert.same(exp_iohandler, proc.fds[C.STDERR])
    assert.same('stub.os.IOHandle', class.name(proc.fds[C.STDERR]))

    -- mk redirect from stderr to stdout
    proc.args = Args.of("2>&1")
    assert.same(true, M.mk_io_redirect(proc))

    assert.equal(proc.fds[C.STDERR], proc.fds[C.STDOUT]) -- one instance

    proc:c_printf(C.STDERR, "one more line to stderr")   -- issue new output to stderr

    local exp = { 'one more line to stderr' }
    assert.same(exp, proc.fds[C.STDERR].lines)
    assert.same(exp, proc.fds[C.STDOUT].lines)
  end)

  it("parse_cd_and_cmd", function()
    local f = M.parse_cd_and_cmd
    assert.same({ '/home/', 'pwd' }, { f("cd /home/ && pwd") })
    assert.same({ '/home/', 'pwd' }, { f("cd  /home/  && pwd") })
    assert.same({ '/home/', 'pwd' }, { f("cd  /home/   && pwd") })
    assert.same({ '"/home/"', 'pwd' }, { f("cd  \"/home/\"   && pwd") })
    assert.same({}, { f("cd  \"/h me/\"&& pwd") }) -- todo

    assert.same({ '/home/', '' }, { f("cd  /home/ && ") })
    assert.same({}, { f("cd  /home/ & ") })
  end)


  it("cd . && echo to-std-err 2>&1", function()
    sys = OS:new()
    local f = M.run_cmdline

    local proc = f(sys, "cd /home/ && echo $PWD")
    assert.same("/home/\n", read0(proc, C.STDOUT))
    assert.same('/home/', proc.cwd)
    assert.same('/home/', proc.envs.PWD)
    assert.same({ '/home/' }, proc.fds[C.STDOUT].lines)

    proc = f(sys, "cd /home/ && echo ${PWD}") -- todo implement ${VARNAME}
    assert.same("\n", read0(proc, C.STDOUT))

    proc = f(sys, "cd /tmp && pwd")
    assert.same("/tmp\n", read0(proc, C.STDOUT))
  end)

  it("cmd_echo_to_stderr and 2>&1 (stderr redirect ot stdout)", function()
    sys = OS:new()
    local f = M.run_cmdline
    local proc = f(sys, 'echo_to_stderr arg1 arg2')
    assert.same('', read0(proc, C.STDOUT))
    assert.same("arg1 arg2\n", read0(proc, C.STDERR))

    proc = f(sys, 'echo_to_stderr arg1 arg2 2>&1')
    assert.same("arg1 arg2\n", read0(proc, C.STDOUT))
    assert.same('', read0(proc, C.STDERR))

    proc = f(sys, 'cd /tmp/ && echo_to_stderr $PWD 2>&1')
    assert.same("/tmp/\n", read0(proc, C.STDOUT))
    assert.same('', read0(proc, C.STDERR))

    proc = f(sys, 'cd /tmp/ && echo_to_stderr $PWD')
    assert.same('', read0(proc, C.STDOUT))
    assert.same("/tmp/\n", read0(proc, C.STDERR))
  end)


  --[[
sh << SUDO_CMD_EOF
cat > /etc/docker/daemon.json << END_OF_FILE
{"debug":false}
END_OF_FILE
SUDO_CMD_EOF| nil
]]
end)
