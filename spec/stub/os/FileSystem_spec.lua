-- 24-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local class = require("oop.class")

local sid = require("stub.id")
local FS = require("stub.os.FileSystem") ---@type stub.os.FileSystem
local Proc = require("stub.os.Proc")
local IOHandle = require("stub.os.IOHandle")
local FileStub = require("stub.os.FileStub")

local D = require 'dprint'
local H = require("spec.stub.os.fs_helper")

-- local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
-- local uv = R.require("luv", vim, "loop")             -- vim.loop

local fs, out, proc0 = nil, {}, nil ---@cast fs stub.os.FileSystem

local E = {}
-- Note: D.enable() -- to enable dprint

-- testing-helper
--
-- the purpose of this function is to create a stub for independent testing
-- Goal: test itself (FileSystem) independed of the OSystem
-- this is a workaround of a issue: emulated io.open depends on OSystem.c_fopen
-- i.g. without OSystem
--
---@param t table? {log} -- to track access log
---@return stub.os.FileSystem
---@return table -- out t
---@return stub.os.Proc
local function new_fs_with_os_stub(fs0, t)
  assert(not t or type(t) == 'table', 'out')
  local fs_self = fs0 or FS:new()
  t = t or {}
  t.log = t.log or {}

  ---@param msg string
  local function log(msg, ...)
    msg = msg ~= nil and (tostring(msg) .. ':') or ''
    for i = 1, select('#', ...) do
      msg = msg .. '|' .. tostring(select(i, ...))
    end
    t.log[#t.log + 1] = msg
  end

  fs_self:bind_os({
    o = {}, -- state
    fs = fs_self,
    -- a stripped-down version that can open files that already exist
    -- in the emulated FS, and create file in write-mode(no atcs for dir)
    ---@return stub.os.IOHandle
    c_fopen = function(self, proc, fn, mode)
      assert(type(self) == 'table', 'self OSystem')
      log('c_fopen', proc, fn, mode)
      mode = mode or 'r'

      local f, lastf, i, path = fs_self:_find_file(fn)
      local mk_new = IOHandle.MOD.W[mode] or IOHandle.MOD.A[mode]

      -- if given file not exists but exists its parent dir and mode is write
      if not f and mk_new and lastf and lastf:is_dir() and #path == i then
        f = FileStub.new_file(lastf, path[i], {})
      end

      return f and IOHandle:new(nil, f, mode) or nil
    end,

    ---@return string
    last_err = function(self, proc)
      assert(type(self) == 'table', 'self OSystem')
      return (proc or self.o.p_init or E).last_err
    end,

    -- local proc, err = self.o.os:c_fpopen(nil, prog, mode) -- def mode is r?
    ---@diagnostic disable-next-line: unused-local
    c_fpopen = function(self, proc, prog, mode)
      -- D.dprint('c_fpopen cmd:', prog, mode)
      log('c_fopen', proc, prog, mode)
      return t.proc or self.o.p_init, t.err -- take own main process by defualt
    end,
  })
  local proc = Proc.of(fs_self.o.os)
  fs_self.o.os.o.p_init = proc

  return fs_self, t, proc
end


describe("stub.os.FileSystem", function()
  setup(function()
  end)

  after_each(function()
    D.disable()
    fs:restore_original():clear_state()
    sid.reset_all()
    out = nil
  end)

  --
  --
  it("override io", function()
    fs = FS:new() -- override _G.io
    assert.is_not_nil(fs)
    ---@diagnostic disable-next-line: undefined-field
    assert.same(sid.TESTING_TAG, fs.io.__tag)
    assert.same(sid.TESTING_TAG, _G.io.__tag)
    assert.equal(_G.io, fs.io)
    assert.equal(io, fs.io)
    fs:restore_original()
    assert.are_not_same(io, fs.io)
  end)


  it("_split_left_dir", function()
    local f = function(p)
      local d, r = FS._path_pop_left(p)
      return tostring(d) .. ' ' .. tostring(r)
    end

    assert.same(" nil", f(''))
    assert.same("a nil", f('a'))
    assert.same("a/ nil", f('a/'))
    assert.same("a/ b/c", f('a/b/c'))
    assert.same("a_b/ c", f('a_b/c'))
  end)

  it("path_split", function()
    assert.same({ '/' }, FS._path_split('/'))
    assert.same({ '/', 'usr' }, FS._path_split('/usr'))
    assert.same({ '/', 'usr' }, FS._path_split('/usr/'))
    assert.same({ '/', 'usr', 'bin' }, FS._path_split('/usr/bin/'))
    assert.same({ '/', 'usr', 'bin', 'file' }, FS._path_split('/usr/bin/file'))
  end)

  it("tree cwd", function()
    fs = FS:new() ---@type stub.os.FileSystem
    local exp = [[
/
    bin/
        bash
    dev/
        null
    etc/
        group
        hosts
        passwd
        shadow
    home/
        user/
            proj/
    tmp/
    usr/
        sbin/
            adduser
    var/
        log/
            syslog
11 directories, 8 files
]]
    assert.same(exp, fs:tree())
  end)

  it("tree fail", function()
    fs = FS:new() ---@type stub.os.FileSystem
    local exp =
        "/home/user/not-exists [error opening dir]\n" ..
        "\n" ..
        "0 directories, 0 files\n"
    assert.same(exp, fs:tree('/home/user/not-exists'))
  end)

  it("tree success", function()
    fs = FS:new() ---@type stub.os.FileSystem
    assert.same('/', fs:pwd())

    local exp =
        "/home/user/\n" ..
        "    proj/\n" ..
        "1 directory, 0 files\n"
    assert.same(exp, fs:tree('/home/user/'))

    assert.same('/', fs:pwd()) -- make sure cwd not changes
  end)

  it("tree no_trailing_slashes for dirs", function()
    fs = FS:new() ---@type stub.os.FileSystem
    assert.same('/', fs:pwd())

    local exp =
        "/home/user\n" ..
        "    proj/\n" ..
        "1 directory, 0 files\n"
    assert.same(exp, fs:tree('/home/user'))

    local exp2 = "/home/user\n    proj\n1 directory, 0 files\n"
    assert.same(exp2, fs:tree('/home/user', { no_trailing_slashes = 1 }))

    assert.same('/', fs:pwd()) -- make sure cwd not changes
  end)

  it("cd", function()
    fs = FS:new() ---@type stub.os.FileSystem
    assert.same(true, fs:cd('/'))
    assert.same(true, fs:cd('/var/log'))
    assert.same(true, fs:cd('/var/'))
    assert.same(true, fs:cd('/var'))
    -- cd to file
    assert.same(false, fs:cd('/var/log/syslog'))
    assert.same('/var/log/syslog: Not a directory', fs.o.last_err)

    assert.same(true, fs:cd('/home/user'))

    assert.same(false, fs:cd('/home/user/not-exists'))
    assert.same('/home/user/not-exists: No such file or directory', fs.o.last_err)
  end)


  it("pwd", function()
    fs = FS:new() ---@type stub.os.FileSystem
    assert.same('/', fs:pwd())
  end)

  -- it("cd force", function()
  --   fs = FS:new() ---@type stub.os.FileSystem
  --   local force = true
  --   assert.same(true, fs:cd('/var/log/syslog', force))
  --   assert.is_nil(fs.o.last_err)
  --   assert.same('/var/log', fs:pwd())
  -- end)

  it("mk_dir absolute path new", function()
    fs = FS:new() ---@type stub.os.FileSystem
    assert.is_not_nil(fs:mk_dir('/app/bin', true))
    assert.same('directory', fs:file_type('/app/bin'))
  end)

  --
  --
  it("mk_dir with existed part", function()
    fs = FS:new() ---@type stub.os.FileSystem

    assert.same('proj/', fs:ls('/home/user/'))
    assert.is_nil(fs:file_type('/home/user/dev/new_proj/src')) -- not exists

    assert.is_not_nil(fs:mk_dir('/home/user/dev/new_proj/src', true))

    local exp2 = [[
/home/user
    dev/
        new_proj/
            src/
    proj/
4 directories, 0 files
]]
    assert.same(exp2, fs:tree('/home/user'))
    assert.same('directory', fs:file_type('/home/user/dev/new_proj/src'))
    assert.same('dev/ proj/', fs:ls('/home/user/'))
  end)

  it("mk_dir without -p", function()
    fs = FS:new() ---@type stub.os.FileSystem
    local dn = '/home/user/proj/src'
    assert.is_nil(fs:file_type(dn))
    fs:mk_dir(dn, false)
    assert.same('directory', fs:file_type(dn))
  end)

  --
  it("mk_dir without -p", function()
    fs = FS:new() ---@type stub.os.FileSystem
    local dn = '/home/user/proj/src/a'
    assert.is_nil(fs:file_type(dn))

    assert.is_nil(fs:mk_dir(dn, false)) -- cannot create no src in proj
    local exp = 'cannot create directory ‘' .. dn .. '’: No such file or directory'
    assert.same(exp, fs.o.last_err)

    assert.is_nil(fs:file_type(dn))
  end)

  -- try to override existed file to dir
  it("mk_dir without -p existed file in path insted desired dir", function()
    fs = FS:new() ---@type stub.os.FileSystem
    local fn = '/home/user/proj/src/a'
    fs:set_file(fn)
    assert.same('file', fs:file_type(fn))

    local dn = fn .. '/dir'
    assert.is_nil(fs:mk_dir(dn, false)) -- cannot create no src in proj
    local exp = 'cannot create directory ‘' .. dn .. '’: File exists'
    assert.same(exp, fs.o.last_err)

    assert.is_nil(fs:file_type(dn))
  end)

  --
  it("stat", function()
    fs = FS:new()
    assert.match('File: /bin/bash', fs:stat('/bin/bash'))
  end)

  it("stat fmt", function()
    assert.match('1000: 1000:', FS:new():stat('/bin/bash', '%u:%U %g:%G'))
  end)

  it("get_file_owner", function()
    assert.same('1000: 1000:', FS:new():get_file_owner('/bin/bash'))
  end)

  -- usecase: make filename "existed" on filesystem
  --
  it("set_file & file_exists", function()
    fs, out = new_fs_with_os_stub()

    local fn = '/tmp/not-existed-in-real.txt'
    fs:set_file(fn, { 'a', 'b' })
    local fstub = fs:get_file(fn)
    assert.is_table(fstub) ---@cast fstub stub.os.FileStub
    assert.same({ 'a', 'b' }, fstub.lines)

    -- return self.o.os:c_fopen(nil, filename, mode)
    local f, err = io.open(fn)
    assert.is_not_nil(f)
    assert.is_nil(err)
    assert.is_table(f) ---@cast f table
    assert.is_function(f.close)

    assert.is_true(H.io_is_exist_file(fn))
    assert.is_false(H.io_is_exist_file(fn .. fn))
    f:close()
    local exp = {
      'c_fopen:|nil|/tmp/not-existed-in-real.txt|nil',
      'c_fopen:|nil|/tmp/not-existed-in-real.txt|rb',
      'c_fopen:|nil|/tmp/not-existed-in-real.txt/tmp/not-existed-in-real.txt|rb'
    }
    assert.same(exp, out.log)
  end)


  -- todo mk mirror with OSystem and c_fopen (io.open
  it("is_file_exits", function()
    fs, out = new_fs_with_os_stub()
    local fn = '/tmp/file.txt'
    assert.same(false, fs:is_file_exits(fn))
    -- check dir
    assert.same(true, fs:is_file_exits('/tmp')) -- no ftype any file

    assert.same(true, fs:is_file_exits('/tmp', 'directory'))
    assert.same(false, fs:is_file_exits('/tmp', 'file'))

    -- create a new file via wrapper around sys-call
    local h, err = io.open(fn, 'w') -- os:c_fopen(nil, fn, 'w')
    assert.is_table(h)
    assert.is_nil(err)
    assert.same(true, fs:is_file_exits(fn, 'file'))
  end)

  --
  it("set_dir", function()
    fs = new_fs_with_os_stub()
    local pr = '/home/user/proj_a/'
    local exp_before = [[
/home
    user/
        proj/
2 directories, 0 files
]]
    assert.same(exp_before, fs:tree('/home'))
    fs:set_dir(pr, { 'src/', 'spec/', '.git/', 'README.md' })
    local exp = [[
/home
    user/
        proj/
        proj_a/
            .git/
            README.md
            spec/
            src/
6 directories, 1 file
]]
    assert.same(exp, fs:tree('/home'))
    assert.is_not_nil(fs:get_file(pr .. 'src/'))
    assert.is_not_nil(fs:get_file(pr .. '.git/'))
    assert.is_not_nil(fs:get_file(pr .. 'spec/'))
    assert.is_not_nil(fs:get_file(pr .. 'spec'))

    local fn = pr .. 'README.md'
    local f = fs:get_file(fn)
    assert.is_table(f) ---@cast f stub.os.FileStub
    assert.same("README.md", f.name)
    assert.is_true(H.io_is_exist_file(fn))

    fn = pr .. 'not-existed'
    f = fs:get_file(fn)
    assert.is_nil(f)
    assert.is_false(H.io_is_exist_file(fn))
  end)

  --
  --
  it("set_dir try to set already existed dir", function()
    local pr = '/home/user/proj/' --, 'src/app/MyClass.lua')
    fs = FS:new() ---@type stub.os.FileSystem
    fs:set_dir(pr .. 'src')
    assert.same('directory', fs:file_type(pr .. 'src'))

    assert.same(pr .. 'src/', tostring(fs:get_file(pr .. 'src')))

    local exp = "/home/user/proj\n    src/\n1 directory, 0 files\n"
    assert.same(exp, fs:tree('/home/user/proj'))

    fs:set_dir(pr, { 'src/', 'test/' })
    local exp2 = [[
/home/user/proj
    src/
    test/
2 directories, 0 files
]]
    assert.same(exp2, fs:tree('/home/user/proj'))
  end)

  --
  --
  it("set_dir try to set already existed dir with files names", function()
    local pr = '/home/user/proj/'
    fs = FS:new() ---@type stub.os.FileSystem
    fs:set_dir(pr .. 'src')
    assert.same('directory', fs:file_type(pr .. 'src'))

    local exp = "/home/user/proj\n    src/\n1 directory, 0 files\n"
    assert.same(exp, fs:tree('/home/user/proj'))

    -- thrown error -- cannot override existed directory by the file
    assert.match_error(function()
      fs:set_dir(pr, { 'src', 'test' }) -- files not directories! src/ test/
    end, 'already exists directory: "src" in dir: /home/user/proj/')
  end)


  --
  --
  it("set_dir already exited file", function()
    local pr = '/home/user/proj/' --, 'src/app/MyClass.lua')
    fs = FS:new() ---@type stub.os.FileSystem
    fs:set_dir(pr, { '.git/', 'readme.md' })
    assert.same("directory", fs:file_type(pr .. '.git'))
    assert.same("file", fs:file_type(pr .. 'readme.md'))
  end)

  -----------------------------------------------------------------------------
  --             test common used utils from fs_helper
  -----------------------------------------------------------------------------

  --
  --
  it("is_exist_file luv & io", function()
    fs = new_fs_with_os_stub()
    local fn = '/tmp/not-existed-in-real.txt'
    fs:set_file(fn, { '--' })

    assert.is_true(H.luv_is_exist_file(fn))
    assert.is_false(H.luv_is_exist_file("/tmp"))

    assert.is_true(H.io_is_exist_file(fn))
    assert.is_true(H.io_is_exist_file("/tmp")) -- for io dir as a file

    assert.is_false(H.luv_is_exist_file('/tmp/1'))
    assert.is_false(H.io_is_exist_file('/tmp/1'))
  end)

  --
  it("is_exist_dir luv & io", function()
    fs = new_fs_with_os_stub()
    local dn = '/tmp/dir'
    fs:set_dir('/tmp/dir')

    assert.is_true(H.luv_is_exist_dir(dn))
    assert.is_true(H.io_is_exist_file(dn)) -- todo is_dir for pure io

    assert.is_false(H.luv_is_exist_dir('/tmp/dir2'))
    assert.is_false(H.luv_is_exist_file('/tmp/dir2'))
  end)

  --
  it("ls", function()
    fs = FS:new()
    -- of directories
    assert.same('bin/ dev/ etc/ home/ tmp/ usr/ var/', fs:ls('/'))
    assert.same('proj/', fs:ls('/home/user'))
    assert.same('syslog', fs:ls('/var/log/'))
    -- file
    assert.same('/var/log/syslog', fs:ls('/var/log/syslog'))

    assert.same('bin/ dev/ etc/ home/ tmp/ usr/ var/', fs:ls()) -- cwd
    fs:cd('/home/user/')
    assert.same('proj/', fs:ls())                               -- cwd
  end)

  --
  it("luv_fs_scandir success", function()
    fs = FS:new()
    local dn = '/tmp/dir'
    fs:set_dir(dn, { 'file1', 'file2', 'file3', 'sdir/' })
    assert.same('file1 file2 file3 sdir/', fs:ls(dn))

    local uv = require("luv")
    local handle, errmsg = uv.fs_scandir(dn)
    assert.same('stub.os.IOHandle', class.name(handle))
    assert.is_nil(errmsg)
    local fn, ft = uv.fs_scandir_next(handle)
    assert.same('file1', fn)
    assert.same('file', ft)

    assert.same('file2', uv.fs_scandir_next(handle))
    assert.same('file3', uv.fs_scandir_next(handle))

    fn, ft = uv.fs_scandir_next(handle)
    assert.same('sdir', fn)
    assert.same('directory', ft)
    assert.same({ 'file1', 'file2', 'file3', 'sdir' }, H.luv_list_of_files(dn))
  end)

  --
  it("luv_fs_scandir fail not exists", function()
    fs = FS:new()
    local dn = '/tmp/dir'

    local uv = require("luv")
    local handle, errmsg = uv.fs_scandir(dn)
    assert.is_nil(class.name(handle))
    assert.same('ENOENT: no such file or directory: /tmp/dir', errmsg)
  end)

  --
  it("luv_fs_mkdir", function()
    fs = FS:new()
    assert.is_nil(fs:file_type('/tmp/123/456'))
    local uv = require("luv")
    assert.same(false, uv.fs_mkdir('/tmp/123/456'))
    assert.same(true, uv.fs_mkdir('/tmp/123'))
    assert.same(true, uv.fs_mkdir('/tmp/123/456'))
    assert.same(false, uv.fs_mkdir('/bin/bash'))
    assert.same('file', fs:file_type('/bin/bash'))
    assert.is_nil(fs:file_type('/bin/bash/')) -- Not a directory
  end)

  it("helper mkdir", function()
    fs = FS:new()
    assert.is_nil(fs:file_type('/tmp/123/456'))
    assert.same(true, H.mkdir('/tmp/123/456'))
    assert.same('directory', fs:file_type('/tmp/123/456'))
    assert.same(false, H.mkdir('/bin/bash'))
    assert.same('file', fs:file_type('/bin/bash'))
    assert.is_nil(fs:file_type('/bin/bash/'))
  end)

  --
  it("rm", function()
    fs = FS:new()
    assert.same(false, fs:rm('/tmp/123'))
    local exp = 'rm: cannot remove /tmp/123: No such file or directory'
    assert.same(exp, fs.o.last_err)

    fs:set_file('/tmp/123')
    assert.same('file', fs:file_type('/tmp/123'))
    assert.same(false, fs:rm('/tmp'))
    assert.same(true, fs:rm('/tmp/123'))
    -- assert.same(1, fs:tree() )
    assert.same(true, fs:rm('/tmp'))
    assert.is_nil(fs:file_type('/tmp/123'))
    assert.is_nil(fs:file_type('/tmp'))
  end)

  --
  it("touch", function()
    fs = FS:new()
    assert.same(false, fs:touch('/tmp/a/123'))
    local exp = "touch: cannot touch '/tmp/a/123': No such file or directory"
    assert.same(exp, fs.o.last_err)

    assert.same(false, fs:touch('/bin/bash/123'))
    local exp2 = "touch: cannot touch '/bin/bash/123': Not a directory"
    assert.same(exp2, fs.o.last_err)

    assert.same(true, fs:touch('/tmp/123'))
    local mtime = fs:get_file('/tmp/123').fs_stat.mtime
    assert.is_not_nil(mtime)
    assert.is_not_nil(mtime.sec)
  end)

  --
  it("os_uname", function()
    fs = FS:new()
    local uv = require("luv")
    assert.is_table(uv.os_uname())
    assert.same('Linux', uv.os_uname().sysname)
  end)

  it("access_inc", function()
    fs = new_fs_with_os_stub()
    assert.same(0, fs:access_cnt('io.open'))
    assert.same(0, fs:access_cnt('io.close'))

    local h, _ = io.open('/bin/bash')
    if h then io.close(h) end

    assert.same(1, fs:access_cnt('io.open'))
    assert.same(1, fs:access_cnt('io.close'))
  end)

  --
  -- debug tool to determine objects, functions, modules
  --
  it("whatis", function()
    local uv = require("luv")
    local prev_io = io
    local prev_io_open = io.open
    local prev_uv_fs_mkdir = uv.fs_mkdir

    fs = FS:new()
    local f = fs.io.open
    assert.same('[emulated] io.open', fs:whatis(f))
    assert.same('[original] io.open', fs:whatis(prev_io_open))

    assert.same('[emulated] luv.fs_mkdir', fs:whatis(fs.luv.fs_mkdir))
    assert.same('[original] luv.fs_mkdir', fs:whatis(prev_uv_fs_mkdir))

    assert.same('original luv module', fs:whatis(uv))
    assert.same('emulated luv module', fs:whatis(fs.luv))

    assert.same('original io module', fs:whatis(prev_io))
    assert.same('emulated io module', fs:whatis(fs.io))

    assert.match('^its me .stub.os.FileSystem. @.*', fs:whatis(fs))

    local some_fs = fs:whatis(FS:new({ nostub = true }))
    assert.match('^.stub.os.FileSystem. @.* .noStub.$', some_fs)
  end)


  it("_get_dirnode", function()
    fs = FS:new()
    local f = FS._get_dirnode
    assert.same(fs.o.root, f(fs, '/'))
    assert.same(fs.o.root.files['tmp'], f(fs, '/tmp'))
    assert.same(fs.o.root.files['var'].files['log'], f(fs, '/var/log'))
    assert.same(nil, f(fs, '/not-existed'))

    local fbash = fs.o.root.files.bin.files.bash
    assert.same(false, fbash:is_dir())
    assert.is_not_nil(fbash)
    assert.same('/bin/bash', fbash:get_absolute_path())
    assert.same(nil, f(fs, '/bin/bash')) -- only dir

    assert.same(nil, f(fs, '/no-dir/var/log'))
  end)


  --
  it("_find_file ./", function()
    fs = FS:new()
    assert.is_not_nil(fs:_find_file('/bin/bash'))

    assert.is_not(fs:_find_file('/bin/bash/'))
    assert.same('Not a directory', fs.o.last_err)
  end)


  -- relative paths

  --
  it("_find_file ./", function()
    fs = FS:new()
    assert.same(true, fs:cd('/bin'))

    local f = fs:_find_file('./bash') -- workload

    assert.is_not_nil(f)              -- @cast f stub.os.FileStub
    assert.same('bash', (f or E).name)
  end)

  it("_find_file ../", function()
    fs = FS:new()
    assert.same(true, fs:cd('/home/user/proj/'))

    local f = fs:_find_file('../proj')
    assert.same('proj', (f or E).name)
  end)

  it("_find_file ../../", function()
    fs = FS:new()
    assert.same(true, fs:cd('/home/user/proj/'))

    local f = fs:_find_file('../../../home')
    assert.same('home', (f or E).name)
  end)

  --
  it("_find_file ./", function()
    fs = FS:new()
    assert.same(true, fs:cd('/home/user/proj/'))

    fs:set_file('.gitignore')
    local exp = '/home/user/proj/.gitignore'
    assert.same(exp, (fs:get_file('.gitignore') or E):get_absolute_path())

    local f = fs:_find_file('././.gitignore')
    assert.same('.gitignore', (f or E).name)

    f = fs:_find_file('./../../user/./proj/.gitignore')
    assert.same('.gitignore', (f or E).name)
  end)

  -- ----------------------------------------------------------------- --

  -- bounсh of file and directory in one line
  -- helper for asserts

  it("has full-paths", function()
    fs = FS:new()
    assert.same('+1', fs:has('/tmp/'))
    assert.same('+2', fs:has('/tmp/', '/bin/bash'))
    assert.same('+2', fs:has('/home/', '/home/user/'))
    assert.same('-2', fs:has('/home/', '/home/user/not-exists'))
    assert.same('-2 -3', fs:has('/home/', '/x', 'y'))
    assert.same('no one', fs:has('x', 'y', 'z'))
  end)

  it("has relative-paths", function()
    fs = FS:new()
    assert.is_not_nil(fs:set_dir("/tmp/dir").o.last_created_filestub)
    assert.is_not_nil(fs:set_file("/tmp/dir/a").o.last_created_filestub)
    assert.is_not_nil(fs:set_file("/tmp/dir/b").o.last_created_filestub)
    assert.is_not_nil(fs:set_file("/tmp/dir/c").o.last_created_filestub)

    assert.same('+3', fs:has('/tmp/dir/a', '/tmp/dir/b', '/tmp/dir/c'))
    fs:cd('/tmp/dir')
    assert.same('+3', fs:has('a', 'b', 'c'))      -- means all existed
    assert.same('-4', fs:has('a', 'b', 'c', 'd')) -- show only which not has

    fs:cd('/tmp')
    assert.same('+3', fs:has('dir/a', 'dir/b', 'dir/c'))          -- has all
    assert.same('+3', fs:has('./dir/a', './dir/b', '/tmp/dir/c')) -- ./
    assert.same('-2', fs:has('../tmp/dir/a', '/dir/b', '../tmp/dir'))

    assert.same('/tmp', fs:pwd())
    assert.is_nil(fs:file_type('/dir/b'))
  end)

  it("assert_has", function()
    fs = FS:new()
    assert.no_error(function() fs:assert_has('/tmp') end)
    assert.no_error(function() fs:assert_has('/') end)

    local exp_err = "not exists path: '/tmp/a' arg#: 1"
    assert.match_error(function() fs:assert_has('/tmp/a') end, exp_err)

    assert.match_error(function()
      fs:assert_has('/', '/tmp', '/bin', '/tmp/a')
    end, "not exists path: '/tmp/a' arg#: 4\nExists: 3/4")

    -- file exists
    assert.no_error(function() fs:assert_has('/bin/bash') end)
    -- not a dir!
    assert.match_error(function()
      fs:assert_has('/bin/bash/')
    end, "not exists path: '/bin/bash/' arg#: 1: Not a directory")
  end)

  --
  -- fs:mk -- to create a bounch of files directories in one line

  --
  it("mk", function()
    fs = FS:new()
    assert.same(3, fs:mk('/tmp/a', '/tmp/b', '/tmp/c'))
    assert.same('+3', fs:has('/tmp/a', '/tmp/b', '/tmp/c'))
  end)

  it("mk multiples files in specified dir", function()
    fs = FS:new()
    assert.same(4, fs:mk('/tmp/dir/', 'a', 'b', 'c')) -- work load

    assert.same('directory', fs:file_type("/tmp/dir"))
    assert.same('file', fs:file_type("/tmp/dir/a"))
    assert.same('file', fs:file_type("/tmp/dir/b"))
    assert.same('file', fs:file_type("/tmp/dir/c"))

    -- another way to checks:
    assert.same('+3', fs:has('/tmp/dir/a', '/tmp/dir/b', '/tmp/dir/c'))
  end)

  it("mk multiples files in specified dir 2", function()
    fs = FS:new()
    assert.same(7, fs:mk('/tmp/dir/', 'a', 'b', '/c', './d', '../e', './f/g'))

    assert.same('directory', fs:file_type("/tmp/dir"))
    assert.same('file', fs:file_type("/tmp/dir/a"))
    assert.same('file', fs:file_type("/tmp/dir/b"))
    assert.same('file', fs:file_type("/c"))
    assert.is_nil(fs:file_type("/tmp/dir/c"))

    assert.same('file', fs:file_type("/tmp/dir/d"))
    assert.same('file', fs:file_type("/tmp/e"))

    assert.same('directory', fs:file_type("/tmp/dir/f"))
    assert.same('file', fs:file_type("/tmp/dir/f/g"))
  end)

  it("mk multiples files in specified dir 2", function()
    fs = FS:new()
    assert.same('/', fs:pwd()) -- cwd before work
    assert.same(5, fs:mk('a', 'b', '/tmp/dir/', 'f/', 'g'))
    -- after /tmp/dir the f/ take prev created directory as cwd

    assert.same('file', fs:file_type("/a"))
    assert.same('file', fs:file_type("/b"))

    assert.same('directory', fs:file_type("/tmp/dir"))
    assert.same('directory', fs:file_type("/tmp/dir/f"))

    assert.same('file', fs:file_type("/tmp/dir/f/g"))
    assert.same('/', fs:pwd()) -- after mk restored original cwd
  end)

  it("mk multiples files in specified dir 2", function()
    fs = FS:new()
    assert.same(5, fs:mk('a', 'b', 'f/', 'g', '../e'))

    assert.same('file', fs:file_type("/a"))
    assert.same('file', fs:file_type("/b"))

    assert.same('directory', fs:file_type("/f"))

    assert.same('file', fs:file_type("/f/g"))
    assert.same('file', fs:file_type("/e"))
  end)

  it("mk multiples files in specified dir 2", function()
    fs = FS:new()
    fs:cd('/tmp') -- set cwd and mk elemnts in it
    assert.same(5, fs:mk('a', 'b', 'f/', 'g', '../e'))

    assert.same('file', fs:file_type("/tmp/a"))
    assert.same('file', fs:file_type("/tmp/b"))

    assert.same('directory', fs:file_type("/tmp/f"))

    assert.same('file', fs:file_type("/tmp/f/g"))
    assert.same('file', fs:file_type("/tmp/e"))
  end)

  it("mk multiples files in specified dir 2", function()
    fs = FS:new()
    -- fs.D.enable()
    assert.same('/', fs:pwd())       -- cwd before work

    assert.same(1, fs:mk('../../e')) -- root is over

    assert.same('file', fs:file_type("/e"))
  end)

  it("real luv.cwd", function()
    fs = FS:new()
    assert.is_not_nil(fs:real_cwd())
  end)

  it("stdin_add_input", function()
    fs = FS:new()
    -- local err = 'stdin must be already created by call fs:mk_io_std_in_out_err'
    -- assert.match_error(function() fs:stdin_add_input('boom!') end, err)
    -- fs:bind_std_iostreams()

    -- add simple string
    assert.same({ 'abc' }, fs:stdin_add_input('abc'))
    assert.same('abc', fs.io.stdin:read())
    assert.is_nil(fs.io.stdin:read())
    assert.is_nil(fs.io.stdin:read())

    -- add "list of stings" - table
    assert.same({ 'abc', 'de', 'f' }, fs:stdin_add_input({ 'de', 'f' }))
    assert.same('de', fs.io.stdin:read())
    assert.same('f', fs.io.stdin:read())
    assert.is_nil(fs.io.stdin:read())

    -- add multiline string
    local exp = { 'abc', 'de', 'f', 'gh', 'ijk', 'l' }
    assert.same(exp, fs:stdin_add_input("gh\nijk\nl"))
    assert.same('gh', fs.io.stdin:read())
    assert.same('ijk', fs.io.stdin:read())
    assert.same('l', fs.io.stdin:read())
    assert.is_nil(fs.io.stdin:read())
  end)
  -- TODO
  -- it("mount", function()
  --   fs = FS:new()
  --   assert.same(36, fs:mount('/tmp', '/tmp'))
  --   assert.same(1, fs:tree())
  -- end)
  --
  it("set_last_err + last_err", function()
    assert.same('abc: "d e"', fs:set_last_err('%s: "%s"', 'abc', 'd e'))
    assert.same('abc: "d e"', fs:last_err())
  end)
  -----------------------------------------------------------------------------
  --                                 MV
  --
  it("mv: same dir", function()
    fs:set_file('/tmp/old')
    assert.same(true, fs:is_exists_file('/tmp/old'))
    assert.same(false, fs:is_exists_file('/tmp/new'))

    assert.same(true, fs:mv('/tmp/old', '/tmp/new')) -- workload

    assert.same(false, fs:is_exists_file('/tmp/old'))
    assert.same(true, fs:is_exists_file('/tmp/new'))
  end)

  it("mv: diff dir", function()
    fs:set_file('/tmp/dir1/oldname')
    assert.same(true, fs:is_exists_file('/tmp/dir1/oldname'))
    assert.same(true, fs:is_exists_dir('/home/user'))
    assert.same(false, fs:is_exists_file('/home/user/newname'))

    assert.same(true, fs:mv('/tmp/dir1/oldname', '/home/user/newname'))

    assert.same(false, fs:is_exists_file('/tmp/dir1/oldname'))
    assert.same(true, fs:is_exists_dir('/tmp/dir1'))
    assert.same(true, fs:is_exists_file('/home/user/newname'))
  end)

  it("mv: try to move to not exists dir", function()
    fs:set_file('/tmp/old')
    assert.same(true, fs:is_exists_file('/tmp/old'))
    assert.same(true, fs:is_exists_dir('/home/user'))
    assert.same(false, fs:is_exists_file('/home/user/not-exists'))

    assert.same(false, fs:mv('/tmp/old', '/home/user/not-exists/new'))
    local exp = "mv: cannot move '/tmp/old' to '/home/user/not-exists/new':" ..
        " No such file or directory"
    assert.same(exp, fs:last_err())

    assert.same(true, fs:is_exists_file('/tmp/old')) -- not moved
    assert.same(false, fs:is_exists_file('/home/user/not-exists/new'))
  end)

  it("mv: try to move not existed file to already existed", function()
    fs:set_file('/home/user/new')
    assert.same(false, fs:is_exists_file('/tmp/old'))
    assert.same(true, fs:is_exists_file('/home/user/new'))

    assert.is_false(fs:mv('/tmp/old', '/home/user/new'))
    local exp = "mv: cannot stat '/tmp/old': No such file or directory"
    assert.same(exp, fs:last_err())

    assert.same(false, fs:is_exists_file('/tmp/old')) -- not moved
    assert.same(true, fs:is_exists_file('/home/user/new'))
  end)

  it("mv: rename in the same dir", function()
    fs:set_file('/tmp/old')
    assert.same(true, fs:is_exists_file('/tmp/old'))
    assert.same(false, fs:is_exists_file('/tmp/new'))

    fs:cd('/tmp/')
    assert.same('/tmp', fs:pwd())

    local st, err = fs:mv('old', 'new')
    assert.is_true(st)
    assert.is_nil(err)

    assert.same(false, fs:is_exists_file('/tmp/old'))
    assert.same(true, fs:is_exists_file('/tmp/new'))
  end)

  it("mv: rename in the diff dir", function()
    fs:set_file('/tmp/old')
    fs:mk_dir('/tmp/dir/')
    assert.same(true, fs:is_exists_file('/tmp/old'))
    assert.same(true, fs:is_exists_dir('/tmp/dir'))

    fs:cd('/tmp/dir')
    assert.same('/tmp/dir', fs:pwd())

    assert.same(true, fs:is_exists_file('../old'))
    local st, err = fs:mv('../old', 'new')
    assert.is_true(st)
    assert.is_nil(err)

    assert.same(false, fs:is_exists_file('/tmp/old'))
    assert.same(true, fs:is_exists_file('/tmp/dir/new'))
  end)

  ----------------------------------------------------------------------------
  --                             popen
  --
  --   Testing here check the basic mechanics without depending on a specific
  --   implementation in stub.io.OSystem.

  -- run a sys cmd and read from its stdout stream
  -- emulate popen with read from the stdout of a opened sys process(command)
  it("popen read(fr stdout): under the hood OSystem:c_fpopen", function()
    fs, out, proc0 = new_fs_with_os_stub(nil, { proc = nil, err = nil })

    assert.is_function(proc0.stdin)
    assert.equal(proc0:stdin(), proc0.fds[0])
    assert.equal(proc0:stdout(), proc0.fds[1])
    assert.equal(proc0:stderr(), proc0.fds[2])

    proc0:stdout():write('emulated output of a custom-command')

    local h = io.popen('custom-command', 'r')
    assert.is_table(h) ---@cast h table file*  or (stub.os.IOHandle)

    local exp = 'emulated output of a custom-command'
    assert.same(exp, h:read('*a'))

    assert.match_error(function()
      h:write('attempting to write to read-only mode must fail')
    end, 'expected can_write &.:PCR:1/1 id: 7')

    h:close()
  end)

  -- run a sys cmd and write to its stdin stream
  -- emulate popen with write to the stdin of the opened sys process(command)
  it("popen write(to stdin): under the hood OSystem:c_fpopen", function()
    fs, out, proc0 = new_fs_with_os_stub(nil, { proc = nil, err = nil })
    proc0:stdout():write('emulated output of a custom-command')

    local h = io.popen('custom-command', 'w')
    assert.is_table(h) ---@cast h table

    assert.match_error(function()
      h:read('*a') -- attempt to read from write-only out-stream
    end, '|!errno_cb| expected can_read &.:POW:0/0 id: 7; 9:Bad file descriptor')

    h:write('emulate text from input')
    assert.same('emulate text from input', proc0:stdin():read('*a'))

    h:close()
  end)

  it("get_stdout_lines", function()
    fs = FS:new()
    assert.same({}, fs:get_stdout_lines())
    io.stdout:write('text to std out')

    assert.same({ 'text to std out' }, fs:get_stdout_lines())
  end)
end)
