-- 09-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("stub.id");

describe("stub.id", function()
  it("next_id", function()
    assert.same(1, M.next_id())
    assert.same(2, M.next_id())
    assert.same(3, M.next_id())
  end)


  it("next_section_id", function()
    local f = M.next_section_id
    assert.same(1, f('abc'))
    assert.same(2, f('abc'))
    assert.same(3, f('abc'))
    assert.same(1, f('another-section-name'))
    assert.same(2, f('another-section-name'))
    assert.same(3, f('another-section-name'))
    assert.same(4, f('abc'))
    assert.same(5, f('abc'))
    assert.same(4, f('another-section-name'))
  end)


  it("get_sections_status", function()
    local _next = M.next_section_id
    _next('section-name-1')
    _next('section-name-1')
    _next('section-name-1')
    _next('B')
    _next('C')
    _next('C')
    local exp =
        "C: next_id: 2\n" ..
        "B: next_id: 1\n" ..
        "another-section-name: next_id: 4\n" ..
        "section-name-1: next_id: 3\n" ..
        "abc: next_id: 5\n"
    assert.same(exp, M.get_sections_status())

    local exp2 = 'section-name-1: next_id:3'
    assert.same(exp2, M.get_sections_status('section-name-1'))
  end)


  it("reset_all", function()
    M.reset_all()

    assert.same(1, M.next_id())
    assert.same(2, M.next_id())

    assert.same(1, M.next_section_id('A'))
    assert.same(2, M.next_section_id('A'))
    assert.same(1, M.next_section_id('B'))

    assert.same({ B = 1, next_id = 2, A = 2 }, M.reset_all())

    assert.same(1, M.next_id())
    assert.same(1, M.next_section_id('A'))
    assert.same(1, M.next_section_id('B'))
  end)


  it("set_next_id", function()
    M.set_next_id(0) -- clean

    assert.same(1, M.next_id())
    assert.same(2, M.next_id())
    assert.same(3, M.next_id())

    assert.same(3, M.set_next_id(0))
    assert.same(1, M.next_id())
  end)


  it("set_next_section_id", function()
    assert.same(1, M.next_section_id('Name'))
    assert.same(2, M.next_section_id('Name'))
    assert.same(3, M.next_section_id('Name'))

    assert.same(3, M.set_next_section_id('Name', 0))
    assert.same(1, M.next_section_id('Name'))
  end)


  it("rand_str", function()
    local seed = 1
    assert.same('QvD6nm39tPKZ', M.rand_str(nil, seed))
  end)
end)
