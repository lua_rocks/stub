#!/usr/bin/env bash

# write to stderr
echoerr() { printf "%s\n" "$*" >&2; }

echoerr msg-from-stderr

echo msg-from-stdout
