---@diagnostic disable: lowercase-global
package = "stub"
version = "0.4.0-1"
source = {
  url = "https://gitlab.com/lua_rocks/stub",
  tag = "v0.4.0"
}
description = {
  summary = "A set of stubs for testing access to the OS and the NVim API",
  detailed = [[
    This is a library that allows you to emulate (stub or mock) such things as:
    - file system structure(access via standard io and some things of luv),
    - responses of the system cli commands (access via io.popen)
    - responses from the internal nvim API(to write tests for nvim plugins)

    The main idea that inspired the writing of this library is to provide the
    ability to easy and quickly override real-existed files and directories,
    and console command responses. So often necessary when writing tests.
]],
  homepage = "https://gitlab.com/lua_rocks/stub",
  license = "MIT"
}

dependencies = {
  "lua >= 5.1",
  'oop >= 0.4',
  'dprint >= 0.3',
  -- 'alogger >= 0.2', -- optional
}

build = {
  type = "builtin",
  modules = {
    ["stub.os.FileStub"] = "src/stub/os/FileStub.lua",
    ["stub.os.FileSystem"] = "src/stub/os/FileSystem.lua",
    ["stub.os.IOHandle"] = "src/stub/os/IOHandle.lua",
    ["stub.os.OSystem"] = "src/stub/os/OSystem.lua",
    ["stub.os.ioutils"] = "src/stub/os/ioutils.lua",
    ["stub.vim.NVim"] = "src/stub/vim/NVim.lua",
    ["stub.id"] = "src/stub/id.lua",
  },
  copy_directories = {
    'doc',
    'examples',
  }
}
