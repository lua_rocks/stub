
```sh
wc << EOF
one two three
four five
EOF
```

In bash these are implemented via temp files
This can be observed via tracing system calls with strace command.
Replace bash with sh to see how /bin/sh performs this redirection.
  ```sh
strace -e open,dup2,pipe,write -f bash -c 'cat <<EOF
> test
> EOF'
```

--[[
 EnvVars  behavior in shell

  Experimental

  V=1 echo $V                   -->   ""
  V=1; echo $V                  -->   1

  V=1; ./show-varname.x V       -->   V: (null)

  V=1 ./show-varname.x V        -->   V: 1

  V=1 ./show-varname.x V > fv1 | V=2 ./show-varname.x V > fv2; cat fv1 fv2
  V: 1
  V: 2

  V=1 ./show-varname.x V > fv1 | ./show-varname.x V > fv2; cat fv1 fv2
  V: (nil)
  V: 2

  sh -c "V=1 ./show-varname.x V > fv1 | V=2 ./show-varname.x V > fv2; cat fv1 fv2"
  V: 1
  V: 2

  V=A sh -c "./show-varname.x V > fv1 | ./show-varname.x V > fv2; cat fv1 fv2"
  V: A
  V: A
]]


```lua
  local h, err = io.popen("pwd")
  assert.is_nil(err)
  assert.is_not_nil(h) ---@cast h file*
  local out = h:read('*a') -- *a give '' but *l give nil on empty input
  print('1', out)  -- /home/user/some/path/\n
  h:close()

  local h2, err = io.popen("cd /tmp/")
  if h2 then
    print('2', h2:read('*a')) -- empty
    h2:close()
  end

  local h3, err = io.popen("pwd")
  if h3 then
    print('3',h3:read('*a')) -- /home/user/some/path  but not a /tmp !
    h3:close()
  end

  local h4, err = io.popen("cd /tmp/ && pwd")
  if h4 then
    print('4',h4:read('*a')) -- /tmp
    h4:close()
  end
```
