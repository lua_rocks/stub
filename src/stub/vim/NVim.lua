---@diagnostic disable: duplicate-set-field
-- 17-11-2023 @author Swarg
-- Goal: Stub for testing emulate the NVim behavior
--
-- NOTE: if you need to use FileSystem and OperationSystem Stubs you should
-- create instance of this class or of FileSystem before require all another
-- modules which can require luv and another packages works with io
--
--
local class = require("oop.class")
local sid = require("stub.id")
local uvim = require("stub.vim.utils")
local OS = require("stub.os.OSystem")

class.package 'stub.vim'

---@class stub.vim.NVim: oop.Object
---@field new fun(self): stub.vim.NVim
---@field o table -- api state
---@field fs stub.os.FileSystem?
---@field os stub.os.OSystem?
local M = class.new_class(nil, 'NVim', {
  original_vim = nil,
  original_api = nil,
  original_ui = nil,
  original_fn = nil,
  executed_vim_cmd = {},
})

M._VERSION = "stub v0.10.4"
M._URL = "https://gitlab.com/lua_rocks/stub"

M.TESTING_TAG = 'testing-api'
M.DEF_WINNR = 1000

-- aliase for dprint
M.D = require("dprint") -- for nvim.D.enable()
local dprint = M.D.mk_dprint_for(M, true)
local E = {}


-- aliase for alogger

-- aliase for logger.logger_setup
-- public static
M.logger_setup = function(quiet, lvl, save)
  local ok, module = pcall(require, 'alogger')
  if ok and type(module) == 'table' then
    return module.fast_setup(quiet, lvl, save) -- debug
  end
  return false
end

M.logger_log_to_mem = function(lvl)
  local ok, module = pcall(require, 'alogger')
  if ok and type(module) == 'table' then
    return module.fast_log_to_mem(lvl)
  end
  return false
end

M.logger_last_msg = function(offset)
  offset = offset or 0
  local ok, log = pcall(require, 'alogger')
  if ok and type(log) == 'table' then
    return log.last_msg(offset)
  end
  return nil
end

-- + clear messages
M.logger_off = function()
  local ok, module = pcall(require, 'alogger')
  if ok and type(module) == 'table' then
    return module.fast_off()
  end
  return false
end




-- constructor
function M:_init()
  local id = sid.next_id()
  dprint(1, 'mk_vim_api id:', id)

  self:backup_original_vim_api()
  _G.vim = _G.vim or {}
  self:mk_vim_stubs()

  self.o = M.new_state(nil, id)

  _G.vim.api = self:mk_api_stub()

  _G.vim.ui = self:mk_ui_stub()

  return _G.vim.api
end

--
-- complain on try use like: NVim:init() insted nvim:init()
-- check is self is instance not a Class
--
---@param self any
---@param msg string?
local function validate_instance(self, msg, ...)
  if msg then dprint(msg, ...) end
  assert(class.is_object(self), 'self must be an instance not a class')
end

--
---@return table
function M:get_state()
  return self.o
end

--
---@return self
function M:clear_state()
  dprint("clear_state")
  self.o = M.new_state(nil, sid.next_id())
  return self
end

--
---@return  stub.os.OSystem
function M:get_os()
  if not self.os then
    dprint('Create new OS')
    self.os = OS:new() ---@type stub.os.OSystem
  end
  return self.os
end

--
---@return self
function M:init_os()
  validate_instance(self, 'init_os')

  self:get_os()

  return self
end

--
-- clear the state of self, fs and  os if exists
--
---@param full boolean?
---@param with_fs boolean?
---@return self
function M:clear_all_state_with_os(full, with_fs)
  validate_instance(self, 'clear_all_state_with_os')

  self:clear_state()

  with_fs = with_fs ~= nil and with_fs or true
  if self.os then self.os:clear_state(full, with_fs) end

  return self
end

--
--
-- deligate for (stub.os.OSystem and stub.osFileSystem) self.os
function M:restore_original_os()
  validate_instance(self, 'restore_original_fs')
  if self.os then
    self.os.fs:restore_original()
    self.os:restore_original()
    self.os = nil
  end
  return self
end

--
-- open given path (in emulated FS) in the new nvim buffer
-- NVim + OSystem
--
-- :edit
---@param path string
---@return number
---@param lnum number?
---@param col number?
function M:open(path, lnum, col)
  local bufnr = self:next_bufnr()
  dprint(1, "new_buf", bufnr, path)
  local _os = self:get_os()
  if not _os:is_file_exits(path, 'file') then
    error('file not found' .. tostring(path))
  end

  local win = self:get_win(0, {})
  win.current_bufnr = bufnr
  self.set_cursor_pos(win, { lnum or 1, col or 1 })
  self.o.bufs[bufnr] = M._new_buf(bufnr, _os:get_file_lines(path), path)

  return bufnr
end

-- create new buf with given settings
---@param bufnr number?
---@param lines table?
---@param lnum number?
---@param col number?
---@param bufname string?
---@param emulate_file boolean? - emulate existed file via FileSystem
function M:new_buf(lines, lnum, col, bufnr, bufname, emulate_file)
  bufnr = bufnr or self:next_bufnr()
  lnum = lnum or 1
  col = col or 0
  dprint(1, "new_buf", bufnr, 'at:', lnum, col)
  local win = self:get_win(0, {})
  win.current_bufnr = bufnr
  self.set_cursor_pos(win, { lnum, col })

  self.o.bufs[bufnr] = M._new_buf(bufnr, lines, bufname)

  if type(bufname) == 'string' and emulate_file then
    self:get_os():set_file(bufname, lines)
  end

  return bufnr
end

-- aliase for api.nvim_buf_get_lines
-- get lines from buffer 0 - from current
function M:get_lines(bufnr, ls, le, strict)
  bufnr = bufnr or 0
  ls = ls or 0
  le = le or -1 -- all
  strict = strict or false
  return self.api.nvim_buf_get_lines(bufnr, ls, le, strict)
end

--
-- to emulate line selection in the given buffer
--
---@param bufnr number
---@param lnum number
---@param lnum_end number
function M:set_selection_visual(bufnr, lnum, lnum_end)
  local winnr = 0
  self.api.nvim_set_mode0(winnr, bufnr, 'v')
  self.api.nvim_win_set_cursor(0, { lnum_end, 1 })
  _G.vim.fn.setpos('v', { 0, lnum, 1, 0 })
end

--------------------------------------------------------------------------------
--                           vim.ui.input
--------------------------------------------------------------------------------

-- make sure ui_input initialized
function M:init_ui_input()
  self.o.ui_input = self.o.ui_input or {}
  self.o.ui_input.data = self.o.ui_input.data or {}
  self.o.ui_input.opts = self.o.ui_input.opts or {} -- prompt, default, completion
  self.o.ui_input.next = self.o.ui_input.next or 1
end

-- input string for vim.ui.input
-- nil - for cancel input
---@param data string|nil
function M:add_ui_input(data)
  self:init_ui_input()
  local i = #self.o.ui_input.data + 1
  self.o.ui_input.data[i] = data or false
end

-- return data what will be passed as input on call vim.ui.input
function M:get_next_ui_input()
  if self.o.ui_input then
    return self.o.ui_input.data[self.o.ui_input.next or 0]
  end
  return false
end

--
-- return showed on vim.ui.input opts (prompt, defualt etc)
-- opts - is a data what will be shown for user on vim.ui.input call
--
---@param offset number? offset from latest opts
---@return string?
function M:get_ui_input_opts(offset)
  if self.o.ui_input then
    local i = #self.o.ui_input.opts - (offset or 0)
    return self.o.ui_input.opts[i]
  end
end

function M:get_next_ui_input_index()
  if self.o.ui_input then
    return self.o.ui_input.next
  end
  return nil
end

---@param opts table?
function M:pop_ui_input(opts)
  -- opts: { prompt = message, default = default, completion = completion },
  local input = self.o.ui_input
  if input and input.next <= #input.data then
    if input.next then
      input.opts[input.next] = opts
      local line = input.data[input.next]
      input.next = input.next + 1
      dprint('pop_ui_input next:', input.next, line)
      return line
    end
  end
  return false
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

---@param winnr number
---@return table
local function _new_win(winnr)
  return {
    winnr = winnr,
    current_bufnr = false,
    cursor_pos = { 1, 0 },
  }
end

---@param bufnr number
---@return table
---@param name string?
function M._new_buf(bufnr, lines, name)
  return {
    bufnr = bufnr,
    name = name or 'No Name',
    lines = lines or {},
    loaded = true, --?
    options = {
      modifiable = true,
    }
    -- selected lines
  }
end

---@param o table?
---@return table
function M.new_state(o, id)
  o = o or {} -- state
  o.id = o.id or id
  o.wins = o.wins or {
    [M.DEF_WINNR] = _new_win(M.DEF_WINNR)
  }
  -- buffer belongs a specific window, but each buffer has the unique id(bufnr)
  o.bufs = o.bufs or {}
  o.executed_cmds = o.executed_cmds or {} -- nvim_exec
  o.next_bufnr = o.next_bufnr or 0

  -- vim.ui.input
  o.input = nil
  --
  o.notify_msgs = nil -- use self.clear_nitify to init

  return o
end

-- to keep original _G.vim and _G.vim.api if has
---@param mk_testing_stub boolean? if true keep _G.vim not nil ouside nvim
function M:backup_original_vim_api(mk_testing_stub)
  local prev_api = (_G.vim or E).api
  local prev_ui = (_G.vim or E).ui
  local prev_fn = (_G.vim or E).fn
  if not prev_api or (prev_api and prev_api.__tag ~= M.TESTING_TAG) then
    dprint('[DEBUG] update original vim.api ', ((_G.vim or E).api or E).__tag)
    self.original_vim = _G.vim
    self.original_api = prev_api
    self.original_ui = prev_ui
    self.original_fn = prev_fn
  end

  if mk_testing_stub and not _G.vim then
    _G.vim = {}
    _G.vim.api = { __tag = 'stub' }
    _G.vim.ui = { __tag = 'stub' }
    self:mk_vim_stubs()
  end
end

--
-- wrap Global vim.api to api wrapper
--
function M:mk_vim_api()
end

function M:restore_original_vim_api()
  dprint(1, "restore_original_vim_api")
  assert(not self.original_api or self.original_api.__tag ~= M.TESTING_TAG, 'sure original')
  _G.vim = self.original_vim
  if _G.vim then
    _G.vim.api = self.original_api or {}
    _G.vim.ui = self.original_ui or {}
    _G.vim.fn = self.original_fn or {}
  end
end

-- TODO
function M:mk_ui_stub()
  return {
    __tag = M.TESTING_TAG,
    ---@param opts table
    ---@param on_confirm function
    input = function(opts, on_confirm)
      opts = opts or {}
      on_confirm = on_confirm or function() end
    end,
  }
end

--------------------------------------------------------------------------------


--
--
--
function M:mk_vim_stubs()
  _G.vim = _G.vim or {}
  ---@diagnostic disable-next-line: duplicate-set-field
  _G.vim.in_fast_event = function() return false end
  _G.vim.notify = self:mk_notify_proxy()

  _G.vim.fn = _G.vim.fn or {}

  ---@diagnostic disable-next-line: unused-local
  _G.vim.fn.setreg = function(regname, value, opt)
    --
  end
  ---@diagnostic disable-next-line: unused-local
  _G.vim.fn.getreg = function(regname)
  end

  ---@diagnostic disable-next-line: unused-local
  ---@param s string
  ---@param opts table
  _G.vim.fn.setpos = function(s, opts)
    assert(type(s) == 'string', 's')
    assert(type(opts) == 'table', 'opts')
    --("'<", { self.bufnr, lnum, 1, 0 })
    local buf = assert(self:get_buf(self:get_win(0), 0), 'current buff')
    buf.posmap = buf.posmap or {}
    buf.posmap[s] = opts
    if s == '.' then -- current position
      self:get_win(0, {}).cursor_pos = { opts[2], opts[3] }
    end
  end

  _G.vim.fn.getpos = function(s)
    local buf = assert(self:get_buf(self:get_win(0), 0), 'current buff')
    buf.posmap = buf.posmap or {}
    local t
    if s == '.' then
      local cp = (self:get_win(0) or E).cursor_pos
      if cp then
        t = { 0, cp[1], cp[2], 0 }
      end
    else
      t = buf.posmap[s]
    end
    return t
  end

  --
  _G.vim.schedule_wrap = function(func)
    func()
  end
  ---@diagnostic disable-next-line: unused-local
  _G.vim.cmd = function(line)
    self.o.executed_vim_cmd = self.o.executed_vim_cmd or {}
    self.o.executed_vim_cmd[#self.o.executed_vim_cmd + 1] = line
    -- TODO handler
  end
end

--
--
---@return function
function M:mk_notify_proxy()
  ---@diagnostic disable-next-line: unused-local, duplicate-set-field
  return function(msg, log_level, notify_opt)
    msg = msg or ''
    if msg:sub(-1, -1) == "\n" then
      msg = msg:sub(1, -2)
    end
    local t = self.o.notify_msgs
    if t then
      t[#t + 1] = msg
    else
      print(msg) -- just to StdOut
    end
  end
end

-- can used as init
---@return self
function M:clear_nitify()
  self.o.notify_msgs = {}
  return self
end

---@return table?
function M:get_nitify_msgs()
  return self.o.notify_msgs
end

---@return table
function M:get_executed_vim_cmd()
  return self.o.executed_vim_cmd
end

-- -- the lines in nvim itself stored with indexes stated from 0 not from 1
-- lines = lines or {}  -- this lines starts from 1 not from 0
-- -- cursor_pos -- starts from 1 not from 0
-- linenr = linenr or 0 -- cursor
-- colnr = colnr or 0

-- Note: lines in the nvim itself starts from 0,
-- but cursor_pos from 1 and line numbers shown in the editor itself also
-- starst from 1
-- here lstart and lend tacked started from 0 but convert to start from 1
---@param lines table
local function normalize(lines, ln_start, ln_end, strict)
  if ln_start < 0 then ln_start = #lines + (ln_start + 1) end
  if ln_end < 0 then ln_end = #lines + (ln_end + 1) end
  dprint('ln_start:', ln_start, 'ln_end:', ln_end)
  if ln_start > #lines then
    if strict then
      error('Index out of bounds lines:' .. #lines .. ' ln_start: ' .. ln_start)
    end
    ln_start = #lines - 1 --?
  end
  if ln_end > #lines then
    if strict then
      error('Index out of bounds lines:' .. #lines .. ' ln_end: ' .. ln_end)
    end
    ln_end = #lines - 1 --?
  end


  -- convert indexes stated from 0 to started from 1
  ln_start, ln_end = ln_start + 1, ln_end + 1

  if strict and (ln_start - 1 > #lines or ln_end - 1 > #lines or
        ln_start < 0 or ln_end < 0) then
    error('Index out of bounds')
  end
  return ln_start, ln_end
end

function M:get_actual_winnr(winnr)
  assert(type(winnr) == 'number', 'winnr')
  if winnr == 0 then
    local o = self.o
    o.current_winnr = o.current_winnr or M.DEF_WINNR
    return o.current_winnr
  end
  return winnr
end

function M:set_current_winnr(winnr)
  assert(type(winnr) == 'number', 'winnr')
  local o = self.o
  o.current_winnr = self:get_actual_winnr(winnr)
  return o.current_winnr
end

---@param winnr number - window handler 0 replaced to current_winnr
---@param def table|nil
function M:get_win(winnr, def)
  assert(def == nil or type(def) == 'table', 'def can be table or nil')
  winnr = self:get_actual_winnr(winnr)
  local o = self.o
  o.wins = o.wins or {}
  o.wins[winnr] = o.wins[winnr] or def --new_win(winnr)
  return o.wins[winnr]
end

---@param win table
---@param bufnr number
function M:get_actual_bufnr(win, bufnr)
  assert(type(win) == 'table', 'win')
  assert(type(bufnr) == 'number', 'bufnr')
  if bufnr == 0 then
    return win.current_bufnr -- can there be a window without a buffer?
  end
  return bufnr
end

---@param win table
---@param bufnr number
function M:get_buf(win, bufnr)
  local o = self.o
  bufnr = self:get_actual_bufnr(win, bufnr)
  if bufnr then
    local buf = o.bufs[bufnr] -- or new_buf(bufnr)
    return buf, win
  end
  return nil
end

--
---@param win table
---@param cursor_pos table{lnum, col}
function M.set_cursor_pos(win, cursor_pos)
  assert(type(win) == 'table', 'win')
  local row, col = cursor_pos[1], cursor_pos[2]
  assert(type(row) == 'number', 'row')
  assert(type(col) == 'number', 'col')

  win.cursor_pos = win.cursor_pos or {}
  win.cursor_pos[1], win.cursor_pos[2] = row, col

  dprint(1, 'set_cursor_pos winnr:', win.winnr, 'row:', row, 'col:', col)
end

--
--
function M.get_cursor_row(win)
  return win and win.cursor_pos and win.cursor_pos[1] or nil
end

--
--
function M:next_bufnr()
  self.o.next_bufnr = self.o.next_bufnr or 0
  self.o.next_bufnr = self.o.next_bufnr + 1
  return self.o.next_bufnr
end

---@param cursor_pos table
---@param lines table
function M.validate_cursor_pos(cursor_pos, lines)
  local lnum, col = cursor_pos[1], cursor_pos[2]

  if lnum < 1 or col < 0 or lnum > #(lines or E) then
    error('Cursor position outside buffer')
  end
  if lines then
    local line = lines[lnum] or ''
    if col >= #line then
      error('Cursor position outside buffer')
    end
  end

  return cursor_pos
end

--------------------------------------------------------------------------------

function M:mk_api_stub()
  dprint(1, "mk_api_stub")
  local o = self.o -- state

  self.api = {
    __tag = M.TESTING_TAG,

    -- nvim_get_api_info = function() dprint(1, "nvim_get_api_info") end,

    nvim_get_current_buf = function()
      local bufnr = self:get_actual_bufnr(self:get_win(0), 0)
      dprint(1, "nvim_get_current_buf ret:", bufnr)
      return bufnr
    end,

    nvim_buf_get_name = function(bufnr)
      local buf = self:get_buf(self:get_win(0), bufnr)
      local name = (buf or E).name
      dprint(1, "nvim_buf_get_name ret:", name)
      return name
    end,

    nvim_list_bufs = function()
      local t, c = {}, 0
      for i, _ in pairs(o.bufs) do
        t[#t + 1] = i
        c = c + 1
      end
      dprint(1, "nvim_list_bufs ret: list.size:", c)
      -- list of bufnr idx-> bufnr
      return t
    end,

    nvim_buf_is_loaded = function(bufnr)
      local loaded = (self:get_buf(self:get_win(0), bufnr) or E).loaded
      dprint(1, "nvim_buf_is_loaded ret:", loaded)
      return loaded
    end,

    ---@return number
    nvim_get_current_win = function()
      local winnr = self:get_actual_winnr(0) -- 0 is current win
      dprint(1, "nvim_get_current_win ret:", winnr)
      return winnr
    end,

    nvim_set_current_win = function(winnr)
      local wnr = self:set_current_winnr(winnr)
      dprint(1, "nvim_set_current_win winnr:", winnr, '->:', wnr)
    end,

    nvim_win_set_buf = function(winnr, bufnr)
      dprint(1, "nvim_win_set_buf winnr:", winnr, 'bufnr:', bufnr)
      assert(type(bufnr) == 'number', 'bufnr')
      self:get_win((winnr)).current_bufnr = bufnr
    end,

    nvim_win_get_buf = function(winnr)
      local bufnr = (self:get_win(winnr) or E).current_bufnr
      dprint(1, "nvim_win_get_buf winnr:", winnr, 'ret: current_bufnr:', bufnr)
      return bufnr
    end,

    ---@diagnostic disable-next-line: unused-local
    nvim_win_get_cursor = function(winnr)
      local cp = (self:get_win(winnr) or E).cursor_pos
      dprint(1, "nvim_win_get_cursor winnr:", winnr,
        ' ret: ', 'row:', (cp or E)[1], 'col:', (cp or E)[2])
      return cp
    end,

    -- index starts from 1
    -- change_position
    ---@diagnostic disable-next-line: unused-local
    ---@param cursor_pos table (row, col)
    nvim_win_set_cursor = function(winnr, cursor_pos)
      assert(type(cursor_pos) == 'table' and #cursor_pos == 2, 'cursor_pos')
      dprint(1, "nvim_win_set_cursor winnr:", winnr,
        'row:', cursor_pos[1], 'col:', cursor_pos[2])
      M.set_cursor_pos(self:get_win(winnr), cursor_pos) -- rise error if win not exists
      return true
    end,

    ---@diagnostic disable-next-line: unused-local
    nvim_feedkeys = function(keys, mode, escape_ks)
      if keys and #keys == 1 then
        -- move cursor by keys
        local offt = uvim.DIRECTIONS_OFFSET[keys]
        if type(offt) == 'table' then
          local win = (self:get_win(0, {}))
          win.cursor_pos = win.cursor_pos or {}
          local new_pos = uvim.move(win.cursor_pos, offt)
          local lines = (self:get_buf(win, 0) or E).lines
          win.cursor_pos = M.validate_cursor_pos(new_pos, lines)
          --
          -- change mode to normal
        elseif keys == uvim.TERMCODE_ESC then
          local buf = assert(self:get_buf(self:get_win(0), 0), 'buff')
          buf.mode = 'n'
        end
      end
    end,

    nvim_replace_termcodes = uvim.replace_termcodes,

    nvim_win_set_option = function(winnr, optname, value)
      local win = self:get_win(winnr)
      win.options = win.options or {}
      assert(type(optname) == 'string', 'optname')
      win.options[optname] = value
    end,

    nvim_win_get_option = function(winnr, optname)
      local win = self:get_win(winnr)
      return (win.options or E)[optname or false]
    end,

    nvim_win_get_config = function(winnr)
      dprint("nvim_win_get_config", winnr)
      return self:get_win(winnr).config or {}
    end,

    -- lines index in nvim buffs starts from 0
    nvim_get_current_line = function()
      local buf, win = self:get_buf(self:get_win(0), 0)
      local row = M.get_cursor_row(win)

      local cline
      if buf and row then
        cline = buf.lines[row]
      end
      dprint(1, "nvim_get_current_line winnr:", (win or E).winnr,
        'bufnr:', (buf or E).bufnr, 'row:', row, 'ret current_line:', cline)
      return cline
    end,

    nvim_del_current_line = function()
      error('TODO')
    end,

    ---@diagnostic disable-next-line: unused-local
    ---@return number
    nvim_buf_line_count = function(bufnr)
      if not bufnr or type(bufnr) ~= 'number' then
        error 'Expected 1 argument'
      end
      local buf = self:get_buf(self:get_win(0), bufnr)
      local count = buf and #(buf.lines or E) or 0
      dprint(1, "nvim_buf_line_count bufnr:", bufnr, 'ret: lines:', count)
      return count
    end,

    -- works: [lstart, lend)
    ---@param bufnr number
    ---@param lstart number   -- index from 0
    ---@param lend number     -- index from 0 (not including
    ---@param strict boolean
    ---@return table
    ---@diagnostic disable-next-line: unused-local
    nvim_buf_get_lines = function(bufnr, lstart, lend, strict)
      dprint(2, "nvim_buf_get_lines bufnr:", bufnr, 'ls:', lstart, 'le:', lend,
        'strict:', strict
      )
      local res_lines = nil
      local lines = (self:get_buf(self:get_win(0), bufnr) or E).lines

      if lines and lstart and lend then
        lstart, lend = normalize(lines, lstart, lend, strict) -- +1 inside

        local t = {}
        for i = lstart, lend - 1, 1 do
          if i > #lines then
            if strict then
              error('Index out of bounds')
            end
            break
          end
          table.insert(t, lines[i])
        end
        res_lines = t
      end

      dprint('nvim_buf_get_lines ret: lines:', (res_lines and #res_lines or nil))
      return res_lines or {}
    end,

    -- Note in nvim lines index starst from 0,
    -- but cursor_pos from 1 and linenumbers showed in editor from 1
    -- ({buffer}, {start}, {end}, {strict_indexing}, {replacement})
    -- range works: [lstart, lend)
    ---@param lstart number  index from 0
    ---@param lend number    index from 0
    ---@param strict boolean throw error on out-of-bounds
    ---@diagnostic disable-next-line: unused-local
    nvim_buf_set_lines = function(bufnr, lstart, lend, strict, replacement)
      dprint(1, "nvim_buf_set_lines bufnr:", bufnr, 'ls:', lstart, 'le:', lend,
        'strict:', strict, 'replacement:', (replacement and #replacement or nil))

      assert(type(strict) == 'boolean', 'strict must be a boolean')
      local buf = self:get_buf(self:get_win(0), bufnr)
      assert(type(buf) == 'table', 'buf exists')
      local lines = buf.lines

      lstart, lend = normalize(lines, lstart, lend, strict)

      local t = {}
      -- before lstart
      for i = 1, lstart - 1 do
        local line = lines[i]
        dprint('BEFORE', i, line)
        t[i] = line
      end

      -- replacement
      for _, new_line in pairs(replacement) do
        dprint('REPLACEMENT', #t + 1, new_line)
        t[#t + 1] = new_line
      end

      -- after lend
      for i = lend, #lines do
        local line = lines[i]
        dprint('AFTER', #t + 1, line)
        t[#t + 1] = line
      end
      dprint('nvim_buf_set_lines ', ':inspect:', lines)
      buf.lines = t    -- change instance of a container
      return buf.lines -- testing
    end,

    --
    --
    ---@param bufnr number
    ---@param optname string
    nvim_buf_get_option = function(bufnr, optname)
      local buf = self:get_buf(self:get_win(0), bufnr)
      assert(type(buf) == 'table', 'the buf must exist')
      assert(type(optname) == 'string', 'optname')

      local value = buf.options[optname]
      if optname == '' then
        return true
      end
      dprint(1, "nvim_buf_get_option", bufnr, optname, 'ret:', value)
      return value
    end,

    nvim_buf_set_option = function(bufnr, optname, value)
      local buf = self:get_buf(self:get_win(0), bufnr)
      assert(type(buf) == 'table', 'the buf must exist')
      assert(type(optname) == 'string', 'optname')
      buf.options[optname] = value
      dprint(1, "nvim_buf_set_option", bufnr, optname, 'value:', value)
    end,

    --
    nvim_buf_set_var = function(bufnr, varname, value)
      assert(type(bufnr) == 'number', 'bufnr')
      assert(type(varname) == 'string', 'varname')
      local buf = self:get_buf(self:get_win(0), bufnr)
      assert(type(buf) == 'table', 'the buf must exist')

      dprint(1, "nvim_buf_set_var bufnr:", bufnr, 'varname:', varname, 'v:', value)
      buf.vars = buf.vars or {}
      buf.vars[varname] = value
    end,

    --
    nvim_buf_get_var = function(bufnr, varname)
      assert(type(bufnr) == 'number', 'bufnr')
      assert(type(varname) == 'string', 'varname')
      local buf = self:get_buf(self:get_win(0), bufnr)
      assert(type(buf) == 'table', 'the buf must exist')

      local value = (buf.vars or E)[varname]
      dprint(1, "nvim_buf_get_var bufnr:", bufnr, 'vn:', varname, 'v:', value)
      return value
    end,

    -- aliase for vim.cmd ?
    ---@param src string
    nvim_exec = function(src, output)
      dprint(1, "nvim_exec", src, output)
      output = output or false --?
      if src and o then
        o.executed_vim_cmd = o.executed_vim_cmd or {}
        table.insert(o.executed_vim_cmd, src)
        -- todo emulate commands and actions?
      end
      return src
    end,

    --
    ---@diagnostic disable-next-line: unused-local
    nvim_buf_set_keymap = function(bufnr, mode, lhs, rhs, opts0)
      dprint(1, "nvim_buf_set_keymap bufnr:", bufnr, mode, lhs, rhs, opts0)
      dprint("no-op")
    end,

    ---@diagnostic disable-next-line: unused-local
    nvim_buf_get_keymap = function(bufnr, mode)
      dprint(1, "nvim_buf_get_keymap bufnr:", bufnr, mode)
      dprint("no-op")
    end,

    --  Creates a new, empty, unnamed buffer.
    ---@param listed boolean
    ---@param scratch boolean -   Creates a "throwaway" |scratch-buffer| for
    --                            temporary work
    ---@return number (buffer handle or 0 on error)
    ---@diagnostic disable-next-line: unused-local
    nvim_create_buf = function(listed, scratch)
      dprint(1, "nvim_create_buf listed:", listed, 'scratch:', scratch)
      -- todo options?
      return self:new_buf({}, 1, 0, nil, nil)
    end,

    nvim_get_mode = function()
      local buf = self:get_buf(self:get_win(0), 0)
      return { blocking = false, mode = (buf or E).mode or 'n' }
    end,

    -- debuggin: there is no such method in the original API
    nvim_set_mode0 = function(winnr, bufnr, mode)
      local buf = assert(self:get_buf(self:get_win(winnr), bufnr), 'buff')
      buf.mode = mode
    end,

    -- autocommands
    ---@return number id of created autogroup
    ---@diagnostic disable-next-line: unused-local
    nvim_create_augroup = function(name, opts)
      return 0
    end,

    ---@param event string|table
    ---@return number id of created autocmd
    ---@diagnostic disable-next-line: unused-local
    nvim_create_autocmd = function(event, opts)
      return 0
    end,

    ---@param id number
    ---@diagnostic disable-next-line: unused-local
    nvim_del_autocmd = function(id)
      return 0
    end,
  }

  return self.api
end

--------------------------------------------------------------------------------
--                                vim.ui
--------------------------------------------------------------------------------

--
--
--
function M:mk_ui_stub()
  dprint(1, "mk_ui_stub")

  self.ui = {
    __tag = M.TESTING_TAG,

    --- vim.ui.input
    ---@param opts table { prompt, default, completion }
    ---@param on_confirm function
    input = function(opts, on_confirm)
      assert(type(opts) == 'table', 'opts')

      local line = self:pop_ui_input(opts)
      if type(line) == 'string' then
        assert(type(on_confirm) == 'function', 'on_confirm')
        on_confirm(line)
      end
    end
  }
  return self.ui
end

class.build(M)
return M
