-- 10-04-2024 @author Swarg
local M = {}
--

M.DIRECTIONS_NAME = {
  h = 'L', j = 'D', k = 'U', l = 'R'
}
-- lnum, col
M.DIRECTIONS_OFFSET = {
  h = { 0, -1 }, j = { 1, 0 }, k = { -1, 0 }, l = { 0, 1 }
}


---@param keys string
---@param from_part boolean
---@param do_lt boolean
---@param special boolean
---@diagnostic disable-next-line: unused-local
function M.replace_termcodes(keys, from_part, do_lt, special)
  local m1, m2, letter

  if keys == '<esc>' then return string.char(27) end

  letter = string.match(keys, '^(%w)$')
  if letter then
    return letter
  end
  local abyte = string.byte('a', 1, 1)

  m1, letter = string.match(keys, '^<(%w)%-(%w)>$')
  -- <c-a> -> "\1",   <c-z> -> "\26"
  if m1 and letter then
    local lb = string.byte(letter, 1, 1)

    if m1 == 'c' then     -- control
      return string.char(lb - abyte + 1)
    elseif m1 == 'a' then -- alt
      -- <a-a> { 128, 252, 8, 97 }
      return string.char(128, 252, 8, 97 + (lb - abyte))
    end
  end

  m1, m2, letter = string.match(keys, '^<(%w)-(%w)%-(%w)>$')

  if m1 == 'c' and m2 == 'a' and letter then
    local lb = string.byte(letter, 1, 1)
    return string.char(128, 252, 8, 1 + (lb - abyte))
  end

  local fn = tonumber(string.match(keys, '^<f(%d%d?)>$'))
  if fn then
    if fn >= 1 and fn <= 10 then
      return string.char(128, 107, 48 + fn)
    elseif fn > 10 then
      return string.char(128, 70, 38 + fn) -- f11 -> 49
    end
  end
  -- <c-a-a>  -> "<80><fc>\b\1"
end

---@param cursor_pos table
---@param offt table
function M.move(cursor_pos, offt)
  local lnum = cursor_pos[1] + offt[1]
  local col = cursor_pos[2] + offt[2]
  return { lnum, col }
end

M.TERMCODE_ESC = M.replace_termcodes('<esc>', true, false, true) -- \27

return M
