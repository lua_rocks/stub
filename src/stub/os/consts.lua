-- 07-02-2024 @author Swarg
local M = {}

M.STDIN = 0
M.STDOUT = 1
M.STDERR = 2

-- constanst from c-lang header-files used by IO ops

-- from /usr/include/asm-generic/errno-base.h:
M.ERRNO = {
  ENOENT = { n = 2, s = "No such file or directory" },
  EBADF  = { n = 9, s = "Bad file descriptor" }, --  Bad file number
  EINVAL = { n = 22, s = "Invalid argument" },
  EISDIR = { n = 21, s = 'Is a directory' },

}

--------------------------------------------------------------------------------

M.SUBSTITUTE_PATTERN = "%${([%w_]+)}" -- ${KEY} ${THE_KEY}

--
-- build string from template based substitutions
-- all keys with false value will tryed to removed with empty line
--
-- in template keys must be specified in this manear:
-- "${KEY}=..." in the "substitutions" table  keys must be without "${" and "}"
-- See ComponentGen.SUBSTITUTE_PATTERN
--
---@param template string
---@param substitutions table
---@return string
function M.substitute(template, substitutions)
  assert(type(template) == 'string', 'expected string template')

  local kv_map = substitutions

  -- normalize template - remove lines with placeholders what not provided
  for key, value in pairs(kv_map) do
    if not value or value == '' then
      -- remove whole lines this one placeholder to empty value
      local p = "\n%${" .. key .. "}\n" -- \n
      if string.match(template, p) then
        template = string.gsub(template, p, "\n")
      else
        kv_map[key] = ''
      end
    end
  end

  local s = string.gsub(template, M.SUBSTITUTE_PATTERN, kv_map)

  -- remove empty lines with whitespaces only "..   \n" -> "..\n"
  -- %s consume and \n too that's why use only spaces('% ') here, not '%s'
  s = string.gsub(s, "% +\n", "") -- ! spaces + \n
  return s
end

--------------------------------------------------------------------------------
--          to be used inside stub/os/bin/executable
--------------------------------------------------------------------------------

-- helper
function M.is_file_exists(proc, filename)
  local h = proc:c_fopen(filename, "r")
  if h then
    h:close()
    return true
  end
  return false
end

--
function M.write_to_file(proc, filename, content, mode)
  local h = proc:c_fopen(filename, mode)
  -- deprecated way: proc.os.fs:write(fn, data, mode, proc.ug)
  if h then
    proc:c_fprintf(h, content)
    proc:c_fclose(h)
    return true
  end
  return false
end

-- test helper
---@param sys stub.os.OSystem
---@param cwd string?
---@param ug stub.os.UG?
---@param parent stub.os.Proc?
---@return stub.os.Proc
function M.mkProc(sys, cwd, ug, parent)
  assert(type(ug) == 'table' or ug == nil, 'ug')
  parent = parent or sys.o.p_init

  return sys.o.p_init:c_fork({
    cwd = cwd,
    errno = 0,
    ug = ug or sys.o.p_init.ug,
  })
end

--------------------------------------------------------------------------------

return M
