-- 05-02-2024 @author Swarg
-- permissions user-id, group-id


---@class stub.os.UG
---@field of fun(uid:number?, gid:number?): stub.os.UG
---@field uid number
---@field gid number
local M = {}


M.DEFAULT_USER_ID = 1000
M.DEFAULT_GROUP_ID = 1000

--
-- factory to create
--
---@param uid number?
---@param gid number?
---@return stub.os.UG
function M.of(uid, gid)
  local ug = {}
  ug.uid = uid or 0
  ug.gid = gid or 0
  return ug
end

-- regular user not a root
-- with uid = M.DEFAULT_USER_ID
---@return stub.os.UG
function M.default()
  return { uid = M.DEFAULT_USER_ID, gid = M.DEFAULT_GROUP_ID }
end

---@return stub.os.UG
function M.root()
  return { uid = 0, gid = 0 }
end

return M
