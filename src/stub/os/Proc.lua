-- 04-02-2024 @author Swarg
-- Goal: to emulate external process with stdin stdout stderr
-- That's why mode inverted stdin has not 'r' but 'w' and stdout has 'r'
-- stdout.mode == 'r'  means "read from process output"
--
local class = require("oop.class")

local IOHandle = require("stub.os.IOHandle")
local IOPipe = require("stub.os.IOPipe")
local C = require("stub.os.consts")
local UG = require("stub.os.UG")
local U = require("stub.os.ioutils")

local D = require("dprint")


class.package 'stub.os'
---@class stub.os.Proc : oop.Object
---@field new fun(self, o:table): stub.os.Proc -- o:{os:stub.os.OSystem, stdin:stub.os.IOHandle, stdout:stub.os.IOHandle, stderr:stub.os.IOHandle, ug:stub.os.UG?, args:stub.os.Args, envs:table?})
---@field pid number   - process id
---@field ppid number  - parent process id
---@field os stub.os.OSystem
---@field cwd string
---@field args stub.os.Args?
---@field fds table  opened file discriptors nr-> stub.os.IOHandle
---@field exitcode number
--- trick to store sys state for working with io
---@field errno number?      https://man7.org/linux/man-pages/man3/errno.3.html
---@field last_err string?
local M = class.new_class(nil, 'Proc', {
})

local instanceof = class.instanceof
local fmt = string.format
local dprint = D.mk_dprint_for(M)
local E = {}

--
-- Constructor
--   p = new Proc(t:table{
---    _os: stub.os.OSystem,
---    stdin: stub.os.IOHandle,
---    stdout: stub.os.IOHandle,
---    stderr: stub.os.IOHandle,
---    args: stub.os.Args,
---    envs: table?,
---    ug: stub.os.UG? {uid, gid},
--     fds: table?
-- }
function M:_init() --_os, stdin, stdout, stderr, ug, args, envs)
  -- self.pid = self.pid or sid.next_id()
  local o = self or {}
  dprint(3, "\n -> newProc pid:", o.pid, 'uid:', (o.ug or E).uid, o.args)

  self.fds = self.fds or {} -- mapping fd --> stub.io.Handle
  self.envs = self.envs or {}
  self.ug = self.ug or UG.default()
  self.cwd = self.cwd or '/' -- todo _os get userhome dir
  --
  assert(type(self.os) == 'table', 'expected os, got: ' .. type(self.os))

  self.fds = self.fds or {}

  -- self.exitcode = nil -- nil is a sign that the process still alive(working)
  self.errno = 0 -- used to emulate c stdio erros for this process
  self.last_err = nil
  dprint(" <- newProc ", M._status, '(', self, true, ')', "\n")
end

--
-- NOTE: throw error if this process does not has self.fds[C.STDIN]
--
---@return stub.os.IOHandle
function M:stdin()
  return IOHandle.cast(M.cast(self).fds[C.STDIN], 'stdin')
end

--
-- NOTE: throw error if this process does not has self.fds[C.STDOUT]
--
---@return stub.os.IOHandle
function M:stdout()
  return IOHandle.cast(M.cast(self).fds[C.STDOUT], 'stdout')
end

--
-- NOTE: throw error if this process does not has self.fds[C.STDOUT]
--
---@return stub.os.IOHandle
function M:stderr()
  return IOHandle.cast(M.cast(self).fds[C.STDERR], 'stderr')
end

--
--
---param self stub.os.Proc
---param h stub.os.IOHandle
-- function M:handler_on_cannot_write(h)
--   if self and h then
--     self:set_errno(C.ERRNO.EBADF, '%s: %s', h._get_file_name(h, ''))
--   end
-- end

--
-- bind only a new created IOHandle to self.tds
--
-- By design, this method is only for linking IOHandles that have not yet been
-- bound to any Proc
--
---@param h stub.os.IOHandle
---@param fd number? defualt is #self.tds+1
---@return stub.os.IOHandle
---@return number            index in self.fds
function M:bind(h, fd)
  fd = fd ~= nil and fd or (#self.fds + 1)

  local prev_h = self.fds[fd]
  if prev_h then --
    -- an attempt to bind a handle to an already binded fd (use c_dup+c_close)
    error('its forbidden to overwrite already opened fd' .. tostring(fd))
  end

  -- IOHandle.bus.subscribe(h, 'cannot_write', M.handler_on_cannot_write, self)
  -- bridge from IOHandle to Proc via callback
  h.errno_cb = function(terrno, format, ...)
    self:set_errno(terrno, format, ...)
  end

  --[[

  -- local prev_fd = h.fd
  if h.fd ~= nil and h.proc then
    local msg = fmt("FORBIDDEN: trying to link an already linked IOHandle\n" ..
      " pid: %s fd:%s h:%s %s", tostring(h.proc.pid), tostring(h.fd), tostring(h),
      tostring(D.is_enabled(M)))

    dprint(msg)
    -- error(msg)
  end
]]
  self.fds[fd] = IOHandle.cast(h, tostring(fd)) --:bind(self, fd)

  -- for redirect io like open file and place it to std{io|out}
  -- if prev_fd and prev_fd > h.fd and self.fds[prev_fd] == h then
  --   dprint("pid:", self.id, 'close previous opened fd:', prev_fd)
  --   self.fds[prev_fd] = nil
  -- end

  return h, fd
end

--
-- factory to create new Process
-- to use inside the process itself(in emulated program works inside process)
-- intended for testing and for shorthand to create predefined process(fr-lines)
--
---@param _os stub.os.OSystem? -- its better to do it via the SystemAPI(syscalls)
---@param args stub.os.Args?
---@param ug stub.os.UG?
---@param lines_in table ?
---@param lines_out table ?
---@param lines_err table ?
function M.of(_os, args, ug, lines_in, lines_out, lines_err)
  return M:new({
    os = _os,
    fds = {
      [C.STDIN] = IOHandle.of(lines_in or {}, 'r'),
      [C.STDOUT] = IOHandle.of(lines_out or {}, 'w'),
      [C.STDERR] = IOHandle.of(lines_err or {}, 'w'),
    },
    ug = ug,
    args = args,
    -- envs =
  })
end

-- the moment the process is created when it starts
-- emulate create process on execute|run


---@param code number|boolean
---@return self
function M:with_exitcode(code)
  local ncode = 0 -- true
  if code == true then
    ncode = 0
  elseif code == false then
    ncode = 256
  else -- if type(code) ~= 'number' then
    ncode = tonumber(code) or 256
  end

  self.exitcode = ncode
  return self
end

---@param code number
function M:exit(code)
  dprint("exit pid:", assert(self, 'Proc expected').pid, 'exitcode:', code)
  self.exitcode = code

  -- close all opened files (to autocleanup tmpfiles from c_tmpfile)
  for fd, h in pairs(self.fds) do
    if not IOHandle._is_stdio(h) then
      self:c_close(fd)
    end
  end

  return self
end

-- ?
-- emulate join to process to wait for exit
--
---@return number -- exitcode
function M:wait()
  -- run self logic here
  return self.exitcode
end

--------------------------------------------------------------------------------
--           Helpers for acting on behalf of the process itself
--------------------------------------------------------------------------------


---
-- write to stderr
-- on behalf of the process itself
--
---param exitcode number
---param format string|table
---return false
function M:_err(exitcode, format, ...)
  assert((self.fds[C.STDERR]), 'has fd:2 (stderr)')
  self:c_printf(C.STDERR, format, ...)
  dprint(1, "_err", format)
  if exitcode then
    self.exitcode = exitcode
  end
  return false
end

---@param self stub.os.Proc
---@param verbose boolean?
function M._opened_fds(self, verbose)
  local s = "opened fd:"

  local keys = {}
  for k, _ in pairs(self.fds) do
    keys[#keys + 1] = k
  end

  table.sort(keys, function(a, b) return a < b end)

  if verbose then
    s = s .. "\n"
  else
    s = s .. " "
  end

  for i, fd in ipairs(keys) do
    if verbose then
      s = s .. self:_fd_status(fd) .. "\n"
    else
      if i > 1 then s = s .. ' ' end
      s = s .. tostring(fd) -- only fd nums
    end
  end
  return s
end

-- status of IOHandle + PipeId
---@param fd number?
function M:_fd_status(fd)
  if self and fd then
    local h = self.fds[fd or false]
    local pipeid, spipeid = IOPipe._find_pipeid_by_handler(h), ''
    if pipeid then spipeid = '  pipeid:' .. tostring(pipeid) end
    return IOHandle._status(h, fd) .. spipeid
  end
  return 'nil'
end

---@param self stub.os.Proc
---@param verbose boolean?
function M._streams_status(self, verbose)
  if self then
    local s = M._opened_fds(self, verbose)
    return s
  end
  return ''
end

---@param self stub.os.Proc
---@param verbose boolean?
function M._status(self, verbose)
  if self then
    local ts = tostring

    return fmt("pid: %s ppid: %s uid:%s %s\n",
          ts(self.pid), ts(self.ppid), ts((self.ug or E).uid), ts(self.args)) ..

        M._streams_status(self, verbose) ..

        fmt(" errno: %s(%s) exitcode:%s\n",
          ts(self.errno), ts(self.last_err), ts(self.exitcode))
  end

  return 'nil'
end

--
--
local function validate_os(self)
  assert(self and self.os and self.os.fs, 'has os and os.fs')
end


--------------------------------------------------------------------------------
-- Proc is a way to emulate run external processes runned via
-- io.popen, os.execute, ...
-- FileSystem is a wrapper around _G.io for the current process
-- to emulate handling errno things I keep errno and last_err in process itself
-- for external processes. in FileSystem only the field "last_err" for debugging


--
-- for inner use emulate field errno in std libc
--
---@param self table (stub.io.Proc | stub.io.FileSystem
---@param terrno table{N,S}
---@param format string?
function M.set_errno(self, terrno, format, ...)
  terrno = terrno or E
  self.errno = terrno.n or 0
  if format then
    self.last_err = string.format(format, ..., (terrno.s or ''))
  else
    self.last_err = terrno.s
  end
  return self.last_err -- c: strerr(errno)
end

--
---@return number
---@return string?
function M.get_errno(self)
  return self.errno, self.last_err
end

-- testing helper
---@param proc stub.os.Proc?
---@return string
function M._errno(proc)
  if proc then
    local n, s = proc:get_errno()
    return tostring(n) .. '|' .. tostring(s)
  end
  return 'proc is nil'
end

--
-- clear errno and last_err
--
function M.clear_last_err(self)
  assert(type(self) == 'table', 'self')
  self.errno = 0
  self.last_err = nil
end

--------------------------------------------------------------------------------
--                Helpers to emulated SystemAPI(SysCalls)
--------------------------------------------------------------------------------

--
-- file descriptor to emulated FILE*(stub.os.IOHandle)
-- return already opened iohandle (emulated FILE*) for given fd
--
---@param fd number
---@return stub.os.IOHandle?
function M:_fd2fh(fd)
  return self.fds[fd or false]
end

-- handle -> fd
--
---@param h stub.os.IOHandle?
---@return number? fd
function M:_fh2fd(h)
  if h then
    for k, v in pairs(self.fds) do
      if v == h then
        return k
      end
    end
  end
  return nil
end

--
-- get all fd`s (indexes in self.fds for given IOHandle
--
---@param h stub.os.IOHandle?
function M:_get_all_fd_for(h)
  local t = {}
  if h then
    for k, v in pairs(self.fds) do
      if v == h then
        t[#t + 1] = k
      end
    end
  end
  return t
end

-- withcount - number of how many fd point to specified handler
---@return number count
---@return number maxfd
function M:_fd_refcount(h)
  local c, maxfd = 0, -1
  if h then
    for k, v in pairs(self.fds) do
      if v == h then
        if k > maxfd then maxfd = k end -- max
        c = c + 1
      end
    end
  end
  return c, maxfd
end

---@param f number|stub.os.IOHandle?
---@return stub.os.IOHandle
function M:_toHandle(f)
  local h = f
  if type(f) == 'number' then
    h = self:_fd2fh(f)
    if not h then
      error('No such fd: ' .. tostring(f)) -- ?
    end
  end
  return IOHandle.cast(h)
end

function M.cast(proc, name)
  assert(instanceof(proc, M), 'expected ' .. tostring(name or '') .. ' Proc' ..
    ', got: ' .. tostring(proc))
  return proc
end

--
---@return number?
function M:_uid()
  return ((self or E).ug or E).uid
end

--------------------------------------------------------------------------------
--
--                  Emulated System API (System Calls)
--
--------------------------------------------------------------------------------

---@return stub.os.Proc
---@param t table?
function M:c_fork(t)
  local _os = assert(self.os, 'expected OSystem at self.os')
  local pid = assert(self.pid, 'has own pid')
  local child_pid = _os:next_pid()
  t = t or E
  dprint("c_fork pid:", pid, 'uid:', self:_uid(), 'child pid:', child_pid, ':inspect:', t)

  local p = M:new({
    os       = self.os,
    pid      = child_pid,
    ppid     = pid,
    args     = t.args or self.args or nil, -- Args:new({ cmdline = 'proc#' .. pid })
    ug       = t.ug or U.tbl_flat_copy(nil, self.ug),
    fds      = t.fds or U.tbl_flat_copy(nil, self.fds),
    envs     = t.envs or U.tbl_flat_copy(nil, self.envs),
    cwd      = t.cwd or self.cwd,
    errno    = t.errno or 0,
    last_err = t.last_err or nil,
  })

  if t.stdin then p.fds[C.STDIN] = t.stdin end
  if t.stdout then p.fds[C.STDOUT] = t.stdout end
  if t.stderr then p.fds[C.STDERR] = t.stderr end

  return p
end

--
---@param path string
---@return number 0 is success
function M:c_chdir(path)
  return self.os:c_chdir(self, path)
end

--
-- https://man7.org/linux/man-pages/man2/open.2.html
-- https://man7.org/linux/man-pages/man3/fopen.3.html
--
-- open, openat, creat - open and possibly create a file
-- int open(const char *pathname, int flags, ... /* mode_t mode */ );
--     ^ original system-call used by high-level wrapper(fopen)
--
-- Note: has extended behavior - return second value a file discriptor
--
---@return stub.os.IOHandle? like FILE* in c
---@return number? fd in self.fds
function M:c_fopen(pathname, mode)
  validate_os(self)
  local h, fd = self.os:c_fopen(self, pathname, mode) -- with self:bind(h, fd...)

  dprint('c_fopen ', pathname, mode, ' ret fh:', h ~= nil, 'fd:', fd)
  assert(not h or (fd == self:_fh2fd(h)), 'synced in self.fds')

  return h, fd
end

-- TODO origianl signature of pure open sys-call no mode buf via flags(nums)
-- O_CREATE ...
-- function M:c_open(pathname, mode) end

--
-- close a stream
-- int fclose(FILE *stream);
-- https://man7.org/linux/man-pages/man3/fclose.3.html
--
---@param handle stub.os.IOHandle?
---@return number
function M:c_fclose(handle)
  local cnt, maxfd = self:_fd_refcount(handle)

  if cnt <= 0 or maxfd < 0 then -- fd  errmsg
    self:set_errno(C.ERRNO.EBADF, '%s: %s', tostring(maxfd))
    return -1
  end

  if handle ~= self.fds[maxfd] then
    error('[DEBUG] unsynched fd:' .. tostring(maxfd) .. 'handle:' .. tostring(handle))
  end

  return self:c_close(maxfd)
end

---@param fd number
function M:c_close(fd)
  dprint(2, "pid:", self.pid, "c_close fd:", fd)
  local handle = self.fds[fd or false] -- self:_fd2fh(fd)

  if not handle or not fd then         --  fd  errmsg
    self:set_errno(C.ERRNO.EBADF, '%s: %s', tostring(fd))
    return -1
  end

  dprint("pid:", self.pid, 'before clear fd:', fd, self:_opened_fds(true))
  self.fds[fd] = nil

  local cnt, maxfd = self:_fd_refcount(handle)
  dprint("pid:", self.pid, "c_fclose fd:", fd, "refs:", cnt, 'maxfd:', maxfd, handle)
  if not cnt or cnt < 1 then
    local stdio = IOHandle._is_stdio(handle) -- shared stdio steams do not close
    dprint("pid", self.pid, 'close:', handle, 'is-stdio:', stdio)
    if not stdio then
      IOHandle.cast(handle):update_time():close()
    end
  end

  return 0 -- no err
end

-- read lines from given handler
-- return number of readed lines
--
---@param buffer table
---@param fd number|stub.os.IOHandle
---@return number
function M:c_fread(fd, buffer)
  assert(type(buffer) == 'table', 'buffer')
  local h = self:_toHandle(fd)
  local n = 0
  while true do
    local line = h:read('*l')
    if not line then break end
    buffer[#buffer + 1] = line
    n = n + 1
  end
  return n
end

--
-- build sting via c_printf and write to already opened stream
-- like c function: int fprintf(FILE *stream, const char *format, ...)
-- but instead of a ref to a stream, here numbers is used for brevity
-- 0 - stdin, 1 - stdout, 2 - stderr
--
---@param h stub.os.IOHandle
---@param format string|table
function M:c_fprintf(h, format, ...)
  local fd = self:_fh2fd(h) or -1
  return self:c_printf(fd, format, ...)
end

---@param fd number|stub.os.IOHandle
function M:c_printf(fd, format, ...)
  local h = self:_toHandle(fd)
  dprint('c_printf fd:', fd, IOHandle._status, '(', h, fd, ')')

  if h then
    -- extra feature pass list of lines instead cancatenate lines to a big string
    if type(format) == 'table' then
      IOHandle.cast(h)
      local c = 0
      for _, line in ipairs(format) do
        c = c + h:write(line)
      end
      dprint('c_fprintf say: ', ':inspect:', format, 'writed to:', h)
      return c
    end

    local line = string.format(format, ...)
    dprint('c_fprintf say: ', ':limit:', 64, line)
    return IOHandle.cast(h):write(line)
  else
    -- todo bad fd
  end

  return -1 -- error
end

-- int fputs(const char *restrict s, FILE *restrict stream);
-- int puts(const char *s);

--
-- in original c syscall used 2-int-array to pass fd of created pipes
-- here just return two this numbers
--
---@return number read
---@return number write
function M:c_pipe()
  -- return self.os:c_pipe(self)  ??
  local pipe = IOPipe:new()
  local t = self.fds

  local rfd = #t + 1
  t[rfd] = pipe.read -- 0

  local wfd = #t + 1
  t[wfd] = pipe.write -- 1

  return rfd, wfd
end

-- hack
---@param fd number
---@return stub.os.IOHandle?
function M:_remove_fd(fd)
  local h = assert(self.fds[fd], 'has IOHandle for given fd:' .. tostring(fd))
  self.fds[fd] = nil
  return h
end

--
-- check is a given fd are stdio (std{in|out|err})
---@param fd number?
---@return boolean
function M:_is_fd_stdio(fd)
  local h = self.fds[fd or false]
  return h ~= nil and IOHandle._is_stdio(h)
end

--
---@param fd number
function M:c_dup(fd)
  dprint("c_dup fd:", fd)
  local h = assert(self.fds[fd or false], 'expected valid fd, got ' .. tostring(fd))
  local new_fd = #self.fds + 1
  self.fds[new_fd] = h
  h.fd = new_fd -- keep maxfd to future remove
  return new_fd
end

--
---@param fd number
---@param new_fd number
function M:c_dup2(fd, new_fd)
  dprint("c_dup2 fd:", fd, 'new_fd:', new_fd)
  local h = assert(self.fds[fd], 'expected valid fd')
  assert(new_fd ~= fd and new_fd ~= nil, 'expected another new_fd')

  local prev_h = self.fds[new_fd]
  if prev_h then
    self:c_fclose(prev_h)
  end

  self.fds[new_fd] = h
  h.fd = new_fd --?

  return new_fd
end

function M:c_tmpfile()
  return self.os:c_tmpfile(self)
end

---@return string
function M:c_getcwd()
  return self.cwd
end

--
--------------------------------------------------------------------------------
--               CoreUtils for writing the emulated programs
--------------------------------------------------------------------------------

-- validate opts after parsing Args
-- Goal: way to throw error on extra(invalid|unknown) opts
-- (based on behavior of the coreutils progs)
--
-- shared for all builtin commands
--
---@param self stub.os.Proc
function M:is_invalid_input()
  local a = assert((self or E).args, 'proc.args')
  if a:is_input_valid() then
    return false -- input is valid!
  end
  dprint("input is invalid", self.args)

  local prog = a:arg(0) or '?'
  -- show only first invalid option if passed multiples
  local bad_optname = next((a.opts or E).UNKNOWN_OPTS) or '?'
  self:c_printf(C.STDERR, "%s: invalid option -- '%s'", prog, bad_optname)
  self:c_printf(C.STDERR, "Try '%s --help' for more information.", prog)

  self.exitcode = 2
  return true
end

class.build(M)
return M
