-- 13-02-2024 @author Swarg
-- Emulate behavior of the `/usr/bin/tee` system command

local Args = require("stub.os.Args");
-- local Proc = require("stub.os.Proc")
local C = require("stub.os.consts")
local D = require("dprint")

local M = {}
---@diagnostic disable-next-line: unused-local
local dprint = D.mk_dprint_for(M)
--

--
-- emulate work of the original `tee` program from GNU coreutils
--
---@param proc stub.os.Proc
function M.run(proc)
  local data = proc:stdin().lines
  dprint("cmd_tee", 'pid:', proc.pid, 'input:', ':inspect:', data)

  local F = Args.O_FLAG
  local a = assert(proc.args, 'has proc.args')
  a:parse_input({
    [{ '-a', '--append' }] = { F, 'append to the given FILEs, do not overwrite' }
  })

  if proc:is_invalid_input() then return proc end

  local mode = a.opts.append == true and 'a' or 'w'

  a:inc(1) -- skip 0 cmdname 'tee'

  while a:has_args() do
    local filename = a:pop_arg()
    local h = proc:c_fopen(filename, mode)
    -- deprecated way: proc.os.fs:write(fn, data, mode, proc.ug)
    if h then
      proc:c_fprintf(h, data)
      proc:c_fclose(h)
    else
      proc:c_printf(C.STDERR, "tee: %s: No such file or directory", filename)
    end
  end

  -- add all from stdin to stdout
  for _, line in ipairs(data) do
    proc:c_printf(C.STDOUT, line) -- proc:stdout():write(line)
  end
  proc:with_exitcode(0)
end

return M
