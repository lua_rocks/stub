-- 04-02-2024 @author Swarg
-- Emulate behavior of the `/usr/bin/cat` system command

local C = require("stub.os.consts")

local M = {}
local E = {}
M.D = require("dprint")
local dprint = M.D.mk_dprint_for(M)

--- helper
---@param proc stub.os.Proc
---@param fd number|stub.os.IOHandle
local function print_from(proc, fd)
  dprint("pid:", proc.pid, 'print_from fd:', fd, 'to stdout ')
  local buffer = {}
  if proc:c_fread(fd, buffer) > 0 then
    proc:c_printf(C.STDOUT, buffer)
  end
end

--
--
--
---@param proc stub.os.Proc
function M.run(proc)
  local a = assert((proc or E).args, 'has proc.args')
  dprint(1, '\nRUN cat pid:', (proc or E).pid, ':inspect:', a)

  a:inc(1) -- skip `cat` prog name

  local code = 0

  if not a:has_args() then
    print_from(proc, C.STDIN)
  else
    dprint('cat mode read from files specified in args')

    while a:has_args() do
      local filename = a:arg(0)
      dprint("cat try to read filename:", filename)
      local h = nil
      if filename == '-' then
        h = C.STDIN
      else
        h = proc:c_fopen(filename, 'r')
        if not h then
          proc:c_printf(C.STDERR, "cat: %s: No such file or directory", filename)
          code = 1
        end
      end
      if h then
        print_from(proc, h)
      end
      a:inc(1)
    end
  end

  return proc:with_exitcode(code)
end

return M
