-- 19-07-2024 @author Swarg
-- Emulate behavior of the `/usr/bin/touch` system command

local Args = require("stub.os.Args");
-- local Proc = require("stub.os.Proc")
local C = require("stub.os.consts")
local D = require("dprint")

local M = {}
---@diagnostic disable-next-line: unused-local
local dprint = D.mk_dprint_for(M)
--

-- helper
local is_file_exists = C.is_file_exists

--
-- emulate work of the original `tee` program from GNU coreutils
--
---@param proc stub.os.Proc
function M.run(proc)
  local data = proc:stdin().lines
  dprint("cmd_touch", 'pid:', proc.pid, 'input:', ':inspect:', data)

  local F = Args.O_FLAG
  local a = assert(proc.args, 'has proc.args')
  a:parse_input({
    [{ '-c', '--no-create' }] = { F, 'do not create any files' }
  })

  if proc:is_invalid_input() then return proc end

  local no_create = a.opts['no-create'] ~= nil

  a:inc(1) -- skip 0 cmdname 'tee'
  local i = 0


  while a:has_args() do
    local filename = a:pop_arg()
    if not filename or filename == '' and i == 0 then
      proc:c_printf(C.STDERR, "touch: missing file operand")
      break
    end

    if no_create then
      if is_file_exists(proc, filename) then
        local h = proc:c_fopen(filename, "a") -- in append mode
        if h then
          proc:c_fclose(h)
        else
          -- cannot touch '/etc/pg_hba.conf': Permission denied
          proc:c_printf(C.STDERR,
            "touch: cannot touch '%s': No such file or directory", filename)
        end
      end
    else
      local h = proc:c_fopen(filename, "w")
      if h then
        proc:c_fclose(h)
      else
        proc:c_printf(C.STDERR,
          "touch: cannot touch '%s': No such file or directory", filename)
      end
    end
    i = i + 1
  end

  -- add all from stdin to stdout
  for _, line in ipairs(data) do
    proc:c_printf(C.STDOUT, line) -- proc:stdout():write(line)
  end
  proc:with_exitcode(0)
end

return M
