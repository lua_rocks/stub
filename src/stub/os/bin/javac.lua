-- 20-07-2024 @author Swarg
-- Emulate behavior of the javac - java compiler
-- the implementation is very primitive - just create empty files in the
-- desired directories

-- local Args = require 'stub.os.Args'
local C = require 'stub.os.consts'
local U = require 'stub.os.ioutils'
local D = require 'dprint'

local M = {}
local dprint = D.mk_dprint_for(M)
--

-- helper
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local join_path = U.join_path
local extract_filename = U.extract_filename

local short_usage = [[
Usage: javac <options> <source files>
use --help for a list of possible options
]]

local usage = [[
Usage: javac <options> <source files>
where possible options include:
  @<filename>                  Read options and filenames from file
  -Akey[=value]                Options to pass to annotation processors
  -d <directory>               Specify where to place generated class files
  -g                           Generate all debugging info
]]

local empty_public_class_A = -- hexdump of empty `public class A {}`
    "fecabeba00003d000d00000a000207030400000c000501061000616a61766c2f" ..
    "6e612f67624f656a746300013c066e697469013e030029280756080000014101" ..
    "00014304646f01650f00694c656e754e626d726561546c6201650a006f537275" ..
    "65636946656c000141066a2e7661006100210007000200000000000100010005" ..
    "0006000100090000001d0001000100002a0500b7b101000001000a0000000600" ..
    "01000000010001000b00000002000c00"

-- javap A.class
--
--   Compiled from "A.java"
--   public class A {
--     public A();
--   }
local templ_head = "${FILENAME}:${LINENUM}: "
local templ_footer =
    "${LINE_WITH_ERROR}\n" ..
    "${ERROR_POINTER}\n" .. -- "  ^"
    "1 error\n"

-- when file Main.java contains  `public class A {}`
local templ_wrong_class_name = templ_head ..
    "error: class ${CLASSNAME} is public, " ..
    "should be declared in a file named ${CLASSNAME}.java\n" ..
    templ_footer

local temp_package_clashes_with_class = templ_head ..
    "error: package ${PACKAGE} clashes with class of same name\n" ..
    templ_footer

--
-- emulate work of the original `tee` program from GNU coreutils
-- javac greetings/*.java
--
---@param proc stub.os.Proc
function M.run(proc)
  dprint("cmd_javac", 'pid:', proc.pid, 'input:', ':inspect:', proc.args)
  local a = assert(proc.args, 'has proc.args')


  a:inc(1) -- skip 0 cmdname 'javac'

  if not a:has_args() then
    return proc:_err(2, usage, 'exit - no args') -- exit
  end

  dprint("parse args and opts...pid:", proc.pid)

  -- cflags -g -Xlint:unchecked -Xdiags:verbose -d -cp
  -- parse opts and params
  local p, sources = M.parse_args_and_opts(a)

  if #sources == 0 then
    return proc:_err(2, "error: no source files") -- exit
  end

  M.mk_absolute_path_for_output_dir(proc, p)

  dprint("compile... params:", ':inspect:', p)

  for _, src_file in ipairs(sources) do
    M.compile(proc, p, src_file)
  end

  proc:with_exitcode(0)
end

---@param p table
function M.mk_absolute_path_for_output_dir(proc, p)
  if p.output_dir and not U.is_absolute_path(p.output_dir) then
    if p.output_dir:sub(1, 2) == './' then
      p.output_dir = p.output_dir:sub(3)
    end
    p.output_dir = join_path(proc.cwd, p.output_dir)
    dprint('absolute_path for output_dir:', p.output_dir, 'cwd:', proc.cwd)
  end
end

---@param proc stub.os.Proc
function M.compile(proc, p, src_file)
  local t = M.parse_java_source(proc, src_file)
  dprint("compile... source-file:", src_file, ':inspect:', t)
  if t == nil then -- not is_file_exists(proc, fn) then
    return proc:_err(2, "error: file not found: %s\n%s", src_file, short_usage)
  end
  if not M.is_valid_class_and_package(proc, src_file, t) then
    return false -- with error output
  end

  local compiled_path
  if not p.output_dir then
    compiled_path = U.replace_ext(src_file, "java", "class")
  else
    local dir, file = M.mk_output_path(p, t, src_file)
    proc.os:mk_dir(dir, true) -- mkdir -p ...
    compiled_path = join_path(dir, file)
  end
  dprint('produce class-file:', compiled_path, 'for:', src_file)
  local bin = U.fromhex(empty_public_class_A) -- stub with valid empty bytecode
  C.write_to_file(proc, compiled_path, bin, "wb")
end

---@param src_file string
---@param p table parsed cli args and opts {output_dir}
---@param t table{pkg,classname} parsed source.java
---@return string
---@return string
function M.mk_output_path(p, t, src_file)
  assert(type(p.output_dir) == 'string', 'output_dir')
  local dir = p.output_dir
  if type(t.pkg) == 'table' and t.pkg.value and t.pkg.value ~= '' then
    dir = join_path(p.output_dir, U.classname_to_path(t.pkg.value) or '')
  end
  local file = extract_filename(src_file) .. '.class'
  return dir, file
end

--
---@param a stub.os.Args
---@return table{output_dir:string?} params
---@return table sources - paths to java sources files
function M.parse_args_and_opts(a)
  local p, sources = {}, {}

  while a:has_args() do
    local arg = a:pop_arg()
    if string.sub(arg, 1, 1) == '-' then
      if arg == '-g' then -- -g:none
        p.debugging_info = 'all'
      elseif arg == '-d' then
        p.output_dir = a:pop_arg()
      elseif arg == '-cp' or arg == '-classpath' or arg == '--classpath' then
        p.classpath = a:pop_arg()
      elseif arg == '-sourcepath' or arg == '--source-path' then
        p.sourcepath = a:pop_arg()
      end
    else
      sources[#sources + 1] = arg
    end
  end

  return p, sources
end

local patterns = {
  ['pkg'] = "^%s*package%s+([%w_%.]+)%s*;?%s*",
  ['classname'] = "class%s+([%w_]+)",
}

---@param path string
---@param proc stub.os.Proc
function M.parse_java_source(proc, path)
  assert(type(path) == 'string', 'expected string path')

  local _, fd = proc.os:c_fopen(proc, path, "rb")
  if fd then
    local h = proc:_toHandle(fd)
    local t = {
      pkg = nil,
      classname = nil,
    }
    local n = 0
    while not t.classname do
      local line = h:read('*l')
      if not line then break end
      n = n + 1
      --
      for k, patt in pairs(patterns) do
        if not t[k] then
          local v = string.match(line, patt)
          if v then
            t[k] = { value = v, lnum = n, line = line };
          end
        end
      end
    end
    return t
  end

  return nil -- file is not exists
end

-- check namespace of classname and package
function M.is_valid_class_and_package(proc, src_file, t)
  local cn = assert((t.classname or E).value, 'has parsed classname from source')
  local pkg = (t.pkg or E).value --, 'has parsed package from source')
  local bn = U.extract_filename(src_file)

  local templ, errline, lnum
  if cn ~= bn then
    templ = templ_wrong_class_name
    lnum = t.classname.lnum
    errline = t.classname.line
    --
  elseif pkg ~= nil and pkg ~= '' and (pkg == cn or U.ends_with(pkg, '.' .. cn)) then
    templ = temp_package_clashes_with_class
    lnum = t.pkg.lnum
    errline = t.pkg.line
  end

  if templ then
    local kv = {
      FILENAME = src_file,
      PACKAGE = pkg,
      CLASSNAME = cn,
      LINENUM = lnum,
      LINE_WITH_ERROR = errline,
      ERROR_POINTER = "^", -- todo
    }
    -- to stderr, exitcode = 2
    return proc:_err(2, C.substitute(templ, kv))
  end
  return true
end

return M
