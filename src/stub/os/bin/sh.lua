-- 05-02-2024 @author Swarg
local M = {}
-- Goal: emulate the behavior of /usr/bin/sh (dash)

local class = require("oop.class")

local sid = require("stub.id")
local Proc = require("stub.os.Proc")
local Args = require("stub.os.Args");
local C = require("stub.os.consts")
local UG = require("stub.os.UG")

local D = require("dprint")
local dprint = D.mk_dprint_for(M)

local redirect_to_fmode = {
  ['>'] = 'w',
  ['>>'] = 'a',
  ['<'] = 'r',
  ['<<'] = 'r',  -- here doc
  ['<<<'] = 'r', -- here sting
}
local STDIN = C.STDIN
local STDOUT = C.STDOUT
local STDERR = C.STDERR


local E, v2s, fmt = {}, tostring, string.format
local instanceof = class.instanceof


---@param a stub.os.Args
---@param ug stub.os.UG?
function M.check_sudo(a, ug)
  if a:is('sudo') then
    a.ai = a.ai - 1
    ug = UG.root()
    a:_remove_arg(0)
    a:_patchup_holes(0)
    dprint('# SUDO uid:', ug.uid, a)
  else
    ug = ug or UG.default() -- regular user
  end
  return ug
end

--
---@param proc stub.os.Proc
---@return stub.os.Proc
function M.run(proc)
  dprint("sh", 'pid:', proc.pid, 'uid:', proc:_uid())

  local F = Args.O_WVAL
  local a = assert(proc.args, 'has proc.args')
  a:parse_input({
    [{ '-c' }] = { F, 'Read commands from the command_string operand instead of from the standard input.' }
  })

  if proc:is_invalid_input() then return proc end
  local cmdline = Args.unwrap(a.opts.c) or proc:stdin():read('*a')

  a:inc(1) -- skip 0 cmdname 'sh'
  if cmdline then
    local t = { ug = proc.ug, cwd = proc.cwd, envs = proc.envs }
    return M.run_cmdline(proc.os, cmdline, t)
  end
  return proc
end

-- todo process subs? <(cmd)
function M.is_shell_lexeme(s)
  return s == '|' or s == '>' or s == '>>' or s == '<' or s == '<<' or s == '<<<'
end

-- {"echo" "123" "|" "cat" ">" "fn" }  --> {"echo" "123"}, {"cat" ">" "fn"}
--
---@param a stub.os.Args
---@param off number?
function M.split_by_pipes(a, off)
  assert(type(a) == 'table', 'a')
  off = off or 0
  local i, j, remains, pipes = off, off, a:args_cnt(), {}

  while i <= remains do
    local s = a:arg(i)
    if s == '|' then
      pipes[#pipes + 1] = a:sub(j, i)
      j = i + 2
    end
    i = i + 1
  end

  pipes[#pipes + 1] = a:sub(j)

  return pipes
end

-- function M.get_proc_args(a, off)
-- if M.is_shell_lexeme(s) then
-- return a:sub(off), remains


---@param s string command line
function M.parse_cd_and_cmd(s)
  local cd_to, rest = string.match(s, "^cd%s+([^%s+;%&]+)%s*&&%s*(.*)")
  return cd_to, rest
end

--
-- emulate /usr/bin/sh parsing and running given cmdline
--
---@param _os stub.os.OSystem
---@param cmdline string
---@param t table?{ug:stub.os.UG?,cwd:string}
---@return stub.os.Proc
---@return string?
function M.run_cmdline(_os, cmdline, t)
  local id = sid.next_section_id('shell') --label = sid.rand_str(4, 1)
  dprint("\n ----> new SHELL #", id, "run:", cmdline)

  assert(type(cmdline) == 'string', 'cmdline')
  if type(_os) ~= 'table' or not _os.c_fpopen == nil then
    -- if not instanceof(_os, 'stub.os.OSystem') then
    error('expected stub.os.OSystem, got ' .. tostring(_os))
  end
  if not _os.fs then
    -- This may be due to incorrect use of the method
    -- when a dynamic method is called not from an object but from its class
    -- M:dyn_method instead of the instance:dyn_method (here M is a Class).
    -- Faced with this error while using incorrect sintax:
    -- incorrect `M:handle_sys_cmd` instead of correct `self:handle_sys_cmd()`
    error('FileSystem Expected in OS: got: ' .. tostring(_os.fs))
  end
  t = t or E
  -- temporary solution to make commands like `cd dir && do-something` work
  local cd_to, rest_cmd = M.parse_cd_and_cmd(cmdline)

  local a = Args.of(rest_cmd or cmdline)
  local ug = UG.default()
  -- original sh (dash) if here no any args then read a command from their stdin
  assert(a:arg(0), 'shell: not supports works without args')
  local cwd = cd_to or t.cwd or _os:get_env('PWD') or _os.get_user_home(_os, ug.uid)
  local exitcode, errmsg = 0, nil
  local pshell = _os.o.p_init:c_fork({ -- /usr/bin/sh
    envs = t.envs or {},
    cwd = cwd,
    args = a,
    errno = 0,
    ug = t.ug,
  })
  pshell.envs.PWD = cwd

  local procs = M.setup_childs(pshell)
  if pshell.errno == 0 then
    dprint('--> run childs')
    exitcode, errmsg = M.run_childs(procs)
    M.exit_childs(procs)
    dprint('<-- run childs done')
  else
    exitcode, errmsg = 1, pshell.last_err
  end

  pshell:exit(exitcode)
  --
  dprint("\n <---- new SHELL #", id, " run:", cmdline, Proc._status, '(', pshell, true, ')')
  return pshell, errmsg -- interest is a (pipe)stdout with result from proc
end

--
-- process Args to detect EnvVars definition
--   - add to envs with overriding old values
--   - remove recognized envars from args
-- envvars case:
--   "KEY=VALUE cmd"
--
---@param a stub.os.Args
---@param envs table
function M.pull_envvars(a, envs)
  dprint("pull_envvars", a)
  assert(type(envs) == 'table', 'envs')

  local passed = 0
  -- for j = 0, a:_args_cnt() do
  for j = 0, a:_last_idx() do
    local arg = a:arg(j)
    -- dprint("arg", arg)
    if arg then
      local key, value = string.match(arg, '([^=%s]+)=(.*)')
      if key and value then
        envs[key] = value
        passed = passed + 1
        a:_remove_arg(j)
        dprint("add key:", key, 'value:', value)
      else
        break
      end
    end
  end
  -- to be safe here we remove envars from Args
  if passed > 0 then
    a:_patchup_holes()
  end
  dprint('found envvars ', passed, 'total envs:', #envs, a)
  return envs
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

---@param proc stub.os.Proc
local function pick_rhs(proc, rhs)
  local a = proc.args ---@cast a stub.os.Args
  dprint("pick_rhs rhs:", rhs, a, a:arg())

  if not rhs or rhs == '' then
    rhs = a:_remove_arg(0) -- case: {'>', 'filename'} not {'>filename'}
  end
  if not rhs or rhs == '' then
    proc:_err(2, 'sh: 1: Syntax error: end of file unexpected') -- shell
    rhs = nil
  end
  dprint("picked rhs:", rhs, 'errno:', proc.errno, 'exitcode:', proc.exitcode)
  return rhs
end

---@param proc stub.os.Proc
---@param lhs string
---@param op string
---@param rhs string?
local function build_write_redirect(proc, lhs, op, rhs)
  dprint("build_write_redirect pid:", proc.pid, 'uid:', proc:_uid(), lhs, op, rhs)

  proc.args:_remove_arg(-1) -- '>' | '>>'

  rhs = pick_rhs(proc, rhs)
  if not rhs or proc.errno ~= 0 then return false end

  if rhs:sub(1, 1) == '&' then                -- &N or &-  where `N` is a file description
    -- lhs:2 op:> rhs:&1
    if lhs == "2" and rhs == "&1" then        --
      dprint("redirect stderr to stdout 2>&1 pid:", proc.pid)
      proc.fds[C.STDERR] = proc.fds[C.STDOUT] -- ??
    else
      error(fmt('Not implements yet lhs:%s op:%s rhs:%s', v2s(lhs), v2s(op), v2s(rhs)))
    end
    --
  else
    -- case: rhs - is afilename
    local fn = rhs
    local mode = assert(redirect_to_fmode[op], 'has mode') -- '>' --> 'w'
    local fh, fd = proc:c_fopen(fn, mode)
    if fh and fd then
      proc:c_dup2(fd, STDOUT)
      proc:c_close(fd)
    else
      -- sh -c 'echo try-write >> sudo-f1; echo $?' ec=2
      -- local reason = 'Permission denied'                      -- Directory nonexistent
      local _, errmsg = proc:get_errno()
      errmsg = errmsg or (fn .. ': ?')
      proc:_err(2, 'sh: 1: cannot create %s', tostring(errmsg)) -- shell?
      return false
    end
  end
end

---@param proc stub.os.Proc
---@param lhs string
---@param op string
---@param rhs string?
local function build_read_redirect(proc, lhs, op, rhs)
  dprint("build_read_redirect pid", proc.pid, 'uid:', proc:_uid(), lhs, op, rhs)
  proc.args:_remove_arg(-1) -- '<'

  rhs = pick_rhs(proc, rhs)
  if not rhs or proc.errno ~= 0 then return false end

  if rhs:sub(1, 1) == '&' then -- &N or &-  where `N` is a file description
    error('Not implemented yet')
    --
  else
    local fn = rhs
    local fh, fd = proc:c_fopen(fn, 'r') -- pshell
    if fh and fd then
      proc:c_dup2(fd, STDIN)
      proc:c_close(fd)
    else
      -- sh -c 'cat < sudo-f1; echo $?' ec=2
      local reason = 'Permission denied' -- No such file
      proc:_err(2, 'sh: 1: cannot create %s: %s', fn, reason)
      return false
    end
  end
end

-- << and <<< "here-doc" & "here-string"
local function build_read_redirect_here_doc(proc, lhs, op, rhs)
  dprint("build_read_redirect_here_doc", proc.pid, 'uid:', proc.uid, lhs, op, rhs)
  proc.args:_remove_arg(-1) -- '<'

  rhs = pick_rhs(proc, rhs)
  if not rhs or proc.errno ~= 0 then return false end

  if rhs then
    local fh, fd = proc:c_tmpfile()
    if fh and fd then
      fh:write(rhs)
      -- todo seek 0 after write
      proc:c_dup2(fd, STDIN)
      proc:c_close(fd)
      return true
    else
      proc:_err(2, 'sh: 1: cannot create %s: %s', 'tmp', '?')
    end
  end

  return false
end


--
--
--  a:idx_t({ '|', '>', '>>', '<', '<<', '<<<' })
--  syntax: lhs op rhs
--              op is > >> <
--  cat > file        -- redirect stdout to the given filename
--  cmd 2>&1          -- redirect stderr(2) into stdout(1)
--  cat << EOF .. EOF -- redirect to stdin(1) `here-doc`
--  cat <<< STRING    -- redirect to stdin(1) `here-sting`
--  cat > fn << DOC
--  cmd 2>/dev/null   -- turnoff stderr without closing the stderr-fd(2)
--  cmd 2>&-          -- close file-description of stderr(2)
--
--  exec 3>file; ... ; # here commands that uses fd=3
--  exec 3>&-          # close fd=3
--
-- << is known as here-document structure  << EOF ... doc EOF
-- <<< is known as here-string.
-- < <()  - process substitution
--
---param pshell stub.os.Proc -- parent process
---@param proc stub.os.Proc {args stub.os.Args} child
---@return boolean
function M.mk_io_redirect(proc)
  dprint("mk_io_redirects pid:", proc.pid, 'uid:', proc:_uid(), proc.args)
  local a = assert(proc.args, 'proc.args')

  assert(a.ai == 1, 'expected ai == 1, got: ' .. tostring(a.ai) .. tostring(a))
  local cnt = 0

  while a:has_args() do
    local arg = a:pop_arg() or ''
    local lhs, op, rhs = string.match(arg, '([^%s><]*)([><]+)([^s]*)')

    if op and op ~= '' then
      dprint('lhs:', lhs, 'op:', op, 'rhs:', rhs)

      if op == '>' or op == '>>' then -- write to given file or to given fd
        build_write_redirect(proc, lhs, op, rhs)
        cnt = cnt + 1
        --
      elseif op == '<' then -- file to stdin
        build_read_redirect(proc, lhs, op, rhs)
        cnt = cnt + 1
      elseif op == '<<' or op == '<<<' then
        build_read_redirect_here_doc(proc, lhs, op, rhs)
        cnt = cnt + 1
      end
    end
  end

  a.ai = 1 -- reset arg index

  if cnt > 0 then
    a:_patchup_holes(0)
  end

  return proc.errno == 0
end

---
--
-- create processes and connect it by pipes. based on parsed command(in (Args)a)
--
-- split parsed cmdline in a into sub_shell sequenses like:
--
-- {'cat', 'fn', '|', 'cat', '>', 'fn2'} --> {'cat' 'fn'}, {'cat', '>', 'fn2' }
-- build processes and pipes
-- bind the whole command(processes) into a single pipeline with pipes
--
-- build pipes via process:  " cmd1  |  cmd2   |  cmd3"
--                             i|e|o    i|e|o     i|e|o
--                               |  `---^ |  `----^ | |
--                               `--------+---------' |
--                                        v   v-------'
--                    shell:          in|err|out
--                                       std
--
--  echo 123 | cat            -->  "123"
--  echo 123 | cat <<< 456    -->  "456"
--  echo 123 > /tmp/f1 | cat  -->  ""
--
---@param pshell stub.os.Proc
---@return table{stub.os.Proc}
function M.setup_childs(pshell)
  dprint("\n----> setup_child processes (with pipes)... ", (pshell.args or E).cmdline)
  assert(type(pshell) == 'table', 'pshell expected, got: ' .. tostring(pshell))
  assert(pshell.args, 'pshell.args')
  local sub_shells_args = M.split_by_pipes(pshell.args, 0)

  local procs, proc_cnt = {}, #sub_shells_args
  local pipe_next_read = pshell:stdin()

  for i = 1, proc_cnt do
    local a = sub_shells_args[i] ---@cast a stub.os.Args
    dprint('\n --> cmd-in-pipe #:', i, '/:', proc_cnt, 'new Proc for:', a)
    local envs = M.merge_keys({}, pshell.envs)
    M.pull_envvars(a, envs)
    local ug = M.check_sudo(a, pshell.ug)

    local proc = pshell:c_fork({
      args = a, envs = envs, stdin = pipe_next_read, ug = ug,
    })
    procs[#procs + 1] = proc

    -- if in a cmd has a redirection to|from file it will be overridden
    if proc_cnt > 1 and i < proc_cnt then
      local rfd, wfd = proc:c_pipe()
      proc:c_dup2(wfd, STDOUT)
      proc:c_close(wfd)
      -- hack to pass read-side of the pipe to the next process
      pipe_next_read = assert(proc:_remove_fd(rfd), 'iohandle')
    end

    -- can override pipes connection: case: `echo 123 > /tmp/f1 | cat`
    if not M.mk_io_redirect(proc) then
      pshell.errno, pshell.last_err = proc.errno, proc.last_err
      break
    end

    dprint("<-- cmd-in-pipe #:", i, Proc._status, '(', proc, true, ')', "\n")

    if proc.errno ~= 0 then
      dprint('errno:', proc.errno, proc.last_err)
      pshell.errno, pshell.last_err = proc.errno, proc.last_err
      return procs
    end
  end

  dprint("<---- setup_child processes. done.")
  return procs
end

--
-- find program for a built process and run it
-- find and execute a programm inside the process
--
---@param proc stub.os.Proc
---@return boolean success
---@return table? effect
---@return string? errmsg
function M.run_child(proc)
  local prog = proc.args:arg(0)
  local effect, errmsg = nil, nil

  local runbin = M.get_builtin_handler(prog) -- function
  dprint('run ', prog, 'uid:', proc:_uid(), 'builtin:', runbin ~= nil, 'exitcode:', proc.exitcode)

  if runbin then
    _, effect = runbin(proc) -- execute program inside process
  else
    local cmd0 = proc.args:get_cmdline()
    dprint('try to find predefined process cmd:', cmd0)

    if not proc.os:apply_predefined_prog_logic(cmd0, proc) then
      dprint(2, 'Stop: cmdline not found in the predefined commands', cmd0)
      errmsg = fmt("sh: 1: %s: not found", cmd0)
      proc.exitcode = 2
      return false, nil, errmsg
    else
      dprint('found and applyed predefined proce logic', cmd0)
    end
  end

  return true, effect, errmsg
end

--
-- run all procs in the pipe of full shell command
--
---@param procs table{stub.os.Proc}
---@return number  exitcode
---@return string? errmsg
function M.run_childs(procs)
  local success, errmsg, exitcode, effect = false, nil, 256, nil

  for i, proc in ipairs(procs) do
    assert(instanceof(proc, Proc), 'expected stub.os.Proc')
    success, effect, errmsg = M.run_child(proc)
    exitcode = proc.exitcode
    -- you can't call `proc:close()` here because it will break the pipeline
    dprint('proc#', i, 'exitcode:', exitcode)

    if not success then
      break
    end
    M.apply_effect(effect, procs, i)
  end

  return exitcode, errmsg
end

---@param effect table?
---@param procs table{stub.os.Proc}
---@param i number
function M.apply_effect(effect, procs, i)
  assert(type(effect) == 'table' or not effect, 'effect expected table got:' .. tostring(effect))

  if effect and procs and i then
    for j = i + 1, #procs do
      M.merge_keys(procs[j], effect)
    end
  end
  return procs
end

--
--
---@param procs table{stub.os.Proc}
function M.exit_childs(procs)
  -- impl way to autoclose all processes via close last pipe in the pipeline
  for _, proc in ipairs(procs) do
    proc:exit(proc.exitcode)
  end
end

--
--
-- flatten for envvars
function M.merge_keys(dst, src)
  dst = dst or {}
  for key, value in pairs(src) do
    dst[key] = value
  end
  return dst
end

--
-- get emulated program handler for a given cmd-name
--
---@return function?
function M.get_builtin_handler(cmd)
  if cmd then
    local f = M.builtin_cmds[cmd]
    if not f then
      dprint('lookup in stub.os.bin', cmd)
      local module_name = 'stub.os.bin.' .. tostring(cmd)

      local ok, m = pcall(require, module_name)
      if ok and type(m) == 'table' then
        if type(m.run) == 'function' then
          M.builtin_cmds[cmd] = m.run
          f = m.run
          dprint('Adding to mappings', cmd, ':dump:', f)
        else
          dprint('cannot find function `run` in the module for ', module_name)
        end
      else
        dprint('err', ':inspect:', m, ok)
      end
    end
    return f
  end
end

--------------------------------------------------------------------------------
--                       Builtin shell-commands
--------------------------------------------------------------------------------

-- $PWD -> cwd
local function resolve_arg_to_string(proc, arg)
  if arg and string.sub(arg, 1, 1) == '$' then -- $PWD or ${PWD}
    local vn = arg:sub(2)                      -- todo for ${PWD}
    return (proc.envs or E)[vn] or proc.os:get_env(vn) or ''
  end
  return arg
end

-- ignore stdin
-- write to stdout given args
--
---@param proc stub.os.Proc
function M.cmd_echo(proc)
  local a = assert(proc.args, 'has args')
  local s = ''
  a:inc(1)
  while a and a:has_args() do
    if #s > 0 then s = s .. ' ' end
    s = s .. resolve_arg_to_string(proc, a:pop_arg()) -- string or value of envvar
  end
  proc:c_printf(STDOUT, s)
  return s
end

-- debugging
function M.cmd_echo_to_stderr(proc)
  local a = assert(proc.args, 'has args')
  local s = ''
  a:inc(1)
  while a and a:has_args() do
    if #s > 0 then s = s .. ' ' end
    s = s .. resolve_arg_to_string(proc, a:pop_arg()) -- string or value of envvar
  end
  proc:c_printf(STDERR, s)
  return s
end

--
-- show a current work directory
--
---@param proc stub.os.Proc
function M.cmd_pwd(proc)
  local cwd = proc.cwd -- os.fs:pwd()
  dprint("shell builtin pwd ret:", cwd)
  proc:c_printf(STDOUT, cwd)
  return proc:with_exitcode(0)
end

--
-- change a work directory
-- TODO cd -
--
---@param proc stub.os.Proc
---@return stub.os.Proc
---@return table?  an effect that must be applied to all subsequent processes
function M.cmd_cd(proc)
  local path = proc.args:arg(1)
  local exitcode = 0
  local effect = nil
  if path then
    if proc:c_chdir(path) ~= 0 then
      proc:c_printf(C.STDERR, "sh: cd: %s: No such file or directory", path)
      exitcode = 256 -- -1
    else
      effect = { cwd = proc.cwd }
    end
  end
  return proc:with_exitcode(exitcode), effect
end

--
-- raise privileges to superuser
--
---@param proc stub.os.Proc
function M.cmd_sudo(proc)
  dprint("cmd_sudo", proc.args)
  -- todo ask pass via stdin
  proc.args:_remove_arg(0)
  proc.args:_patchup_holes(0)
  dprint("cmd_sudo2:", proc.pid, proc.args)

  local success, effect, errmsg = M.run_child(proc)
  -- errmsg for shell ??
  if success then
    effect = M.merge_keys(effect or {}, { ug = UG.root() })
  end
  return proc, effect, errmsg
end

-- inner self testing helpers

--
-- Inner:Experimental for selt-testing and self debugging
--
-- save stdin to given file and print to stdout (like original `tee`-command)
--  cmd | cat > /tmp/f1
--
---@param proc stub.os.Proc
function M.cmd_builtin_save_stdin_to(proc)
  local fn = proc.args:arg(1) or ''
  dprint("cmd_builtin_save_stdin_to", fn)
  local data, suc = proc:stdin().lines, false

  -- local ok = proc.os.fs:write(fn, data, 'w', proc.ug)
  local h, err = proc:c_fopen(fn, 'w')
  if h then
    proc:c_fprintf(h, data)
    proc:c_fclose(h) -- h:close() free fd from self.fds
    suc = true
  else
    proc:c_printf(STDERR, '%s', tostring(err))
  end

  local line1 = (data or {})[1]
  local ts = tostring
  proc:c_printf(STDOUT, '%s: saved:%s line1:\'%s\'', ts(fn), ts(suc), ts(line1))

  return proc:with_exitcode(0)
end

--
-- Inner:Experimental for selt-testing and self debugging
--
-- save stdin to given file and print to stdout (like original `tee`-command)
--  cmd | cat > /tmp/f1
-- save input(stdin) into given file
---@param proc stub.os.Proc
function M.cmd_builtin_show_envvar(proc)
  local name = proc.args:arg(1) or ''
  local value = proc.envs[name] or ''
  proc:c_printf(STDOUT, "%s: '%s'", name, value)
  return proc
end

-- collect all cmd_ into map
-- cmd_pwd --> pwd
-- cmd_cd  --> cd
-- ans so on
function M.build_builtins()
  local t = {}
  for k, v in pairs(M) do
    if k and k:sub(1, 4) == 'cmd_' then
      t[k:sub(5)] = v
    end
  end
  return t
end

M.builtin_cmds = M.build_builtins()

return M
