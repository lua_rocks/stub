-- 24-11-2023 @author Swarg
--
local D = require("dprint")
local class = require("oop.class")
local UG = require("stub.os.UG")

-- local sid = require("stub.id")
-- local E = {}

class.package 'stub.os'
---@class stub.os.FileStub : oop.Object
---@field new fun(self, o, name, atype, body, ug, mode, time):stub.os.FileStub
---@field parent stub.os.FileStub?
---@field name string
---@field open any
---@field body table? -- for file is lines for dir is files?
---@field fs_stat table -- state
local M = class.new_class(nil, 'FileStub', {
})

local E = {}
local dprint = D.mk_dprint_for(M)

M.DATA_FORMAT = '%Y-%m-%d %H:%M:%S'
M.T = {
  FILE = 'file',
  DIR = 'directory',
}

local next_inode = 0

-- for each file own new inode?
local function next_ino()
  next_inode = next_inode + 1
  return next_inode
end


---@param atype string
---@param size number
---@param ug table {uid:number, gid:number} default is 1000 used-n-group
---@param time table?{nsec, sec}
local function mk_fs_stat(atype, size, ug, time, mode)
  atype = atype or M.T.FILE -- 'file'
  if atype == 'd' or atype == 'dir' then
    atype = M.T.DIR         --'directory'
    size = 4096
  end

  time = time or { nsec = 900000000, sec = 1700808888 }

  local fs_stat = {
    type = atype,
    size = size or 0,
    uid = (ug or E).uid or UG.DEFAULT_USER_ID, -- 0 for root, 1000 for main user
    gid = (ug or E).gid or UG.DEFAULT_GROUP_ID,
    mode = mode or 33188,                      -- ?
    blocks = 8,
    blksize = 4096,
    dev = 2053, -- device
    rdev = 0,
    flags = 0,
    gen = 0,
    ino = next_ino(), -- inode
    nlink = 1,
    atime = time,
    ctime = time,
    mtime = time,
    birthtime = time,
  }
  return fs_stat
end

--
-- new file_stub
-- create a new stub for given filename
--
---@param name string
---@param content table<string> lines for file or files for dir
---@param atype string file|directory
function M:_init(name, atype, content, ug, mode, time)
  dprint(2, 'New FileStub:', name, atype, 'uid:', (ug or E).uid)
  self.name = name or self.name

  self.fs_stat = self.fs_stat or mk_fs_stat(atype, time, ug, mode)

  if self:is_dir() then
    self.files = content -- content of the file or dir
  else                   -- is_file
    self.lines = content
    -- self.open = nil   -- closed  aka state for io.open|close
  end
end

---
-- an approximate analogue of the program /usr/bin/stat
--
---@param fmt string? --  stat --format '%u/%U %g/%G' filename
---@param users table? {number = {uname, gname,...}} name mapping for uid guid
---@return string
function M:stat(fmt, users)
  local s = self.fs_stat or E
  local ts = tostring
  local uname, gname = '', ''
  if users then
    local user = users[s.uid or false] or E
    uname = user.uname or ''
    gname = user.gname or ''
  end
  local amode = ts(s.mode) .. '/' -- TODO
  local trep = {
    ['%a'] = s.mode,              -- 744        permission bits in octal
    ['%A'] = s.mode,              -- -rwxrwxr-- permis. bits and file type (human readable)
    ['%u'] = s.uid,
    ['%U'] = uname,
    ['%g'] = s.gid,
    ['%G'] = gname,
    ['%s'] = s.size,
  }
  if fmt then
    local SUBSTITUTE_PATTERN = "(%%[%w])" -- ${KEY} ${THE_KEY}
    local st = string.gsub(fmt, SUBSTITUTE_PATTERN, trep)
    return st
  end
  return string.format(
    "  File: %s\n" ..
    "  Size: %-16s  Blocks: %-11s IO Block: %-8s %s\n" ..
    "Device: %-16s  Inode: %-11s  Links: %s\n" ..
    "Access: (%-15s) Uid: (%6s/%8s) Gid:(%6s/%8s)\n" ..
    "Access: %s\n" ..
    "Modify: %s\n" ..
    "Change: %s\n" ..
    " Birth: %s\n"
    ,
    self:get_absolute_path(),
    ts(s.size), ts(s.blocks), ts(s.blksize), ts(s.type),
    ts(s.dev), ts(s.ino), ts(s.nlink),
    amode, ts(s.uid), uname, ts(s.gid), gname,
    M.fmt_time(s.atime),
    M.fmt_time(s.mtime),
    M.fmt_time(s.ctime),
    M.fmt_time(s.birthtime)
  )
end

---
---@param time table {sec, nsec}?
function M.fmt_time(time)
  if time and time.sec and time.nsec then
    -- 9 zeros for nsec
    return os.date(M.DATA_FORMAT, time.sec) .. '.' .. time.nsec
  end
  return '?'
end

---
---@param dir stub.os.FileStub?
---@return self
function M:bind_to(dir)
  assert(self.name ~= nil, 'expected filestub with name')

  if self.name ~= '/' then -- no parent only for root
    assert(class.instanceof(dir, M),
      'dir must be a FileStub ' .. tostring(dir) .. ' for ' .. self.name
    )
    ---@cast dir stub.os.FileStub
    assert(type(dir) == 'table' and dir:is_dir(), 'parent must be a directory')
  end

  if dir then
    dir.files = dir.files or {}
    local f = dir.files[self.name] ---@cast f stub.os.FileStub

    if f == self and dir == self.parent then
      return self -- already binded
    end

    if f ~= nil then
      error(string.format('cannot override alredy existed %s: "%s" in "%s"',
        f:type(), tostring(self.name), tostring(dir)))
    end
    -- clear from previos parent
    if ((self.parent or E).files or E)[self.name] == self then
      self.parent.files[self.name] = nil
    end

    dir.files[self.name] = self
    self.parent = dir
  end
  return self
end

---
---@param parent stub.os.FileStub
---@param name string
---@param lines table<string>? lines in file
---@param ug table? {uid, gid}
---@param mode number?
---@param time number?
---@return stub.os.FileStub
function M.new_file(parent, name, lines, ug, mode, time)
  dprint(2, 'FileStub New File:', name, 'parent:', parent)
  return M:new(nil, name, M.T.FILE, lines, ug, mode, time):bind_to(parent)
end

---@param parent stub.os.FileStub?
---@param name string
---@param files table? {string = stub.os.FileStub}
---@param ug table? {uid, gid}
---@param mode number?
---@param time number?
---@return stub.os.FileStub
function M.new_dir(parent, name, files, ug, mode, time)
  dprint(2, 'FileStub New Dir:', name, 'parent:', parent)
  return M:new(nil, name, M.T.DIR, files, ug, mode, time):bind_to(parent)
end

--
--
function M:is_dir()
  return (self.fs_stat or E).type == M.T.DIR
end

--
--
function M:is_file()
  return (self.fs_stat or E).type == M.T.FILE
end

--
--
function M:type()
  return ((self or E).fs_stat or E).type or M.T.FILE
end

--
--
function M:get_absolute_path()
  local path = ''
  local limit = 1024
  local cwd = self

  while cwd and limit > 0 do
    if path ~= '' and cwd.name ~= '/' then path = '/' .. path end
    path = cwd.name .. path
    cwd = cwd.parent
    limit = limit - 1
  end

  return path
end

--
--
function M:__tostring()
  local suff = (self:is_dir() and self.parent ~= nil) and '/' or ''
  return self:get_absolute_path() .. suff
end

class.build(M)
return M
