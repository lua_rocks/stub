-- 06-02-2024 @author Swarg
--

local class = require("oop.class")

local sid = require("stub.id")
local IOHandle = require("stub.os.IOHandle")
local C = require("stub.os.consts")

local D = require("dprint")


class.package 'stub.os'
---@class stub.os.IOPipe : oop.Object
---@field new fun(self, o:table?):stub.os.IOPipe
---@field read stub.os.IOHandle  --  in stream for stdin  (reader)
---@field write stub.os.IOHandle -- out stream for stdout (writer)
local M = class.new_class(nil, 'IOPipe', {
})
local dprint = D.mk_dprint_for(M)
local instanceof = class.instanceof


local pipes = {} -- alive pipes still used by some processes
local bus = IOHandle.bus
local E, v2s, fmt = {}, tostring, string.format


function M:_init()
  self.id = sid.next_section_id('pipe')
  pipes[self.id] = self
  -- local f = IOHandle._status
  local w, r

  if self.write and not self.read then -- pipe stdout --> stdin
    local lines = assert(self.write.lines, 'writer has lines')
    self.read = IOHandle.of(lines, 'r')
    w, r = 'old', 'new'
    --
  elseif self.read and not self.write then -- pipe  stdout
    local lines = assert(self.read.lines, 'reader has lines')
    self.write = IOHandle.of(lines, 'w')
    w, r = 'new', 'old'
    --
  elseif not self.write and not self.read then
    local shared_lines = {}
    self.write = IOHandle.of(shared_lines, 'w')
    self.read = IOHandle.of(shared_lines, 'r')
    w, r = 'new', 'new'
    --
  else -- has write and has read
    local shared_lines = self.read.lines
    -- empty buf in reader but has data in writer
    if self.read.lines and not next(self.read.lines) and
        self.write.lines and next(self.write.lines) then
      shared_lines = self.write.lines
    end
    self.read.lines = shared_lines
    self.write.lines = shared_lines
    w, r = 'old', 'old'
  end

  dprint(2, 'new Pipe-id:', self.id,
    w, 'write:', IOHandle._status, '(', self.write, C.STDOUT, ')', ' --> ', -- 1
    r, 'read:', IOHandle._status, '(', self.read, C.STDIN, ')'              -- 0
  )

  bus.subscribe(self.read, 'close', M.handler_on_close, self)
  bus.subscribe(self.write, 'close', M.handler_on_close, self)

  -- file descriptors are added only for debugging convenience
  -- to be able to compare which side of the pipe the io-stream(handle) belongs to
  -- self.read.fd = self.read.fd or C.STDIN
  -- self.write.fd = self.write.fd or C.STDOUT
end

---@param pipe any
---@param name string?
function M.cast(pipe, name)
  assert(instanceof(pipe, M),
    fmt('expected %s IOPipe, got: %s', v2s(name or ''), v2s(pipe)))
  return pipe
end

--
--
--
function M.handler_on_close(self, ioh)
  dprint("handler_on_close", IOHandle._status, '(', ioh, ')')
  local pipe = pipes[(self or E).id or false]

  if pipe then
    -- cannot close stdio streams because they are in shared use by all processes
    if IOHandle._is_stdio(ioh) then
      -- its a way to "close" std stream (unconnect from pipe)
      if ioh == pipe.read then
        pipe.read = nil
      elseif ioh == pipe.write then
        pipe.write = nil
      end
      D.dprint("cannot close stdio - remove from pipe w:", pipe.write, 'r:', pipe.read)
    end
    -- auto clean up from EBUS.bus
    bus.unsubscribe(ioh, 'close', M.handler_on_close)

    if (not pipe.write or pipe.write.open == false) and
        (not pipe.read or pipe.read.open == false) then
      pipes[self.id] = nil -- clear this pipe from alive pipes

      dprint("cleanup from alive pipes - all parts is closed of pipe", self.id)
    end
  else
    dprint('given pipe is not in the pipes(Already closed?).')
  end
end

--
--
--
---@param pipe stub.os.IOPipe
---@param verbose boolean?
local function status(pipe, verbose)
  local r, w = pipe.read, pipe.write
  local ios = IOHandle._status
  local spipe = ' pipe-id:' .. tostring(pipe.id)
  if verbose then
    spipe = spipe .. ' ' .. tostring(pipe)
  end
  return fmt('W:%s > R:%s%s', ios(w, 0, verbose), ios(r, 1, verbose), spipe)
end

--
--
--
---@param id number?
---@param verbose boolean?
function M._status(id, verbose)
  if id then
    local pipe = pipes[id]
    if pipe then
      return status(pipe, verbose)
    else
      return 'not found pipe with id: ' .. tostring(id)
    end
  else
    local s = ''
    for _, pipe in pairs(pipes) do
      s = s .. status(pipe, verbose) .. "\n"
    end
    return s
  end
end

-- for testing
-- aliase for is-alive pipe(~=nil)
function M._get_pipe(id)
  return pipes[id or false]
end

---
---
---@param handler stub.os.IOHandle
---@return number?
function M._find_pipeid_by_handler(handler)
  for id, pipe in pairs(pipes) do
    if pipe.read == handler or pipe.write == handler then
      return id
    end
  end
  return nil
end

return M
