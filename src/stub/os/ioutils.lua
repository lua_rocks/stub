-- 27-01-2024 @author Swarg
--
-- Goal: tools to work with real filesystem then stub.os.FileSystem is active
--  - provide a way to read data while the FS stub is running
--  - usecases:
--      - a convenient way to load data from the test resources directory.
--      - ability to define answer on cli command from existed file
--        right in the test where it is needed without the need to roll back
--        the stub.os.FileSystem.
--
local M = {}

-- todo move to consts
M.NO_SUCH_FILE_OR_DIR = "No such file or directory"


-- only for unix-like OS
local pwd = os.getenv('PWD') -- unix
local path_sep = pwd ~= nil and '/' or '\\'
local test_resources_dir = '/spec/resources'


function M.to_bool(v, def_on_nil)
  if v == nil then
    return def_on_nil
  end
  if v == false or v == 'no' or v == 'false' or v == '0' or v == 0 or v == '-' then
    return false
  end
  return true
end

---@param str string
---@param start string ends
function M.starts_with(str, start)
  return str ~= nil and start ~= nil and str:sub(1, #start) == start
end

---@param str string?
---@param e string ends
function M.ends_with(str, e)
  return str ~= nil and e ~= nil and (e == "" or str:sub(- #e) == e)
end

function M.fromhex(s)
  return (s:gsub('..', function(cc)
    return string.char(tonumber(cc, 16))
  end))
end

function M.tohex(s)
  return (s:gsub('.', function(c)
    return string.format('%02x', string.byte(c))
  end))
end

--------------------------------------------------------------------------------

---@param dst table?
---@param src table
function M.tbl_flat_copy(dst, src)
  assert(type(src) == 'table', 'src')
  dst = dst or {}
  for k, v in pairs(src) do
    dst[k] = v
  end
  return dst
end

--
-- if input is a string with "\n" split to lines and add into tbl
-- if input is a table then copy all it lines into tbl
--
-- otherwise throw error
--
---@param tbl table
---@param input string|table
function M.add_lines(tbl, input)
  local inp_type = type(input)

  if inp_type == 'string' then
    local pos, p_end = 1, #input

    -- split to lines with keeping an empty lines
    while true do
      local p = string.find(input, "\n", pos, true)
      if not p or p > p_end then
        break
      end
      tbl[#tbl + 1] = string.sub(input, pos, p - 1)
      pos = p + 1
    end

    if pos <= p_end then
      tbl[#tbl + 1] = string.sub(input, pos, p_end)
    end
  elseif inp_type == 'table' then
    for _, line in ipairs(input) do
      M.add_lines(tbl, line) -- tbl[#tbl + 1] = line
    end
    --
  else
    error('input type is ' .. inp_type)
  end
  return tbl
end

--
---@param dir string
---@param file string
---@return string
function M.join_path(dir, file)
  assert(type(dir) == 'string' and dir ~= '', 'dir must be not empty string')
  assert(type(file) == 'string' and file ~= '', 'file must be not empty string')

  local h2 = file:sub(1, 1) == path_sep
  if h2 then -- full path
    return file
  end
  local h1 = dir:sub(-1, -1) == path_sep
  local sep = path_sep
  if h1 or h2 then
    sep = ''
    if h1 == h2 then
      file = file:sub(2)
    end
  end
  return dir .. sep .. file
end

--
-- Goal: create full path to resources
-- use global _G.resources_dir  or PWD + /spec/resources (test_resources_dir)
--
---@param rel string
---@return string
function M.mk_res_path(rel)
  assert(type(rel) == 'string' and rel ~= '', 'relative path must be a string')
  local resources_dir = _G['resources_dir'] or (pwd .. test_resources_dir)
  if rel:sub(1, 2) == './' then
    rel = rel:sub(3)
  end
  return M.join_path(resources_dir, rel)
end

--
---@param filename string?
function M.file_exists(filename, _io)
  if filename and filename ~= '' then
    local f = (_io or io).open(filename, "rb")
    if f then f:close() end
    return f ~= nil
  end
  return false
end

--
-- read lines from given file
---@param fn string
---@return table?
function M.read_lines(fn, _io)
  if M.file_exists(fn, _io) then
    local lines = {}
    local iolines = (_io or io).lines
    for line in iolines(fn) do
      lines[#lines + 1] = line
    end
    return lines
  end
end

-- Read all content in binary mode
---@param fn string
---@return string|nil all bytes from file with path
function M.read_all(fn, _io)
  local file = (_io or io).open(fn, "rb") -- r read mode and b binary mode
  if file then
    local content = file:read "*a"        -- *a or *all reads the whole file
    file:close()
    return content
  end
  return nil
end

--
-- parse command line into args(words) with quotes groups supporting
--
-- supports:
--   - quotes(' ")
--   - multiline << EOF multi line \n file content EOF
-- not supports:
--   - parentress () {} []
--
---@param s string
---@return table
function M.parse_cmdline(s)
  if not s then return {} end

  local i, pi, open, t = 0, 1, nil, {}

  while i <= #s do
    i = i + 1
    local c = s:sub(i, i)
    if (c == '"' or c == '\'') then
      if not open then
        open = c
      else
        open = nil
      end
    elseif not open then
      if c == ' ' or c == "\n" then
        local arg = M.unwrap(s:sub(pi, i - 1))
        -- if arg == '<<' then
        if c == "\n" and t[#t] == "<<" then
          -- find end of the input by end-marker -- \nEOF\n...
          local e = s:find("\n" .. arg, i + 1) or #arg
          t[#t + 1] = s:sub(i + 1, e) -- << EOF ... EOF
          pi = i + 1                  -- no last word
          break
        elseif #t > 0 or arg ~= '' then
          t[#t + 1] = arg
        end
        pi = i + 1
      end
    end
  end

  if pi < i then t[#t + 1] = s:sub(pi) end -- last word

  return t
end

--
-- Extract dir-path from full-file-name
--
-- '/tmp/dir/file' -> '/tmp/dir/'
-- '/tmp/dir/' -> '/tmp/dir/'
-- 'text' -> nil
--
---@param path string
function M.extract_path(path)
  if type(path) == "string" then
    return path:match("(.*[/\\])") --or path
  end
end

---@return string? path
---@return string? file
function M.split_to_path_and_file(path)
  local dir = M.extract_path(path)
  if dir then
    local fn = path:sub(#dir + 1)
    if fn == '' then fn = nil end
    return dir, fn
  end
  return '.', path -- file without /
end

--
-- Same behavior as the "basename" system command
--
-- "/"             --> '/'
-- "//"            --> '/'
-- "/dir/file"     --> 'file'
-- "/dir/file.md"  --> 'file.md'
-- "path/to/file"  --> 'file'
-- "/path/to/dir/" --> 'dir'
---@param path string?
function M.basename(path)
  if type(path) == "string" then
    if path == '/' then return '/' end

    while #path > 1 and path:sub(-1, -1) == path_sep do
      path = path:sub(1, -2)
    end
    return path:match("[^\\/]+$") or path
  end
end

function M.replace_ext(filename, old_ext, new_ext)
  if filename and old_ext and new_ext then
    if string.sub(filename, #filename - #old_ext + 1) == old_ext then
      return string.sub(filename, 1, #filename - #old_ext) .. new_ext
    end
  end
  return nil
end

---@return boolean
function M.is_absolute_path(path)
  local c = path ~= nil and path:sub(1, 1) or nil
  return c == path_sep -- '/' or c == '\\'
end

-- filename without extension
---@param path string
function M.extract_filename(path)
  local fn = path:match("[^\\/]+$")
  if fn then
    return fn:match("(.+)[%.]") or fn
  end
end

-- add a slash to the end of the path if it is not already there
---@param path string
---@return string
function M.ensure_dir(path)
  if path and #path > 1 then
    if path:sub(-1, -1) ~= path_sep then
      path = path .. path_sep
    end
  end
  return path
end

-- org/comp/Main -> org.comp.Main
--
---@param path string
---@return string|nil
---@param pkg_sep string|nil defualt is dot (java)
function M.path_to_classname(path, pkg_sep)
  local name = nil
  pkg_sep = pkg_sep or '.'
  if path and type(path) == "string" then
    name = path:gsub(path_sep, pkg_sep)
  end
  return name
end

-- org.comp.Main -> org/comp/Main
--
---@param name string
---@param pkg_sep string|nil defualt is dot (java)
---@return string|nil
function M.classname_to_path(name, pkg_sep)
  local path = nil
  pkg_sep = pkg_sep or '%.'
  if name and type(name) == "string" then
    path = name:gsub(pkg_sep, path_sep)
  end
  return path
end

return M
