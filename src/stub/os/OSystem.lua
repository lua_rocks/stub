-- 26-11-2023 @author Swarg
local class = require("oop.class")
-- Goal: emulate answers to popen() commands

local sid = require("stub.id")
local IOHandle = require("stub.os.IOHandle")
local FileSystem = require 'stub.os.FileSystem'
local FileStub = require("stub.os.FileStub")
local IOPipe = require("stub.os.IOPipe")
local Proc = require("stub.os.Proc")
local Args = require("stub.os.Args")
local UG = require("stub.os.UG")
local C = require("stub.os.consts")
local iou = require("stub.os.ioutils");
local bin_sh = require("stub.os.bin.sh");

-- local E = {}


class.package 'stub.os'
---@class stub.os.OSystem: oop.Object
---@field new fun(self, o, fs:stub.os.FileSystem?): stub.os.OSystem
---@field fs stub.os.FileSystem
---@field o table -- state{p_init}
---@field no_def_progs boolean?
local M = class.new_class(nil, 'OSystem', {
})


M._VERSION = "stub v0.10.4"
M._URL = "https://gitlab.com/lua_rocks/stub"


M.D = require("dprint")
local dprint = M.D.mk_dprint_for(M)

-- defaults
M.DEF_DEV_NAME = 'AuthorName'
M.DEF_UNAME = {
  machine = "x86_64",
  sysname = "Linux",
  version = "Debian",
  release = "789-amd64",
}
local E = {}

--------------------------------------------------------------------------------
--                               INTERFACE
-- set_popen

-- constructor
---@param fs stub.os.FileSystem?
function M:_init(fs)
  assert(not fs or type(fs) == 'table', 'expected fs, got:' .. type(fs))

  local id = sid.next_id()
  dprint(2, "\n[NEW] OSystem stub id:", id, self)

  -- if the file system is not explicitly specified, create it automatically
  -- this is done for ease of use in tests, so
  -- that you don’t have to set it explicitly every time
  if not fs then
    dprint(">> FileSystem not defined create a new one with stdin,stdout,stderr")
    fs = FileSystem:new()
  end

  assert(class.instanceof(fs, FileSystem), 'expected FileSystem')

  self:bind_fs(fs)

  self.o = M.new_state(sid.next_id())
  if not self.no_def_progs then
    self:set_default_progs(self)
  end

  self:backup_original()
  _G.os = self:mk_os_stub() -- self.io

  self:mk_unix_os()
end

-- used to implement user_answers(to stdin) triggered by write to stdout
---@param self stub.os.OSystem
---@param iohandle table|stub.os.IOHandle
function M:handler_on_write_to_stdout(iohandle, content)
  dprint("on_write_to_stdout", IOHandle._status, '(', iohandle, ')')
  assert(type(self) == 'table', 'self - OSystem')
  self:reply_to_stdout(content)
end

--
-- bind fs with os and setup hooks for IOHandles(stdio-streams)
--
---@param fs stub.os.FileSystem
function M:bind_fs(fs)
  dprint("bind_fs:", ((self.fs or E).o or E).id, ' to os:', (self.o or E).id)
  self.fs = assert(fs, 'expected FileSystem')
  fs:bind_os(self)
  IOHandle.bus.subscribe(fs.io.stdout, 'write', M.handler_on_write_to_stdout, self)
end

--
---@param id number
function M.new_state(id)
  return { -- self.o
    id = id,
    next_pid = 1,
    processes = {}, -- to emulate some progs from coreutils (number=Proc)
    envs = {},
    tmpfiles = nil, -- c_tmpfile.  for now table of the tmpfiles is used as
    --                             counters instead of random names
    --
    cmd_mocks = {}, -- procs predefined answers to sys commands
    -- to emulate user input in response to messages in the stdout stream
    user_answers = {},
    --
    persistents = nil, -- { procs = nil, envs = nil, user_answers = nil }
  }
end

-- minimal
function M:mk_unix_os()
  -- environtent variables
  local envs = self.o.envs
  envs.USER = 'user'
  envs.HOME = '/home/user/'
  envs.PATH = '/bin:/usr/bin:/usr/local/bin:/home/user/.local/bin/'
  envs.PWD = envs.HOME

  self.o.p_init = self:newProc({ cmdline = 'init', envs = self.o.envs })
end

--
-- next process id
--
---@return number
function M:next_pid()
  local o = assert((self or E).o, 'has self state')
  local id = o.next_pid
  o.next_pid = o.next_pid + 1
  return id
end

--
-- only to create the init process.
-- all other processes must be created through a fork from this parent process
--
---@param t table?{ug:stub.os.UG?, args:stub.os.Args?, envs:table?)
---@return stub.os.Proc
function M:newProc(t)
  t = t or E
  local _io = assert(self.fs.io, 'has self.fs.io')
  assert(_io.stdin, 'FS has io.stdin')

  -- main-proc -- use it for uniform access to the emulated system(io.open,...)
  local proc = Proc:new({
    os = self,
    pid = self:next_pid(),
    ppid = 0,
    fds = {
      [C.STDIN] = t.stdin or _io.stdin,    -- IOHandle.of({}, 'r', nil), -- 0),
      [C.STDOUT] = t.stdout or _io.stdout, -- IOHandle.of({}, 'w', nil), -- 1),
      [C.STDERR] = t.stderr or _io.stderr, -- IOHandle.of({}, 'w', nil), -- 2),
    },
    ug = t.ug or UG.default(),
    args = t.args or Args:new({ cmdline = t.cmdline or '' }),
    envs = t.envs or self.o.envs,
  })

  return proc
end

--
-- used after each test
--
-- cleanup environtent to setup a new fresh environment
--
---@param full boolean? with persistent data
---@param clear_fs boolean?
---@return self
function M:clear_state(full, clear_fs)
  dprint('OSystem:clear_state')
  local prev_state = self.o

  sid.reset_all() -- ?
  self.o = M.new_state(sid.next_id())
  -- self.fs:clear_state() ??
  self:mk_unix_os() -- ?

  self:keep_persistent(prev_state, full)
  self.fs:clear_stdio()

  if clear_fs then
    self.fs:clear_state()
    self:bind_fs(self.fs)
  end

  self:set_default_progs(self)

  return self
end

--
-- create backup before mock global functions
--
---@return self
function M:backup_original()
  local prev_os = _G.os

  if not prev_os or (prev_os.__tag ~= sid.TESTING_TAG) then
    dprint('override original os: ', type(_G.os), 'tag:', _G.os.__tag)
    self.original_os = _G.os
  end

  return self
end

--
-- remove stubs
--
---@return self
function M:restore_original()
  dprint(1, "restore_original os")
  assert(not self.original_os or self.original_os.__tag ~= sid.TESTING_TAG,
    'make sure original os')

  _G.os = self.original_os or _G.os

  return self
end

--
-- mechanics to avoid cleanup all innner state of the emulated OS
-- rollback to new fresh environtent persistents states
--
---@param o table -- prev_state
---@return boolean?
function M:keep_persistent(o, skip)
  dprint("OS:keep_persistent prev state:", o ~= nil, 'skip:', skip)
  assert(type(o) == 'table', 'arg1 - prev state expected table')
  if skip then return end

  local t = o.persistents or E
  self.o.persistents = t -- pass itselt to next cleanup

  if t.procs and next(t.procs) then
    for cmd, _ in pairs(t.procs) do -- key - cmd, value - true
      local f = o.cmd_mocks[cmd]
      assert(type(f) == 'table' and f.lines, 'filestub, got: ' .. tostring(f))
      dprint('cmd: ', cmd, 'answer-lines:', #(f.lines or E))
      self.o.cmd_mocks[cmd] = f
    end
  else
    dprint('OS: No presistent command answers')
  end

  if t.envs then
    for env, _ in pairs(t.envs) do
      self.o.envs[env] = o.envs[env]
    end
  end
  if t.user_answers then
    for output, _ in pairs(t.user_answers) do
      self.o.user_answers[output] = o.user_answers[output]
    end
  end
end

--
-- mark specific resource as persistent (autorestored after cleanup)
-- used for: proc, envs, user_answers
--
---@return boolean
local function mark_as_persistent(self, section, key)
  if self and self.o and key and section then
    self.o.persistents = self.o.persistents or {}
    self.o.persistents[section] = self.o.persistents[section] or {}
    self.o.persistents[section][key] = true
    dprint('mark_as_persistent section:', section, 'key:', key)
    return true
  end
  return false
end

-- set the answer to command asked via io.popen
-- to delete command use answe == nil
-- handle = io.popen("somecommand 2>&1") -- redirect stderr to stdout
--
---@param cmd string
---@param answer table? - lines of the answer from given command or nil for delete
---@param persistent boolean? -- keep in state after clear_state
---@param err boolean? -- flag for show answer via stderr not stdout
function M:set_popen(cmd, answer, persistent, err)
  local p = nil
  if answer ~= nil then
    if self.o.cmd_mocks[cmd] then
      -- just complain no thrown errors
      dprint('[WARN] command already exists', cmd)
    end
    -- f = FileStub:new(nil, cmd, FileStub.T.FILE, answer)
    p = { lines = answer, err = err } -- will be used to create FileStub
  else
    dprint('remove from predefined cmd:', cmd)
  end
  self.o.cmd_mocks[cmd] = p

  if persistent and answer ~= nil then
    mark_as_persistent(self, 'procs', cmd)
  end
end

-- set the answer to command asked via io.popen
-- take the answer from the specified file in real FS
--
-- to delete command use answe == nil
--
---@param cmd string
---@param fn string full-file-name file with answer
---@param persistent boolean?
---@param err boolean? -- flag for show answer via stderr not stdout
function M:set_popen_from_file(cmd, fn, persistent, err)
  local f
  if fn ~= nil and fn ~= '' then
    if self.o.cmd_mocks[cmd] then
      -- just complain no thrown errors
      dprint('[WARN] command already exists', cmd)
    end
    local fullpath = iou.mk_res_path(fn)
    local lines = self:host_read_lines(fullpath)
    if not lines then
      error('Not found file: ' .. tostring(fullpath))
    end
    -- f = FileStub:new(nil, cmd, FileStub.T.FILE, lines)
    f = { lines = lines, err = err }
  end

  self.o.cmd_mocks[cmd] = f

  if persistent then
    mark_as_persistent(self, 'procs', cmd)
  end
  return f
end

--
-- for testing and checks is given cmd existed (setuped)
--
---@param cmd string
---@return stub.os.FileStub?
function M:get_popen(cmd)
  return self.o.cmd_mocks[cmd or false]
end

--------------------------------------------------------------------------------

--
-- to track the order in which system commands are called
--
---@param cmd string
function M:log_cmd_access(cmd)
  if (self.o and self.o.collect_cmd_access) then
    dprint('add cmd to log', cmd)
    self.o.cmd_access_log = self.o.cmd_access_log or {}
    self.o.cmd_access_log[#self.o.cmd_access_log + 1] = cmd
    return true
  end
  return false
end

--
---@param enable boolean
function M:collect_cmd_access(enable)
  self.o.collect_cmd_access = enable
end

--
---@return table?
function M:get_cmd_access_log()
  return (self.o or E).cmd_access_log
end

--
-- remove all collected log messages with access to io.popen and os.execute
--
---@return table? old log
function M:clear_cmd_access_log()
  local old = self.o.cmd_access_log
  self.o.cmd_access_log = {}
  return old
end

--------------------------------------------------------------------------------

--
---@param name string
---@param value string
---@param persistent boolean? to keep value after clear sys state
---@return string? prev value
function M:set_env(name, value, persistent)
  dprint('set_env', name, value, persistent)
  assert(type(name) == 'string' and name ~= '', 'expected string envvar name')

  -- shorthand to cd via set_end(PWD, new-dir)
  -- returns nil if given dir not exists
  if name == 'PWD' then
    if not self:cd(value) then
      dprint('set_env[PWD] cannot change directory to ', value)
      return nil
    end
  end

  local prev = self.o.envs[name]
  self.o.envs[name] = value


  if persistent then
    mark_as_persistent(self, 'envs', name)
  end
  dprint('previous value', prev)
  return prev
end

---@return string?
---@param name string?
function M:get_env(name)
  local dir = ((self.o or E).envs or E)[name or false]
  if dir and dir:sub(-1, -1) == '/' then dir = dir:sub(1, -2) end
  return dir
end

--
-- default setup
--
-- set the answers on then system commands
-- used to emulate answers to the system commands asked via io.popen
--
---@param o table?
function M:set_default_progs(o)
  o = o or E
  self:set_popen('git config user.name', { (o.devname or M.DEF_DEV_NAME) })
end

--------------------------------------------------------------------------------

--
-- check response presets when messages are printed to standard output
-- to emulate user input in response to messages in the stdout
-- write('Your Name:').. read(..predefined-input)..
--
---@param content table? -- text writen to stdout
function M:reply_to_stdout(content)
  dprint("reply_to_stdout", #(content or ''))

  -- dprint("stdout_lines", ':inspect:', stdout_lines)
  if not content or #content == 0 then
    return
  end

  local output
  if type(content) == 'table' then
    output = content[1]
  else
    output = tostring(content)
  end
  local answer = self.o.user_answers[output]
  if answer then
    local output0 = output:sub(1, math.min(#output, 24))
    local answer0 = answer:sub(1, math.min(#answer, 24))
    dprint('output:', output0, 'answer:', answer0) --, ':inspect:', stdout_lines)
    -- add input and return all lines(for testing)
    return self.fs:stdin_add_input(answer)
  else
    dprint('not found reply for output:', output:sub(1, math.min(#output, 32)))
  end
end

--
-- set a new user response to a message in the output stream
-- will be used to emulate user input in response to messages in the stdout
--
---@param output string
---@param answer string|table|nil - nil to delete recod from table(map)
---@param persistent boolean?
function M:set_user_answer(output, answer, persistent)
  local at = type(answer)
  assert(at == 'string' or at == 'table', 'answer must be a string or table')
  assert(self.o, 'expected state self.o')
  assert(output ~= nil, 'output')

  if answer ~= nil then
    if self.o.user_answers[output] then
      -- just complain no thrown errors
      dprint('[WARN] rewrite already existed output:', output)
    end
  end
  self.o.user_answers[output] = answer

  if persistent then
    mark_as_persistent(self, 'user_answers', output)
  end
end

--
-- for testing and checks is given cmd existed (setuped)
--
---@param output string
---@return stub.os.FileStub?
function M:get_user_answer(output)
  return self.o.user_answers[output or false]
end

--------------------------------------------------------------------------------
--                            OS Stubs
--------------------------------------------------------------------------------
--
function M:mk_os_stub()
  local _os = {
    __tag = sid.TESTING_TAG,


    ---@return number -- exit code
    ---@return string? -- errmsg
    execute = function(cmd)
      local proc, errmsg = self:c_fpopen(nil, cmd) -- fstub, err
      local code = 256                             -- -1
      if proc == nil then
        code = 32512                               -- sh: 1: not-existed-cmd: not found
      end
      if proc then
        -- 256, mv: cannot stat 'not-existed-cmd': ko such file or directory
        code = tonumber(proc.exitcode) or 256
      end
      return code, errmsg
    end,

    getenv = function(vn)
      return self.o.envs[vn]
    end,

    rename = function(oldname, newname)
      local st, err = self.fs:mv(oldname, newname), self.fs:last_err()
      if err then
        -- convert behavior of the `mv` sys command to lua function `os.rename`
        local dst, msg = err:match('\'([^\']+)\': (.*)$')
        if dst then
          err = tostring(dst) .. ': ' .. tostring(msg or err)
        else
          dprint('original err:', err) -- debugging
        end
      end
      return st, err
    end,

    remove = function(filename)
      local st, err = self.fs:rm(filename), self.fs:last_err()
      if err then
        err = err:gsub('rm: cannot remove ', '') -- sys cmd rm to os.remove
      end
      return st, err
    end,

    tmpname = function()
      return self.o.tmpname or self.original_os.tmpname()
    end,

    clock = function()
      return self.o.clock or self.original_os.clock()
    end,

    time = function(date)
      return self.o.time or self.original_os and self.original_os.time(date)
    end,

    date = function(format, time)
      return self.o.date or self.original_os.date(format, time)
    end,

    difftime = function(t2, t1)
      return self.original_os.difftime(t2, t1)
    end,
  }

  self.os = _os
  return _os
end

--------------------------------------------------------------------------------
--                    /etc/passwd  /etc/group
--------------------------------------------------------------------------------

--
-- parse given lines or lines from /etc/group (emulated)
--
---@return table {[num_gid] = { name = string, members = table }}
function M:get_groups(lines)
  local t = {}
  lines = lines or self.fs:get_file_lines('/etc/group')
  if lines then
    for _, line in ipairs(lines) do
      local name, gid, members0 = line:match('^([^:]+):[^:]+:([^:]+):(.*)$')
      if name then
        local members = {}
        for m in string.gmatch(members0, '([^,]+)') do
          members[#members + 1] = m
        end
        gid = tonumber(gid) or gid
        t[gid] = {
          name = name, -- group-name
          members = members
        }
      end
    end
  end
  return t
end

--
-- parse given lines or from /etc/passwd (emulated)
--
---@param lines table?
---@return table?
function M:get_users(lines)
  dprint("get_users")
  lines = lines or self.fs:get_file_lines('/etc/passwd')
  if lines then
    local groups = self:get_groups()
    -- root:x:0:0:root:/root:/bin/bash
    local t = {}

    local ptrn = '^([^:]+):[^:]+:([%d]+):([%d]+):([^:]+):([^:]+):(.*)$'
    for _, line in ipairs(lines) do
      local name, uid, gid, info, home, shell = line:match(ptrn)
      if name and uid then
        uid = tonumber(uid) or uid
        gid = tonumber(uid) or gid
        t[uid] = {
          uid = uid,
          gid = gid,
          uname = name, -- user name
          gname = (groups[gid] or E).name,
          info = info,
          home = home,
          shell = shell
        }
      end
    end
    return t
  end
end

---@param uid number?
---@return string?
function M:get_user_name(uid)
  uid = uid or UG.DEFAULT_USER_ID
  return ((self:get_users() or E)[uid] or E).uname
end

---@param uid number?
function M:get_user_home(uid)
  uid = uid or UG.DEFAULT_USER_ID
  return ((self:get_users() or E)[uid] or E).home or '/'
end

--------------------------------------------------------------------------------
--                     API delegates to FileSystem
--                   to use by human to write a tests
--------------------------------------------------------------------------------


--
-- To recognize different classes, objects and functions and modules
--
---@param f any
---@return string
function M:whatis(f)
  if f == self then
    return 'its me os: ' .. class.tostring(f) .. FileSystem.hasStub(self.fs)
    --
  elseif f == self.fs then
    return 'binded fs: ' .. class.tostring(f) .. FileSystem.hasStub(f)
  end
  return self.fs:whatis(f)
end

-- see set_env

--
-- See FileSystem:tree
--
---@param spath string?
---@param opts table? no_dir_slash (for dir nodes in the end)
---@return string?
function M:tree(spath, opts)
  dprint(1, "tree", spath)
  return self.fs and self.fs:tree(spath, opts) or nil
end

function M:treel(spath, opts)
  if self.fs then
    return iou.add_lines({}, self.fs:tree(spath, opts))
  end
end

--
-- make(create) a FileStubs for given filenames and dirnames
-- create a list of given files|directories
-- alias for set_files_or_dirs
--
---@return number of created files|dirs
function M:mk(...)
  return self.fs:mk(...)
end

--
-- ensure all given files and dirs are existed in the FS
-- throw error if given path not exists in FS
--
---@return self
function M:assert_has(...)
  self.fs:assert_has(...)
  return self
end

--
-- See FileSystem:mk_dir
--
---@param dir string
---@param mode number?
---@return boolean
function M:mk_dir(dir, mk_parents, mode)
  dprint(1, "mk_dir", dir, mode)
  if self.fs then
    return self.fs:mk_dir(dir, mk_parents, mode) ~= nil
  end
  return false
end

---@param dir string
function M:cd(dir)
  dprint(1, "cd", dir)
  if self.fs then
    if self.fs:cd(dir) then
      self.o.envs.PWD = dir
      return true
    end
  end
  return false
end

---@param spath string?
---@param sep string?   default is ' '
---@return string?
---@return boolean success flag
function M:ls(spath, sep)
  dprint(1, "ls", spath, sep)
  if self.fs then return self.fs:ls(spath, sep) else return nil, false end
end

--
-- create a new directory and change to it   mkdir + cd
--
---@return boolean
---@param dir string
---@param mode number? -- access code for a new directory
function M:mkcdir(dir, mode)
  dprint(1, "mkcdir", dir, mode)
  assert(type(dir) ~= 'strnig' or dir == '', 'expected not empty dir name')
  if self:mk_dir(dir, true, mode) then
    return self:cd(dir)
  end
  return false
end

function M:pwd()
  dprint(1, "pwd")
  return self.fs and self.fs:pwd() or nil
end

--
-- See FileSystem:stat
--
---@param fn string
function M:stat(fn, fmt, users)
  dprint(1, "stat", fn, fmt)
  if not self.fs then return 'No FileSystem' end

  local f = self.fs:get_file(fn)
  if not f then
    return "stat: cannot statx '" .. tostring(fn) .. "': No such file or directory"
  end
  users = users or self:get_users()
  return f:stat(fmt, users), users
end

function M:touch(path, no_create)
  self.fs:touch(path, no_create)
end

--
-- See FileSystem:get_file_owner
--
---@param fn string
function M:stat_owner(fn)
  dprint(1, "stat_owner", fn)
  return self.fs and self.fs:get_file_owner(fn, self:get_users()) or nil
end

-- create directory [with files]
-- usage example:
-- fs:set_dir(pr, { 'src/', 'spec/', '.git/', 'README.md' })
--
---@param path string
---@param new_files table? list of files
function M:set_dir(path, new_files)
  return self.fs:set_dir(path, new_files)
end

-- create stub file for given filename and lines(content)
-- mk file existed
---@param filename string
---@param lines table<string>? --
---@param ug table? {uid, gid}
---@param mode number?
---@param time number?
function M:set_file(filename, lines, ug, mode, time)
  return self.fs:set_file(filename, lines, ug, mode, time)
end

--
-- check is given file exists
-- if not ftype then return true if asked file is a regular file or dir
-- if ftype defined then return true only if file exists and type mathed
-- i.g. no ftype - any type of existed file
--
-- [ -f fn ]
--
---@param fn string
---@param ftype string? {'file', 'directory'} see FileStub.T.FILE
---@return boolean
function M:is_file_exits(fn, ftype)
  dprint(1, "is_file_exits", fn, ftype)
  return self.fs:is_file_exits(fn, ftype)
end

---@param spath string?
function M:file_type(spath)
  return self.fs:file_type(spath)
end

--
-- return content of given filename if file exists in the emulated FileSystem
--
---@param fn string
---@return table?
function M:get_file_lines(fn)
  local lines = self.fs:get_file_lines(fn)
  dprint(1, "get_file_lines", fn, 'sz:', (lines and #lines or 'nil'))
  return lines
end

---@param fn string
---@return string?
function M:get_file_content(fn)
  return self.fs:get_file_content(fn)
end

---@param fn string
---@param off number?
---@param endpos number?
function M:get_hexdump(fn, off, endpos)
  local s = iou.tohex(self.fs:get_file_content(fn))
  endpos = endpos or #s
  off = off or 1
  if off > 1 or endpos < #s then
    off = off * 2 - 1
    endpos = endpos * 2
    s = string.sub(s, off, endpos)
  end
  return s
end

---@param fn string
---@param content string|table
---@param ug stub.os.UG? def is 1000 UID
---@param mode number? asccess
---@param time number?
function M:set_file_content(fn, content, ug, mode, time)
  local typ = type(content)
  assert(typ == 'table' or typ == 'string', 'content')

  local fd = self.fs:get_file(fn)

  if typ == 'string' then
    content = iou.add_lines({}, content)
  end

  if not fd and self.fs then -- create new
    self.fs:set_file(fn, content, ug, mode, time)
    return self.fs.o.last_created_filestub ~= nil
    --
  elseif fd and fd:is_file() then
    assert(type(content) == 'table', 'content must be a table')
    fd.lines = content
    return true
  end

  return false
end

--
-- return inner stub for given filename
--
---@param fn string the file name
function M:get_file(fn)
  dprint(1, "get_file", fn)
  return self.fs:get_file(fn)
end

--------------------------------------------------------------------------------

--
-- Read all lines from given file in real FileSystem
-- Goal: provide way to read file after apply the FileSystem Stub
--
---@param path string
function M:host_read_lines(path)
  dprint(1, "host_read_lines", path)
  local _io = io
  if (self.fs or E).original_io then
    _io = self.fs.original_io
  end
  local lines = iou.read_lines(path, _io)
  if not lines then
    error('File not found: "' .. tostring(path) .. '"')
  end
  return lines
end

--------------------------------------------------------------------------------
--                  Emulated System API (System Calls)
--------------------------------------------------------------------------------

--
-- return formated error-string and errno like in c-lang: perror(errno);
--
---@return string?, number?
---@param proc stub.os.Proc
function M:last_err(proc)
  proc = assert(proc or self.o.p_init, 'has proc')
  local n, s = proc:get_errno()
  return s, n
end

--
-- https://man7.org/linux/man-pages/man2/open.2.html
-- https://man7.org/linux/man-pages/man3/fopen.3.html
--
-- open, openat, creat - open and possibly create a file
-- int open(const char *pathname, int flags, ... /* mode_t mode */ );
--

--
-- open(create) a file
-- emulate function fopen from the stdio.h c-lang lib
-- (wrapper around syscall:open)
-- for use inside stub.io.Proc
-- (inside emulated process in io.opopen or os.execute)
--
---@param proc stub.os.Proc? {ug, errno, last_err}
---@param fn string
---@param mode string? -- 'r', "a", "w", 'w+b',...(def: r) see IOHandle.MOD
---@return stub.os.IOHandle?  like FILE* in c
---@return number? fd of iohandle
function M:c_fopen(proc, fn, mode)
  local fs = assert(self.fs, 'expected FileSystem at self.fs')
  ---@cast fs stub.os.FileSystem
  proc = assert(proc or self.o.p_init, 'has init process')
  dprint("c_fopen pid:", proc.pid, fn, mode, ':inspect:', proc.ug)

  proc:clear_last_err() -- proc.errno = 0 proc.last_err = nil
  mode = mode or 'r'    -- see sources: lua-5.1.5/src/liolib.c

  local cwd_node = fs:_get_dirnode(proc.cwd)
  local fd, last_df, i, path = fs:_find_file(fn, cwd_node)
  local can_mk_new = (IOHandle.MOD.W[mode] or IOHandle.MOD.A[mode])


  if fd and fd:is_dir() then -- try open directory as a file
    -- case: try to read diretory as a file -- stdin in c give no errors+empty
    if IOHandle.MOD.R[mode] then
      local fstub = nil -- allow read but return empty - use buffer only
      return proc:bind(IOHandle:new(nil, fstub, mode, {}))
    end
    -- case: try to write into a directory
    proc:set_errno(C.ERRNO.EISDIR, "%s: %s", fn) -- is a directory
    return nil, nil
  end

  if not fd then
    -- can create a new file if parent dir exists and mode is write|append
    if can_mk_new and last_df and last_df:is_dir() and i == #path then
      -- todo check permissions and mode, time
      fd = FileStub.new_file(last_df, path[i], {}, proc.ug)
    else
      proc:set_errno(C.ERRNO.ENOENT, "%s: %s", fn) -- no such file or dir
      return nil, nil
    end
  end

  assert(fd, 'has fd')

  if IOHandle.MOD.W[mode] then
    fd.lines = {} -- erase the existing data from the file
    --
  elseif not IOHandle.MOD.A[mode] and not IOHandle.MOD.R[mode] then
    dprint('unsupported open mode: ', mode, ' use: (r|w|a) see IOHandle.MOD')
    proc:set_errno(C.ERRNO.EINVAL)
    return nil, nil
  end

  return proc:bind(IOHandle:new(nil, fd, mode))
end

--
-- intended to use inside emulated shell: stub.os.bin.sh
-- apply predefined logic (output to stdout or stderr) if such cmd was setuped
-- via set_popen
-- modify
--
---@param cmd string
---@param proc stub.os.Proc modify inner state based on found logic
---@return boolean
function M:apply_predefined_prog_logic(cmd, proc)
  local p = self.o.cmd_mocks[cmd] -- { lines:table, err:boolean?}
  if p and p.lines then
    local dst_tbl = p.err == true and proc:stderr().lines or proc:stdout().lines
    iou.add_lines(dst_tbl, p.lines) -- copy
    proc:with_exitcode(p.err == true and 1 or 0)
    return true
  end
  return false
end

-- cmd as plain text -- simple mappings
-- check predefined responses on popen
---@param proc stub.os.Proc
---@return stub.os.Proc?
function M:get_predefined_proc(proc, cmd, mode)
  local p = self.o.cmd_mocks[cmd] -- { lines:table, err:boolean?}
  if p and p.lines then
    -- wrap lines into stub.io.Proc
    local ug = (cmd:sub(1, 4) == 'sudo') and UG.root() or
        (proc or E).ug or UG.default()

    if string.find(mode, 'r') then -- fd == 1 then -- stdout read from popen
      local out, err
      if p.err then
        err = p.lines
      else
        out = p.lines
      end
      dprint('bind to predefined process out:', #(out or E), 'err:', #(err or E))
      local pp = Proc.of(self, Args:new({ cmdline = cmd }), ug, nil, out, err)
      pp:with_exitcode(p.err == true and 1 or 0)
      return pp -- +errno?
      --
    elseif string.find(mode, 'w') then
      error('Not implements yet')
    end
  end
  return nil
end

--  Create a new stream connected to a pipe running the given command.
--
--  This function is a possible cancellation point and therefore not
--  marked with __THROW.
-- extern FILE *popen (const char *__command, const char *__modes) __wur;
--
---@param proc stub.os.Proc? current proc
---@param mode string? r, w
-- look like lua used
---@return stub.os.Proc? --FileStub?
---@return string? -- errmsg
function M:c_fpopen(proc, cmd, mode)
  proc = proc or self.o.p_init
  dprint('c_fpopen cmd:', cmd, mode, 'pid:', (proc or E).pid)
  mode = mode or 'rb'
  --
  -- find given command in already setuped
  --
  ---param fd number? 0 - stdin, 1 - stdout, 2 - stderr, nil - stub.os.Proc
  if cmd then
    self:log_cmd_access(cmd)
    local p = self:get_predefined_proc(proc, cmd, mode)
    if p then
      return p, nil
    else
      dprint(2, 'cmdline not found in the predefined commands')
    end
    -- coreutils commands such as pwd cat (emulated by stub.os.bin.*
    return bin_sh.run_cmdline(self, cmd, nil)
  end
end

---@param exepath string
---@param params table{cwd:string, args:table, env:table,
--                           stdio = {stdin, stdout, stderr} }
---@param on_close function?
function M:spawn_proc(exepath, params, on_close)
  dprint("spawn_proc", exepath, params, on_close)
  assert(type(exepath) == 'string', 'exepath')
  assert(type(params) == 'table', 'params')
  local args = assert(params.args, 'args')
  local stdio = assert(params.stdio, 'stdio')
  local cmd = exepath .. " " .. table.concat(args, " ")

  local proc = self.o.p_init --:fork()
  if proc then
    local pipe_stdin = IOPipe.cast(stdio[1], 'stdio.stdin')
    local pipe_stdout = IOPipe.cast(stdio[2], 'stdio.stdout')
    local pipe_stderr = IOPipe.cast(stdio[3], 'stdio.stderr')
    pipe_stdout.read = proc:stdin() -- ?
    pipe_stdin.write = proc:stdout()
    pipe_stderr.read = proc:stdout()
  end

  --
  -- find given command in already setuped
  --
  ---param fd number? 0 - stdin, 1 - stdout, 2 - stderr, nil - stub.os.Proc
  self:log_cmd_access(cmd)
  local proc0 = self:get_predefined_proc(proc, cmd, 'r') -- mode?
  if proc0 then
    return proc0, (proc0 or E).pid
  else
    dprint(2, 'cmdline not found in the predefined commands')
  end
  -- coreutils commands such as pwd cat (emulated by stub.os.bin.*
  proc = bin_sh.run_cmdline(self, cmd, nil)

  return proc, (proc or E).pid
end

--
-- the last event with the removal of all subscribers to this stream
--
---@param h stub.os.IOHandle
function M:handler_delete_file_on_close(h)
  dprint("handler_delete_file_on_close", IOHandle._status, '(', h)
  local fn = h:_get_file_name()
  if fn then
    self.fs:rm(fn, false)
    -- h.bus.unsubscribe(h, 'close', M.handler_delete_file_on_close)
    h.bus.free(h) -- remove all handlers for this instance(cleanup

    if self.o.tmpfiles then
      for i, fn0 in pairs(self.o.tmpfiles) do
        if fn0 == fn then
          self.o.tmpfiles[i] = nil
          break
        end
      end
    end
  end
end

--
-- create a new temp file with "random" name
-- which should be automatically deleted after the process completes
--
---@param proc stub.os.Proc
function M:c_tmpfile(proc)
  assert(type(proc) == 'table', 'proc')
  dprint("c_tmpfile for pid:", proc.pid, 'uid:', proc:_uid())

  self.o.tmpfiles = self.o.tmpfiles or {}
  local t = self.o.tmpfiles
  local fn = '/tmp/tmp' .. tostring(proc.pid) .. '_' .. tostring(#t + 1)
  t[#t + 1] = fn

  local fstub = self.fs:set_file(fn, {}, proc.ug).o.last_created_filestub
  local h = IOHandle:new(nil, fstub, 'wb+', nil, IOHandle.TYPE.FILE)
  h.bus.subscribe(h, 'close', M.handler_delete_file_on_close, self)

  -- todo close all fd in proc.fds on exit

  return proc:bind(h)
end

--
-- change cwd dir in proc
--
---@param proc stub.os.Proc
---@param path string
---@return number
function M:c_chdir(proc, path)
  assert(type(path) == 'string', 'path')
  local cwd_node = self.fs:_get_dirnode(proc.cwd or '/')
  assert(type(cwd_node) == 'table', 'cwd_node')

  local fstub = self.fs:_find_file(path, cwd_node)
  if fstub and fstub:is_dir() then
    proc.cwd = fstub:get_absolute_path()
    return 0
  end
  return -1
end

class.build(M)
return M
