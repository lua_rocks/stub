-- 24-11-2023 @author Swarg
-- Goal: emulate handle returned from io.open(filename, mode)

local D = require("dprint")
local class = require("oop.class")

local sid = require("stub.id")
local C = require("stub.os.consts")
local FileStub = require("stub.os.FileStub")
local eventbus = require("alib.eventbus")


class.package 'stub.os'
---@class stub.os.IOHandle: oop.Object
---@field new fun(self, o:table?, file:stub.os.FileStub?, mode:string?, buffer:table?, atype:number?):stub.os.IOHandle
---@filed type number?
---@field fn string? original file name to show errors errno
---@field file stub.os.FileStub? - may be a stream not associated with any file
---@field lines table --
---@field errno_cb function? callback to set errno and errmsg to the process
---@filed last_err string? human readable helper for debugging only
local M = class.new_class(nil, 'IOHandle', {
})

M.bus = eventbus.new()
local dprint = D.mk_dprint_for(M)
local instanceof = class.instanceof
local fmt = string.format
local fire_event = M.bus.fire_event
local E = {}

-- w and w+ is from c-lang lib: stdio w+b
M.MOD = {
  W = { w = true, ['w+'] = true, wb = true, ['w+b'] = true, ['wb+'] = true },
  A = { a = true, ['a+'] = true, ab = true, ['a+b'] = true, ['ab+'] = true },
  R = { r = true, ['r+'] = true, rb = true, ['r+b'] = true, ['rb+'] = true },
}
--[[ modes:
"r" Opens a file for reading. The file must exist.
"w" Creates an empty file for writing.
    If a file with the same name already exists,
    its content is erased and the file is considered as a new empty file.
"a" Appends to a file. Writing operations, append data at the end of the file.
    The file is created if it does not exist.
"r+" Opens a file to update both reading and writing. The file must exist.
"w+" Creates an empty file for both reading and writing.
"a+" Opens a file for reading and appending.
]]

M.TYPE = {
  FILE = 1, STD_STREAM = 2, PIPE = 3, SOCKET = 4,
}
-- short names
M.NAMES = {
  [M.TYPE.FILE]       = 'F',
  [M.TYPE.STD_STREAM] = 'T', -- "terminal" stdin stdout stderr
  [M.TYPE.PIPE]       = 'P',
  [M.TYPE.SOCKET]     = 'S', -- ? N- network?
}

--
-- f, err = io.open(name, mode)
-- in original returns <userdata >
--
---@param file stub.os.FileStub?
---@param mode string
---@param buffer table? -- to create stream without any associated file
---@param atype number?
function M:_init(file, mode, buffer, atype)
  self.id = sid.next_section_id('iohandle')
  self.mode, self.lnum, self.open = mode, 0, true
  self.can_write, self.can_read = false, false
  self.last_err = nil

  if not file and buffer then -- for IOPipe and StdStreams
    assert(type(buffer) == 'table', 'buffer: expected table, got:' .. type(buffer))
    self.type = atype or self.type or M.TYPE.PIPE
    self.file = nil -- work via buffer without the Filestub (as Pipe)
    self.lines = buffer
    self.can_write = M.MOD.W[mode] == true or M.MOD.A[mode] == true
    self.can_read = M.MOD.R[mode] == true -- ? or mode == 'w+' or mode == 'w+b'  -- ? mode == 'r'
    assert(self.can_write or self.can_read, 'can write|read')
    return self
  end

  assert(file and instanceof(file, FileStub),
    'expected FileStub got: ' .. tostring(file))

  -- file.open = true --?

  self.file, self.lines = file, file.lines
  self.type = self.type or M.TYPE.FILE

  if file:is_file() then
    -- io.open -- Shift-K call hover
    if M.MOD.W[mode] then
      self.file.lines = {} -- erase the previous data
      self.can_write = true
      self.can_read = string.find(mode, '+') ~= nil
      --
    elseif M.MOD.A[mode] then
      self.file.lines = self.file.lines or {} -- previous data is preserved.
      self.can_write = true
      self.can_read = string.find(mode, '+') ~= nil
      --
    elseif M.MOD.R[mode] then
      self.file.lines = self.file.lines or {} -- previous data is preserved.
      self.can_read = true
      self.can_write = string.find(mode, '+') ~= nil
    end
    self.lines = self.file.lines
    --
  elseif file:is_dir() then -- ? for ls
    self.can_read = true    -- ?
    self.lines = {}
    for fn, _ in pairs(file.files or E) do
      self.lines[#self.lines + 1] = fn
    end
    table.sort(self.lines, function(a, b)
      return string.upper(a) < string.upper(b)
    end)
    dprint('iohandle for directory:', file.name, ' size:', self.lines)
  end
end

--
-- bridge back to parent Process to set errno
--
---@param self stub.os.IOHandle
---@param terrno table
---@param format string
local function set_errno(self, terrno, format, ...)
  if self.errno_cb then
    self.errno_cb(terrno, format, ...)
  else
    local ts = tostring
    error('|!errno_cb| ' .. ts(self.last_err) .. ' ' .. M._status(self) ..
      '; ' .. ts((terrno or E).n) .. ':' .. ts((terrno or E).s))
  end
  return -1
end

--
--
function M._get_file_name(self, def)
  if not self then return def end

  if self.file then
    if self.file.get_absolute_path then
      return FileStub.get_absolute_path(self.file) or def
    else
      return self.file.name or def
    end
  elseif self.type == M.TYPE.STD_STREAM then
    -- todo stdin stdout stderr
  end
  return def
end

local get_file_name = M._get_file_name

--
-- for reading from emulated stdin keep open alive
-- for all other files we close as soon as we read all the remaining data (lines)
-- from the emulated file.
---
---@param m number|string? -- bytes-to-read|mode
---@return string?
function M:read(m)
  self.last_err = nil
  dprint('iohandle:read mode:', m, M._status, '(', self)
  -- "*n" -- Reads a numeral and returns it as number.
  -- "*a" -- Reads the whole file.
  -- "*l" -- Reads the next line skipping the end of line.
  -- "*L" -- Reads the next line keeping the end of line.
  -- assert(self.can_read, 'expected can_read id:' .. tostring(self.id))
  m = m or '*l'

  if not self.can_read then
    self.last_err = 'expected can_read'
    set_errno(self, C.ERRNO.EBADF, '%s: %s', self:_get_file_name(''))
    return nil
  end

  fire_event(self, 'read', m)

  if self.open and self.lines then
    if m == '*a' then           -- read all
      self.lnum = self.lnum + 1 -- because at _init self.lnum = 0
      -- to support case to read all from stdin
      local res = ''

      for i = self.lnum, #self.lines do
        local line = self.lines[i]
        if line then
          if #res > 0 then res = res .. "\n" end
          res = res .. self.lines[i]
        else
          break
        end
      end

      self.lnum = #self.lines -- mark as all readed
      -- Always close (self.open = false) except when reading from stdin
      self:close()
      return res
      --
    elseif m == '*l' then
      self.lnum = self.lnum + 1
      if self.lnum > #self.lines then
        self:close()
        return nil -- io.popen given nil on *l then input is empty
      end

      local line = self.lines[self.lnum]
      if type(line) == 'table' then
        line = line.name
      end
      return line
      --
    elseif type(m) == 'number' and m > 0 then
      -- case:  local block = h1:read(size);   h2:write(block)
      local bytes2read = m
      self.offset = self.offset or 0
      local pstart = self.offset
      local bin = table.concat(self.lines, "\n")
      local remains = #bin - self.offset
      if remains <= 0 then
        return nil
      end
      if remains < bytes2read then
        bytes2read = remains
      end
      self.offset = self.offset + bytes2read + 1
      -- print('bytes to read', bytes2read, 'offset:', self.offset)

      return bin:sub(pstart, self.offset - 1)
    else
      error('unsupported mode: ' .. m)
    end
  end
  return nil
end

--
-- c stdio: fwrite()
-- https://man7.org/linux/man-pages/man3/fwrite.3.html
--
---@return number
function M:write(content)
  self.last_err = nil
  if not self.can_write then
    self.last_err = 'expected can_write'
    return set_errno(self, C.ERRNO.EBADF, '%s: %s', self:_get_file_name('')) -- mk_err_bad_file(self)
  end

  content = tostring(content)
  dprint('iohandle:write:', M._status, '(', self, ')', ':limit:', 64, content)

  self.lines = self.lines or {} --self.file.lines or {}
  local lines = self.lines

  if content:find("\n") then
    local i, j = 1, 1
    while j <= #content do
      i = content:find("\n", j) or #content + 1
      lines[#lines + 1] = content:sub(j, i - 1)
      j = i + 1
    end
  else
    -- TODO: how about case: write('A'); write('B'); --> 'AB' not {'A', 'B'} ??
    lines[#lines + 1] = content
  end

  --?
  if self.file then -- forced sync
    -- flush?
    self.file.lines = lines
  end

  fire_event(self, 'write', content) -- event_post_write(self, content)
  -- bridge with OSystem for apply the user_answers into stdin
  -- (emulate input to io.stdin triggered by stdout) write('Your Name:').. read..

  return #content
end

--
--
function M:flush()
  dprint('iohandle: flush:', M._status, '(', self, ')')
  -- io:stdout:flush()
end

--
-- notes: stdio streams shared across all processes and therefore by design
-- they cannot be closed (IOHandle.open = false)
function M:close()
  dprint(1, "iohandle:close", M._status, '(', self)
  if self.type == M.TYPE.STD_STREAM then
    dprint('[!!WARN!!] attempt to close std-iostream')
    -- self.open = true
  else
    self.open = false
  end

  fire_event(self, 'close')
  return true
end

--------------------------------------------------------------------------------
--                            inner helpers
--------------------------------------------------------------------------------

--
-- cast with validate
-- if handle is not instanceof IOHandle - thrown error
--
---@param handle any
---@return stub.os.IOHandle
---@param name string?
function M.cast(handle, name)
  assert(instanceof(handle, M),
    'expected ' .. tostring(name or '') .. ' IOHandle, got: ' .. tostring(handle))
  return handle
end

---@param self table|stub.os.IOHandle?
---@param exactly table|stub.os.IOHandle?  (io.stdin|io.stdout|io.stderr)
function M._is_stdio(self, exactly)
  return self ~= nil and self.type == M.TYPE.STD_STREAM and
      (not exactly or self == exactly)
end

--
--
---@param self table|stub.os.IOHandle?
function M._is_stdout(self)
  return self ~= nil and self.type == M.TYPE.STD_STREAM and self == io.stdout
end

--
---@param self table|stub.os.IOHandle?
function M._is_file(self)
  return self ~= nil and self.type == M.TYPE.FILE
end

--
-- factory to create IOHandle without FileStub as stream with buffer(lines)
-- used to emulate stdin/stdout/stderr for stub.os.Proc
--
---@param lines table
---@return stub.os.IOHandle
---@param mode string? 'r' is default value (readonly)
---@param atype number?
function M.of(lines, mode, atype)
  local fstub = nil
  local iohandle = M:new(nil, fstub, mode or 'r', lines, atype)
  return iohandle
end

---@return self
function M:update_time()
  if self.file and self.can_write then
    assert(type(self.file.fs_stat) == 'table', 'fs_stat')

    local t = self.file.fs_stat
    local time = { sec = os.time() or 0, nsec = 0 }
    t.mtime = time
    t.atime = time
    t.ctime = time
  end
  return self
end

-- debugging
---@param self stub.os.IOHandle?
---@param verbose boolean? with runtime hash
---@param fdn number? fd in proc.fds
function M._status(self, fdn, verbose)
  if self then
    local ts = tostring
    local sfd = '&' .. (fdn ~= nil and tostring(fdn) or '?') .. ':'
    local o = self.open == true and 'O' or 'C'
    local t = M.NAMES[self.type or false] or '?' -- self.file ~= nil and 'F' or 'p' -- file | pipe(buffer)
    local r = self.can_read == true and 'R' or ''
    local w = self.can_write == true and 'W' or ''
    local hash = ''
    local name = get_file_name(self, '')
    if #name > 0 then name = ' ' .. name end
    if verbose then
      hash = ' ' .. tostring(self)
    end
    local id = ' id: ' .. tostring(self.id)
    return fmt("%s%s%s%s%s:%s/%s%s%s%s",
      sfd, t, o, r, w, ts(self.lnum), ts(#(self.lines or E)), id, hash, ts(name))
  elseif fdn then
    return 'fd:' .. tostring(fdn) .. ' is nil'
  end
  return 'nil'
end

-- for testing
function M:_cleanup_state()
  assert(self.type == M.TYPE.STD_STREAM, 'intended for steams only')
  self.lnum = 0
  self.open = true
  self.lines = {}
end

-- lua local f, err = io.open('/tmp/1', 'rb'); print(require'inspect'(f)) f:close() --io.close(f)

class.build(M)
return M
