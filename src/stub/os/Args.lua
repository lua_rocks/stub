-- 04-02-2024 @author Swarg
-- Goal: wrapper around arguments - parset cmdline

local D = require("dprint")
local class = require("oop.class")

class.package 'stub.os'
---@class stub.os.Args: oop.Object
---@field new fun(self, o, cmdline):stub.os.Args
---@field ai number
---@field args table
---@field opts table?    - parsed only after pull_opts or parse_input
---@field cmdline string - original cmdline
---@field input_definition table? see parse_input
local M = class.new_class(nil, 'Args', {
})
local dprint = D.mk_dprint_for(M)

--
-- Constants
-- Types of options
--
M.O_FLAG = 0 -- flag (without agruments -- not pick next arg) if optkey defined
-- then value will be true
M.O_WVAL = 1 -- type: option with value -- value strictly required

-- input_validation
M.IV_VALID = 1        -- all input correct, no errors
M.IV_NO_ARGS = -1     -- no self.ai or self.args, not parsed cmdline?
M.IV_INVALID_OPT = -2 -- input has unknown(unexpected) options


-- factory
---@param cmdline string|table{ai,args}
function M.of(cmdline)
  if type(cmdline) == 'table' then
    local o = {
      ai = assert(cmdline.ai, 'ai'),
      args = assert(cmdline.args, 'args')
    }
    return M:new(o) -- directly without parsing
  end
  return M:new(nil, cmdline)
end

--
-- Constructor
--
function M:_init(line)
  if self.ai and self.args then
    self.ai = self.ai
    self.args = self.args
    dprint(3, 'Args:Direct from:', ':inspect:', self.args, self.ai)
  elseif line then
    self.ai = self.ai or 1
    self.args = self.args or M.parse_cmdline(line)
    dprint(3, 'Args:Line parsed to args:', ':inspect:', self.args, self.ai)
  else
    dprint(3, 'Args:Only cmdline without parsing') -- for OSystem.set_popen
    self.ai, self.args = 1, {}
  end
  self.cmdline = self.cmdline or line --?
end

function M:_is_valid()
  return self ~= nil and self.ai ~= nil and self.args ~= nil
end

---@param self stub.os.Args
---@param off number
---@param abs boolean?
---@return number
local function _get_abs_idx(self, off, abs)
  if self then
    return abs == true and off or (self.ai + off)
  end
  return 0
end

---@param off number? def is 0
---@param abs boolean?
---@return string?
function M:arg(off, abs)
  off = off or 0
  if M._is_valid(self) then
    return self.args[_get_abs_idx(self, off, abs)]
  end
end

--
-- return current arg and do self.ai++(emulate pop of current and move to next)
function M:pop_arg()
  if M._is_valid(self) then
    local arg = self.args[self.ai]
    self.ai = self.ai + 1
    return arg
  end
end

-- set nil all args before current arg(self.ai-1)
function M:clear_passed()
  if M._is_valid(self) then
    local k = 0
    for i = 1, self.ai - 1 do
      self.args[i] = nil
      k = k + 1
    end
    return k
  end
  return 0
end

--
-- check is current arg equals given string
-- if so return true and aout inc self.ai(offset)
-- change self.ai if s matched current arg
--
---@param s string?
function M:is(s)
  if s and self:arg() == s then
    self.ai = self.ai + 1
    return true
  end
  return false
end

-- check is given sting equals arg(off)
---@param s string
---@param off number?
function M:iseq(s, off)
  off = off or 0
  return s ~= nil and self:arg(off) == s
end

---@param pattern string  to match with current arg (of in given offset)
---@param off number?
function M:match(pattern, off)
  off = off or 0
  local arg = self:arg(off)
  if arg and pattern then
    return string.match(arg, pattern)
  end
  return false
end

--
-- ins self.ai
--
---@param off number? default is 1
---@return self
function M:inc(off)
  off = off or 1
  if self.ai then
    self.ai = self.ai + off
  end
  return self
end

-- do not use htis to calc size of args-array with holes
-- (will show incorrect value -- falls into a hole and goes no further)
--
---@return number
function M:args_cnt()
  return M._is_valid(self) and (#self.args - self.ai + 1) or 0
end

--
--
---@return boolean
function M:has_args()
  return M._is_valid(self) and self.ai <= #self.args or false
end

---@param off number?
function M:no(off, abs)
  off = off or 0
  if M._is_valid(self) then
    return self:arg(off, abs) == nil
  end
  return nil -- no ai or args
end

--
-- find index of given string
--
---@param s string
---@param off number?
---@param abs boolean?
function M:idxof(s, off, abs)
  off = off or 0
  if self:_is_valid() then
    for i = _get_abs_idx(self, off, abs), #self.args do
      if self.args[i] == s then
        return i - ((abs == true and 0) or self.ai)
      end
    end
  end
  return -1
end

---@param tbl table
---@param off number?
---@param abs boolean?
function M:idxof_t(tbl, off, abs)
  assert(type(tbl) == 'table' and next(tbl) ~= nil, 'tbl must be a not empty table')
  off = off or 0
  local t = {}
  if self.ai and self.args then
    for i = _get_abs_idx(self, off, abs), #self.args do
      for _, s in ipairs(tbl) do
        if self.args[i] == s then
          t[s] = i - ((abs == true and 0) or self.ai)
        end
      end
    end
  end
  return t
end

-- -k --key --another-key_name
---@param s string?
---@return boolean
function M.is_opt_name(s)
  return s ~= nil and type(s) == 'string' and
      s:match('^%-[%-%w%_]+') ~= nil and not s:match('^%-[%-]?[%d]+.*')
end

--
-- get arg for given offset-index,
-- remove it from self.args and return value of the removed arg
-- not affected self.ai (arg-index)
--
---@param off number? def is 0
---@param abs boolean?
---@return string?
function M:_remove_arg(off, abs)
  off = off or 0
  if M._is_valid(self) then
    local idx = _get_abs_idx(self, off, abs)
    local arg = self.args[idx]
    self.args[idx] = nil
    return arg
  end
end

-- to get the last index of an array with holes
--
---@return number
function M:_last_idx()
  local max = 0
  if M._is_valid(self) then
    for k, _ in pairs(self.args) do
      if k and k >= max then
        max = k
      end
    end
  end
  return max
end

--
-- { [1] = 'cmd', [3] = 'fn'} -> {[1]='cmd', [3]='fn'}
--
---@param off number?
---@param abs boolean?
function M:_patchup_holes(off, abs)
  off = off or 0
  local t = {}
  for i = off, self:_last_idx() do
    t[#t + 1] = self:_remove_arg(i, abs) -- self:arg(i) -- discard nils
  end

  local j = _get_abs_idx(self, off, abs) -- index to start from offset point
  for i = 1, #t do
    self.args[j] = t[i]
    j = j + 1
  end

  return self
end

--
--
--
---@param opt string|table{name,shortname}
---@return string
---@return string?
local function get_opt_names(opt)
  local name, sn
  if type(opt) == 'table' then
    if #opt == 1 then
      return tostring(opt[1])
    end
    name, sn = assert(opt[1], 'full-name'), assert(opt[2], 'short-name')
    if #name < #sn then -- swap
      local tmp = sn
      sn = name
      name = tmp
    end
  else
    name = tostring(opt)
  end
  return name, sn
end

--
-- pull defined(known) opts from args to t-table and remove it from self.args
--
---@param kopts table    - the definition of supported and available options
---@param t table?       - table with default values for to fill out by opts val
---@param off number?    - offset from current arg (self.ai)
---@param o_end number?  - to limit the range of search
---@param abs boolean?   - use off as absolute index not as offset from self.ai
---@return table         - opts {key- optname value - optvalue}
function M:pull_opts(kopts, t, off, o_end, abs)
  dprint('before pull_opts', ':inspect:', self.args)

  assert(type(kopts) == 'table', 'known opts must be a table')
  assert(next(kopts), 'known opts must be not empty')
  off = off or 0
  o_end = o_end or self:args_cnt()
  abs = abs or false

  t = t or {}
  local removed = 0

  -- traverse args
  for i = off, o_end do
    local arg = self:arg(i)
    if arg and M.is_opt_name(arg) then
      local oname, oval, known_opt = arg, nil, nil -- current arg is a opt-key
      local eqp = string.find(arg, '=')
      if eqp then
        oname, oval = arg:sub(1, eqp - 1), arg:sub(eqp + 1)
      end

      -- traverse known opts
      for opt, props in pairs(kopts) do
        local name, sn = get_opt_names(opt)
        local otype = props[1] -- second is description

        if oname == name or (sn and oname == sn) then
          dprint(1, 'i:', i, 'arg:', arg, 'opt:', name, sn)
          self:_remove_arg(i, abs)
          removed = removed + 1
          known_opt = true
          name = string.match(name, '^%-[%-]?([%w_%-]+)$') or name
          if otype == M.O_WVAL then
            if oval then -- from --key=value   case: --key=
              t[name] = oval
            else
              local next_arg = self:arg(i + 1)
              if next_arg and not M.is_opt_name(next_arg) then
                t[name] = next_arg
                self:_remove_arg(i + 1)
                removed = removed + 1
              else
                t[name] = '' -- without value case: --key --next-key val
                -- trigger error
              end
            end
          else -- M.O_FLAG
            t[name] = true
          end
        end
      end
      --
      if not known_opt then
        t.UNKNOWN_OPTS = t.UNKNOWN_OPTS or {}
        t.UNKNOWN_OPTS[#t.UNKNOWN_OPTS + 1] = oname -- arg
      end
    end
  end
  dprint('before patchup', ':inspect:', self.args, 'removed:', removed)
  if removed > 0 then
    self:_patchup_holes(off, abs)
  end
  dprint('after patchup', ':inspect:', self.args)

  return t
end

--
-- parse opts from already parsed args by specified definition
-- affected self.args add self.opts
--
---@param definition table - the definition of supported and available options
--  {key: optname or {optnames} => {[1]=type, [2]=description}}
---@return self
function M:parse_input(definition, t, off, o_end, abs)
  self.opts = self:pull_opts(definition, t, off, o_end, abs)
  self.input_definition = definition
  return self
end

--
-- check is input valid
-- validate already parsed opts
--
function M:is_input_valid()
  if self and self.opts then
    if self.opts.UNKNOWN_OPTS then
      return M.IV_INVALID_OPT
    end
  end
  return M._is_valid(self) and M.IV_VALID or M.IV_NO_ARGS
end

--------------------------------------------------------------------------------

--
---@param s string
function M.unwrap(s)
  if s then
    local fc = s:sub(1, 1)
    if (fc == '"' or fc == "'") and s:sub(-1, -1) == fc then
      s = s:sub(2, -2)
    end
  end
  return s
end

-- wrap to quotes
function M.Q(s)
  if s and string.find(s, ' ') then
    local q = "'"
    local sq = string.find(s, "'")
    local dq = string.find(s, '"')
    if sq and not dq then
      q = '"'
    end
    return q .. s .. q
  end
  return s
end

--
-- parse command line into args(words) with quotes groups supporting
--
-- supports:
--   - quotes(' ")
--   - multiline << EOF multi line \n file content EOF
-- not supports:
--   - parentress () {} []
--
---@param s string
---@return table
function M.parse_cmdline(s)
  if not s then return {} end

  local i, pi, open, t = 0, 1, nil, {}

  while i <= #s do
    i = i + 1
    local c = s:sub(i, i)
    if (c == '"' or c == '\'') then
      if not open then
        open = c
      elseif c == open then
        open = nil
      end
    elseif not open then
      if (c == ' ' or c == "\n") then
        if pi < i then -- skip empty args case: "a  'b c'  de" (multiples spaces)
          local arg0 = s:sub(pi, i - 1)
          -- dprint('arg0:', arg0, pi, i - 1)
          local arg = M.unwrap(arg0)
          -- if arg == '<<' then
          if c == "\n" and t[#t] == "<<" then
            -- find end of the input by end-marker -- \nEOF\n...
            local e = s:find("\n" .. arg, i + 1) or #arg
            t[#t + 1] = s:sub(i + 1, e) -- << EOF ... EOF
            pi = i + 1                  -- no last word
            break
          elseif #t > 0 or arg ~= '' then
            t[#t + 1] = arg
          end
        end
        pi = i + 1
      end
    end
  end

  if pi < i then t[#t + 1] = s:sub(pi) end -- last word

  return t
end

---@param s number
---@param e number?
function M:sub(s, e)
  e = e or (#self.args - self.ai + 1)
  local t = {}
  -- -1 to acts like lua strings:sub()
  for i = s - 1, e - 1 do
    t[#t + 1] = self:arg(i)
  end

  return M:new({ ai = 1, args = t })
end

--
-- reverse operation from parsing a string into arguments
--
---@return string
function M:get_cmdline()
  -- self.cmdline --? env vars?
  local s = ''
  for i = 0, self:args_cnt() do
    local arg = self:arg(i)
    if arg and arg ~= '' then
      if #s > 0 then
        s = s .. ' '
      end
      s = s .. M.Q(arg)
    end
  end
  return s
end

--
--
--
---@return string
function M:toString()
  if self then
    if self.cmdline then
      return tostring(self.cmdline)
    else
      return self:get_cmdline()
    end
  end
  return ''
end

class.build(M)
return M
