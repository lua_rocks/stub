-- 24-11-2023 @author Swarg
--
-- Goal: emulate file system for testing
--
-- NOTE: if you need to use FileSystem Stubs you should create the instance of
-- this class before require all another modules which can require luv and
-- another packages for works with file-system. To create proxy_require and
-- put the stubs into package.loaded table.
-- To make sure that needed module actualy works with stub use method "whatis"
--

local class = require("oop.class")

local sid = require("stub.id")
local FileStub = require("stub.os.FileStub")
local IOPipe = require("stub.os.IOPipe")
local IOHandle = require("stub.os.IOHandle")
local U = require("stub.os.ioutils")
local UG = require("stub.os.UG")



class.package 'stub.os'
---@class stub.os.FileSystem: oop.Object
---@field new fun(...): stub.os.FileSystem
---@field original_io table? -- from _G.io
---@field original_luv table? -- luv rock-package
---@field original_require function?
---@field nostub boolean?    -- to create new instance without stubbing real FS
---@field o table -- state
---       o.last_created_filestub - for file or dir created by set_file, set_dir
---@field verbose_require boolean
local M = class.new_class(nil, 'FileSystem', {
  verbose_require = false
})

local dprint = require("dprint").mk_dprint_for(M)
local E = {}

--------------------------------------------------------------------------------
--                               INTERFACE
--------------------------------------------------------------------------------
--
-- whatis     -- determine that is a given object passed as param
--
-- set_dir
-- set_file
-- get_file
-- is_exists_dir is_exists_file
-- file_type
--
-- pwd stat touch ls tree cd mk_dir rm(os.remove) mv(os.rename)
--
--------------------------------------------------------------------------------

local fmt = string.format
-- ArgN, FuncName, Got
M.P_BAD_SARG = "bad argument #%s (string expected, got %s)"

-- constructor
---self.nostub boolean? - true to create instance without stubbing real FS
function M:_init()
  local id = sid.next_id()
  dprint(1, 'new FileSystem stub id:', id, 'nostub:', self.nostub)

  if not self.nostub then
    self:backup_original()
    self.o = M.new_state(id)

    _G.io = self:mk_io_stub()                  -- self.io
    package.loaded['luv'] = self:mk_luv_stub() -- self.luv

    require = self:mk_require_proxy()          -- self.require
    --
  else
    -- for self-testing purposes .create instance without stubbing real FS
    self.o = M.new_state(id)
  end

  self:bind_std_iostreams() -- stdin stdout stderr needed for Proc and OSystem
end

local function root_ug()
  return UG.root()
end

--
-- experimental
--
function M.mk_unix_fs()
  dprint("\n>>--------------  mk_unix_fs  ---------------->>")

  local root = FileStub.new_dir(nil, '/', nil, root_ug())

  local bin = FileStub.new_dir(root, 'bin')
  FileStub.new_file(bin, 'bash')

  -- /usr/sbin/adduser
  local usr = FileStub.new_dir(root, 'usr', nil, root_ug())
  local sbin = FileStub.new_dir(usr, 'sbin', nil, root_ug())
  FileStub.new_file(sbin, 'adduser', {}, root_ug())

  local etc = FileStub.new_dir(root, 'etc', nil, root_ug())

  M.mk_unix_etc_files(etc)

  local dev = FileStub.new_dir(root, 'dev', nil, root_ug())
  FileStub.new_file(dev, 'null', {}, root_ug()) -- todo special behavior

  -- /var/log/syslog
  local var = FileStub.new_dir(root, 'var', nil, root_ug())
  local var_log = FileStub.new_dir(var, 'log', nil, root_ug())
  FileStub.new_file(var_log, 'syslog', {}, root_ug()) -- adm

  -- /tmp
  FileStub.new_dir(root, 'tmp')

  -- /home/user/proj/
  local home = FileStub.new_dir(root, 'home')
  local user = FileStub.new_dir(home, 'user')
  FileStub.new_dir(user, 'proj')

  dprint("--[DONE]--------  mk_unix_fs  ----------[DONE]--\n")
  return root
end

--
-- /etc/hosts, /etc/passwd, /etc/shadow /etc/group
--
---@param etc stub.os.FileStub
function M.mk_unix_etc_files(etc)
  -- /etc/hosts
  FileStub.new_file(etc, 'hosts', {
    '127.0.0.1  localhost',
  })
  -- /etc/passwd
  FileStub.new_file(etc, 'passwd', {
    'root:x:0:0:root:/root:/bin/bash',
    'user:x:1000:1000:User,,,:/home/user:/bin/bash'
  })
  -- /etc/group
  FileStub.new_file(etc, 'group', {
    'root:x:0:',
    'adm:x:4:',
    'sudo:x:27:user',
    'user:x:1000:',
  })
  -- /etc/shadow
  FileStub.new_file(etc, 'shadow', {
    'root:hashedpass.:19299:0:99999:7:::',
    'user:hashedpass.:19299:0:99999:7:::'
  })
  -- command to generate hashed password. -1 means md5 (the weakest)
  -- $1$salt1$LLd.mgajv//NgDiobyYuv/
  --  ^ ^^^^^ ^^^^^^^^^^^^^^^^^^^^^
  --  1
end

--
--
---@param id number
function M.new_state(id)
  local root = M.mk_unix_fs()
  return {
    id = id,
    root = root, -- the root of the tree containing links to all files and dirs
    -- of the emulated file system
    cwd = root,  -- '/', used for convenience only when describing the structures
    -- of the emulated file system and not for emulated processes
    last_err = nil,
  }
end

--
--
function M:backup_original()
  -- io
  local prev_io = _G.io

  if not prev_io or (prev_io.__tag ~= sid.TESTING_TAG) then
    dprint('override original io: ', type(_G.io), 'tag:', _G.io.__tag)
    self.original_io = _G.io
  end

  --- luv
  local ok, luv = pcall(require, 'luv') -- package.loaded['luv'] or require('luv')
  if ok then
    local prev_luv = luv

    if not prev_luv or (prev_luv.__tag ~= sid.TESTING_TAG) then
      dprint('override original luv: ', type(prev_luv), 'tag:', luv.__tag)
      self.original_luv = luv
    end
  end

  -- require
  local prev_require = require
  assert(type(prev_require) == 'function', 'prev_require is a function')

  if prev_require ~= _G.original_require and _G.original_require == nil then
    dprint('override original require:', ':dump:', prev_require)
    _G.original_require = prev_require
    self.original_require = prev_require
  elseif _G.original_require then
    self.original_require = _G.original_require
  end

  return self
end

--
--
---@return self
function M:restore_original()
  dprint(1, "restore_original")
  assert(not self.original_io or self.original_io.__tag ~= sid.TESTING_TAG,
    'make sure original io')

  _G.io = self.original_io or _G.io


  assert(not self.original_luv or self.original_luv.__tag ~= sid.TESTING_TAG,
    'make sure original luv')
  package.loaded['luv'] = self.original_luv or package.loaded['luv']


  -- require
  if self.original_require and self.original_require ~= self.require then
    assert(type(self.original_require) == 'function')
    require = self.original_require
    if _G.original_require == self.original_require then
      _G.original_require = nil -- clear
    end
    --
  elseif type(_G.original_require) == 'function' then
    require = _G.original_require
  end

  return self
end

--
--
function M:clear_state()
  dprint('FileSystem:clear_state')
  local _os = self.o.os
  self.o = M.new_state(sid.next_id())
  self.o.os = _os -- keep binds with os. necessarily before the call of next
  self:clear_std_iostreams()
  return self
end

-- if not initialized do not create
-- use self:mk
function M:clear_std_iostreams()
  local _io = self.io or E
  if _io.stdin or _io.stdout or _io.stderr then
    self:bind_std_iostreams() -- 251
  else
    dprint('no stdin, stdout, stderr')
  end
end

--------------------------------------------------------------------------------

-- tools for path
function M.basename(path)
  if type(path) == "string" then
    return path:match("[^\\/]+$") or path
  end
end

-- Extract dir-path from full-file-name
-- '/tmp/dir/file' -> '/tmp/dir/'
-- '/tmp/dir/' -> '/tmp/dir/'
-- 'text' -> nil
---@param path string
function M.dirname(path)
  if type(path) == "string" then
    return path:match("(.*[/\\])") -- or path
  end
end

--
---@param name string?
---@return boolean
local function is_dir_name(name)
  return name ~= nil and name:sub(-1, -1) == '/'
end

--
-- is path is not absolute
--
-- is filename starts with relative elements like ./ or ../ without root /
---@return boolean
local function is_path_relative(fn)
  if fn then
    local fc = fn:sub(1, 1)
    if fc == '.' then
      local sc = fn:sub(2, 2)
      return sc == '/' or sc == '.' and fn:sub(3, 3) == '/' -- ../
    end

    return fc ~= '/' -- not starts with root slash
  end
  return false
end

--
-- "/usr/bin/" ->  { '/', 'usr', 'bin' }
-- "/" ->  { '/' }
function M._path_split(path)
  assert(type(path) == 'string', 'path')
  local i, prev, t = 1, 1, {}
  if path:sub(1, 1) == '/' then t[1] = '/' end

  while prev <= #path do
    i = path:find('/', prev, true) or (#path + 1)
    local s = path:sub(prev, i - 1)
    prev = i + 1
    if s ~= '' then
      t[#t + 1] = s
    end
  end

  return t
end

--
-- "path/to/file.md"  --> path/,  "to/file.md"
function M._path_pop_left(path)
  if not path then return path end
  local i = path:find('/')
  local left, rem0 = path, nil
  if i then
    left, rem0 = path:sub(1, i), path:sub(i + 1)
    if rem0 == '' then rem0 = nil end
  end
  return left, rem0
end

--------------------------------------------------------------------------------
--
-- create directory [with files]
-- usage example:
-- fs:set_dir(pr, { 'src/', 'spec/', '.git/', 'README.md' })
--
---@param path string
---@param new_files table? list of files
function M:set_dir(path, new_files)
  dprint("set_dir", path)
  self.o.last_created_filestub = nil
  assert(type(path) == 'string', 'path got:' .. tostring(path))
  assert(not new_files or type(new_files) == 'table',
    'new_files must be a table or nil')

  local fdir = self:mk_dir(path, true)
  if not fdir then
    error(self.o.last_err or ('cannot create dir: ' .. tostring(path)))
  end
  dprint('fdir:', fdir)

  if new_files then
    fdir.files = fdir.files or {}

    for _, name in pairs(new_files) do
      if is_dir_name(name) then
        name = name:sub(1, -2)
        if fdir.files[name] == nil then
          FileStub.new_dir(fdir, name, {})
        else
          dprint('already exists dir:', name, 'in dir:', fdir)
        end
      else
        local f = fdir.files[name] ---@cast f stub.os.FileStub
        if f == nil then
          FileStub.new_file(fdir, name)
        else
          error(string.format('already exists %s: "%s" in dir: %s',
            f:type(), name, tostring(fdir)))
        end
      end
    end
  end
  self.o.last_created_filestub = fdir

  return self
end

-- create stub file for given filename and lines(content)
-- mk file existed
---@param filename string
---@param lines table<string>? --
---@param ug table? {uid, gid}
---@param mode number? access permissions encoded to number
---@param time number?
function M:set_file(filename, lines, ug, mode, time)
  dprint(1, 'set-file', filename)
  self.o.last_created_filestub = nil
  assert(type(filename) == 'string', 'filename has:' .. tostring(filename))

  local basename, dir = M.basename(filename), M.dirname(filename)
  assert(basename and basename ~= '', 'basename')
  local cwd = self.o.cwd

  if dir ~= nil then
    dprint('try to create new dir', dir, 'for fn:', filename)
    cwd = self:mk_dir(dir, true)
    if not cwd then
      error('cannot create dir:' .. tostring(dir) .. tostring(self.o.last_err))
      return false
    end
  else
    dprint('try to add file into cwd: ', cwd)
  end

  local f = FileStub.new_file(cwd, basename, lines, ug, mode, time)
  dprint('new-file to', cwd, 'dir:', dir, 'basename:', basename)
  self.o.last_created_filestub = f

  return self
end

--
-- make(create) a FileStubs for given filenames and dirnames
-- create a list of given files|directories
-- alias for set_files_or_dirs
--
---@return number of created files|dirs
function M:mk(...)
  local c, last_dir, cwd = 0, nil, (self.o.cwd or self.o.root)

  for i = 1, select('#', ...) do
    local fn = select(i, ...)
    assert(type(fn) == 'string', 'fn arg#:' .. tostring(i))

    if is_path_relative(fn) and last_dir then
      dprint('set cwd:', last_dir, 'from:', self.o.cwd)
      self.o.cwd = last_dir -- cd into last directory
    else
      self.o.cwd = cwd
    end

    if is_dir_name(fn) then
      self:set_dir(fn)
      last_dir = self.o.last_created_filestub
      dprint('after create dir:', fn, 'last_dir:', last_dir)
    else
      self:set_file(fn)
    end

    if self.o.last_created_filestub ~= nil then
      c = c + 1
    end
  end

  self.o.cwd = cwd -- restore

  return c
end

--
-- check is given filename exists, if not throw an exception
--
---@return string report
function M:has(...)
  local s, cnt, no = '', select('#', ...), 0

  for i = 1, cnt do
    local fn = select(i, ...)
    assert(type(fn) == 'string', 'fn arg#:' .. tostring(i))

    local f = self:_find_file(fn)

    if not f then
      if #s > 0 then s = s .. ' ' end
      s = s .. '-' .. i -- not exists
      no = no + 1
    end
  end

  if s == '' then
    s = '+' .. cnt -- shortcut for "all exists"
  elseif no == cnt and cnt > 0 then
    s = 'no one'
  end
  return s
end

--
-- ensure all given files and dirs are existed in the FS
-- throw error if given path not exists in FS
--
---@return self
function M:assert_has(...)
  local cnt, no, s = select('#', ...), 0, ''

  for i = 1, cnt do
    local fn = select(i, ...)
    assert(type(fn) == 'string', 'fn arg#:' .. tostring(i))
    if self:_find_file(fn) == nil then
      if s ~= '' then s = s .. "\n" end
      s = s .. "not exists path: '" .. tostring(fn) .. "' arg#: " .. i
      if self.o.last_err then s = s .. ': ' .. self.o.last_err end
      no = no + 1
    end
  end

  if s ~= '' then
    if cnt > 1 then
      s = s .. string.format("\nExists: %s/%s\n", cnt - no, cnt)
    end
    error(s, 2) -- thrown all not existed
  end
  return self
end

--
-- return inner stub for given filename
--
---@param fn string the file name
---@return stub.os.FileStub?
---@return stub.os.FileStub? -- parent dir if has
function M:get_file(fn)
  dprint('get_file fn:', fn)
  assert(type(fn) == 'string' and #fn > 0, 'filename')

  local fstub, fdir = self:_find_file(fn)
  return fstub, fdir
end

-- Uid:(1000/user) Gid:(1000/user)
function M:get_file_owner(fn, users)
  dprint('get_file_owner fn:', fn)
  assert(type(fn) == 'string' and #fn > 0, 'filename')

  local fstub = self:_find_file(fn)
  return fstub and fstub:stat('%u:%U %g:%G', users) or nil
end

--
-- return content of given filename if file exists in the emulated FileSystem
--
---@param fn string
---@return table?
function M:get_file_lines(fn)
  dprint(1, 'get_file_lines fn:', fn)
  -- error('give me trace')
  assert(type(fn) == 'string' and #fn > 0, 'filename')

  return (self:_find_file(fn) or E).lines
end

---@param fn string
---@return string?
function M:get_file_content(fn)
  local lines = self:get_file_lines(fn)
  dprint(1, "get_file_content", fn, 'lines:', (lines and #lines or 'nil'))
  if lines then
    local s = ''
    for _, line in ipairs(lines) do s = s .. line .. "\n" end
    return s
  end
  return nil
end

--
-- absolute path to node in tree contained at self.o.root
---@param dir string
---@return stub.os.FileStub?
function M:_get_dirnode(dir)
  local path = M._path_split(dir)
  assert(path[1] == '/', 'expected absolute path')
  local node, i = self.o.root, 2

  while node and node:is_dir() and i <= #path do
    local name = path[i]
    if name and name ~= '' then -- // ?
      node = (node.files or E)[name]
    else
      return nil
    end
    i = i + 1
  end

  if node and not node:is_dir() then
    node = nil
  end
  -- dprint('ret:', node~=nil and node:get_absolute_path() or 'nil')
  return node
end

--
---@param spath string?
---@param cwd stub.os.FileStub?
---@return stub.os.FileStub? not nil if found for given spath
---@return stub.os.FileStub last_cwd - fd of last founded element from path[i]
---@return number index in path
---@return table path
function M:_find_file(spath, cwd)
  -- dprint('_find_file', spath)
  self.o.last_err = nil

  local i, path = 1, M._path_split(spath)
  cwd = cwd or self.o.cwd or self.o.root
  assert(type(cwd) == 'table', 'cwd') ---@cast cwd stub.os.FileStub

  local last_cwd = cwd

  while cwd and cwd:is_dir() and i <= #path do
    local name = path[i]
    dprint('name:', name, i, 'cwd', cwd)
    if i == 1 and name == '/' then
      cwd = self.o.root
    elseif name == '.' then
      cwd = cwd
    elseif name == '..' then
      cwd = cwd.parent or self.o.root
    elseif name and name ~= '' then -- // ?
      cwd = (cwd.files or E)[name]
    end
    if not cwd then break end
    i = i + 1
    last_cwd = cwd
  end

  -- if the full path to given spath (file or dir) is not found
  -- cwd == nil last_cwd == last existed dir|file
  if i <= #path and cwd then
    -- case: /tmp/not-existed-file/tmp/file
    --            found ^ file
    cwd = nil
  end

  -- has file but request the dir: "/bin/bash/"  - rise error
  if cwd and is_dir_name(spath) and cwd:is_file() then
    self.o.last_err = 'Not a directory'
    last_cwd = cwd.parent or self.o.root -- root ?
    cwd = nil
  end

  dprint('_find_file ret:', cwd, last_cwd, 'i:', i, #path)
  return cwd, last_cwd, i, path
end

--
-- get specified by spath directory, or cwd if spath is nil
--
---@param spath string?
---@param self stub.os.FileSystem
---@return stub.os.FileStub?
local function _wd(self, spath, only_dir)
  local wd = self.o.cwd
  if spath and spath ~= '.' and spath ~= './' then
    dprint('try to cd into', spath)

    local d = self:_find_file(spath)
    if d and wd:is_dir() then
      wd = d
    elseif only_dir then
      wd = nil -- to print error "directory not found"
    end
    dprint('now cwd is', wd)
  end

  return wd
end


--
-- check is given file exists
-- if not ftype then return true if asked file is a regular file or dir
-- if ftype defined then return true only if file exists and type mathed
-- i.g. no ftype - any type of existed file
--
-- [ -f fn ]
--
---@param fn string
---@param ftype string? {'file', 'directory'} see FileStub.T.FILE
---@return boolean
function M:is_file_exits(fn, ftype)
  dprint('is_file_exits fn:', fn)
  assert(type(fn) == 'string' and #fn > 0, 'filename')

  local fstub = self:_find_file(fn)
  if fstub then
    if ftype then
      return fstub:type() == ftype
    end
    return true
  end
  return false
end

--
--
function M:is_exists_dir(spath)
  local f = self:_find_file(spath)
  return f ~= nil and f:is_dir()
end

--
--
function M:is_exists_file(spath)
  local f = self:_find_file(spath)
  return f ~= nil and f:is_file()
end

--
--
---@param spath string?
function M:file_type(spath)
  local f = self:_find_file(spath)
  return ((f or E).fs_stat or E).type
end

--
-- from original luv
-- usefull then luv overrided and need to get cwd
--
---@return string?
function M:real_cwd()
  return self.original_luv and self.original_luv.cwd()
end

--
--------------------------------------------------------------------------------
--                      FileSystem commands
--------------------------------------------------------------------------------
--
-- pwd stat cd ls tree mk_dir touch cat rm mount
-- for interact with emulated fs as if on behalf of the root
--
-- TODO: the same commands with access rights(permissions) checking

-- current working directory self.o.cwd
---@return string
function M:pwd()
  return self.o.cwd:get_absolute_path()
end

--
-- analog of the /usr/bin/stat
--
---@param spath string
---@param format string?
---@param users table?
function M:stat(spath, format, users)
  local f = self:_find_file(spath)
  if f then
    return f:stat(format, users)
  end
end

local function compare_strings(a, b)
  return string.upper(a) < string.upper(b)
end

-- build list of file and dirs names. add "/" for dirs
---@param files table
---@param mark_dirs boolean
local function get_sorted_fnames(files, mark_dirs)
  assert(type(files) == 'table', 'files')
  local t = {}
  for name, f in pairs(files) do
    if mark_dirs and f:is_dir() then name = name .. '/' end
    t[#t + 1] = name
  end
  table.sort(t, compare_strings)

  return t
end

--
---@param spath string?
---@param sep string?   default is ' '
---@return string
---@return boolean success flag
function M:ls(spath, sep)
  local s = ''
  sep = sep or ' '

  local cwd = _wd(self, spath, false) -- dir or file for given path

  if not cwd then
    s = fmt("ls: cannot access '%s': %s", tostring(spath), U.NO_SUCH_FILE_OR_DIR)
    return s, false
    --
  elseif cwd:is_dir() then
    local names = get_sorted_fnames(cwd.files or E, true)
    for _, name in ipairs(names) do
      if s ~= '' then s = s .. sep end
      s = s .. name
    end
    --
  elseif cwd:is_file() then
    s = cwd:get_absolute_path()
  end

  return s, true
end

-- function M:main_proc()
--   return self.o.os.o.proc
-- end

--
-- write content to filename
-- high-level wrapper arround emulated syscalls
-- for inner use
--
---@param fn string
---@param lines table
---@param mode string -- "a" "w"
---@return boolean
function M:write(fn, lines, mode, ug)
  self.o.last_err = nil
  local fd, last_df, i, path = self:_find_file(fn)
  mode = mode or 'w'

  if not fd then
    if last_df and last_df:is_dir() and i == #path then
      fd = FileStub.new_file(last_df, path[i], lines, ug)
      return true -- empty is a no error
    end
    self:set_last_err("%s: %s", fn, U.NO_SUCH_FILE_OR_DIR)
    --
  elseif fd:is_dir() then
    self.o.last_err = 'is dir'
  elseif fd.lines or fd:is_file() then
    if mode == 'a' then
      fd.lines = U.add_lines(fd.lines or {}, lines)
    elseif mode == 'w' then
      fd.lines = U.add_lines({}, lines)
    else
      error('unsupported mode: ' .. tostring(mode) .. ' use: (w|a)')
    end
    return true
  end
  return false
end

--
-- Original tree:
-- tree -F -L 2 --charset ascii -I
-- -F - add trailing slashes for directory-nodes
--
---@param spath string?
---@param opts table?{no_trailing_slashes}
function M:tree(spath, opts)
  self.o.last_err = nil
  local cwd = _wd(self, spath, true)
  local trailing_slash = ''
  if spath and string.sub(spath, -1, -1) == '/' then trailing_slash = '/' end

  local s, suffix, d, f = '', '', 0, 0
  -- for compatibility with the original tree-program
  -- to match output with origina `tree` withot `-F` option
  local no_trailing_slashes = U.to_bool((opts or E).no_trailing_slashes)

  ---@param files table{string=stub.os.FileStub}
  local function build_tree(files, tab)
    if not files then return end

    local names = get_sorted_fnames(files, false)
    -- for name, stub in pairs(files) do
    for _, name in ipairs(names) do
      local stub = files[name]
      if (stub.fs_stat or E).type == FileStub.T.DIR then
        suffix = no_trailing_slashes == true and '' or '/'
        d = d + 1
      else
        suffix = ''
        f = f + 1
      end
      s = s .. tab .. name .. suffix .. "\n"
      if (stub or E).files then -- is dir
        build_tree(stub.files, tab .. '    ')
      end
    end
  end

  if not cwd or cwd:is_file() then
    dprint('attempt to tree a regular file or non existed dir')
    s = spath .. " [error opening dir]\n\n"
  elseif cwd:is_dir() then
    -- s = U.ensure_dir(cwd:get_absolute_path()) .. "\n"
    s = cwd:get_absolute_path() .. trailing_slash .. "\n"
    build_tree(cwd.files, '    ')
  end
  local dn = d == 1 and 'y' or 'ies'
  local fsuff = f ~= 1 and 's' or '' -- files or file
  s = s .. string.format("%d director%s, %d file%s\n", d, dn, f, fsuff)
  return s
end

---
--- change directory to a given path
---
---@param spath string
---@return boolean
function M:cd(spath)
  dprint(1, 'cd path:', spath)
  self.o.last_err = nil

  local cwd, _, i = self:_find_file(spath)

  if cwd == nil then
    dprint('not exists given path:', spath, 'i:', i)
    self.o.last_err = spath .. ': ' .. U.NO_SUCH_FILE_OR_DIR
    --
  elseif cwd:is_file() then
    dprint('try to cd to existed file ', cwd.name)
    self.o.last_err = spath .. ': Not a directory'
    --
  elseif cwd:is_dir() then
    dprint('success dir changed to already existed ', cwd)
    self.o.cwd = cwd
    return true
  end

  return false
end

---
--- Create new Directory
--
---@param spath string
---@param mk_parents boolean? no error if existing, make parent dirs as needed
---@param mode number?
function M:mk_dir(spath, mk_parents, mode)
  dprint("mk_dir", spath, '-p:', mk_parents)
  assert(type(spath) == 'string' and spath ~= '', 'path got: ' .. tostring(spath))
  self.o.last_err = nil

  mode = mode or 448 -- 0700 -> decimal TODO

  -- get fstub for specified spath
  local fd, last_fd, i, path = self:_find_file(spath)

  if fd == nil and last_fd then
    fd = last_fd
    dprint('now base dir is ', fd)
  end
  assert(fd ~= nil, 'last_fd always supplied')

  local is_dir = fd:is_dir()

  -- one dir
  if is_dir and not mk_parents and i == #path then -- ?
    return FileStub.new_dir(fd, path[i])
  end

  if is_dir and mk_parents and i <= #path then
    while i <= #path do
      local name = path[i]
      -- dprint('create in dir:', fd, 'new dir:', name)
      fd = FileStub.new_dir(fd, name)
      i = i + 1
    end
    --
  elseif is_dir and mk_parents then -- dir already exists
    fd = fd
    --
  else -- attempt to create already existed without -p flag
    local err = "cannot create directory ‘" .. spath .. "’: "
    if not is_dir then
      err = err .. "File exists"
    else
      err = err .. U.NO_SUCH_FILE_OR_DIR
    end
    self.o.last_err = err
    dprint(err)
    fd = nil -- fail
  end

  return fd
end

--
--
--
---@param fn string
---@param no_create boolean?
function M:touch(fn, no_create)
  dprint('touch', fn, '--no-create:', no_create)
  self.o.last_err = nil
  assert(type(fn) == 'string', 'fn')

  local f, df, i, path = self:_find_file(fn)
  if not f and df then
    if df:is_dir() and i == #path then
      if not no_create then
        f = FileStub.new_file(df, path[i], {})
      else
        dprint('--no_create') -- keep silent
      end
      --
    elseif df:is_file() then
      local err = "touch: cannot touch '" .. fn .. "': Not a directory"
      self.o.last_err = err
      dprint(err)
      return false
    end
  end
  if not f then
    local err = fmt("touch: cannot touch '%s': %s", fn, U.NO_SUCH_FILE_OR_DIR)
    self.o.last_err = err
    dprint(err)
    return false
  end
  --
  assert(type(f.fs_stat) == 'table', 'fs_stat')

  local t = f.fs_stat
  local time = { sec = os.time(), nsec = 0 } -- ? os.clock() }
  t.mtime = time
  t.atime = time
  t.ctime = time

  return true
end

--
-- delete file or directory
--
---@param fn string
---@param reqursive boolean?
---@return boolean
function M:rm(fn, reqursive)
  dprint('rm ', fn, '-r:', reqursive)
  assert(type(fn) == 'string' and fn ~= '', 'fn')
  self.o.last_err = nil

  local f = self:_find_file(fn)

  if f then
    assert(f.parent ~= nil, 'has dir')
    assert(f.name ~= nil, 'has name')
    if f:is_dir() and next(f.files or E) and not reqursive then
      dprint('cannot delete directory')
      return false
    end
    if f.parent.files[f.name] == f then
      f.parent.files[f.name] = nil
      f.parent = nil

      return true
    end
  end
  self.o.last_err = fmt('rm: cannot remove %s: %s', fn, U.NO_SUCH_FILE_OR_DIR)
  dprint(self.o.last_err)
  return false
end

--
-- implemented based on the behavior of the os.rename function
-- if you try to rename(mv) to already existed file it just replace extisted
-- file content by content(newname) from the oldname
--
-- /tmp/ex$ mv old new  # old not existed
-- mv: cannot stat 'old': No such file or directory
--
-- /tmp/ex$ mv new ./dir0/new2
-- mv: cannot move 'new' to './dir0/new2': No such file or directory
--
---@param oldname string
---@param newname string
---@return boolean
function M:mv(oldname, newname)
  dprint('mv', oldname, 'to', newname, 'cwd:', self.o.cwd)
  assert(type(newname) == 'string', fmt(M.P_BAD_SARG, 1, tostring(newname)))
  assert(type(oldname) == 'string', fmt(M.P_BAD_SARG, 2, tostring(oldname)))
  self.o.last_err = nil

  if oldname ~= '' and newname ~= '' and oldname ~= newname then
    local old_f = self:_find_file(oldname)
    if not old_f then -- try to move not existed file|dir
      self:set_last_err("mv: cannot stat '%s': %s", oldname, U.NO_SUCH_FILE_OR_DIR)
      return false
    end

    assert(old_f.parent ~= nil, 'has dir')
    assert(old_f.name ~= nil, 'has name')

    local dst_dir = U.extract_path(newname)
    local newbase = U.basename(newname)
    local dst_fdir = dst_dir == nil and self.o.cwd or self:_find_file(dst_dir)
    dprint('for', newname, 'dst_dir:', dst_dir, 'base', newbase, 'f:', dst_fdir)

    if not dst_fdir then -- try to move into not existed dir
      -- self.o.last_err = fmt('%s: %s', dst_dir, U.NO_SUCH_FILE_OR_DIR)
      self:set_last_err("mv: cannot move '%s' to '%s': %s",
        oldname, newname, U.NO_SUCH_FILE_OR_DIR)
      return false
    end

    if old_f.parent.files[old_f.name] == old_f then -- make sure synced
      dst_fdir.files = dst_fdir.files or {}         -- it can be empty dir

      if dst_fdir.files[newbase] then
        dprint('mv: replace', newname, 'by', oldname)
      end
      -- cp oldname newname (with replace newname if has (checked on linux)
      dst_fdir.files[newbase] = old_f
      old_f.parent.files[old_f.name] = nil -- rm oldname from parent dir
      old_f.name = newbase
      old_f.parent = dst_fdir

      return true
    end
  end

  return false
end

--
-- mount a real path into emulated FS
-- UseCase - maps real files into memory for testing
--
---@param from string - path to real directory which copy into stub.FS
---@param to string - path inside stub.FS
---@param with_content boolean?  copy files content
---@return number - count of created FileStubs
function M:mount(from, to, with_content)
  dprint('mount', from, to)
  self.o.last_err = nil

  assert(type(from) == 'string', 'from')
  -- assert(type(to) == 'string', 'to')
  local uv = self.original_luv
  assert(uv, 'has original luv')

  local fd = self.o.cwd

  self:_find_file(to)
  if not fd then
    self:set_dir(to)
    fd = self.o.last_created_filestub
  end
  assert(fd ~= nil, 'has dir to mount')

  local dir = from

  local handle, errmsg = uv.fs_scandir(dir)

  if type(handle) == "string" then
    self.o.last_err = handle
    return -1
    --
  elseif not handle then
    self.o.last_err = errmsg
    return -1
  end

  local c = 0
  fd.files = fd.files or {}

  while true do
    local name = uv.fs_scandir_next(handle)
    if not name then
      break
    end
    local path = dir .. '/' .. name
    local stat = uv.fs_stat(path)
    if stat then
      local f = nil
      if stat.type then
        f = FileStub:new({ name = name, parent = fd, fs_stat = stat })
        fd.files[name] = f -- bind to parent
        c = c + 1
      end
      if stat.type == 'directory' then
        -- TODO recursive
      elseif stat.type == 'file' and with_content then
        -- TODO read content from the file
      else
        print('[WARN] unknown type ' .. tostring(stat.type) .. ' ' .. name)
      end
    end
  end

  return c
end

--
-- get_last_error (io)
--
---@return string?
function M:last_err()
  return self.o.last_err
end

---@param format string
---@return string
function M:set_last_err(format, ...)
  self.o.last_err = string.format(format, ...)
  return self.o.last_err
end

--------------------------------------------------------------------------------
--                    Count access to emulated functions
--------------------------------------------------------------------------------


---@param name string
function M:access_inc(name)
  self.o.counter = self.o.counter or {}
  if not self.o.counter[name] then
    self.o.counter[name] = 1
  else
    self.o.counter[name] = self.o.counter[name] + 1
  end
end

---@return number
function M:access_cnt(name)
  return (self.o.counter or E)[name or false] or 0
end

--------------------------------------------------------------------------------
--                            Emulate OS
--------------------------------------------------------------------------------

--
-- aliase with_os()
--
---@param _os table -- stub.os.OSystem
---@return self
function M:bind_os(_os)
  -- dprint('bind os:', _os, ' to fs: ', self)
  self.o.os = _os
  -- _os.fs = self --?
  return self
end

-- ---@param cmd string
-- ---@param mode string
-- ---@return stub.os.IOHandle?
-- ---@return string? -- errmsg
-- function M:handle_os_cmd(cmd, mode)
--   if self.o.os then
--     local fd = 1 -- stdout
--     if mode == 'w' then
--       fd = 0     --stdin
--     end
--     local ans, errmsg = self.o.os:run(cmd, fd) ---@type stub.os.FileStub
--     if ans then
--       return ans, errmsg
--     end
--   end
-- end

--------------------------------------------------------------------------------
--                               Stubs
--------------------------------------------------------------------------------


-- ~/.local/share/nvim/mason/packages/lua-language-server/libexec/meta/LuaJIT en-us utf8/io.lua
function M:mk_io_stub()
  local _io = {
    __tag = sid.TESTING_TAG,

    ---
    ---Opens a file, in the mode specified in the string `mode`.
    ---
    ---[View documents](http://www.lua.org/manual/5.1/manual.html#pdf-io.open)
    --- This function opens a file, in the mode specified in the string mode.
    --  It returns a new file handle, or, in case of errors,
    --  nil plus an error message.
    --  The mode string can be any of the following:
    --    "r": read mode (the default);
    --    "w": write mode;
    --    "a": append mode;
    --    "r+": update mode, all previous data is preserved;
    --    "w+": update mode, all previous data is erased;
    --    "a+": append update mode, previous data is preserved,
    --          writing is only allowed at the end of file.
    --
    -- The mode string can also have a 'b' at the end,
    -- which is needed in some systems to open the file in binary mode.
    -- This string is exactly what is used in the standard C function fopen.
    --
    ---@param filename string
    ---@param mode?    openmode
    ---@return table? -- file*?
    ---@return string? errmsg
    ---@nodiscard
    open = function(filename, mode)
      self:access_inc('io.open')
      dprint(2, 'io.open', filename, mode)
      -- bad argument #1 to 'open' (string expected, got nil)
      assert(type(filename) == 'string', 'filename')
      assert(not mode or type(mode) == 'string', 'mode')
      assert(self.o.os, 'expected OSystem')

      local h = self.o.os:c_fopen(nil, filename, mode) -- def mode is 'r'
      local err = self.o.os:last_err()
      return h, err
    end,

    -- return stub.io.IOHandle?
    -- return string?  errmsg
    popen = function(prog, mode)
      self:access_inc('io.popen')
      dprint(1, 'io.popen', prog, mode)
      assert(type(prog) == 'string', 'prog')
      mode = mode or 'r'

      local proc, err = self.o.os:c_fpopen(nil, prog, mode) -- def mode is r?
      dprint("popen: c_fpopen ret: proc.pid:", (proc or E).pid, 'err:', err)
      local h = nil
      if proc then
        dprint('popen: Create pipe for ', prog, mode)
        if string.find(mode, 'r') then -- 2>&1  ??
          h = IOPipe:new({ write = proc:stdout() }).read
        elseif string.find(mode, 'w') then
          h = IOPipe:new({ read = proc:stdin() }).write
        else
          error('Unexpected mode in popen: ' .. tostring(mode))
        end
      end
      dprint('popen: h:', IOHandle._status(h), 'err:', err)
      return h, err
    end,

    close = function(handle)
      self:access_inc('io.close')
      dprint(1, 'io.close')

      IOHandle.cast(handle):close() -- with validate
    end,

    output = function(handle)
      self:access_inc('io.output')
      dprint(1, 'io.output')
      local prev = self.io._output
      if handle then
        self.io._output = IOHandle.cast(handle)
      end
      return prev
    end,

    -- io.output(fd)
    -- io.write(string)
    -- io.close(fd)
    write = function(content)
      self:access_inc('io.write')
      IOHandle.cast(self.io._output):write(content)
    end,
  }

  self.io = _io
  return _io
end

-- given streams can be already associated with the os.o.proc (main-proc)
--
---@param stdin stub.os.IOHandle?
---@param stdout stub.os.IOHandle?
---@param stderr stub.os.IOHandle?
function M:bind_std_iostreams(stdin, stdout, stderr)
  dprint("----> bind std io-streams os:", self.o.os)

  self.io = self.io or {}
  local _io = self.io

  _io.stdin = stdin or IOHandle.of({}, 'r', IOHandle.TYPE.STD_STREAM)
  _io.stdout = stdout or IOHandle.of({}, 'w', IOHandle.TYPE.STD_STREAM)
  _io.stderr = stderr or IOHandle.of({}, 'w', IOHandle.TYPE.STD_STREAM)

  -- there may be desynchronization with loss of data integrity proc.fds<>self._io

  self.io._output = self.io.stdout

  dprint("<---- bind std io-streams: done.\n")
end

function M:clear_stdio()
  assert(self.io and self.io.stdout, 'has already binded stdio')
  local _io = self.io
  _io.stdin:_cleanup_state()
  _io.stdout:_cleanup_state()
  _io.stderr:_cleanup_state()
end

--
-- define user input to stdin
--
-- throw error if io.stdin stub is not initialized via bind_std_iostreams
--
---@param input string|table
---@return table of stdin
function M:stdin_add_input(input)
  assert((self.io or E).stdin and class.instanceof(self.io.stdin, IOHandle),
    'stdin must be already created by call fs:bind_std_iostreams')
  assert(self.io.stdin.lines ~= nil, 'has lines')

  local lines = self.io.stdin.lines

  self.io.stdin.open = true
  -- case: sync lnum after attempt of read lines with empty input
  if self.io.stdin.lnum > #lines then
    self.io.stdin.lnum = #lines
  end

  U.add_lines(lines, input)
  dprint('stdin_add_input lines:', ':inspect:', lines)
  return lines
end

-- throw error if io.stdout stub is not initialized via bind_std_iostreams
---@return table
function M:get_stdout_lines()
  local _io = (self or E).io
  assert(_io.stdout and class.instanceof(_io.stdout, IOHandle),
    'stdout must be already created by call fs:bind_std_iostreams')
  return _io.stdout.lines
end

-- throw error if io.stderr stub is not initialized via mk_io_std_in_out_err
---@return table
function M:get_stderr_lines()
  local _io = (self or E).io
  assert(_io.stdout and class.instanceof(_io.stdout, IOHandle),
    'stdout must be already created by call fs:mk_io_std_in_out_err')
  return _io.stdout.file.lines
end

--[[
  local oldout = io.output() -- without argument returns actual output
  io.output(fd)
  io.write(content)
  io.close(fd)
  if oldout == io.stdout then -- can rise error: attempt to use a closed file,
    io.output(oldout)         -- set back to oldout ( normally io.stdout )
  end

]]
--
--
--
--
function M:mk_luv_stub()
  local luv = {
    __tag = sid.TESTING_TAG,

    cwd = function()
      assert(self.o.cwd ~= nil, 'has self.o.cwd')
      return self.o.cwd:get_absolute_path()
    end,

    fs_stat = function(path)
      dprint(1, 'fs_stat', path)
      local f = self:_find_file(path)
      if f then
        return f.fs_stat
      end
    end,

    ---@return table?, string? -- handle, errmsg
    fs_scandir = function(dir)
      dprint(1, "fs_scandir", dir)
      local f = self:_find_file(dir)
      if f and f:is_dir() then
        return IOHandle:new(nil, f), nil
      else
        return nil, "ENOENT: no such file or directory: " .. tostring(dir)
      end
    end,

    ---@param handle any
    fs_scandir_next = function(handle)
      local fn, ft = IOHandle.cast(handle):read('*l'), nil -- next_file')
      if fn then
        ft = (((((handle or E).file or E).files or E)[fn] or E).fs_stat or E).type
      end
      return fn, ft
    end,

    fs_mkdir = function(dir, mode)
      dprint(1, "fs_mkdir", dir, mode)
      assert(type(dir) == 'string', 'dir')
      local fd = self:mk_dir(dir, false, mode)
      return fd ~= nil and fd:is_dir()
    end,

    os_uname = function()
      dprint(1, "os_uname")
      return (self.o.os or E).uname or require("stub.os.OSystem").DEF_UNAME
    end,

    -- to run system process via uv.spawn

    ---@diagnostic disable-next-line: unused-local
    new_pipe = function(bool)
      return IOPipe:new({
        read = IOHandle.of({}, 'r'),
        write = IOHandle.of({}, 'w'),
      })
    end,

    ---@param exepath string
    ---@param params table{cwd:string, args:table, env:table,
    --                           stdio = {stdin, stdout, stderr} }
    ---@param on_close function?
    spawn = function(exepath, params, on_close)
      return self.o.os:spawn_proc(exepath, params, on_close)
    end,

    read_start = function(pipe, handler)
      -- TODO next to impl
    end,
  }
  --[[

  local on_close = function(code, signal)
    if handle then
      stdout:read_stop()
      stderr:read_stop()
      close_handle(stdin)
      close_handle(stdout)
      close_handle(stderr)
      close_handle(handle)

      pp.code = code
      local exit_ok = code == 0
      done(pp, exit_ok, signal)
      handle = nil
    end
  end

  local exepath = M.get_exepath(pp.cmd)
  local spawn_params = {
    args = pp.args,
    env = pp.envs, -- {}, vim.loop.env
    stdio = { stdin, stdout, stderr },
    cwd = pp.cwd or uv.cwd(),
  }
  handle, pp.pid = uv.spawn(exepath, spawn_params, on_close)
]]

  self.luv = luv
  return luv
end

--
--
function M:mk_require_proxy()
  local _fs = self
  assert(type(self.original_require) == 'function', 'has orig-require')

  self.require = function(module)
    -- show all required modules
    if self.verbose_require then dprint(1, 'require', module) end

    local __fs = _fs --?
    if module == 'luv' then
      if not self.verbose_require then dprint(1, 'require', module) end
      return __fs.luv
    end
    return __fs.original_require(module)
  end

  return self.require
end

--
--
--
function M.find_by_value(t, fv)
  assert(type(t) == 'table', 't')
  for k, v in pairs(t) do
    if v == fv then
      return k
    end
  end
end

--
---@param f any (table| stub.io.FileSystem)
---@return string
function M.hasStub(f)
  return (type(f) == 'table' and f.nostub ~= nil) and ' (noStub)' or ''
end

--
-- determine
-- find name of object
-- it can be a function from emulated module or the module itself
--
function M:whatis(f)
  if f then
    if f == self then return 'its me ' .. class.tostring(f) .. M.hasStub(f) end
    if f == self.luv then return 'emulated luv module' end
    if f == self.io then return 'emulated io module' end
    if f == self.original_luv then return 'original luv module' end
    if f == self.original_io then return 'original io module' end
    if f == self.require then return 'proxy_require' end
    if f == self.original_require then return 'original require' end
    if f == (self.o or E).os then
      return 'parent OS: ' .. class.tostring(f) .. M.hasStub(self.o.os.fs)
    end

    if self.luv then
      local func = M.find_by_value(self.luv, f)
      if func then return '[emulated] luv.' .. tostring(func) end
    end

    if self.io then
      local func = M.find_by_value(self.io, f)
      if func then return '[emulated] io.' .. tostring(func) end
    end

    if self.original_io then
      local fn = M.find_by_value(self.original_io, f)
      if fn then return '[original] io.' .. tostring(fn) end
    end

    if self.original_luv then
      local fn = M.find_by_value(self.original_luv, f)
      if fn then return '[original] luv.' .. tostring(fn) end
    end

    local t = type(f)

    if t == 'table' then
      if getmetatable(f) == M then -- instanceof stub.io.FileSystem
        return class.tostring(f) .. ((f.nostub ~= nil) and ' (noStub)' or '')
        --
      elseif class.instanceof(f, class.Object) then
        return class.tostring(f)
      end

      local s = 'table'
      if getmetatable(f) then
        s = s .. ' with metatable'
      end
      return s
    end
  end
end

class.build(M)
return M
