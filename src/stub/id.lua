-- 24-11-2023 @author Swarg
-- Goal: provide unique id for stubs
local M = {}

M.TESTING_TAG = 'this-module-overriden-for-testing'
local next_id = 0
local sections = {}

---@return number
function M.next_id()
  next_id = next_id + 1
  return next_id
end

--
--
---@return number
---@param name string
function M.next_section_id(name)
  name = name or ''
  local nid = (sections[name] or 0) + 1
  sections[name] = nid
  return nid
end

--
-- readable status of one specified or all sections
--
---@param name string? nil to show all ids of sections
---@return string
function M.get_sections_status(name)
  if name then
    local n = sections[name]
    return tostring(name) .. ': next_id:' .. tostring(n)
  end

  local s = ''
  for name0, id in pairs(sections) do
    s = s .. tostring(name0) .. ': next_id: ' .. tostring(id) .. "\n"
  end
  return s
end

--
-- reset all next_id and sections id and return last counters in one table
--
---@return table
function M.reset_all()
  local prev_id, prev_secs = next_id, sections
  next_id = 0
  sections = {}

  prev_secs.next_id = prev_id
  return prev_secs
end

--
--
---@param n number? 0 is default
function M.set_next_id(n)
  local prev = next_id
  next_id = n or 0
  return prev
end

--
--
---@param name string
---@param n number? 0 is default
function M.set_next_section_id(name, n)
  local prev = sections[name or false]
  sections[name] = n or 0
  return prev
end

function M.rand_str(length, seed)
  local normals = {
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
    "abcdefghijklmnopqrstuvwxyz",
    "0123456789",
  }

  length = length or 12
  if seed ~= "old" then
    seed = seed or os.time()
    math.randomseed(seed)
  end

  local chars = normals
  local index, pw = 0, ""
  repeat
    -- index = index + 1
    index = math.random(#chars)
    local rnd = math.random(chars[index]:len())
    local ch = chars[index]:sub(rnd, rnd)
    if math.random(2) == 1 then
      pw = pw .. ch
    else
      pw = ch .. pw
    end
    -- index = index % #chars
  until pw:len() >= length
  return pw
end

return M
